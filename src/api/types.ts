export type SearchOptions = {
  limit?: number;
  skip?: number;
  slug?: string;
};
