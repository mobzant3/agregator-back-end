import { Controller, Get, Query } from '@nestjs/common';
import { SlugsService } from './slugs.service';

@Controller('slugs')
export class SlugsController {
  constructor(private readonly slugsService: SlugsService) {}

  @Get('/')
  async findSlugCombinations() {
    const slugs = this.slugsService.findAllCombination();

    return { slugs };
  }

  @Get('/similar-searchs')
  async generateSimialarSearchs(@Query('slug') slug, @Query('limit') limit) {
    const data = this.slugsService.generateSimialarSearchs(slug, limit);

    return { data };
  }

  @Get('/search-prompt-info')
  async searchPromptInfo(@Query('slug') slug) {
    const data = this.slugsService.searchPromptInfo(slug);

    return { data };
  }
}
