import { Injectable } from '@nestjs/common';
import productTypes from '../../dictionaries/productTypes';
import materials from '../../dictionaries/materials';
import colors from '../../dictionaries/colors';
import { promptInfoJson, promptInfoKeys } from 'src/dictionaries/promptInfo';
import { UrlFormat } from 'src/utils';

@Injectable()
export class SlugsService {
  findAllCombination() {
    const combination = [...productTypes, ...materials, ...colors];
    productTypes.forEach((pt) => {
      materials.forEach((mr) => {
        colors.forEach((c) => {
          combination.push(
            `${pt.toLowerCase()}-${mr.toLowerCase()}-${c.toLowerCase()}`,
          );
        });
      });
    });

    return [...new Set(combination)];
  }

  private getRandomElementInLowerCase(array: string[]): string {
    return array[Math.floor(Math.random() * array.length)].toLowerCase();
  }

  generateSimialarSearchs(slug, limit = 15) {
    const slugs = new Set();

    do {
      if (slug && productTypes.includes(slug.toUpperCase())) {
        slugs.add(
          `${slug}-${this.getRandomElementInLowerCase(
            materials,
          )}-${this.getRandomElementInLowerCase(colors)}`,
        );
      } else {
        slugs.add(
          `${this.getRandomElementInLowerCase(
            productTypes,
          )}-${this.getRandomElementInLowerCase(
            materials,
          )}-${this.getRandomElementInLowerCase(colors)}`,
        );
      }
    } while (slugs.size < limit);

    return [...slugs];
  }
  searchPromptInfo(prompt = '') {
    let data = {};
    const promptToNormalForm = UrlFormat(prompt);
    if (promptInfoKeys.includes(promptToNormalForm)) {
      data = promptInfoJson[prompt];
    }
    return data;
  }
}
