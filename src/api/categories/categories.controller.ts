import { Controller, Get, Param, Query } from '@nestjs/common';
import { CategoriesService } from './categories.service';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Get('most-popular')
  findProducts(@Query('limit') limit = '3') {
    const searchOptions = {
      limit: parseInt(limit),
    };

    return this.categoriesService.findMostPopularCategories(searchOptions);
  }

  @Get('product-types')
  getProductTypes() {
    const data = this.categoriesService.getProductTypes();

    return { data };
  }

  // check if the item has category, if not return item
  @Get('product-types/:slug')
  getItemCategory(@Param('slug') slug = '') {
    const getItem = slug.split('-')[0];
    const data = this.categoriesService.getItemCategory(getItem);

    return { data };
  }
}
