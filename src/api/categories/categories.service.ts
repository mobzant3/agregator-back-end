import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SearchOptions } from '../types';
import { Product, ProductDocument } from '../products/schemas/product.schema';
import { productTypesByCategory } from 'src/dictionaries/productTypes';
import { ObjectWithUrlAndOriginalValue } from 'src/utils';

@Injectable()
export class CategoriesService {
  findOne(id: any) {
    return this.productsModel.findById(id);
  }
  constructor(
    @InjectModel(Product.name) private productsModel: Model<ProductDocument>,
  ) {}

  async findMostPopularCategories({ limit }: SearchOptions) {
    const categories = await this.productsModel
      .aggregate([
        { $unwind: '$indexableWords' },
        { $group: { _id: '$indexableWords', count: { $sum: 1 } } },
        {
          $group: {
            _id: null,
            indexableWords: { $push: { name: '$_id', count: '$count' } },
          },
        },
        {
          $project: {
            indexableWords: 1,
          },
        },
      ])
      .limit(limit);

    const words = categories[0].indexableWords as {
      name: string;
      count: number;
    }[];

    // TODO Hacer con mongodb

    const mostPopular = words
      .sort((a, b) => b.count - a.count)
      .slice(0, limit)
      .map((w) => w.name);

    return mostPopular;
  }

  getProductTypes() {
    const productTypes = {};

    for (const category in productTypesByCategory) {
      productTypes[category] = ObjectWithUrlAndOriginalValue(
        productTypesByCategory[category],
      );
    }

    return productTypes;
  }

  getItemCategory(item: string): string {
    let itemCategory = item;
    const productTypes = this.getProductTypes();

    for (const category in productTypes) {
      const categoryTypes = productTypes[category];
      const type: string = categoryTypes.find(
        (t) => t.value === item.toUpperCase(),
      );
      if (type) {
        itemCategory = category;
      }
    }

    return itemCategory;
  }
}
