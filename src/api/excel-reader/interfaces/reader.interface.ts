export type MatchingColumnData = {
  url: string;
  id: string;
  data: any;
};
