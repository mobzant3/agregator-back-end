export class ReadExcelDto {
  wordsToSearch: string[];
  columnToSearchOn: string;
  fileName: string;
}
