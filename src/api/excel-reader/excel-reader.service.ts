import { Injectable } from '@nestjs/common';
import { ScrapperService } from '../../scrapper/scrapper.service';
import Searcher from '../../utils/searcher';
import { ProductsService } from '../products/products.service';
import { Product } from '../products/schemas/product.schema';
import { MatchingColumnData } from './interfaces/reader.interface';
import Reader from './utils/reader';

@Injectable()
export class ExcelReaderService {
  reader: Reader;
  searcher: Searcher;

  constructor(
    private readonly productService: ProductsService,
    private readonly scrapperService: ScrapperService,
  ) {
    this.reader = new Reader();
    this.searcher = new Searcher();
  }

  async searchAndStoreMatchingData(
    data: string,
    columnToSearchOn: string,
    wordsToSearch: string[],
  ) {
    const matchingColumnData = this.reader.returnPageDescriptionAndData(
      data,
      columnToSearchOn,
    );

    if (matchingColumnData) {
      const containsSomeWords = this.searcher.search(
        matchingColumnData.data,
        wordsToSearch,
      );

      if (containsSomeWords) {
        const products = await this.getProductDataFromUrl(
          matchingColumnData.url,
          matchingColumnData,
        );

        await this.storeProductData(products);
      }
    }
  }

  async returnSearchAndStoreMatchingPromises({
    readableStream,
    columnToSearchOn,
    wordsToSearch,
  }) {
    // const values = await this.scrapperService.getProductDetails(
    //   'luzgentilitienda.mitiendanube.com',
    // );
    return new Promise<Promise<any>[]>((resolve, _reject) => {
      const storeFunctionPromises = [];
      readableStream.on('line', async (data) => {
        storeFunctionPromises.push(
          async () =>
            await this.searchAndStoreMatchingData(
              data,
              columnToSearchOn,
              wordsToSearch,
            ),
        );
      });

      readableStream.on('close', () => {
        this.reader.restoreValues();
        resolve(storeFunctionPromises);
      });
    });
  }

  async storeProductData(productData: Product[]) {
    return await this.productService.bulkCreate(productData);
  }

  async getProductDataFromUrl(
    url: string,
    matchingColumnData: MatchingColumnData,
  ) {
    const products = await this.scrapperService.getProductDetails(
      url,
      matchingColumnData,
    );

    return products;
  }
}
