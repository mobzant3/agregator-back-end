import {
  Body,
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import ExcelManipulator from '../../utils/excel-manipulator';
import { ExcelReaderService } from './excel-reader.service';
import { lowerCaseStoreKeywords } from '../../dictionaries/storeKeywords';

const FILE_SOURCE = 'src/files';

@Controller('excel-reader')
export class ExcelReaderController {
  constructor(private readonly excelReaderService: ExcelReaderService) {}

  @Post('/store-matching-words')
  @UseInterceptors(FileInterceptor('file'))
  async storeMatchingWords(@UploadedFile() file, @Body() body) {
    const fileName = file.fieldname;

    const { columnToSearchOn } = body;

    const excelManipulator = new ExcelManipulator({
      fileName: `${FILE_SOURCE}/${fileName}`,
    });

    console.log('Creating xlsx file...');
    excelManipulator.createFile(file.buffer, 'xlsx');
    console.log('Xlsx file created');

    try {
      console.log('Transforming from stored xlsx file to CSV file... ');
      excelManipulator.storedXlsxToCSVFile();
      console.log('Transformation done! ');

      console.log('Deleting unused Xlsx file...');
      excelManipulator.deleteFile(`${FILE_SOURCE}/${fileName}.xlsx`);
      console.log('Xlsx file deleted');

      console.log('Getting readable stream from csv file...');
      const readableStream = excelManipulator.getReadableStreamFromStoredCsv();
      console.log('Reading CSV stream...');
      const searchAndStoreMatchingPromises =
        await this.excelReaderService.returnSearchAndStoreMatchingPromises({
          readableStream,
          columnToSearchOn,
          wordsToSearch: lowerCaseStoreKeywords,
        });
      console.log('Reading done!');
      console.log('Deleting unused csv file after reading...');
      excelManipulator.deleteFile(`${FILE_SOURCE}/${fileName}.csv`);
      console.log('CSV file deleted');

      for await (const searchAndStoreFunction of searchAndStoreMatchingPromises) {
        try {
          await searchAndStoreFunction();
        } catch (error) {
          console.error(
            'Scrapping page failed, check if the page has products :(',
          );
          console.error(error.message);

          continue;
        }
      }

      return 'Data stored!';
    } catch (err) {
      console.error(err);
    }
  }
}
