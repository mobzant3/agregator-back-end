import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ScrapperModule } from '../../scrapper/scrapper.module';
import { ProductModule } from '../products/products.module';
import { ExcelReaderController } from './excel-reader.controller';
import { ExcelReaderService } from './excel-reader.service';

@Module({
  controllers: [ExcelReaderController],
  imports: [ProductModule, HttpModule, ScrapperModule],
  providers: [ExcelReaderService],
})
export class ExcelReaderModule {}
