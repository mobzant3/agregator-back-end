import { Test, TestingModule } from '@nestjs/testing';
import { ExcelReaderController } from './excel-reader.controller';

describe('ExcelReaderController', () => {
  let controller: ExcelReaderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExcelReaderController],
    }).compile();

    controller = module.get<ExcelReaderController>(ExcelReaderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
