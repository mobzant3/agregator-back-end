import { MatchingColumnData } from '../interfaces/reader.interface';

export default class Reader {
  index: number;
  columnIndex: number;
  constructor() {
    this.index = 0;
    this.columnIndex = 0;
  }

  searchColumnIndex(columnName: string, columns: string[]) {
    return columns.findIndex((col) => col === columnName);
  }

  restoreValues() {
    this.columnIndex = 0;
    this.index = 0;
  }

  returnPageDescriptionAndData(
    data: string,
    columnToSearchOn: string,
  ): MatchingColumnData {
    const splittedData = data.split(',');
    if (this.index === 0) {
      this.columnIndex = this.searchColumnIndex(columnToSearchOn, splittedData);
      this.index++;
      return;
    }

    const columnData = splittedData[this.columnIndex];

    this.index++;
    return { url: splittedData[0], id: splittedData[1], data: columnData };
  }
}
