import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IProduct, Product, ProductDocument } from './schemas/product.schema';
import { SearchOptions } from '../types';

interface IProductResponse {
  total: number;
  data: IProduct[];
}

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productsModel: Model<ProductDocument>,
  ) {}

  findOne(id: any) {
    return this.productsModel.findById(id);
  }

  create(createProductDto) {
    const createdProduct = new this.productsModel(createProductDto);
    return createdProduct.save();
  }

  bulkCreate(createProductDto) {
    return this.productsModel.insertMany(createProductDto);
  }

  delete(id) {
    return this.productsModel.findByIdAndDelete(id);
  }

  private async getData({
    slug,
    limit,
    skip,
  }: SearchOptions): Promise<IProductResponse> {
    const slugWords = slug.split('-');
    let query = {};
    if (slugWords.length > 0 && slugWords[0] !== '') {
      query = { indexableWords: { $all: slugWords } };
    }
    return {
      data: await this.productsModel.find(query).skip(skip).limit(limit),
      total: await this.productsModel.countDocuments(query).exec(),
    };
  }

  private generateSlugCombinations(words: string[]): string[][] {
    const combinations: string[][] = [];
    if (words.length === 0) return [];
    const firstItem = words[0];
    if (words.length === 1) return [[firstItem]];
    combinations.unshift([...words]);
    for (let i = 1; i < words.length; i++) {
      const newCombination = [firstItem, words[i]];
      combinations.push([...newCombination]);
    }
    combinations.push([firstItem]);
    return combinations;
  }
  async findAll({ slug = '', limit = 0, skip = 0 }: SearchOptions) {
    const prioritizedData: IProduct[] = [];
    const initialSlug = slug.split('-');
    const combinations = this.generateSlugCombinations(initialSlug);
    let totalDocuments = 0;
    for (const combination of combinations) {
      const { data, total } = await this.getData({
        slug: combination.join('-'),
        limit,
        skip,
      });
      totalDocuments += total;
      if (data.length > 0) {
        for (const product of data) {
          const commonWords = product.indexableWords.filter((word) =>
            combination.includes(word),
          );
          const uniqueCommonWords = Array.from(new Set(commonWords));
          product.priority = uniqueCommonWords.length;
          prioritizedData.push(product);
        }
      }
      if (prioritizedData.length >= limit + skip) {
        break;
      }
    }
    prioritizedData.sort((a, b) => b.priority - a.priority);
    if (skip !== 0 && limit === 0) {
      return {
        total: totalDocuments,
        data: prioritizedData.slice(skip, skip + totalDocuments),
      };
    } else if (limit !== 0 || skip !== 0) {
      return {
        total: totalDocuments,
        data: prioritizedData.slice(skip, skip + limit),
      };
    } else {
      return {
        total: totalDocuments,
        data: prioritizedData,
      };
    }
  }
  async checkSlugProducts(slug = '') {
    const slugWords = slug.split('-');
    let query = {};
    if (slugWords.length > 0 && slugWords[0] !== '') {
      query = { indexableWords: { $all: slugWords } };
    }
    const products = await this.productsModel.countDocuments(query).exec();
    return products > 0;
  }
}
