import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ProductDocument = HydratedDocument<Product>;
type Variant = {
  color: string;
  size: string;
  test: any;
};

export interface IProduct {
  installments_data: string;
  product_id: number;
  price_short: string;
  price_long: string;
  price_number: number;
  compare_at_price_short: string;
  compare_at_price_long: string;
  compare_at_price_number: number;
  stock: number;
  sku: number;
  available: boolean;
  contact: boolean;
  id: number;
  image: number;
  name: string;
  url: string;
  indexable: boolean;
  indexableWords: string[];
  slugCombinations: string[];
  image_url: string;
  variants: Variant[];
  siteId: string;
  tags: string[];
  description: string;
  priority?: any;
}
@Schema()
export class Product {
  @Prop({ name: 'installments_data' })
  installments_data: string;
  @Prop({ name: 'product_id' })
  product_id: number;
  @Prop({ name: 'price_short' })
  price_short: string;
  @Prop({ name: 'price_long' })
  price_long: string;
  @Prop({ name: 'price_number' })
  price_number: number;
  @Prop({ name: 'compare_at_price_short' })
  compare_at_price_short: string;
  @Prop({ name: 'compare_at_price_long' })
  compare_at_price_long: string;
  @Prop({ name: 'compare_at_price_number' })
  compare_at_price_number: number;
  @Prop()
  stock: number;
  @Prop()
  sku: number;
  @Prop()
  available: boolean;
  @Prop()
  contact: boolean;
  @Prop()
  id: number;
  @Prop()
  image: number;
  @Prop()
  name: string;
  @Prop()
  indexable: boolean;
  @Prop()
  shopName: string;
  @Prop()
  indexableWords: string[];
  @Prop()
  slugCombinations: string[];
  @Prop()
  url: string;
  @Prop({ name: 'image_url' })
  image_url: string;
  @Prop()
  variants: Variant[];
  @Prop()
  siteId: string;
  @Prop()
  tags: string[];
  @Prop()
  description: string;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
