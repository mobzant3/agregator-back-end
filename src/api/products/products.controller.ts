import axios from 'axios';
import { ProductsService } from './products.service';
import {
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Query,
} from '@nestjs/common';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  findProducts(
    @Query('limit') limit,
    @Query('skip') skip,
    @Query('slug') slug,
  ) {
    const searchOptions = {
      limit: limit ? parseInt(limit) : 0,
      skip: skip ? parseInt(skip) : 0,
      slug: slug,
    };

    return this.productsService.findAll(searchOptions);
  }

  @Get('/checkURLHealth/:id')
  async checkURLHealth(@Param('id') id: string) {
    const product = await this.productsService.findOne(id);

    if (!product) {
      throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
    }

    try {
      const response = await axios.head(product.url);

      if (response.status > 200 && response.status < 300) {
        return { status: 'URL OK' };
      }
    } catch (error) {
      if (error.response.status === 404) {
        await this.productsService.delete(id);
        throw new HttpException(
          '404 URL. Product deleted',
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw new HttpException('Error checking', HttpStatus.NOT_FOUND);
      }
    }
  }

  @Get(':id')
  findOne(@Param() params) {
    return this.productsService.findOne(params.id);
  }

  @Get('/slug-combinations')
  async findSlugCombinations() {
    const products = await this.productsService.findAll({});

    return products;
  }

  @Get('/utils/check-slug-products')
  async checkSlugProducts(@Query('slug') slug = '') {
    const data = await this.productsService.checkSlugProducts(slug);

    return { data };
  }

  @Delete(':id')
  delete(@Param() params) {
    return this.productsService.delete(params.id);
  }
}
