import * as promptInfo from '../json/promptInfo.js';

export const promptInfoJson = promptInfo.default;

export const promptInfoKeys = Object.keys(promptInfoJson);
