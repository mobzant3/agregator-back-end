import { toNormalForm } from '../utils/text';

const storeKeywords = [
  'Ropa',
  'Vestimenta',
  'Moda',
  'Indumentaria',
  'Atuendo',
  'Prenda',
  'Traje',
  'Indumento',
  'Vestuario',
  'Zapatillas',
  'ZAPATOS',
].map((word) => toNormalForm(word));

export const lowerCaseStoreKeywords = storeKeywords.map((c) => c.toLowerCase());
export default storeKeywords;
