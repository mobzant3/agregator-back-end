import { toNormalForm } from '../utils/text';

export const productTypesByCategory = {
  accesorios: [
    'BUFANDA',
    'CAPA',
    'CORBATA',
    'GUANTES',
    'MEDIA',
    'ROPA INTERIOR',
    'SOMBRERO',
  ],
  ropa: [
    'ABRIGO',
    'ANORAK',
    'BERMUDAS',
    'BLAZER',
    'BLUSA',
    'BODY',
    'CAMISA',
    'CAMISETA',
    'CAZADORA',
    'CHALECO',
    'CHAQUETA',
    'FALDA',
    'POLLERA',
    'GABARDINA',
    'JERSEY',
    'LEGGINGS',
    'PANTALONES',
    'PANTALONES CORTOS',
    'PIJAMA',
    'REMERA',
    'TOP',
    'SUÉTER',
    'SUDADERA',
    'TRAJE',
    'TRAJE BAÑO',
    'VESTIDO',
  ],
  calzado: ['BOTAS', 'SANDALIAS', 'TACONES', 'ZAPATILLAS', 'ZAPATOS'],
};

export const productTypes = [
  ...productTypesByCategory.accesorios,
  ...productTypesByCategory.ropa,
  ...productTypesByCategory.calzado,
];

export const formatedProductTypes = productTypes.map((word) =>
  toNormalForm(word),
);

export const lowerCaseProductTypes = formatedProductTypes.map((c) =>
  c.toLowerCase(),
);

export default formatedProductTypes;
