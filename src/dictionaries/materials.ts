import { toNormalForm } from '../utils/text';

const materials = [
  'Algodón',
  'Lino',
  'Seda',
  'Lana',
  'Poliéster',
  'Rayón',
  'Denim',
  'Cuero',
  'Terciopelo',
  'Nylon',
  'Piel',
  'sintética',
  'Satén',
  'Franela',
  'Gasa',
  'Encaje',
  'Cachemira',
  'Tela',
  'vaquera',
  'Tejido',
  'punto',
  'Tafetán',
  'Tela',
  'seda',
  'artificial',
  'Tweed',
  'Organdí',
  'Jacquard',
  'Brocado',
  'Punto',
  'jersey',
].map((word) => toNormalForm(word));

export const lowerCaseMaterials = materials.map((c) => c.toLowerCase());
export default materials;
