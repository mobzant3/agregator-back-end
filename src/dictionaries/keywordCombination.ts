import { lowerCaseColors } from './colors';
import { lowerCaseMaterials } from './materials';
import { lowerCaseProductTypes } from './productTypes';
import { lowerCaseStoreKeywords } from './storeKeywords';

const allLowercaseKeywords = [
  ...lowerCaseColors,
  ...lowerCaseMaterials,
  ...lowerCaseProductTypes,
  ...lowerCaseStoreKeywords,
];

export const materialAndProductTypeWords = [
  ...lowerCaseMaterials,
  ...lowerCaseProductTypes,
];

export default allLowercaseKeywords;
