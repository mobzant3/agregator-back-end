import { toNormalForm } from '../utils/text';

const colors = [
  'Blanco',
  'Negro',
  'Gris',
  'Marrón',
  'Beige',
  'Rojo',
  'Naranja',
  'Amarillo',
  'Verde',
  'Azul',
  'Morado',
  'Rosa',
  'Turquesa',
  'Celeste',
  'Aguamarina',
  'Verde',
  'lima',
  'Verde',
  'esmeralda',
  'Azul',
  'marino',
  'Azul',
  'claro',
  'Azul',
  'eléctrico',
  'Borgoña',
  'Oro',
  'Plata',
  'Bronce',
].map((word) => toNormalForm(word));

export const lowerCaseColors = colors.map((c) => c.toLowerCase());
export default colors;
