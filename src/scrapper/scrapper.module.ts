import { Module } from '@nestjs/common';
import { ScrapperService } from './scrapper.service';
import { AuditoryModule } from '../auditory/auditory.module';

@Module({
  imports: [AuditoryModule],
  providers: [ScrapperService],
  exports: [ScrapperService],
})
export class ScrapperModule {}
