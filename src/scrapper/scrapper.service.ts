/* eslint-disable @typescript-eslint/no-var-requires */
import { Injectable } from '@nestjs/common';
import { MatchingColumnData } from '../api/excel-reader/interfaces/reader.interface';
import { Product } from '../api/products/schemas/product.schema';
import { lowerCaseColors } from '../dictionaries/colors';
import { materialAndProductTypeWords } from '../dictionaries/keywordCombination';
import { getSlugs } from '../utils/combinations';
import { toNormalForm } from '../utils/text';
import { AuditoryService } from '../auditory/infrastructure';
// const chrome = require('selenium-webdriver/chrome');
const { Builder, By, until } = require('selenium-webdriver');

type ProductData = {
  product_id: number;
  price_short: string;
  price_long: string;
  price_number: number;
  compare_at_price_short: any;
  compare_at_price_long: any;
  compare_at_price_number: any;
  stock: number;
  sku: any;
  available: boolean;
  contact: boolean;
  option0: string;
  option1: string;
  option2: any;
  id: number;
  image: number;
  image_url: string;
  installments_data: string;
};

type DBProduct = ProductData & {
  name: string;
  url: string;
  siteId: string;
  indexable: boolean;
  description: string;
  tags: string[];
  variants: {
    color: string;
    size: string;
    test: string;
  }[];
  indexableWords?: string[];
  slugCombinations?: string[];
};

@Injectable()
export class ScrapperService {
  screen: any;
  driver: any;
  constructor(private readonly auditoryService: AuditoryService) {
    this.screen = {
      width: 640,
      height: 480,
    };

    this.driver = new Builder().forBrowser('chrome').setChromeOptions().build();
  }

  checkIfTheProductIsIndexable(product: DBProduct) {
    let isIndexable = false;

    const indexableWords = [...product.name.split(' ')];

    isIndexable = materialAndProductTypeWords.some((word) => {
      if (product.name.includes(word) || product.description.includes(word)) {
        console.log(`Contains the word ${word} on it's description or name`);
        indexableWords.push(word);
        return true;
      }
    });

    lowerCaseColors.forEach((color) => {
      if (product.variants.find((variant) => variant.color.includes(color))) {
        indexableWords.push(color);
      }
    });

    return { isIndexable, indexableWords };
  }

  async getProductDetails(
    url: string,
    matchingColumnData: MatchingColumnData,
  ): Promise<Product[]> {
    let page = 1;
    const products = [];
    let hasProducts = true;
    let currentUrl = '';

    const { id } = matchingColumnData;
    console.log(`Scrapping ${url} products...`);
    do {
      console.log(`Scrapping page ${page}...`);
      currentUrl = `https://${url}/productos/page/${page}/`;
      await this.driver.get(currentUrl);

      // This is the recommended way of seeing if a element is present,
      // findElement would throw an error, findElements returns an empty array
      await this.driver.wait(
        until.elementIsVisible(
          await this.driver.findElement(By.css('.logo-img')),
        ),
        30000,
      );

      const productTableList = await this.driver.findElements(
        By.css('.js-product-table'),
      );

      if (productTableList.length === 0) {
        hasProducts = false;
      } else {
        const productNames = await this.driver.findElements(
          By.css('.js-item-name'),
        );

        const productNamesPromises = productNames.map((element) =>
          element.getAttribute('title'),
        );

        const names = await Promise.all(productNamesPromises);

        try {
          for (const name of names) {
            if (name) {
              console.log(`Getting description for product ${name}...`);

              await this.driver.executeScript(`
            const aTag = document.querySelectorAll('a[title="${name}"]')[0]; aTag.click();
         `);

              await this.driver.wait(
                until.elementIsVisible(
                  await this.driver.findElement(By.id('product-name')),
                ),
                30000,
              );

              const productUrl = await this.driver.getCurrentUrl();

              const shopName = await this.driver
                .findElement(By.xpath("//meta[@property='og:site_name']"))
                .getAttribute('content');

              const productContainer = await this.driver.findElement(
                By.css(`.js-product-container`),
              );

              const productDataJsonString = await productContainer.getAttribute(
                'data-variants',
              );

              const productData = JSON.parse(
                productDataJsonString,
              ) as ProductData[];

              const mainProductInfo = { ...productData[0] };
              mainProductInfo['variants'] = [];

              productData.forEach((x) => {
                const variant = {
                  color: '',
                  size: '',
                  test: '',
                };

                if (x.option0) {
                  variant.color = x.option0.toLowerCase();
                }

                if (x.option1) {
                  variant.size = x.option1.toLowerCase();
                }

                if (x.option2) {
                  variant.test = x.option2.toLowerCase();
                }
                mainProductInfo['variants'].push(variant);
              });

              delete mainProductInfo.option0; // color
              delete mainProductInfo.option1; // talle
              delete mainProductInfo.option2; // ??
              mainProductInfo['description'] = '';
              mainProductInfo['tags'] = [];
              try {
                const descriptionContainer = await this.driver.findElement(
                  By.css(`.product-description`),
                );

                const textElements = await descriptionContainer.findElements(
                  By.css('p'),
                );

                const textPromises = textElements.map((element) =>
                  element.getText(),
                );

                const textsResolved = await Promise.all(textPromises);

                const description = toNormalForm(
                  textsResolved.join(' ').toLowerCase(),
                );

                const tags = description
                  .split(' ')
                  .map((word) => word.toLowerCase());

                mainProductInfo['description'] = description;
                mainProductInfo['tags'] = tags;
              } catch (error) {
                console.log(
                  `The product ${name}, does not have a description :(`,
                );
                this.auditoryService.storeAuditory('Products', {
                  entityName: 'Products',
                  options: {
                    state: 'ERROR',
                    message: error.message,
                    url: currentUrl,
                  },
                });
                console.log(error.message);
              } finally {
                const productToPush = {
                  ...mainProductInfo,
                  name: toNormalForm(name.toLowerCase()),
                  shopName: shopName,
                  url: productUrl,
                  siteId: id,
                  indexable: false,
                  indexableWords: null,
                } as unknown as DBProduct;

                const { isIndexable, indexableWords } =
                  this.checkIfTheProductIsIndexable(productToPush);

                productToPush.indexable = isIndexable;
                productToPush.indexableWords = indexableWords;
                productToPush.slugCombinations = getSlugs(indexableWords, '-');

                console.log(`Pushing ${name} to the product list`);

                products.push(productToPush);
              }

              console.log('Going back...');
              await this.driver.navigate().back();

              await this.driver.wait(
                until.elementIsVisible(
                  await this.driver.findElement(By.css('.logo-img')),
                ),
                30000,
              );
            }
          }
        } catch (error) {
          console.log("Maybe an element you tried to scrap doesn't exists :(");
          console.log(error.message);
          this.auditoryService.storeAuditory('Products', {
            entityName: 'Products',
            options: {
              state: 'ERROR',
              message: error.message,
              url: currentUrl,
            },
          });
        }

        page++;
      }
    } while (hasProducts);

    console.log(`Scrapping done!`);
    return products;
  }
}
