class Searcher {
  options = {
    includeScore: true,
  };

  search(dataToSearch: string, wordsToSearch: string[]) {
    const wordArray = dataToSearch.toLowerCase().split(' ');

    const results = [];

    wordsToSearch.forEach((x) => {
      results.push(wordArray.includes(x.toLowerCase()));
    });

    return results.some((r) => r);
  }
}

export default Searcher;
