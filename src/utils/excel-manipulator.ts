// eslint-disable-next-line @typescript-eslint/no-var-requires
const XLSX = require('xlsx');
import * as fs from 'fs';
import * as readline from 'node:readline';

type ExcelManipulatorParams = {
  fileName: string;
};

class ExcelManipulator {
  public fileName: string;
  public outputFileName: string;

  constructor(parameters: ExcelManipulatorParams) {
    this.fileName = parameters.fileName;
  }

  createFile(data: Buffer, extension: string) {
    return fs.writeFileSync(`${this.fileName}.${extension}`, data);
  }

  fileExists() {
    return fs.existsSync(`${this.fileName}`);
  }

  deleteFile(fileName: string) {
    return fs.unlinkSync(fileName);
  }

  storedXlsxToCSVFile() {
    const workBook = XLSX.readFile(`${this.fileName}.xlsx`);
    XLSX.writeFile(workBook, `${this.fileName}.csv`, {
      bookType: 'csv',
    });
    this.outputFileName = `${this.fileName}.csv`;

    return this.outputFileName;
  }

  getReadableStreamFromStoredCsv() {
    const readStream = fs.createReadStream(`${this.fileName}.csv`, 'utf-8');
    const rl = readline.createInterface({ input: readStream });
    return rl;
  }
}

export default ExcelManipulator;
