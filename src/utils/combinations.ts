import { lowerCaseColors } from '../dictionaries/colors';
import { lowerCaseMaterials } from '../dictionaries/materials';
import { lowerCaseProductTypes } from '../dictionaries/productTypes';

const getCombinations = (valuesArray: string[]) => {
  const combinations = [];
  let tempCombination = [];
  const iterations = Math.pow(2, valuesArray.length);

  for (let i = 0; i < iterations; i++) {
    tempCombination = [];
    for (let j = 0; j < valuesArray.length; j++) {
      if (i & Math.pow(2, j)) {
        tempCombination.push(valuesArray[j]);
      }
    }
    if (tempCombination.length > 0) {
      combinations.push(tempCombination);
    }
  }

  combinations.sort((a, b) => a.length - b.length);
  return combinations;
};

export const getSlugs = (values: string[], wordSeparator = '-') => {
  const productTypeSlugs = [];
  const materialSlugs = [];
  const slugs = [];

  lowerCaseProductTypes.forEach((type) => {
    if (values.includes(type)) {
      productTypeSlugs.push(type);
    }
  });

  if (!productTypeSlugs.length) {
    return productTypeSlugs;
  }

  lowerCaseMaterials.forEach((material) => {
    if (values.includes(material)) {
      productTypeSlugs.forEach((x) => {
        if (!materialSlugs.some((sl) => sl.includes(material))) {
          materialSlugs.push(`${x}${wordSeparator}${material}`);
        }
      });
    }
  });

  lowerCaseColors.forEach((color) => {
    if (values.includes(color)) {
      materialSlugs.forEach((x) => {
        if (!slugs.some((sl) => sl.includes(color))) {
          slugs.push(`${x}${wordSeparator}${color}`);
        }
      });
    }
  });

  if (slugs.length > 0) {
    return slugs;
  }
  if (materialSlugs.length > 0) {
    return materialSlugs;
  }
  if (productTypeSlugs.length > 0) {
    return productTypeSlugs;
  }

  return [];
};


export default getCombinations;
