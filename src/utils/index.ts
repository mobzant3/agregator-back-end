import { toNormalForm } from './text';

// return object with string formated and original string
// ['PRODUCT NAME'] => [{ url: 'product-name', value: 'PRODUCT NAME' }]
export const ObjectWithUrlAndOriginalValue = (values: string[]) => {
  return values.reduce((acc, type) => {
    acc.push({
      url: UrlFormat(type),
      value: type,
    });
    return acc;
  }, []);
};

export const UrlFormat = (text: string) => {
  return toNormalForm(text).replace(/ /g, '-').toLowerCase();
};
