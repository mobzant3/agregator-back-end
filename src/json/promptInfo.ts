const json = {
  'abrigo-algodon-bronce': {
    prompt: 'abrigo algodon bronce',
    opinionBody:
      'El abrigo de algodón en color bronce ha recibido excelentes opiniones por parte de los usuarios. Muchos destacan su suavidad y comodidad gracias al algodón utilizado en su confección. Además, el color bronce le añade un toque de sofisticación y elegancia. Los usuarios también alaban su excelente capacidad de abrigar en climas fríos sin sacrificar el estilo. En resumen, el abrigo de algodón en color bronce ha demostrado ser una opción confiable y satisfactoria para quienes buscan estar a la moda y bien abrigados.',
    combineBody:
      'El abrigo de algodón en color bronce es extremadamente versátil y fácil de combinar. Para un look casual y chic, se puede combinar con unos jeans oscuros y una camiseta blanca. Si se quiere un estilo más elegante, se puede combinar con una falda midi y una blusa de seda en tonos neutros. Los accesorios en dorado o cobre complementarán a la perfección el color bronce del abrigo. En definitiva, las posibilidades de combinación son infinitas, lo que convierte a este abrigo en una prenda imprescindible en cualquier guardarropa.',
    cleanBody:
      'Para mantener el abrigo de algodón en color bronce en óptimas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, se recomienda leer las instrucciones de lavado que vienen en la etiqueta del abrigo, ya que cada prenda puede tener indicaciones específicas. En general, se recomienda lavarlo a mano o en ciclo suave en la lavadora, utilizando agua fría. Es importante evitar el uso de lejía o productos químicos agresivos. Para mantener la forma del abrigo, se debe secar al aire libre, evitando la exposición directa al sol. Finalmente, se puede planchar a baja temperatura si es necesario. Siguiendo estas recomendaciones, el abrigo de algodón en color bronce se mantendrá en perfectas condiciones durante',
  },
  'abrigo-denim-electrico': {
    prompt: 'abrigo denim electrico',
    opinionBody:
      'El abrigo denim eléctrico es una prenda versátil y moderna que ha recibido excelentes reseñas por parte de los usuarios. Su diseño en denim le da un toque casual pero sofisticado, y el color eléctrico agrega un toque de audacia a cualquier look. Los usuarios han elogiado la calidad del material, ya que es resistente y duradero. Además, se destaca su comodidad y ajuste perfecto. Sin duda, el abrigo denim eléctrico es una elección acertada para aquellos que buscan un estilo único y trendy.',
    combineBody:
      'El abrigo denim eléctrico es una prenda llamativa que puede combinarse de múltiples formas. Para un look casual y moderno, puedes combinarlo con unos jeans ajustados y una camiseta básica en tonos neutros. Si prefieres un estilo más elegante, puedes llevarlo sobre un vestido midi en tonos sólidos o estampados sutiles. Para agregar un toque de sofisticación, puedes combinarlo con unos pantalones de cuero y una blusa blanca. En definitiva, el abrigo denim eléctrico es una prenda versátil que se adapta a diferentes estilos y ocasiones.',
    cleanBody:
      'Para mantener el abrigo denim eléctrico en óptimas condiciones, es recomendable seguir algunas pautas de cuidado. Asegúrate de leer las instrucciones de lavado que vienen con la prenda, ya que cada marca puede tener recomendaciones específicas. En general, se recomienda lavar el abrigo en ciclo suave y con agua fría para evitar que se desgaste o decolore. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar el tejido. Además, es importante secar el abrigo a la sombra y evitar el uso de secadora, ya que el calor puede deformar el denim. Siguiendo estas recomendaciones, podrás disfrutar de tu abrigo denim eléctrico por mucho tiempo.',
  },
  'abrigo-encaje-morado': {
    prompt: 'abrigo encaje morado',
    opinionBody:
      'El abrigo encaje morado es una elección audaz y original para cualquier ocasión. Las opiniones sobre este producto son muy positivas, ya que su diseño único y la combinación del encaje con el color morado lo convierten en una prenda elegante y sofisticada. Además, el material de encaje proporciona una sensación de suavidad y caída perfecta. Sin duda, el abrigo encaje morado es una opción ideal para quienes buscan destacar y lucir a la moda.',
    combineBody:
      'El abrigo encaje morado es versátil y se adapta a diferentes estilos y ocasiones. Para un look elegante y formal, puedes combinarlo con un vestido negro y unos tacones a juego. Si prefieres un estilo más casual, puedes llevarlo con unos vaqueros ajustados y una camiseta blanca. Además, puedes añadir accesorios en tonos neutros como el negro o el gris para resaltar aún más el color morado del abrigo. Sea cual sea tu elección, el abrigo encaje morado será el protagonista de tu outfit.',
    cleanBody:
      'Para mantener el abrigo encaje morado en perfecto estado, es recomendable seguir algunas pautas de limpieza y cuidado. En primer lugar, es preferible optar por la limpieza en seco, ya que el encaje puede ser delicado y requerir un tratamiento especial. En caso de manchas localizadas, se puede utilizar un paño húmedo y suave para limpiar suavemente la zona afectada. Es importante evitar el uso de productos agresivos o procesos de lavado que puedan dañar el material. Por último, se recomienda guardar el abrigo encaje morado en un lugar fresco y seco, y evitar la exposición prolongada a la luz solar para evitar la decoloración. Siguiendo estas recomendaciones, podrás disfrutar de tu abrigo encaje morado durante mucho tiempo.',
  },
  'abrigo-lana-aguamarina': {
    prompt: 'abrigo lana aguamarina',
    opinionBody:
      'El Abrigo de Lana Aguamarina ha recibido excelentes críticas por su calidad y estilo. Los usuarios destacan la suavidad y calidez de la lana, lo que lo convierte en una opción perfecta para los meses más fríos del año. El color aguamarina también ha sido muy elogiado, ya que es elegante y combina fácilmente con diferentes looks. Además, los clientes aprecian los detalles de diseño y la durabilidad de este abrigo.',
    combineBody:
      'El Abrigo de Lana Aguamarina se puede combinar de muchas formas para lucir un look chic y sofisticado. Para un estilo casual, puedes combinarlo con unos jeans ajustados, una camiseta básica blanca y botines de cuero negro. Si buscas algo más formal, puedes usarlo sobre un vestido midi y combinarlo con zapatos de tacón. También puedes jugar con los accesorios y optar por una bufanda o gorro en tonos neutros para resaltar el color aguamarina del abrigo.',
    cleanBody:
      'Para mantener el Abrigo de Lana Aguamarina en perfectas condiciones, es recomendable seguir algunas pautas de cuidado. Se recomienda lavarlo en seco para evitar dañar la lana. Si es necesario, puedes sacudirlo suavemente para eliminar el polvo y las pelusas. Para guardar el abrigo, es recomendable utilizar una percha de madera para evitar que se deforme. Además, se recomienda guardarlo en una bolsa de tela transpirable para protegerlo del polvo y la humedad. Con estos cuidados adecuados, el Abrigo de Lana Aguamarina te acompañará durante muchos años.',
  },
  'abrigo-nylon-rojo': {
    prompt: 'abrigo nylon rojo',
    opinionBody:
      'Los usuarios destacan la calidad y resistencia del material de nylon en este abrigo. Además, el color rojo intenso es muy llamativo y agrega un toque de estilo a cualquier outfit. Muchos comentan que es perfecto para días fríos, ya que protege del viento y la lluvia. En general, se considera una excelente elección para quienes buscan un abrigo funcional y con un diseño moderno.',
    combineBody:
      'El abrigo de nylon rojo es extremadamente versátil a la hora de combinarlo. Para un look casual, puedes utilizarlo con unos jeans y una camiseta blanca, o incluso con leggings y una sudadera para un estilo más deportivo. Si prefieres un look más elegante, puedes llevarlo sobre un vestido negro o con pantalones de cuero y una blusa. El color rojo vibrante le dará un toque de sofisticación a cualquier combinación.',
    cleanBody:
      'Para mantener este abrigo impecable, se recomienda limpiarlo en seco o seguir las instrucciones indicadas en la etiqueta. Si necesitas quitar manchas o suciedad pequeña, puedes utilizar un paño húmedo con un poco de detergente suave. Evita utilizar productos químicos fuertes o blanqueadores, ya que pueden dañar el material de nylon. Asimismo, guárdalo en un lugar fresco y seco para evitar la acumulación de humedad. Siguiendo estas pautas de cuidado, podrás disfrutar de tu abrigo de nylon rojo por mucho tiempo.',
  },
  'abrigo-piel-bronce': {
    prompt: 'abrigo piel bronce',
    opinionBody:
      'Este abrigo de piel bronce ha recibido excelentes opiniones por parte de nuestros clientes. Destacan su elegante color bronce y su suave textura de piel, que proporciona un gran confort y calidez en los días fríos. También comentan que su diseño versátil permite combinarlo fácilmente con diferentes estilos de vestimenta.',
    combineBody:
      'El abrigo de piel bronce es muy versátil y se puede combinar con diferentes prendas y atuendos. Para un look sofisticado, puedes lucirlo con un vestido negro ajustado y tacones altos. Si prefieres un look más casual, puedes combinarlo con unos jeans ajustados y una camiseta básica. También queda genial con faldas o pantalones de cuero para un estilo más arriesgado y moderno.',
    cleanBody:
      'Para mantener tu abrigo de piel bronce en óptimas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. Primero, evita exponerlo directamente a la luz del sol o a fuentes de calor, ya que esto puede dañar el color y la textura de la piel. Si se mancha, utiliza un cepillo suave o una esponja húmeda con agua y jabón neutro para eliminar la suciedad. También es recomendable aplicar productos especiales para el cuidado de la piel, como acondicionadores o cremas hidratantes, para mantenerla suave y protegida. Finalmente, guárdalo en una bolsa de tela transpirable en un lugar fresco y seco.',
  },
  'abrigo-poliester-azul': {
    prompt: 'abrigo poliester azul',
    opinionBody:
      'Este abrigo de poliéster azul ha recibido excelentes opiniones por su diseño moderno y elegante. Los usuarios destacan la comodidad que proporciona gracias a su material ligero y resistente. Además, el color azul brillante añade un toque de estilo y distinción a cualquier outfit. Sin duda, es una prenda versátil y funcional que no puede faltar en el armario de cualquier persona amante de la moda.',
    combineBody:
      'Este abrigo de poliéster azul es perfecto para combinar con una amplia variedad de prendas. Para un look casual, puedes llevarlo con unos jeans ajustados, una camiseta básica blanca y unas zapatillas deportivas. Si prefieres algo más formal, puedes combinarlo con unos pantalones de vestir en tonos grises o negros, una camisa blanca y unos zapatos de cuero. En definitiva, las posibilidades son infinitas y este abrigo azul ofrece un sinfín de opciones para crear outfits elegantes y estilosos.',
    cleanBody:
      'Para mantener este abrigo de poliéster azul en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Para eliminar manchas superficiales, puedes utilizar un paño húmedo y jabón neutro, frotando suavemente sobre la zona afectada. Si necesitas lavarlo por completo, te recomendamos seguir las instrucciones específicas de cuidado del fabricante, ya que algunas prendas de poliéster pueden ser lavadas a máquina con agua fría. Para mantener su forma y evitar arrugas, es aconsejable colgarlo en un perchero de forma adecuada después de su uso. Además, es recomendable evitar el contacto con sustancias químicas agresivas y la exposición prolongada al sol. Siguiendo estos consejos, podrás disfrutar de tu abrigo de poliéster azul durante mucho tiempo.',
  },
  'abrigo-tela-rosa': {
    prompt: 'abrigo tela rosa',
    opinionBody:
      'El abrigo de tela rosa ha recibido excelentes opiniones por parte de los usuarios. La combinación del color rosa con la suavidad y calidez de la tela lo convierte en una opción perfecta para los días fríos. Además, su diseño elegante y versátil hace que sea una prenda muy fácil de combinar y llevar en cualquier ocasión.',
    combineBody:
      'El abrigo de tela rosa es muy versátil y combina con una gran variedad de estilos y colores. Para un look elegante, puedes combinarlo con colores neutros como el blanco, el gris o el negro. Si prefieres un look más llamativo, puedes combinarlo con tonos vibrantes como el rojo, el amarillo o el azul. También puedes optar por combinarlo con estampados como el animal print o las rayas para agregar un toque de modernidad a tu outfit.',
    cleanBody:
      'Para limpiar y mantener tu abrigo de tela rosa, te recomendamos seguir las instrucciones de cuidado del fabricante. Por lo general, se recomienda lavar a mano con agua fría y utilizar un detergente suave. Evita usar lejía o productos químicos agresivos, ya que podrían dañar la tela y el color. Después de lavar, es importante secar al aire libre en un lugar fresco y evitar la exposición directa al sol. Si es necesario planchar, utiliza una temperatura baja y coloca un paño entre la plancha y el abrigo para proteger la tela. Con estos cuidados, tu abrigo de tela rosa lucirá impecable y te acompañará durante mucho tiempo.',
  },
  'abrigo-vaquera-plata': {
    prompt: 'abrigo vaquera plata',
    opinionBody:
      'El abrigo de vaquera plata es una elección audaz y moderna para aquellos que quieren destacar en su estilo. Los usuarios han elogiado la calidad del material, así como el diseño único y llamativo. Su color plateado le da un toque único y sofisticado, atrayendo la atención de todos a su alrededor. Los usuarios también han destacado la comodidad y versatilidad de este abrigo, ya que puede ser usado tanto durante el día como en ocasiones especiales. Sin duda, el abrigo de vaquera plata es una opción atrevida y elegante para aquellos que buscan deslumbrar con su estilo.',
    combineBody:
      'El abrigo de vaquera plata es una prenda versátil que puede ser combinada de diversas maneras. Para un look casual y juvenil, puedes combinarlo con unos jeans ajustados y una camiseta blanca básica. Si quieres un estilo más sofisticado, puedes optar por llevarlo sobre un vestido negro ajustado y unos tacones altos. También puedes añadir accesorios en tonos plateados para completar el look. Sea cual sea tu elección, el abrigo de vaquera plata añadirá un toque de estilo y originalidad a cualquier conjunto.',
    cleanBody:
      'Para mantener el abrigo de vaquera plata en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Siempre lee las instrucciones de cuidado en la etiqueta antes de lavarlo. En general, se recomienda lavar a mano con agua fría y un detergente suave. Evita el uso de lejía y suavizante, ya que pueden dañar el color y el material. Una vez lavado, déjalo secar al aire libre, evitando la exposición directa al sol para evitar la decoloración. Si es necesario plancharlo, utiliza una temperatura baja y coloca un paño entre el abrigo y la plancha para evitar daños. Siguiendo estos consejos, podrás disfrutar de tu abrigo de vaquera plata durante mucho tiempo.',
  },
  'anorak-algodon-claro': {
    prompt: 'anorak algodon claro',
    opinionBody:
      'El anorak de algodón claro ha recibido excelentes comentarios por parte de nuestros clientes. Su material de algodón garantiza suavidad y comodidad, lo que lo convierte en la opción perfecta para los días fríos de invierno. Además, su color claro le da un toque de elegancia y versatilidad, permitiendo combinarlo fácilmente con cualquier tipo de outfit. Los usuarios destacan su calidad en cuanto a durabilidad y resistencia, convirtiéndolo en una prenda imprescindible en el armario durante años.',
    combineBody:
      'El anorak de algodón claro es una prenda muy versátil que se puede combinar de diversas maneras. Para un look casual, puedes llevarlo con jeans y una camiseta básica. Si buscas un estilo más formal, puedes optar por combinarlo con pantalones chinos y una camisa. Para un toque más femenino, puedes llevarlo con una falda midi y unas botas altas. El color claro del anorak permite combinarlo con una amplia gama de colores, lo que te da la libertad de experimentar y crear diferentes estilos según la ocasión.',
    cleanBody:
      'Para mantener tu anorak de algodón claro en perfecto estado, te recomendamos seguir algunos consejos de limpieza y cuidado. Para eliminar manchas leves, puedes utilizar un paño húmedo con un poco de jabón suave y frotar suavemente sobre la zona afectada. Si necesitas lavarlo por completo, es importante leer las instrucciones de cuidado en la etiqueta para asegurarte de utilizar la temperatura adecuada y el ciclo de lavado adecuado. Después de lavarlo, te recomendamos colgarlo en una percha y dejarlo secar al aire libre, evitando exponerlo directamente al sol. Para mantener su suavidad, puedes plancharlo a baja temperatura utilizando un paño protector. Siguiendo estos sencillos consejos, podrás disfrutar de tu anorak de algodón claro por mucho tiempo.',
  },
  'anorak-algodon-verde': {
    prompt: 'anorak algodon verde',
    opinionBody:
      'Los usuarios que han adquirido nuestro anorak de algodón verde han quedado gratamente sorprendidos. Destacan su excelente calidad, la suavidad del material de algodón y su resistencia al frío. Además, el color verde es muy llamativo y combina perfectamente con otros tonos de ropa. Sin duda, es una prenda imprescindible para los días más frescos.',
    combineBody:
      'Este anorak es muy versátil a la hora de combinarlo en tus outfits diarios. Puedes optar por un look casual y sport combinándolo con unos jeans y una camiseta blanca. Si buscas un estilo más elegante, puedes llevarlo con unos pantalones negros y una blusa de tonos neutros. Además, el verde del anorak combina muy bien con otros colores como el azul marino o el gris, permitiéndote crear outfits únicos y llenos de estilo.',
    cleanBody:
      'Para mantener tu anorak de algodón verde en perfectas condiciones, te recomendamos lavarlo a máquina con agua fría y utilizar un detergente suave. Evita utilizar productos a base de cloro y secadora, ya que podrían dañar el tejido. Para eliminar manchas, puedes utilizar un poco de detergente líquido y frotar suavemente con un cepillo suave. No olvides colgarlo para que se seque al aire libre y evita plancharlo a altas temperaturas. Siguiendo estos cuidados, tu anorak de algodón verde se mantendrá como nuevo durante mucho tiempo.',
  },
  'anorak-cachemira-lima': {
    prompt: 'anorak cachemira lima',
    opinionBody:
      'Los usuarios que han adquirido este anorak de cachemira en color lima destacan su increíble suavidad y calidez. El material de cachemira es de alta calidad y ofrece una sensación deluxe al usarlo. Además, el color lima le da un toque moderno y llamativo al outfit. Sin duda, una excelente elección para quienes buscan estar abrigados y a la moda.',
    combineBody:
      'La versatilidad del anorak de cachemira en color lima permite combinarlo fácilmente con diferentes prendas y estilos. Para un look casual, puedes llevarlo con unos jeans y una camiseta blanca básica. Si buscas algo más elegante, combínalo con unos pantalones negros y una blusa de seda en tonos neutros. También puedes agregar accesorios en tonos tierra para darle un toque más sofisticado.',
    cleanBody:
      'Es importante tener en cuenta que la cachemira es un material delicado, por lo que se recomienda seguir las instrucciones de lavado del fabricante. En general, se aconseja lavar a mano con agua fría y con un detergente suave para prendas delicadas. Evita retorcer o frotar en exceso, ya que esto puede dañar las fibras de la cachemira. Para secarlo, colócalo en una superficie plana y déjalo secar al aire libre. Recuerda no colgarlo para evitar deformaciones.',
  },
  'anorak-cuero-azul': {
    prompt: 'anorak cuero azul',
    opinionBody:
      'Este anorak de cuero azul ha recibido excelentes opiniones por parte de nuestros clientes. Su material de cuero de alta calidad es resistente y duradero, ofreciendo una excelente protección contra el viento y el frío. Además, su color azul añade un toque moderno y elegante a cualquier conjunto. Los usuarios destacan su comodidad y funcionalidad, convirtiéndolo en una opción ideal para el uso diario o para ocasiones más formales. Sin duda, este anorak se ha convertido en una de las opciones favoritas de nuestros compradores.',
    combineBody:
      'El anorak de cuero azul es extremadamente versátil y puede combinarse con una amplia variedad de prendas y estilos. Para un look casual, puedes usarlo con unos vaqueros ajustados y unas zapatillas blancas. Si buscas un atuendo más elegante, combínalo con unos pantalones de vestir y unos zapatos de cuero. Además, puedes añadirle accesorios en tonos neutros, como una bufanda o un bolso en beige o negro, para darle un toque extra de sofisticación. Con este anorak, las posibilidades son infinitas y podrás crear conjuntos que se adapten a cualquier ocasión.',
    cleanBody:
      'Para mantener tu anorak de cuero azul en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, asegúrate de leer las instrucciones de lavado del fabricante antes de proceder. En general, se recomienda limpiar el cuero con un paño húmedo y un detergente suave. Evita el uso de productos químicos agresivos o solventes que puedan dañar el material. Además, es aconsejable guardar el anorak en un lugar fresco y seco cuando no lo estés utilizando, alejado de la luz directa del sol y de fuentes de calor. Siguiendo estos consejos, tu anorak de cuero azul lucirá impecable durante mucho tiempo.',
  },
  'anorak-gasa-lima': {
    prompt: 'anorak gasa lima',
    opinionBody:
      'Los usuarios que han adquirido este anorak de gasa color lima han quedado encantados con su estilo moderno y llamativo. Además, destacan la ligereza y comodidad que proporciona gracias al material de gasa. Su diseño versátil permite usarlo en diferentes ocasiones, ya sea para un look casual o más elegante. Sin duda, es una prenda perfecta para agregar un toque de color y estilo a cualquier outfit.',
    combineBody:
      'El anorak de gasa color lima es ideal para añadir un toque de frescura y originalidad a tu conjunto. Combínalo con unos jeans ajustados y una camiseta blanca para un look casual y desenfadado. Si buscas un estilo más sofisticado, puedes combinarlo con una falda midi en tonos neutros y unos tacones. No temas experimentar con otros colores y estampados, ya que el anorak lima es un excelente complemento para jugar con diferentes contrastes y crear looks únicos.',
    cleanBody:
      'Para mantener tu anorak de gasa color lima en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarlo a mano con agua fría y un detergente suave. Evita retorcer o frotar bruscamente, ya que la gasa es un tejido delicado. Si es posible, cuélgalo para que se seque al aire libre. En caso de que necesite ser planchado, utiliza una temperatura baja y coloca un paño húmedo entre la plancha y el anorak para evitar daños en el material. Siguiendo estos simples cuidados, podrás disfrutar de tu anorak lima durante mucho tiempo.',
  },
  'anorak-gasa-marino': {
    prompt: 'anorak gasa marino',
    opinionBody:
      'Las opiniones de los usuarios sobre el anorak de gasa marino son en su mayoría muy positivas. Destacan su diseño moderno y versátil, ideal para añadir un toque elegante a cualquier outfit casual. Además, valoran su material ligero y transpirable, perfecto para los días de entretiempo. El color marino también es muy apreciado, ya que combina fácilmente con todo tipo de prendas.',
    combineBody:
      'El anorak de gasa marino es una prenda muy versátil que se puede combinar de diversas formas. Si buscas un look casual y cómodo, puedes combinarlo con unos jeans ajustados y unas zapatillas blancas. Si prefieres un look más sofisticado, puedes llevarlo con un vestido ajustado y unos tacones negros. Además, puedes añadir complementos en colores neutros para destacar el tono marino del anorak.',
    cleanBody:
      'Para mantener en buen estado el anorak de gasa marino, es recomendable seguir algunas pautas de limpieza y cuidado. Se recomienda lavarlo a mano o a máquina en un programa suave, utilizando detergentes suaves. Es importante evitar el uso de lejía o blanqueadores. A la hora de secarlo, se aconseja colgarlo en una percha en un lugar ventilado, evitando la exposición directa al sol. Con estos cuidados, podrás disfrutar de tu anorak de gasa marino durante mucho tiempo.',
  },
  'anorak-jersey-rojo': {
    prompt: 'anorak jersey rojo',
    opinionBody:
      'El anorak jersey rojo es uno de los productos más populares de nuestra tienda. Los clientes adoran su diseño moderno y llamativo, así como su material de alta calidad que proporciona calidez y comodidad. Además, el color rojo vibrante da un toque de estilo audaz a cualquier conjunto. Los usuarios destacan su durabilidad y resistencia al desgaste, así como su capacidad para mantener el calor en climas fríos. Sin duda, este anorak jersey rojo es una excelente elección para aquellos que buscan un abrigo funcional y a la moda.',
    combineBody:
      'Este anorak jersey rojo es una prenda versátil que se puede combinar de muchas formas diferentes. Para un estilo casual y relajado, puedes combinarlo con unos jeans oscuros y unas zapatillas blancas. Si buscas un look más elegante, puedes usarlo sobre un vestido negro ajustado y combinarlo con botas altas a juego. También puedes optar por un estilo deportivo combinándolo con leggings negros y zapatillas deportivas. Sea cual sea tu estilo, el anorak jersey rojo añadirá un toque de color vibrante y estilo a cualquier conjunto.',
    cleanBody:
      'Para mantener el anorak jersey rojo en buenas condiciones, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlo a máquina con agua fría y en ciclo suave. Evita usar lejía y suavizante de tela, ya que pueden dañar el material. Una vez lavado, puedes colgarlo para que se seque al aire libre, evitando exponerlo directamente a la luz solar intensa. Si necesitas quitar alguna mancha, es recomendable tratarla con un quitamanchas suave antes de lavarlo. Siguiendo estas instrucciones, podrás mantener tu anorak jersey rojo en óptimas condiciones durante mucho tiempo.',
  },
  'anorak-organdi-plata': {
    prompt: 'anorak organdi plata',
    opinionBody:
      'Los usuarios que han adquirido el anorak organdi plata hablan maravillas de su diseño elegante y moderno. El material de organdi le da un toque único y sofisticado, además de brindar resistencia al agua y protección contra el viento. El color plata le confiere un aspecto brillante y llamativo que se destaca en cualquier ocasión. Sin duda, este anorak es una excelente opción para aquellos que buscan combinar estilo y funcionalidad en una prenda de abrigo.',
    combineBody:
      'El anorak organdi plata es extremadamente versátil y se adapta a una amplia variedad de outfits. Para un look casual chic, puedes combinarlo con unos jeans ajustados y una blusa blanca. Si quieres un estilo más elegante, puedes optar por una falda midi y una blusa satinada en tonos neutros. También puede ser una excelente opción para un outfit deportivo, combinado con leggings y zapatillas blancas. Las posibilidades son infinitas con este anorak, ¡permite tu creatividad y disfruta de un estilo único!',
    cleanBody:
      'Para mantener tu anorak organdi plata en óptimas condiciones, es recomendable seguir ciertos cuidados. Para la limpieza regular, puedes utilizar un paño húmedo para eliminar manchas o suciedad superficial. Si es necesario, puedes lavarlo a mano con agua fría y un detergente suave. Evita utilizar blanqueadores o productos químicos agresivos que puedan dañar el material. Además, es importante secarlo al aire libre y evitar exponerlo directamente a la luz solar intensa, ya que podría alterar el color. Siguiendo estos simples consejos, podrás disfrutar de tu anorak organdi plata por mucho tiempo.',
  },
  'anorak-organdi-rosa': {
    prompt: 'anorak organdi rosa',
    opinionBody:
      'Los usuarios que han adquirido el anorak organdi rosa destacan la calidad y comodidad de este artículo. Su material ligero y resistente lo convierte en una opción ideal para los días de lluvia o viento moderado. Además, su color rosa añade un toque de feminidad y estilo a cualquier outfit. Los clientes también mencionan que el anorak organdi rosa se ajusta perfectamente a su figura, brindando un aspecto elegante y juvenil.',
    combineBody:
      'El anorak organdi rosa puede combinarse de diversas formas para crear looks modernos y sofisticados. Para un estilo casual, puedes combinarlo con unos jeans ajustados y una camiseta básica blanca. Si deseas un look más elegante, puedes combinarlo con una falda midi y un top de encaje en tonos neutros. También podrías optar por un outfit más deportivo, combinándolo con unos leggings y una sudadera con estampado floral. Las posibilidades son infinitas, ¡solo deja volar tu creatividad!',
    cleanBody:
      'Para mantener el anorak organdi rosa en buen estado, se recomienda lavarlo a mano o a máquina en agua fría y con un detergente suave. Evita usar blanqueadores o suavizantes, ya que pueden dañar el material. Después de lavarlo, lo mejor es colgarlo para que se seque al aire libre. Si es necesario, puedes plancharlo a baja temperatura. Asimismo, es importante guardar el anorak organdi rosa en un lugar fresco y seco, alejado de la luz directa del sol, para evitar que se desgaste prematuramente. Siguiendo estos sencillos consejos, podrás disfrutar de tu anorak organdi rosa durante mucho tiempo.',
  },
  'anorak-rayon-azul': {
    prompt: 'anorak rayon azul',
    opinionBody:
      'Los usuarios han quedado encantados con el anorak rayon azul. Destacan su comodidad y versatilidad, ya que se adapta fácilmente a diferentes estilos y ocasiones. Además, el material de rayon proporciona un tacto suave y ligero, perfecto para usar durante toda la temporada. Su color azul intenso también recibe elogios, ya que añade un toque de estilo a cualquier outfit.',
    combineBody:
      'El anorak rayon azul es una prenda muy versátil que se puede combinar de múltiples formas. Para un look casual, puedes llevarlo con unos jeans ajustados y unas zapatillas blancas. Si buscas un estilo más elegante, puedes optar por combinarlo con una falda midi y unas botas de tacón. El color azul se complementa muy bien con tonos neutros como el blanco, el gris o el negro, pero también se ve genial con colores más llamativos como el rojo o el amarillo.',
    cleanBody:
      'Para mantener tu anorak rayon azul en buen estado, es importante seguir las instrucciones de cuidado del fabricante. Normalmente, se recomienda lavarlo a mano o a máquina en agua fría con detergente suave. Evita usar lejía o productos químicos agresivos que puedan dañar el material. Para secarlo, puedes colgarlo en una percha o tenderlo en una superficie plana. Si es necesario, puedes plancharlo a baja temperatura. Recuerda consultar la etiqueta de cuidado para obtener instrucciones específicas para tu anorak rayon azul.',
  },
  'anorak-sintetica-marron': {
    prompt: 'anorak sintetica marron',
    opinionBody:
      'Las opiniones sobre el anorak sintético marrón son muy positivas. Los usuarios destacan la calidad del material sintético, que proporciona una excelente protección contra el frío y el viento. Además, el color marrón es muy versátil y combina fácilmente con otras prendas, lo que hace que este anorak sea una opción elegante y funcional para cualquier ocasión.',
    combineBody:
      'El anorak sintético marrón es extremadamente versátil y combina muy bien con una amplia variedad de prendas. Para un look casual, puedes combinarlo con jeans desgastados y unas botas de montaña. Si buscas un estilo más elegante, puedes usarlo sobre un vestido ajustado y combinarlo con botas altas. Incluso puedes darle un toque sofisticado combinándolo con pantalones de cuero y botines.',
    cleanBody:
      'Para mantener el anorak sintético marrón en perfectas condiciones, es recomendable seguir algunas instrucciones de limpieza y cuidado. En primer lugar, verifica las instrucciones específicas del fabricante, ya que pueden variar según el tipo de material sintético utilizado. En general, se recomienda lavarlo a máquina con agua fría y utilizando un detergente suave. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el material. Para mantenerlo en buen estado, guárdalo en un lugar fresco y seco cuando no lo estés usando.',
  },
  'anorak-vaquera-blanco': {
    prompt: 'anorak vaquera blanco',
    opinionBody:
      'Los usuarios han quedado encantados con este anorak vaquero blanco. Destacan la comodidad que brinda gracias a su material de alta calidad y su diseño moderno. Además, el color blanco le da un toque fresco y versátil que se adapta a cualquier ocasión. Sin duda, es una prenda imprescindible en cualquier armario.',
    combineBody:
      'El anorak vaquero blanco es una prenda muy versátil que se puede combinar de varias formas. Para un look casual, puedes combinarlo con unos jeans y una camiseta básica. Si quieres lucir más elegante, puedes usarlo con un vestido y unas botas. Además, puedes añadir accesorios como un gorro o una bufanda para darle un toque de estilo extra. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu anorak vaquero blanco en perfecto estado, es importante seguir algunas recomendaciones. Para limpiarlo, puedes lavarlo a máquina con agua fría y utilizar un detergente suave. Evita usar lejía o productos abrasivos que puedan dañar el tejido. Después de lavarlo, déjalo secar al aire libre, evitando la exposición directa al sol. Si es necesario, puedes plancharlo a baja temperatura. Recuerda guardarlo en un lugar seco y ventilado para evitar la humedad. Con estos cuidados, tu anorak vaquero blanco lucirá como nuevo durante mucho tiempo.',
  },
  'anorak-vaquera-rosa': {
    prompt: 'anorak vaquera rosa',
    opinionBody:
      'El anorak vaquera rosa ha obtenido excelentes críticas por su estilo único y versatilidad. Los usuarios destacan su material de alta calidad que proporciona una sensación de comodidad y durabilidad. El color rosa le da un toque femenino y juvenil, convirtiéndolo en la prenda perfecta para añadir un toque de estilo a cualquier outfit. Además, su diseño funcional con bolsillos y capucha lo convierte en una opción ideal para mantenerse abrigado durante los días más fríos. En resumen, los usuarios están encantados con el anorak vaquera rosa por su estilo, calidad y funcionalidad.',
    combineBody:
      'El anorak vaquera rosa es una prenda versátil que se puede combinar de diversas formas. Para un look casual y desenfadado, puedes combinarlo con unos jeans ajustados y una camiseta básica blanca. Si buscas un look más elegante, puedes combinarlo con una falda midi y una blusa de seda en tonos neutros. Para un estilo más urbano, puedes llevarlo con unos leggings negros y una sudadera oversized. También puedes añadir complementos como una gorra y unas zapatillas blancas para darle un toque más moderno a tu outfit. Las posibilidades de combinación con el anorak vaquera rosa son infinitas, ¡solo necesitas usar tu creatividad!',
    cleanBody:
      'Para mantener tu anorak vaquera rosa en óptimas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. En primer lugar, lee cuidadosamente las instrucciones de lavado que vienen en la etiqueta del producto, ya que pueden variar según el material de la prenda. En general, se recomienda lavarla a mano o a máquina en un ciclo suave y con agua fría. Evita usar lejía y suavizantes, ya que pueden dañar el color y la calidad del tejido. Para secar el anorak, es preferible colgarlo en una percha en un lugar fresco y sombreado, evitando la exposición',
  },
  'bermudas-cachemira-plata': {
    prompt: 'bermudas cachemira plata',
    opinionBody:
      'Las bermudas de cachemira plata son una elección sofisticada y elegante para cualquier ocasión. Con su combinación de un material suave y lujoso como la cachemira y el color plata, estas bermudas se destacan por su estilo refinado y moderno. Los usuarios que han adquirido estas bermudas han expresado su satisfacción con la calidad del material y el ajuste cómodo de las mismas. Además, destacan la versatilidad de estas bermudas, ya que pueden ser utilizadas tanto en eventos formales como en ocasiones más relajadas. En general, las opiniones sobre las bermudas de cachemira plata son muy positivas, resaltando su estilo, comodidad y durabilidad.',
    combineBody:
      'Las bermudas de cachemira plata son una prenda versátil que puede combinarse de diversas formas para crear diferentes looks. Para un estilo más formal, puedes combinarlas con una camisa blanca y unos zapatos de vestir en tonos neutros. Si buscas un look más relajado pero elegante, puedes optar por una camiseta de algodón en un tono sólido y unas zapatillas blancas. También puedes añadir accesorios como un cinturón de cuero y una chaqueta de denim para darle un toque más casual. El color plata de estas bermudas permite una gran variedad de combinaciones, por lo que puedes experimentar con diferentes tops y calzado para crear el look que más te guste.',
    cleanBody:
      'Para garantizar la durabilidad y la apariencia impecable de tus bermudas de cachemira plata, es importante seguir algunos cuidados especiales. En primer lugar, es recomendable leer las instrucciones de lavado que vienen con la prenda, ya que cada fabricante puede tener recomendaciones específicas. En general, es preferible lavarlas a mano o en un ciclo suave con agua fría para evitar dañar las fibras de cachemira. Utiliza un detergente suave y evita retorcer o frotar en exceso al lavarlas. Después del lavado,',
  },
  'bermudas-cuero-marino': {
    prompt: 'bermudas cuero marino',
    opinionBody:
      'Las bermudas de cuero marino son altamente recomendadas por su estilo elegante y su material duradero. Los usuarios destacan la comodidad que brinda el cuero, así como su resistencia a las arrugas. Además, el color marino le da un toque sofisticado que las hace versátiles para diferentes ocasiones.',
    combineBody:
      'Las bermudas de cuero marino son muy versátiles y se pueden combinar con diversas prendas. Para un look casual y relajado, puedes combinarlas con una camiseta blanca y unas zapatillas blancas. Para un estilo más formal, puedes optar por una camisa de tonos neutros y unos mocasines. Si quieres un look más llamativo, puedes combinarlas con una camiseta estampada y unas botas negras.',
    cleanBody:
      'Para limpiar las bermudas de cuero marino, se recomienda utilizar un paño húmedo con agua tibia y un poco de jabón neutro. Es importante evitar el uso de productos químicos fuertes que puedan dañar el cuero. Después de limpiarlas, se recomienda aplicar un acondicionador de cuero para mantener la suavidad y flexibilidad del material. Además, es importante guardar las bermudas en un lugar fresco y seco para evitar la acumulación de humedad que pueda dañar el cuero.',
  },
  'bermudas-gasa-azul': {
    prompt: 'bermudas gasa azul',
    opinionBody:
      'Las bermudas de gasa azul son altamente recomendadas por su comodidad y versatilidad. Los clientes destacan su diseño elegante y ligero, perfecto para los días de verano. Además, el color azul resalta su estilo y permite combinarlas con diferentes prendas. La mayoría de los usuarios coinciden en que son ideales para ocasiones informales y para disfrutar de una tarde relajada en la playa o en la piscina.',
    combineBody:
      'Para crear un look fresco y casual, puedes combinar las bermudas de gasa azul con una camiseta blanca y unas sandalias de cuero. Si prefieres un estilo más sofisticado, puedes optar por una blusa de seda en tonos neutros y unos zapatos de tacón alto. Además, puedes agregar algunos accesorios como un sombrero de paja y unas gafas de sol para completar el outfit de manera perfecta.',
    cleanBody:
      'Para mantener tus bermudas de gasa azul en perfectas condiciones, se recomienda lavarlas a mano con agua fría y un detergente suave. Evita el uso de cloro y suavizantes, ya que pueden dañar el tejido. Después de lavarlas, sécalas al aire libre y evita la exposición directa al sol para evitar que se decoloren. Si es necesario, puedes plancharlas a baja temperatura, siempre utilizando un paño delgado para proteger la gasa. Con estos cuidados, tus bermudas de gasa azul estarán siempre impecables y listas para usarse.',
  },
  'bermudas-nylon-claro': {
    prompt: 'bermudas nylon claro',
    opinionBody:
      'Las bermudas de nylon claro son una opción popular entre aquellos que buscan un estilo versátil y cómodo. Los usuarios han elogiado la durabilidad de este material, que es resistente al desgaste y a las manchas. Además, el nylon claro proporciona una sensación ligera y transpirable, ideal para los días calurosos de verano. Los usuarios también han destacado la comodidad de estas bermudas, gracias a su tejido elástico que permite una mayor libertad de movimiento. En general, las opiniones sobre las bermudas de nylon claro son muy positivas, destacando su comodidad, durabilidad y versatilidad.',
    combineBody:
      'Las bermudas de nylon claro son muy versátiles y pueden ser combinadas de diferentes formas para crear distintos looks. Para un estilo casual y relajado, puedes combinarlas con una camiseta básica blanca y unas zapatillas deportivas. Para una apariencia más elegante, puedes optar por una camisa de lino en tonos neutros y unos mocasines de cuero. También puedes agregar accesorios como un cinturón de tela o una gorra para darle un toque personalizado a tu outfit. Además, las bermudas de nylon claro se pueden combinar fácilmente con cualquier color, lo que te permite experimentar con diferentes combinaciones para adaptarlas a tu propio estilo.',
    cleanBody:
      'Para mantener las bermudas de nylon claro en buen estado, es importante seguir algunas pautas de limpieza y cuidado. En primer lugar, es recomendable seguir las instrucciones de lavado que vienen indicadas en la etiqueta del producto. En general, se recomienda lavar las bermudas a máquina utilizando agua fría y un detergente suave. Evita el uso de blanqueador y suavizante, ya que pueden dañar el material. Después de lavarlas, es importante colgarlas para que se sequen al aire libre, evitando la exposición directa al sol. En caso de manchas, se recomienda tratarlas de inmediato con un quitamanchas suave antes de lavarlas. Sigui',
  },
  'bermudas-organdi-azul': {
    prompt: 'bermudas organdi azul',
    opinionBody:
      'Las bermudas organdi azul son una excelente elección para el verano. Su material ligero y transpirable proporciona comodidad durante todo el día, y el color azul añade un toque fresco y vibrante a cualquier look. Los usuarios destacan la calidad de la tela y la durabilidad de estas bermudas, además de la versatilidad para combinarlas con diversas prendas. Sin duda, una opción recomendada para aquellos que buscan estilo y confort en una prenda de verano.',
    combineBody:
      'Las bermudas organdi azul pueden combinarse de diversas formas para crear conjuntos elegantes y casuales. Para un look más formal, puedes combinarlas con una camisa blanca de algodón y zapatos de cuero en tonos neutros. Si buscas un estilo más relajado, puedes optar por una camiseta estampada y unas zapatillas blancas. Además, puedes agregar accesorios como un cinturón de tela o unas gafas de sol para darle un toque personalizado a tu outfit. Con estas bermudas, las posibilidades son infinitas.',
    cleanBody:
      'Para mantener tus bermudas organdi azul en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Primero, verifica siempre la etiqueta de cuidado del fabricante para conocer las instrucciones específicas. Generalmente, se recomienda lavar a máquina en agua fría y utilizar un detergente suave. Evita usar suavizantes de tela, ya que pueden dañar la estructura de la tela. Para secarlas, lo ideal es colgarlas a la sombra y evitar la exposición directa al sol, ya que esto puede afectar el color. Siguiendo estos sencillos consejos, tus bermudas organdi azul se mantendrán en perfecto estado durante mucho tiempo.',
  },
  'bermudas-piel-azul': {
    prompt: 'bermudas piel azul',
    opinionBody:
      'Las bermudas de piel azul son una excelente opción para aquellos que desean lucir elegantes y a la moda durante los días de calor. La suavidad y durabilidad del material de piel garantizan una prenda de alta calidad que se adaptará cómodamente a tu cuerpo. Además, el color azul aporta un toque de frescura y estilo, convirtiendo estas bermudas en una elección versátil para cualquier ocasión. Los usuarios que han adquirido estas bermudas destacan su diseño moderno y la comodidad que brindan, convirtiéndolas en una prenda imprescindible en su guardarropa.',
    combineBody:
      'Las bermudas de piel azul son altamente versátiles y pueden combinarse de diferentes formas para crear diferentes looks. Para un estilo casual y relajado, puedes combinarlas con una camiseta blanca de algodón y unas zapatillas blancas. Si buscas un look más elegante, puedes combinarlas con una camisa de lino y unos mocasines de cuero marrón. Además, puedes utilizar accesorios como un cinturón de cuero o una gorra para complementar tu outfit. La versatilidad del color azul te permitirá experimentar y crear diferentes combinaciones según tu estilo y gusto personal.',
    cleanBody:
      'Para mantener tus bermudas de piel azul en perfectas condiciones, es importante seguir algunas recomendaciones de cuidado. En primer lugar, antes de utilizarlas por primera vez, es recomendable aplicar un protector de cuero para evitar daños por manchas o líquidos. Si ocurriera algún derrame o mancha, es importante limpiarlo de inmediato con un paño suave y húmedo, evitando el uso de productos químicos agresivos. Para limpiar y acondicionar las bermudas de piel azul de forma regular, puedes utilizar un jabón neutro específico para cuero y frotar suavemente con un paño. Finalmente, es importante guardar las bermudas de piel azul en un lugar fresco y seco, evitando la exposición directa al sol para mantener su color y',
  },
  'bermudas-punto-azul': {
    prompt: 'bermudas punto azul',
    opinionBody:
      'Las bermudas de punto azul son una opción versátil y cómoda para el verano. Los usuarios han elogiado la suavidad y ligereza del material, que proporciona una sensación fresca y transpirable durante los días calurosos. El color azul añade un toque de estilo y elegancia a cualquier conjunto. Los clientes también han destacado la durabilidad de estas bermudas, ya que resisten bien el paso del tiempo y conservan su forma y color incluso después de varios lavados. En resumen, las opiniones sobre las bermudas de punto azul son muy positivas, destacando su comodidad, estilo y durabilidad.',
    combineBody:
      'Las bermudas de punto azul son una prenda versátil que se puede combinar de diversas formas. Para un look casual, puedes llevarlas con una camiseta blanca y unas zapatillas deportivas. Si buscas un estilo más elegante, puedes combinarlas con una camisa de manga corta en tonos claros y unos mocasines. Para un outfit veraniego y relajado, puedes conjuntar las bermudas con una camiseta de rayas y unas sandalias. Además, puedes jugar con los complementos, como un sombrero de paja o una mochila de lona, para añadir un toque personalizado a tu conjunto. Las posibilidades son infinitas, ¡anímate a experimentar!',
    cleanBody:
      'Para mantener tus bermudas de punto azul en buen estado, es importante seguir algunas pautas de limpieza y cuidado. Primero, asegúrate de leer las instrucciones de lavado proporcionadas por el fabricante. En general, se recomienda lavarlas a máquina en agua fría o templada, utilizando un detergente suave. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar el tejido. Para secarlas, es preferible colgarlas al aire libre o utilizar la secadora a baja temperatura. Si es necesario plancharlas, utiliza una temperatura baja y coloca un paño fino entre la plancha y las bermudas para evitar daños en el',
  },
  'bermudas-punto-bronce': {
    prompt: 'bermudas punto bronce',
    opinionBody:
      'Las bermudas de punto bronce son una elección excelente para aquellos que buscan una prenda versátil y moderna. Su color bronce es sofisticado y atractivo, y el material de punto le da un toque de confort y elasticidad. Los usuarios destacan la comodidad y la calidad de estas bermudas, afirmando que son una opción ideal para el verano. Además, el color bronce es bastante favorecedor y fácil de combinar, lo que las convierte en una prenda perfecta para cualquier ocasión.',
    combineBody:
      'Las bermudas de punto bronce son muy versátiles y fáciles de combinar. Puedes crear un look casual y relajado combinándolas con una camiseta blanca y unas zapatillas blancas. Si prefieres un outfit más elegante, puedes optar por combinarlas con una camisa de color neutro y unos mocasines. Si quieres darle un toque más veraniego, puedes añadir un sombrero de paja y unas sandalias. El color bronce es muy versátil y combina bastante bien con colores tierra, tonos neutros e incluso con estampados llamativos.',
    cleanBody:
      'Para mantener tus bermudas de punto bronce en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. Antes de lavarlas, asegúrate de leer las instrucciones de cuidado en la etiqueta del producto. En términos generales, es recomendable lavarlas a mano con agua fría y un detergente suave, para evitar que pierdan su forma y color. Evita usar lejía o productos químicos agresivos, ya que podrían dañar el material. Una vez lavadas, déjalas secar al aire libre, evitando la exposición directa al sol. Si es necesario, puedes plancharlas a baja temperatura. Siguiendo estos sencillos cuidados, tus bermudas de punto bronce estarán listas para lucir impecables en cualquier ocasión.',
  },
  'bermudas-punto-lima': {
    prompt: 'bermudas punto lima',
    opinionBody:
      'Las bermudas de punto lima son una opción muy popular entre los amantes de la moda. Fabricadas con materiales de alta calidad, como el algodón o la lana, estas bermudas ofrecen un ajuste cómodo y una gran durabilidad. Los usuarios destacan su suavidad al tacto y su capacidad para mantenerse frescas durante los días de calor. Además, el color lima le da un toque vibrante y moderno a cualquier look. En resumen, las opiniones sobre las bermudas de punto lima son muy positivas, ya que ofrecen estilo y comodidad en una sola prenda.',
    combineBody:
      'Las bermudas de punto lima son una prenda versátil que se puede combinar de diferentes maneras. Para un look casual y relajado, puedes combinarlas con una camiseta básica blanca y unas zapatillas deportivas. Si prefieres un estilo más elegante, puedes añadir una camisa de lino en tonos neutros y unos mocasines. Si quieres darle un toque más sofisticado, puedes optar por una blusa de seda y unos tacones. En definitiva, las bermudas de punto lima se adaptan a diferentes estilos y ocasiones, por lo que las opciones de combinación son infinitas.',
    cleanBody:
      'Para mantener tus bermudas de punto lima en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. La mayoría de las prendas de punto se deben lavar a mano o en ciclo suave en la lavadora, utilizando agua fría y un detergente suave. Es importante evitar el uso de suavizantes, ya que pueden dañar las fibras del tejido. Después de lavarlas, es recomendable secarlas en posición horizontal para evitar que pierdan su forma original. Si es necesario plancharlas, utiliza una temperatura baja y coloca un paño entre la plancha y las bermudas para evitar quemaduras. Siguiendo estos consejos, tus bermudas de punto lima se mantendrán en óptimas condiciones durante mucho tiempo.',
  },
  'bermudas-saten-lima': {
    prompt: 'bermudas saten lima',
    opinionBody:
      'Las bermudas de satén en color lima han recibido excelentes opiniones por parte de nuestros clientes. El material de satén ofrece un tacto suave y lujoso, mientras que el color lima le agrega un toque vibrante y llamativo. Los usuarios han elogiado la comodidad de estas bermudas, así como su aspecto moderno y elegante. Además, destacan la calidad de los materiales y la durabilidad de las prendas. Sin duda, las bermudas de satén en color lima son una elección acertada para aquellos que buscan una prenda versátil y a la moda.',
    combineBody:
      'Las bermudas de satén en color lima son una opción audaz y versátil que se pueden combinar de muchas formas. Para un look casual y fresco, puedes combinarlas con una camiseta blanca básica y zapatillas blancas. Si deseas un estilo más sofisticado, puedes optar por una blusa de seda en tonos neutros y sandalias de tacón alto. También puedes añadir accesorios en tonos dorados o plateados para darle un toque elegante. Recuerda que el color lima es llamativo por sí mismo, por lo que es recomendable combinarlo con colores neutros para mantener el equilibrio en tu outfit.',
    cleanBody:
      'Mantener las bermudas de satén en color lima en buen estado es fundamental para alargar su vida útil. Para su limpieza, es recomendable lavarlas a mano con agua fría y jabón suave. Evita retorcer o frotar en exceso la prenda, ya que esto podría dañar el tejido. Si decides lavarlas a máquina, utiliza un ciclo suave y colócalas dentro de una bolsa de lavado para protegerlas. Para el secado, es preferible colgarlas al aire libre o en una percha, evitando el uso de la secadora. Además, es importante evitar el contacto con productos químicos como perfumes o sprays, ya que podrían manchar o da',
  },
  'bermudas-tela-rojo': {
    prompt: 'bermudas tela rojo',
    opinionBody:
      'Las bermudas de tela rojo son una opción estilosa y versátil para cualquier guardarropa. Los usuarios que han comprado estas prendas han destacado su comodidad y la calidad del material. Además, el color rojo vibrante añade un toque de audacia y personalidad a cualquier conjunto. Ya sea para una ocasión casual o para un evento más formal, las bermudas de tela rojo son una elección segura que definitivamente llamará la atención.',
    combineBody:
      'Las bermudas de tela rojo pueden combinarse de varias formas para crear diferentes estilos. Para un look relajado de verano, puedes optar por una camiseta blanca y unas sandalias o zapatillas blancas. Si buscas un conjunto más sofisticado, puedes combinar las bermudas con una camisa de manga corta de tono neutro y unos mocasines o zapatos de vestir. También puedes añadir accesorios como un cinturón de cuero marrón o una gorra para darle un toque extra de estilo. Las posibilidades son infinitas, ¡solo necesitas dejar volar tu creatividad!',
    cleanBody:
      'Para mantener tus bermudas de tela rojo en perfectas condiciones, es importante seguir algunas pautas de cuidado. Antes de lavarlas, asegúrate de leer las instrucciones específicas en la etiqueta de la prenda. La mayoría de las bermudas de tela pueden lavarse a máquina en un ciclo suave con agua fría. Para evitar que los colores se desvanezcan, es recomendable darle la vuelta a las bermudas antes de lavarlas. Además, es preferible secarlas al aire libre en lugar de utilizar la secadora, ya que esto puede hacer que el tejido se deteriore más rápido. Por último, plancha las bermudas a baja temperatura si es necesario, y guárdalas en un lugar fresco y seco para mantener su forma y color originales.',
  },
  'bermudas-tela-verde': {
    prompt: 'bermudas tela verde',
    opinionBody:
      'Las bermudas de tela verde son una excelente opción para aquellos que buscan un estilo fresco y casual. Con su color vibrante y su suave material, estas bermudas son perfectas para disfrutar del verano. Los usuarios han elogiado su comodidad y versatilidad, ya que pueden combinar fácilmente con una variedad de prendas y estilos. Además, muchos han destacado la durabilidad de estas bermudas, lo que las convierte en una excelente inversión a largo plazo. En resumen, las opiniones sobre las bermudas de tela verde son muy positivas.',
    combineBody:
      'Las bermudas de tela verde ofrecen infinitas posibilidades de combinación. Para un look casual y relajado, puedes combinarlas con una simple camiseta blanca y unas zapatillas deportivas. Si buscas un estilo más elegante, puedes optar por una camisa de lino en tonos neutros y unos mocasines de cuero. Para un toque de frescura, puedes agregar accesorios en tonos tropicales como un sombrero de paja o una pulsera colorida. Las bermudas de tela verde son extremadamente versátiles y pueden adaptarse a cualquier situación o estilo.',
    cleanBody:
      'La limpieza y el mantenimiento adecuados son esenciales para asegurar que tus bermudas de tela verde se mantengan en buen estado. Para limpiarlas, se recomienda seguir las instrucciones de cuidado del fabricante. En general, las bermudas de tela verde pueden lavarse a máquina con agua fría y un detergente suave. Evita el uso de blanqueadores o suavizantes de telas, ya que pueden dañar el color y la calidad del material. Para secarlas, es mejor colgarlas al aire libre en lugar de usar la secadora, ya que el calor excesivo puede encoger o deformar las bermudas. Siguiendo estos consejos de limpieza y mantenimiento, tus bermudas de tela verde te durarán mucho tiempo y se verán impecables en cada uso.',
  },
  'bermudas-vaquera-lima': {
    prompt: 'bermudas vaquera lima',
    opinionBody:
      'Las bermudas vaqueras en color lima han sido todo un éxito entre nuestros clientes. Su material de alta calidad y su diseño moderno las convierten en una opción perfecta para cualquier ocasión. Los usuarios destacan su comodidad y estilo fresco, perfecto para el verano. Además, el color lima agrega un toque de originalidad a cualquier conjunto. Sin duda, estas bermudas son una elección acertada para aquellos que buscan destacar y lucir a la moda.',
    combineBody:
      'Las bermudas vaqueras en color lima son muy versátiles y se pueden combinar de diferentes maneras para crear distintos estilos. Para un look casual y relajado, puedes combinarlas con una camiseta blanca y zapatillas blancas o negras. Si quieres un look más elegante, puedes optar por una camisa de manga corta en tonos neutros y unos zapatos en color marrón. Además, los accesorios en tonos dorados complementan muy bien el color lima de las bermudas. ¡Deja volar tu imaginación y crea tu propio estilo único!',
    cleanBody:
      'Para asegurar la durabilidad y mantener el color vibrante de las bermudas vaqueras en color lima, es importante seguir algunas recomendaciones de cuidado. Aconsejamos siempre leer las etiquetas de lavado antes de proceder. En general, se recomienda lavarlas del revés y utilizar agua fría o templada. Evita el uso de blanqueadores y sécalas al aire libre o a baja temperatura. Para mantener su forma, plancha las bermudas a temperatura media. Siguiendo estos consejos, tus bermudas vaqueras en color lima estarán siempre impecables y listas para lucir en cada ocasión.',
  },
  'blazer-gasa-borgona': {
    prompt: 'blazer gasa borgona',
    opinionBody:
      'Los usuarios han quedado encantados con este elegante blazer de gasa en color borgoña. Destacan su caída suave y su ajuste perfecto, que realza la figura de manera favorecedora. Además, el material de gasa le da un toque sofisticado y ligero, perfecto para ocasiones especiales. Sin duda, es una opción versátil y chic que no puede faltar en tu guardarropa.',
    combineBody:
      'Este blazer de gasa en color borgoña es una prenda versátil que se puede combinar de múltiples formas. Para un look elegante, puedes llevarlo con una blusa de seda en tonos neutros y unos pantalones de vestir a juego. Si buscas un estilo más casual, puedes usarlo con jeans ajustados y una camiseta básica blanca. Además, puedes jugar con los accesorios para darle un toque personalizado, como unos zapatos de tacón en tonos metálicos o un bolso de mano en cuero negro. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu blazer de gasa en color borgoña en perfectas condiciones, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlo a mano con agua fría y detergente suave. Evita retorcerlo o frotarlo con fuerza para no dañar el material. Después de lavarlo, seca el blazer al aire libre, preferiblemente en un lugar sombreado, para evitar la exposición directa al sol. Para mantener su forma original, puedes plancharlo a temperatura baja y con el uso de una tela suave entre la plancha y el blazer. Siguiendo estos cuidados, podrás disfrutar de tu blazer de gasa borgoña por mucho tiempo.',
  },
  'blazer-lino-plata': {
    prompt: 'blazer lino plata',
    opinionBody:
      'El blazer de lino plata es una excelente opción para aquellos que buscan destacar y agregar un toque de elegancia a su estilo. Los usuarios están encantados con la calidad del tejido de lino, que le da a esta prenda una apariencia sofisticada y fresca. Además, el color plata es altamente versátil, ya que se puede combinar con una amplia variedad de colores y estampados. Los usuarios también destacan la comodidad de este blazer, ya que el lino es un material transpirable, ideal para climas cálidos. En resumen, los comentarios positivos sobre el blazer de lino plata confirman su estatus como una prenda imprescindible en cualquier guardarropa.',
    combineBody:
      'El blazer de lino plata es una prenda muy versátil que se puede combinar de varias maneras. Para un look formal, puedes combinarlo con unos pantalones negros o grises y una camisa blanca. También puedes optar por un estilo más casual, llevándolo con unos jeans y una camiseta básica. Para darle un toque de color, puedes añadir accesorios en tonos vivos, como un pañuelo o un bolso en colores pastel. Para un look más arriesgado, puedes combinarlo con estampados florales o rayas. En resumen, las posibilidades de combinación con el blazer de lino plata son infinitas, lo que lo convierte en una prenda muy versátil y fácil de adaptar a diferentes ocasiones.',
    cleanBody:
      'Para mantener tu blazer de lino plata en perfectas condiciones, es importante seguir algunas pautas de limpieza y cuidado. En primer lugar, es recomendable leer las instrucciones de cuidado del fabricante, ya que algunas prendas de lino pueden requerir limpieza en seco. Si puedes lavarlo en casa, es mejor hacerlo a mano o en un ciclo suave en la lavadora, utilizando agua fría o tibia. Evita el uso de blanqueadores o suavizantes, ya que pueden dañar el tejido de lino. Para secar, es preferible colgar',
  },
  'blazer-tela-esmeralda': {
    prompt: 'blazer tela esmeralda',
    opinionBody:
      'Los usuarios han quedado encantados con este blazer de tela esmeralda. Destacan la calidad del material, que es suave y resistente, así como el perfecto ajuste y corte del blazer. Además, el color esmeralda les ha encantado, ya que es elegante y sofisticado, perfecto para lucir en ocasiones especiales o en el día a día. Sin duda, es una prenda que no puede faltar en el armario de cualquier persona que quiera añadir un toque de estilo a su outfit.',
    combineBody:
      'El blazer de tela esmeralda es muy versátil a la hora de combinarlo. Puedes optar por lucirlo con pantalones negros ajustados y una blusa blanca para lograr un look elegante y formal. Si buscas algo más casual, puedes combinarlo con unos jeans claros y una camiseta estampada. Además, puedes añadir accesorios dorados para darle un toque de glamour. También puedes usarlo con vestidos, faldas o shorts, dependiendo de la ocasión y el estilo que desees lograr. Este blazer es una prenda clave para crear looks increíbles y llenos de estilo.',
    cleanBody:
      'Para mantener el blazer de tela esmeralda en óptimas condiciones, es recomendable seguir ciertos cuidados. En primer lugar, es importante leer las indicaciones de lavado en la etiqueta del blazer, ya que pueden variar según el tipo de tela utilizado. En general, se recomienda lavar a mano o en ciclo suave con agua fría. Evita el uso de lejía o productos químicos agresivos que puedan dañar el color. Para secarlo, es mejor colgarlo en una percha en un lugar fresco y bien ventilado, evitando la exposición directa al sol. Si es necesario, puedes plancharlo a baja temperatura, siempre utilizando un paño fino o una tela protectora para evitar dañar la tela. Siguiendo estos consejos, podrás disfrutar del blazer de tela esmeralda durante mucho tiempo y lucir impec',
  },
  'blazer-tela-oro': {
    prompt: 'blazer tela oro',
    opinionBody:
      'El blazer de tela oro es una prenda que destaca por su elegancia y sofisticación. Los usuarios que han adquirido este producto coinciden en que es perfecto para ocasiones especiales o eventos formales. El color dorado brinda un toque de glamour y se adapta a diferentes estilos y tonalidades de piel. Además, el material de tela utilizado en este blazer asegura una excelente calidad y durabilidad, lo que lo convierte en una inversión segura.',
    combineBody:
      'A la hora de combinar un blazer de tela oro, las opciones son infinitas. Este color es versátil y se puede utilizar tanto en looks formales como informales. Para un evento elegante, se puede combinar con una blusa o camisa blanca y pantalones negros o una falda lápiz. Si se busca un look más casual, se puede optar por unos jeans oscuros y una camiseta básica, aportando un toque de sofisticación a un outfit relajado. Además, se puede jugar con los complementos, como accesorios en tonos neutros o dorados, para realzar aún más el blazer.',
    cleanBody:
      'Para mantener el blazer de tela oro en perfectas condiciones, es importante seguir algunos consejos de limpieza y cuidado. Se recomienda leer las etiquetas de lavado que vienen con la prenda para seguir las instrucciones específicas del fabricante. En general, se suele recomendar el lavado en seco para este tipo de prendas, evitando exponerlas a la humedad o al sol directo. Además, es importante evitar aplicar productos químicos agresivos y planchar a baja temperatura, utilizando un paño entre la plancha y la tela para evitar daños. Siguiendo estas recomendaciones, el blazer de tela oro se mantendrá en perfectas condiciones y lucirá impecable en cada ocasión.',
  },
  'blazer-tweed-naranja': {
    prompt: 'blazer tweed naranja',
    opinionBody:
      'Este blazer tweed en color naranja ha recibido excelentes críticas por parte de nuestros clientes. El material de tweed le da un aspecto elegante y sofisticado, mientras que el color naranja le añade un toque de estilo único. Los usuarios destacan la calidad y la durabilidad de este blazer, además de su versatilidad para combinar con diferentes prendas.',
    combineBody:
      'El blazer tweed naranja es perfecto para darle un toque de color a tus conjuntos formales o casuales. Puedes combinarlo con una camisa blanca y unos pantalones de vestir en tonos neutros como el gris o el negro para crear un look elegante y moderno. Si quieres un estilo más relajado, puedes llevarlo con unos jeans en tonos claros y una camiseta básica. ¡Este blazer será el protagonista de cualquier outfit!',
    cleanBody:
      'Para mantener tu blazer tweed naranja en buen estado, te recomendamos seguir las instrucciones de cuidado del fabricante. Por lo general, este tipo de prendas se pueden limpiar en seco o lavar a mano con agua fría y un detergente suave. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el tejido. Además, es recomendable colgarlo en una percha para evitar arrugas. Si necesitas eliminar pequeñas manchas, puedes utilizar un quitamanchas suave, pero asegúrate de probarlo primero en una zona pequeña y poco visible del blazer. Con los cuidados adecuados, podrás disfrutar de tu blazer tweed naranja por mucho tiempo.',
  },
  'blusa-algodon-claro': {
    prompt: 'blusa algodon claro',
    opinionBody:
      'Las opiniones sobre la blusa de algodón claro suelen ser muy positivas. Los usuarios destacan la suavidad y frescura del material, perfecto para los días calurosos de verano. Además, mencionan que el algodón claro da un aspecto elegante y luminoso, ideal para cualquier ocasión. Muchos también resaltan la comodidad de la blusa y su versatilidad para combinarla con diferentes prendas.',
    combineBody:
      'La blusa de algodón claro es una prenda muy versátil que se puede combinar con diferentes estilos y colores. Para un look más casual, puedes combinarla con unos jeans ajustados y sandalias. Si quieres un outfit más elegante, puedes optar por una falda midi y tacones. También queda genial con shorts de colores neutros y zapatillas para un estilo más relajado. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu blusa de algodón claro en perfecto estado, es recomendable lavarla a mano o utilizando el ciclo delicado de la lavadora con agua fría. Evita el uso de lejía o productos químicos agresivos. Para secarla, es mejor colgarla al aire libre o utilizar la secadora en su modo más suave. Recuerda plancharla a baja temperatura y de forma suave para evitar dañar las fibras de algodón. Con estos cuidados, tu blusa de algodón claro lucirá como nueva durante mucho tiempo.',
  },
  'blusa-cachemira-celeste': {
    prompt: 'blusa cachemira celeste',
    opinionBody:
      'La blusa de cachemira celeste ha recibido excelentes opiniones por parte de nuestros clientes. Su suave y lujoso material de cachemira la convierte en una prenda sumamente cómoda y elegante. El color celeste le brinda un toque delicado y femenino, perfecto para lucir en diferentes ocasiones. Además, los usuarios han destacado la calidad de la blusa y su durabilidad, lo que la convierte en una inversión duradera en tu guardarropa.',
    combineBody:
      'La blusa de cachemira celeste es extremadamente versátil a la hora de combinarla con otras prendas. Para un look casual, puedes combinarla con unos jeans ajustados y unos zapatos de cuero en tono neutro. Si buscas un look más formal, puedes combinarla con una falda lápiz o pantalones de vestir en tonos oscuros. Los accesorios en tonos plateados o dorados le darán un toque sofisticado a tu outfit. ¡No tengas miedo de experimentar y crear diferentes looks con esta blusa!',
    cleanBody:
      'La cachemira es una fibra delicada que requiere de cuidados especiales para mantener su suavidad y apariencia. Para limpiar tu blusa de cachemira celeste, te recomendamos lavarla a mano con agua fría y un detergente suave. Evita retorcerla o frotarla en exceso para evitar dañar las fibras. Luego, sécala en posición horizontal sobre una toalla para evitar que se estire. Recuerda no colgarla, ya que esto puede deformarla. Para mantenerla en óptimas condiciones, guárdala doblada en un lugar fresco y seco. Si necesitas quitar cualquier arruga, puedes utilizar una plancha a baja temperatura y asegúrate de colocar un paño delgado entre la plancha y la blusa para evitar dañarla. Con estos cuidados adecuados, tu blusa de cachemira celeste se verá como nueva por mucho tiempo.',
  },
  'blusa-lino-verde': {
    prompt: 'blusa lino verde',
    opinionBody:
      'La blusa de lino verde se ha convertido en una favorita entre nuestros clientes. Su diseño elegante y suave material de lino hacen que sea una prenda perfecta para cualquier ocasión. Los usuarios han elogiado su estilo versátil y cómo se adapta a diferentes estilos de vestir. Además, el color verde ha sido muy apreciado por su frescura y su capacidad para resaltar la piel. Definitivamente, la blusa de lino verde es una opción acertada para lucir cómoda y a la moda.',
    combineBody:
      'La blusa de lino verde ofrece una gran variedad de combinaciones. Su tono verde vibrante se puede complementar con tonos neutros, como blanco, beige o negro, para lograr un look más sofisticado y elegante. También se puede combinar con colores similares, como verde oliva o verde menta, para crear un conjunto único y llamativo. Otra opción es añadir accesorios dorados o plateados para darle un toque de glamour. Sea cual sea tu estilo, la blusa de lino verde te permitirá crear combinaciones únicas y llenas de personalidad.',
    cleanBody:
      'La blusa de lino verde requiere algunos cuidados especiales para mantener su calidad y apariencia. Recomendamos lavarla a mano con agua fría y detergente suave. Evita retorcerla y sécala al aire libre para evitar dañar las fibras de lino. Si deseas plancharla, hazlo a baja temperatura y con vapor para evitar arrugas. Además, es importante guardarla en un lugar fresco y seco para evitar que se manche o se dañe. Sigue estos sencillos consejos y tu blusa de lino verde lucirá como nueva durante mucho tiempo.',
  },
  'blusa-sintetica-amarillo': {
    prompt: 'blusa sintetica amarillo',
    opinionBody:
      'La blusa sintética amarilla ha recibido excelentes críticas por parte de nuestros clientes. Su material sintético de alta calidad asegura que la prenda sea duradera y resistente al desgaste. Además, el color amarillo vibrante le da un toque de estilo y energía a cualquier outfit. Los usuarios destacan la comodidad y su ajuste perfecto, sin sacrificar el estilo. Definitivamente, una blusa que no puede faltar en tu guardarropa.',
    combineBody:
      'La versatilidad de la blusa sintética amarilla permite combinarla con diferentes prendas y accesorios para crear diversos looks. Para un estilo casual, puedes optar por jeans ajustados en tonos oscuros, como el azul marino o el negro. Si buscas un look más elegante, puedes combinarla con una falda lápiz en tonos neutros, como el gris o el blanco. Completa el outfit con unos zapatos de tacón en color negro y accesorios dorados para resaltar aún más el amarillo de la blusa.',
    cleanBody:
      'La blusa sintética amarilla es fácil de limpiar y mantener. Para mantenerla en óptimas condiciones, se recomienda lavarla a mano o en ciclo suave en la lavadora con agua fría. Evita utilizar lejía o suavizantes, ya que pueden dañar el material sintético. Para secarla, es preferible colgarla en una percha para evitar que se deforme. Si necesitas plancharla, utiliza una temperatura baja y coloca un paño fino entre la plancha y la blusa para proteger el material. Con estos sencillos cuidados, tu blusa sintética amarilla lucirá siempre como nueva.',
  },
  'blusa-tafetan-blanco': {
    prompt: 'blusa tafetan blanco',
    opinionBody:
      'La blusa de tafetán blanco ha recibido excelentes críticas por su elegancia y versatilidad. Los usuarios destacan la suavidad del material y cómo realza su figura. Además, el color blanco añade un toque de frescura y luminosidad a cualquier conjunto. Sin duda, una prenda que no puede faltar en el armario de ninguna mujer.',
    combineBody:
      'La blusa de tafetán blanco es tan versátil que se puede combinar con una gran variedad de prendas. Para un look formal, puedes llevarla con una falda lápiz negra y unos tacones. Si prefieres un estilo más casual, combínala con unos jeans ajustados y unas zapatillas blancas. Además, puedes añadir accesorios en tonos dorados para darle un toque de sofisticación.',
    cleanBody:
      'Para mantener tu blusa de tafetán blanco en perfecto estado, es importante seguir algunas recomendaciones de cuidado. Se recomienda lavarla a mano con agua fría y un detergente suave. Evita el uso de blanqueadores y sécala al aire libre para evitar que se dañe el material. Además, es importante guardarla en un lugar fresco y oscuro para evitar que se decolore con el tiempo.',
  },
  'blusa-tejido-turquesa': {
    prompt: 'blusa tejido turquesa',
    opinionBody:
      'La blusa de tejido turquesa ha recibido excelentes críticas por parte de nuestras clientas. El tejido ligero y suave ofrece una sensación de comodidad y frescura, ideal para los días más calurosos. El color turquesa es muy favorecedor y añade un toque de estilo a cualquier conjunto. Además, el tejido garantiza una durabilidad excepcional, lo que hace de esta blusa una inversión que vale la pena.',
    combineBody:
      'La versatilidad de la blusa de tejido turquesa permite combinarla con una amplia gama de prendas y estilos. Para un look casual, puedes combinarla con unos jeans ajustados y sandalias planas. Si buscas un estilo más elegante, opta por combinarla con una falda midi y tacones altos. El color turquesa también se ve increíble con tonos neutros como el blanco, el gris y el negro. ¡Experimenta y crea tu propio estilo único!',
    cleanBody:
      'Para mantener tu blusa de tejido turquesa en óptimas condiciones, se recomienda lavarla a mano con agua fría y detergente suave. Evita retorcerla o frotarla en exceso, ya que esto podría dañar el tejido. Una vez limpia, déjala secar al aire libre y evita la luz solar directa para prevenir la decoloración. Además, plancha la blusa con cuidado a temperatura baja si es necesario. Siguiendo estos sencillos pasos de cuidado, tu blusa de tejido turquesa lucirá impecable durante mucho tiempo.',
  },
  'body-cachemira-verde': {
    prompt: 'body cachemira verde',
    opinionBody:
      'Los usuarios han quedado encantados con este body de cachemira verde. Destacan su suavidad y comodidad, gracias al material de alta calidad con el que está fabricado. Además, el color verde ofrece un toque fresco y elegante, perfecto para lucir en cualquier ocasión. Sin duda, es una prenda versátil y favorecedora que ha recibido excelentes valoraciones por parte de quienes lo han probado.',
    combineBody:
      'Este body de cachemira verde es muy versátil y fácil de combinar. Puedes crear looks casuales y relajados combinándolo con jeans o pantalones de cuero negro. Para un look más elegante, puedes combinarlo con una falda midi de color negro o marrón. Además, puedes añadir accesorios dorados para darle un toque más sofisticado. En definitiva, las opciones son infinitas a la hora de combinar este body, permitiéndote crear outfits para diversas ocasiones.',
    cleanBody:
      'Para mantener este body de cachemira verde en perfecto estado, es recomendable seguir algunas pautas de limpieza y cuidado. Es importante lavarlo a mano o en ciclo delicado en la lavadora, utilizando agua fría. Evita retorcerlo para evitar deformaciones y déjalo secar al aire libre, evitando la exposición directa al sol. Para prevenir la formación de bolitas, puedes utilizar una máquina depiladora específica para prendas de cachemira. Asimismo, es aconsejable guardar el body en un lugar seco, alejado de la humedad, y utilizar perchas acolchadas para mantener su forma. Siguiendo estas recomendaciones, podrás disfrutar de tu body de cachemira verde durante mucho tiempo.',
  },
  'body-cuero-esmeralda': {
    prompt: 'body cuero esmeralda',
    opinionBody:
      'El body cuero esmeralda ha recibido excelentes críticas por parte de los usuarios. Su diseño ajustado y elegante realza la figura y se adapta perfectamente al cuerpo. El material de cuero brinda un aspecto sofisticado y duradero, mientras que el color esmeralda agrega un toque de originalidad y estilo. Los usuarios destacan su comodidad, la calidad de los acabados y lo versátil que resulta para diferentes ocasiones. Sin duda, el body cuero esmeralda es una elección acertada para quienes desean lucir a la moda y destacar en cualquier evento.',
    combineBody:
      'El body cuero esmeralda ofrece muchas opciones de combinación para crear looks sofisticados y modernos. Puedes llevarlo con una falda lápiz negra y tacones para un look elegante y sexy. También puedes combinarlo con unos pantalones de cuero negro y botines para un estilo más atrevido. Si buscas algo más casual, puedes usarlo con unos jeans ajustados y zapatillas blancas. Para agregar un toque de color, puedes combinarlo con accesorios en tonos dorados o plateados. Con el body cuero esmeralda, las posibilidades de estilo son infinitas.',
    cleanBody:
      'El body cuero esmeralda requiere algunos cuidados especiales para mantenerlo en óptimas condiciones. Se recomienda limpiarlo con un paño humedecido en agua tibia y jabón suave, evitando el uso de productos químicos agresivos que puedan dañar el cuero. Es importante secarlo al aire libre y evitar el contacto con fuentes de calor directas, como radiadores o secadoras, ya que pueden causar deformaciones en el material. Para mantener el color esmeralda vibrante, es aconsejable guardar el body en un lugar fresco y oscuro cuando no se esté utilizando. Siguiendo estos consejos de limpieza y cuidado, podrás disfrutar de tu body cuero esmeralda durante mucho tiempo.',
  },
  'body-encaje-negro': {
    prompt: 'body encaje negro',
    opinionBody:
      'El body encaje negro es uno de los productos más populares cuando se trata de lencería sexy y seductora. Su diseño elegante y sensual hace que las mujeres se sientan hermosas y confiadas. Muchas usuarias destacan la comodidad y suavidad del material de encaje, que se adapta perfectamente al cuerpo sin causar molestias. Además, el color negro agrega un toque de misterio y sofisticación. En general, las opiniones sobre el body encaje negro son muy positivas, ya que es una prenda que no solo favorece la apariencia, sino también la autoestima.',
    combineBody:
      'El body encaje negro es una prenda versátil que se puede combinar de diversas formas para crear distintos estilos. Para un look sensual y provocativo, puedes combinarlo con una falda de cuero o unos pantalones ajustados. Si prefieres un estilo más elegante, puedes usarlo debajo de un blazer o chaqueta. También puedes combinarlo con prendas más casuales, como jeans o una falda vaquera, para un look más informal pero chic. En cuanto al calzado, unas sandalias de tacón alto o unos botines negros añadirán un toque de glamour al conjunto. Las posibilidades de combinación con el body encaje negro son infinitas, ¡solo depende de tu creatividad!',
    cleanBody:
      'Para mantener el body encaje negro en buen estado durante más tiempo, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, es recomendable lavarlo a mano con agua fría y utilizando un detergente suave. Evita retorcer o frotar demasiado la prenda, ya que esto puede dañar el delicado encaje. Para secarlo, es preferible colgarlo al aire libre en un lugar sombreado, evitando la exposición directa al sol. También puedes usar una toalla limpia para absorber el exceso de humedad antes de colgarlo. Si prefieres utilizar la lavadora, asegúrate de colocar el body encaje negro en una bolsa de lavado para protegerlo de pos',
  },
  'body-jacquard-amarillo': {
    prompt: 'body jacquard amarillo',
    opinionBody:
      'Los usuarios han quedado encantados con el body jacquard amarillo. Destacan su diseño único y elegante, gracias al patrón jacquard que le da un toque sofisticado. Además, el color amarillo vibrante hace que este body sea perfecto para destacar en cualquier ocasión. El material utilizado es de alta calidad, lo que garantiza comodidad y durabilidad. Sin duda, es una prenda que no puede faltar en tu armario.',
    combineBody:
      'El body jacquard amarillo se puede combinar de diversas formas para crear looks impresionantes. Si buscas un estilo casual, puedes combinarlo con unos vaqueros de tiro alto y unas zapatillas blancas. Para un look más sofisticado, puedes usarlo con una falda midi plisada y unos tacones negros. También puedes añadir accesorios dorados para resaltar el tono amarillo del body. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener el body jacquard amarillo en perfectas condiciones, es recomendable lavarlo a mano con agua fría y utilizar detergentes suaves. Evita retorcerlo para evitar deformaciones en el tejido. Si es necesario plancharlo, utiliza una temperatura baja y coloca un paño entre la plancha y el body para proteger el material. Al guardarlo, es recomendable hacerlo en un lugar fresco y seco, preferiblemente colgado para evitar arrugas. Siguiendo estos cuidados, podrás disfrutar de tu body jacquard amarillo por mucho tiempo.',
  },
  'body-punto-azul': {
    prompt: 'body punto azul',
    opinionBody:
      'El body punto azul es una prenda muy versátil y cómoda. Los usuarios han dejado opiniones positivas sobre su ajuste perfecto y su suave material. Además, destacan el bonito color azul que le da un toque único y elegante. Sin duda, es una opción ideal para lucir a la moda y resaltar en cualquier ocasión.',
    combineBody:
      'El body punto azul se puede combinar de múltiples maneras para crear diferentes looks. Para un outfit casual, puedes usarlo con unos jeans o una falda vaquera y zapatillas blancas. Si deseas un estilo más formal, puedes combinarlo con una falda lápiz o unos pantalones de vestir y tacones. También puedes añadir accesorios dorados o plateados para darle un toque de glamour. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener el body punto azul en perfecto estado, sigue estas recomendaciones de limpieza y cuidado. Lávalo a mano o en ciclo suave en la lavadora con agua fría y detergente suave. Evita usar blanqueador y secadora, ya que pueden dañar el material. En su lugar, déjalo secar al aire libre o cuélgalo en una percha. Si es necesario plancharlo, utiliza baja temperatura y colócalo del revés. Siguiendo estos consejos, podrás disfrutar de tu body punto azul por mucho tiempo.',
  },
  'body-seda-gris': {
    prompt: 'body seda gris',
    opinionBody:
      'El body de seda gris es muy valorado por su elegancia y sofisticación. Los usuarios destacan su suave y delicado tejido de seda, que proporciona una sensación de lujo y comodidad. Además, el color gris es versátil y fácil de combinar, lo que lo convierte en una opción popular para diferentes ocasiones. Los clientes también aprecian la excelente calidad y durabilidad del material, así como su ajuste ceñido y favorecedor. En resumen, el body de seda gris ha recibido críticas muy positivas por su estilo, comodidad y versatilidad.',
    combineBody:
      'El body de seda gris es una prenda versátil que se puede combinar de muchas formas diferentes. Para un look elegante y formal, puedes combinarlo con una falda lápiz negra y tacones altos. Para un estilo más casual pero chic, pruébalo con unos vaqueros ajustados y botines. También puedes llevarlo debajo de un traje de chaqueta para un toque de sofisticación adicional. Si quieres añadir un toque de color, puedes combinarlo con accesorios en tonos vivos como rojo o rosa. En definitiva, las opciones de combinación con el body de seda gris son infinitas, lo que lo convierte en una prenda versátil y elegante.',
    cleanBody:
      'La seda es un material delicado que requiere cuidados especiales para mantener su belleza y durabilidad. Para limpiar el body de seda gris, se recomienda lavarlo a mano con agua fría y un detergente suave. Evita retorcer o frotar en exceso la prenda para evitar dañar las fibras. Después del lavado, enjuágalo con agua fría y cuélgalo para que se seque al aire libre, evitando la exposición directa al sol. Si es necesario plancharlo, utiliza una temperatura baja y coloca un paño delgado entre la plancha y el body para proteger el tejido. Siguiendo estos consejos de limpieza y mantenimiento, podrás disfrutar de tu body de seda gris en perfectas condiciones durante mucho tiempo',
  },
  'body-sintetica-celeste': {
    prompt: 'body sintetica celeste',
    opinionBody:
      'El body sintética celeste es una prenda muy versátil y moderna que ha recibido excelentes comentarios por parte de quienes lo han adquirido. Su material sintético permite que tenga un ajuste perfecto al cuerpo, resaltando las curvas y brindando comodidad al mismo tiempo. Además, su color celeste le da un toque de frescura y feminidad, convirtiéndolo en una opción ideal para ocasiones especiales o para lucir un look sensual y atractivo. Los usuarios destacan su calidad y durabilidad, así como la gran variedad de estilos y diseños disponibles en este color.',
    combineBody:
      'El body sintética celeste es una prenda muy versátil que se puede combinar de diferentes maneras para crear looks increíbles. Para un estilo casual y relajado, puedes combinarlo con unos jeans ajustados y unas zapatillas blancas. Si deseas un look más elegante y sofisticado, puedes combinarlo con una falda midi o pantalones de vestir y unos tacones altos. Además, puedes agregar accesorios como aretes o pulseras en tonos plateados o dorados para resaltar el color celeste y añadir un toque de brillo a tu outfit. La clave está en jugar con las texturas y colores para obtener un resultado armonioso y moderno.',
    cleanBody:
      'Para mantener el body sintética celeste en buen estado es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, es necesario leer las instrucciones de lavado que vienen especificadas en la etiqueta de la prenda, ya que pueden variar dependiendo del tipo de material sintético utilizado. En general, se recomienda lavar a mano con agua fría y detergente suave, evitando frotar o retorcer la prenda para no dañarla. Después del lavado, es importante secar al aire libre, evitando la exposición directa al sol para que no pierda su color. Además, se recomienda guardar en un lugar fresco y seco para evitar deformaciones o arrugas en el body sintética celeste.',
  },
  'body-tafetan-turquesa': {
    prompt: 'body tafetan turquesa',
    opinionBody:
      'Este body de tafetán turquesa ha recibido excelentes opiniones por parte de nuestros clientes. Su diseño ajustado realza las curvas de forma elegante y suave, mientras que el material de tafetán le otorga un aspecto lujoso. El color turquesa vibrante hace que este body sea perfecto para ocasiones especiales y fiestas. Además, su tejido elástico brinda comodidad durante todo el día. En resumen, los usuarios están encantados con este body de tafetán turquesa por su atractivo diseño, calidad y versatilidad.',
    combineBody:
      'El body de tafetán turquesa es increíblemente versátil y puede combinarse de varias formas para crear looks fabulosos. Para un estilo elegante y sofisticado, combínalo con una falda lápiz negra y tacones altos. Si prefieres un enfoque más casual pero chic, úsalo con unos jeans de cintura alta y zapatillas blancas. Para una ocasión más formal, combínalo con pantalones de vestir negros y una chaqueta blazer a juego. Este body también se puede usar debajo de un vestido sin espalda para agregar un toque de glamour. Las posibilidades son infinitas con este body de tafetán turquesa.',
    cleanBody:
      'Para mantener el body de tafetán turquesa en perfectas condiciones, te recomendamos seguir estos consejos de limpieza y cuidado. Lávalo a mano con agua fría y un detergente suave. Evita el uso de lejía y plancha para evitar daños en el material y el color. Para secarlo, colócalo en una superficie plana y déjalo secar al aire libre, evitando la exposición directa al sol. Además, guárdalo en un lugar fresco y seco para evitar la deformación y el deterioro. Siguiendo estos consejos simples, podrás disfrutar del body de tafetán turquesa durante mucho tiempo y mantenerlo siempre impecable.',
  },
  'body-vaquera-azul': {
    prompt: 'body vaquera azul',
    opinionBody:
      'La body vaquera azul es una prenda versátil y cómoda que ha ganado popularidad en los últimos años. Los usuarios destacan su ajuste perfecto y la calidad del material vaquero utilizado. Además, el color azul añade un toque moderno y juvenil a cualquier outfit. Sin duda, es una opción favorecedora y fácil de combinar.',
    combineBody:
      'La body vaquera azul es una prenda que se puede adaptar a diferentes estilos y ocasiones. Para un look casual, puedes combinarla con unos jeans negros y zapatillas blancas. Si buscas un outfit más elegante, puedes optar por combinarla con una falda midi y unos tacones de color neutro. También puedes añadir accesorios como un cinturón de cuero y una chaqueta de punto para completar el look.',
    cleanBody:
      'Para mantener tu body vaquera azul en las mejores condiciones, es importante seguir algunas recomendaciones de cuidado. Lo ideal es lavarla del revés y en agua fría para evitar que el color se desgaste. Además, es recomendable utilizar detergentes suaves y evitar el uso de blanqueadores. Para mantener la forma y evitar que se deforme, es recomendable secarla al aire libre o a baja temperatura en la secadora. Siguiendo estos cuidados, podrás disfrutar de tu body vaquera azul durante mucho tiempo.',
  },
  'botas-cuero-blanco': {
    prompt: 'botas cuero blanco',
    opinionBody:
      'Las botas de cuero blanco son una elección audaz y elegante para cualquier outfit. Muchos usuarios coinciden en que son un calzado llamativo y sofisticado que aporta personalidad a cualquier look. Algunos destacan la durabilidad y la resistencia del cuero, que garantiza que estas botas serán un acompañante fiel durante mucho tiempo. Otros resaltan su versatilidad, ya que se pueden combinar tanto con outfits casuales como formales. Sin embargo, también hay opiniones en cuanto a su limpieza, ya que el color blanco puede ser más propenso a ensuciarse. En general, las opiniones sobre las botas de cuero blanco son muy positivas, destacando su estilo y calidad.',
    combineBody:
      'Las botas de cuero blanco se pueden combinar de muchas formas para crear outfits únicos y llamativos. Para un look casual, puedes combinarlas con unos jeans ajustados y una camiseta blanca. También puedes optar por una falda o vestido estampado para darle un toque más femenino y moderno. Si buscas un look más elegante, puedes usarlas con una falda midi o pantalones de vestir en tonos neutros como el gris o el beige. Para un outfit más atrevido, puedes combinarlas con prendas de colores vibrantes como el rojo o el azul eléctrico. En definitiva, las botas de cuero blanco son muy versátiles y se pueden combinar de muchas formas para crear looks originales y a la moda.',
    cleanBody:
      'Mantener tus botas de cuero blanco en buen estado es importante para garantizar su durabilidad y belleza a lo largo del tiempo. Para su limpieza, es recomendable utilizar un cepillo suave y agua mezclada con un poco de jabón neutro. Después de limpiarlas, asegúrate de secarlas bien para evitar la aparición de manchas de agua. Es importante evitar el contacto con sustancias que puedan manchar el cuero, como aceites o productos químicos agresivos. Si tus botas de cuero blanco se ensucian con alguna sustancia difícil de quitar, es acon',
  },
  'botas-cuero-rosa': {
    prompt: 'botas cuero rosa',
    opinionBody:
      'Las botas de cuero rosa son una elección audaz y moderna para quienes desean destacar en su estilo. Muchos usuarios han elogiado su aspecto llamativo y su capacidad para añadir un toque de color a cualquier atuendo. La mayoría también destaca la alta calidad del cuero utilizado en su fabricación, lo que les brinda durabilidad y resistencia. Algunos usuarios incluso han comentado lo cómodas que son, lo que las convierte en una opción perfecta para aquellos que buscan estilo y comodidad en un solo producto.',
    combineBody:
      'Las botas de cuero rosa son una excelente opción para agregar un toque de feminidad y originalidad a cualquier conjunto. Pueden combinarse de diferentes maneras, dependiendo de la ocasión y el estilo personal. Para un look casual, se pueden combinar con unos jeans ajustados y una chaqueta de cuero negra para lograr un contraste moderno. También se ven geniales con faldas o vestidos cortos, añadiendo un toque juvenil y divertido. Si se busca un look más elegante, se pueden combinar con pantalones de tela y una blusa en tonos neutros para crear un equilibrio sutil y sofisticado.',
    cleanBody:
      'Para mantener las botas de cuero rosa en óptimas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, se recomienda utilizar un cepillo suave para eliminar cualquier suciedad o polvo acumulado en la superficie. Si hay manchas difíciles de quitar, se pueden utilizar productos de limpieza específicos para el cuero. Es importante aplicarlos de manera suave y, después, limpiar el cuero con un paño limpio y seco. Para mantener el color vibrante de las botas, se puede aplicar un acondicionador de cuero específico cada cierto tiempo. Además, guardar las botas en un lugar fresco y seco ayudará a preservar su aspecto y prolongar su vida útil.',
  },
  'botas-denim-marron': {
    prompt: 'botas denim marron',
    opinionBody:
      'Las botas denim marrón son una opción muy popular entre los amantes de la moda. Su material denim las hace perfectas para combinar con todo tipo de outfits, desde jeans hasta faldas y vestidos. Además, su color marrón ofrece un toque de elegancia y versatilidad. Muchos usuarios destacan su comodidad y durabilidad, asegurando que son ideales para usar durante largas jornadas sin causar molestias. Sin duda, las botas denim marrón son una elección acertada para añadir un toque trendy a cualquier look.',
    combineBody:
      'Las botas denim marrón se convierten en un aliado perfecto a la hora de combinar outfits casuales y chic. Puedes lucirlas con un par de jeans ajustados y una camiseta gráfica para un estilo desenfadado pero moderno. Si quieres un look más femenino, prueba combinándolas con una falda floral y una blusa suelta. También puedes añadir un abrigo largo y un sombrero para un toque bohemio. En definitiva, las botas denim marrón son tan versátiles que podrás incorporarlas a tu estilo diario fácilmente.',
    cleanBody:
      'Para asegurar una mayor durabilidad de tus botas denim marrón, es importante tener en cuenta algunos consejos de limpieza y mantenimiento. Si deseas eliminar manchas o suciedad, utiliza un cepillo suave y un poco de agua con detergente suave. Por otro lado, es recomendable evitar el contacto con líquidos que puedan dañar el material denim. Para mantener el color y la apariencia de tus botas, puedes aplicar un protector especial para prendas de denim. Asimismo, guarda tus botas en un lugar fresco y seco cuando no las estés usando. Siguiendo estos simples cuidados, podrás disfrutar de tus botas denim marrón por mucho tiempo.',
  },
  'botas-gasa-verde': {
    prompt: 'botas gasa verde',
    opinionBody:
      'Las botas de gasa verde son altamente valoradas por su combinación única de estilo y comodidad. Los usuarios destacan la suavidad del material y cómo se adapta al pie, brindando una sensación de ligereza y libertad al caminar. Además, el color verde añade un toque de frescura y originalidad a cualquier outfit. Muchos usuarios también resaltan la durabilidad de estas botas, asegurando que son una inversión que vale la pena. En general, las opiniones sobre las botas de gasa verde son muy positivas, siendo consideradas como una elección elegante y versátil.',
    combineBody:
      'Las botas de gasa verde son una excelente opción para darle vida a tu vestuario. Puedes combinarlas con jeans ajustados y una chaqueta de cuero para lograr un look casual y chic. Si quieres algo más femenino, puedes optar por una falda midi de colores neutros o estampados y una blusa blanca. Para una ocasión más formal, puedes lucir estas botas con un vestido negro o un traje sastre en tonos grisáceos. No tengas miedo de experimentar y jugar con diferentes colores y texturas, ya que las botas de gasa verde tienen la versatilidad de adaptarse a diversos estilos y prendas.',
    cleanBody:
      'Para mantener tus botas de gasa verde en óptimas condiciones, es importante seguir ciertos cuidados. Siempre revisa las instrucciones específicas del fabricante antes de proceder a su limpieza. En general, puedes comenzar eliminando cualquier suciedad o polvo con un cepillo suave. Luego, utiliza un paño ligeramente húmedo con agua y un detergente suave para limpiar suavemente la superficie de las botas. Evita sumergirlas en agua y no utilices productos abrasivos o blanqueadores, ya que podrían dañar el material. Una vez limpias, déjalas secar al aire libre, lejos de la luz directa del sol. Para mantener el color de las botas, también puedes aplicar un spray protector específico para',
  },
  'botas-jersey-amarillo': {
    prompt: 'botas jersey amarillo',
    opinionBody:
      'Las botas de jersey amarillo son sin duda una elección audaz y llamativa para cualquier ocasión. Los usuarios que las han adquirido coinciden en que son extremadamente cómodas y de alta calidad. El material de jersey es suave y flexible, lo que proporciona una sensación de calidez y comodidad a tus pies. El color amarillo añade un toque de luminosidad y alegría a cualquier outfit. Además, su diseño moderno y versátil las convierte en la opción perfecta tanto para outfits casuales como para ocasiones más formales. En definitiva, las botas de jersey amarillo son un accesorio imprescindible para aquellos que desean destacar con estilo.',
    combineBody:
      'Las botas de jersey amarillo son extremadamente versátiles y pueden combinarse de varias formas para crear diferentes looks. Para un estilo casual y fresco, puedes combinarlas con unos jeans ajustados y una camiseta básica de colores neutros. Si buscas un look más elegante, puedes optar por combinarlas con una falda midi y una blusa blanca. Para añadir un toque de contraste, puedes optar por accesorios en tonos neutros o en colores complementarios, como el gris o el negro. En definitiva, las posibilidades son infinitas y las botas de jersey amarillo te permiten crear outfits únicos y llenos de estilo.',
    cleanBody:
      'Para mantener tus botas de jersey amarillo en óptimas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, es recomendable cepillarlas suavemente con un cepillo de cerdas suaves para eliminar cualquier suciedad o polvo acumulado. Evita usar productos químicos agresivos o sumergirlas en agua, ya que esto puede dañar el material de jersey. Si se producen manchas, puedes utilizar un paño húmedo con agua tibia y jabón suave para limpiarlas suavemente. Recuerda dejarlas secar al aire libre y evitar exponerlas directamente a la luz solar intensa, ya que esto puede desvanecer el color.',
  },
  'botas-organdi-bronce': {
    prompt: 'botas organdi bronce',
    opinionBody:
      'Las botas organdi bronce son un calzado que destaca por su estilo único y original. Su color bronce le agrega un toque de elegancia y sofisticación a cualquier outfit. Muchos usuarios han elogiado la calidad de los materiales utilizados en su fabricación, lo que las hace duraderas y resistentes. Además, su diseño versátil permite combinarlas fácilmente con diferentes prendas, tanto para ocasiones casuales como formales. Sin duda, las botas organdi bronce son una excelente opción para quienes buscan agregar un toque de estilo y glamour a sus looks.',
    combineBody:
      'Las botas organdi bronce son extremadamente versátiles y pueden combinarse de diversas formas para crear outfits únicos y elegantes. Para un look casual pero sofisticado, puedes combinarlas con unos jeans ajustados y una blusa de seda en tonos neutros. Si buscas un estilo más vanguardista, puedes optar por una falda midi de cuero y una camiseta gráfica. Para ocasiones más formales, puedes llevarlas con un vestido negro ceñido al cuerpo y complementarlo con accesorios dorados. Con las botas organdi bronce, las posibilidades son infinitas y siempre estarás a la moda.',
    cleanBody:
      'Para mantener las botas organdi bronce en buen estado, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable cepillarlas suavemente después de cada uso para eliminar el polvo y la suciedad. Si se han manchado, puedes utilizar un paño húmedo con agua tibia y jabón suave para limpiar las áreas afectadas. Evita sumergirlas en agua o utilizar productos químicos agresivos, ya que podrían dañar el material. Para mantener el brillo del color bronce, puedes aplicar un producto específico para calzado de cuero en forma de aerosol o crema. Recuerda guardarlas en un lugar fresco y seco y alejarlas de la luz solar directa. Siguiendo estos sencillos consejos, podrás disfrutar de tus botas organdi bronce por mucho',
  },
  'botas-rayon-oro': {
    prompt: 'botas rayon oro',
    opinionBody:
      'Las botas Rayon oro han recibido excelentes críticas por su estilo moderno y sofisticado. Los usuarios destacan la calidad del material utilizado, que brinda durabilidad y confort. Además, el color oro le da un toque elegante y llamativo. Sin duda, estas botas son perfectas para lucir a la moda y recibir elogios.',
    combineBody:
      'Las botas Rayon oro son extremadamente versátiles y se pueden combinar con diferentes estilos y prendas. Para un look casual, puedes usarlas con jeans ajustados y una camiseta blanca. Si quieres un look más elegante, combínalas con una falda midi o un vestido oscuro. No importa la ocasión, estas botas doradas agregarán un toque de glamour a cualquier outfit.',
    cleanBody:
      'Para mantener las botas Rayon oro en perfectas condiciones, es importante seguir algunos pasos de limpieza y cuidado. Primero, utiliza un paño húmedo para limpiar cualquier suciedad o mancha superficial. Evita el uso de productos químicos agresivos. Luego, asegúrate de guardarlas en un lugar fresco y seco, lejos de la luz solar directa. Siempre es recomendable utilizar un protector para el material y frotar suavemente con un cepillo de cerdas suaves para mantener el brillo de las botas en oro. Siguiendo estos consejos, tus botas Rayon oro se mantendrán como nuevas por mucho tiempo.',
  },
  'botas-sintetica-claro': {
    prompt: 'botas sintetica claro',
    opinionBody:
      'Las botas sintéticas claras son una opción popular entre quienes buscan un calzado versátil y de moda. Los usuarios han elogiado la comodidad y ligereza de estas botas, perfectas para largas caminatas o para estar de pie durante muchas horas. Además, el material sintético ofrece resistencia al agua y a manchas, lo que las convierte en una excelente opción para los días lluviosos. La combinación de color claro permite que se puedan adaptar fácilmente a diferentes estilos y colores de ropa, lo que las convierte en una opción versátil para cualquier ocasión.',
    combineBody:
      'Las botas sintéticas claras son un complemento perfecto para cualquier atuendo casual o elegante. Puedes combinarlas con jeans y una camiseta para un look más relajado, o con una falda o vestido para una apariencia más femenina. Además, el color claro de estas botas se adapta fácilmente a diferentes colores y estampados, lo que te brinda la posibilidad de experimentar con diferentes combinaciones. También puedes agregar accesorios como un bolso de mano o una bufanda en tonos neutros para complementar aún más tu estilo.',
    cleanBody:
      'Para mantener tus botas sintéticas claras siempre en buen estado, es importante seguir ciertos pasos de limpieza y mantenimiento. En primer lugar, utiliza un paño húmedo para eliminar cualquier suciedad o mancha superficial. Evita el uso de productos químicos agresivos, ya que pueden dañar el material sintético. Si las botas tienen manchas difíciles de eliminar, puedes usar un detergente suave mezclado con agua y frotar suavemente la mancha con un cepillo de cerdas suaves. Por último, asegúrate de mantener tus botas en un lugar fresco y seco cuando no las estés usando, para evitar el deterioro del material y prolongar su vida útil.',
  },
  'botas-tafetan-azul': {
    prompt: 'botas tafetan azul',
    opinionBody:
      'Las botas de tafetán azul son una elección muy popular entre aquellos que buscan añadir un toque elegante y sofisticado a sus conjuntos. Muchos usuarios destacan la suavidad y la comodidad del material de tafetán, que proporciona un ajuste perfecto y una sensación de lujo. Además, el color azul añade un toque de originalidad y estilo a cualquier atuendo. En general, las opiniones son muy positivas y recomiendan estas botas como una excelente elección para destacar en cualquier ocasión.',
    combineBody:
      'Las botas de tafetán azul ofrecen una gran versatilidad a la hora de combinarlas con diferentes prendas y estilos. Para un look elegante y formal, puedes llevarlas con un vestido o una falda de tonos neutros, como el blanco o el negro. Si prefieres un estilo más casual, puedes combinarlas con unos vaqueros ajustados y una camiseta básica. Incluso puedes jugar con contrastes y añadir un toque audaz combinándolas con prendas de colores vivos. Las posibilidades son infinitas, y podrás crear looks únicos y atractivos con estas botas en cualquier ocasión.',
    cleanBody:
      'Para mantener tus botas de tafetán azul en perfectas condiciones, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable utilizar un cepillo suave o un paño húmedo para eliminar el polvo y la suciedad superficial. Si las botas presentan manchas más difíciles de eliminar, se pueden utilizar productos de limpieza específicos para el tafetán o incluso llevarlas a una tintorería especializada. También es importante guardar las botas en un lugar fresco y seco, lejos de la luz directa del sol, para evitar que el color se desvanezca. Siguiendo estos sencillos cuidados, tus botas de tafetán azul lucirán impecables durante mucho tiempo.',
  },
  'botas-tafetan-negro': {
    prompt: 'botas tafetan negro',
    opinionBody:
      'Las botas de tafetán negro son una opción elegante y sofisticada para completar cualquier look. Los usuarios que han comprado este producto destacan su calidad y durabilidad. Además, mencionan que son cómodas de usar durante largos períodos de tiempo gracias a su diseño y materiales de alta calidad. Sin duda, las botas de tafetán negro son una elección acertada para cualquier ocasión.',
    combineBody:
      'Las botas de tafetán negro son extremadamente versátiles y pueden combinarse con una amplia variedad de prendas y estilos. Para un look casual, puedes combinarlas con unos jeans ajustados y una chaqueta de cuero. Si buscas un outfit más elegante, opta por una falda midi o un vestido negro. También puedes añadir accesorios en tonos dorados para darle un toque de sofisticación adicional. Las posibilidades son infinitas con estas increíbles botas.',
    cleanBody:
      'Para mantener tus botas de tafetán negro en óptimas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. Primero, asegúrate de limpiar cualquier suciedad o polvo de la superficie de las botas con un paño suave y seco. Si hay manchas más difíciles de limpiar, puedes usar un cepillo suave o un paño ligeramente húmedo con agua y jabón suave.',
  },
  'bufanda-artificial-borgona': {
    prompt: 'bufanda artificial borgona',
    opinionBody:
      'La bufanda artificial borgoña ha recibido excelentes opiniones por parte de nuestros clientes. El material artificial utilizado garantiza una textura suave y cómoda al tacto, además de una gran durabilidad. El color borgoña es muy elegante y versátil, lo que la convierte en un accesorio perfecto para añadir un toque de estilo a cualquier outfit. Nuestros clientes han destacado la calidad de esta bufanda y la han recomendado como una opción imprescindible para los meses más fríos.',
    combineBody:
      'La bufanda artificial borgoña se puede combinar de muchas maneras para crear looks increíbles. Esta tonalidad cálida y sofisticada se adapta perfectamente a diferentes estilos y colores de ropa. Puedes combinarla con un abrigo negro para crear un contraste elegante y clásico, o con tonos neutros como gris o beige para obtener un look más suave y sofisticado. Si deseas añadir un toque de color a tu outfit, puedes combinarla con una chaqueta verde militar o un suéter mostaza. La versatilidad de esta bufanda te permitirá crear looks únicos y llenos de estilo.',
    cleanBody:
      'La bufanda artificial borgoña es muy fácil de limpiar y mantener en buen estado. Se recomienda lavarla a mano con agua fría y un detergente suave. Evita retorcerla o frotarla con fuerza para mantener su forma original. Después de lavarla, puedes dejarla secar al aire libre o utilizar una toalla para retirar el exceso de agua. Para mantener su suavidad y evitar que se formen arrugas, puedes plancharla a baja temperatura con un paño húmedo encima. Siguiendo estos simples pasos, podrás disfrutar de tu bufanda artificial borgoña durante mucho tiempo y mantenerla siempre en perfectas condiciones.',
  },
  'bufanda-cuero-azul': {
    prompt: 'bufanda cuero azul',
    opinionBody:
      'La bufanda de cuero azul ha recibido excelentes opiniones por parte de los usuarios. Su material de cuero le brinda un acabado elegante y duradero, mientras que el color azul añade un toque de estilo único. Los usuarios destacan su suavidad y calidez, ideal para mantenerse abrigado en épocas de frío. Además, su diseño versátil permite combinarla fácilmente con diferentes prendas.',
    combineBody:
      'La bufanda de cuero azul es una pieza versátil que puede ser combinada de diversas formas. Puedes lucirla con un abrigo negro o gris para lograr un contraste interesante, o incluso agregar un toque de color combinándola con una chaqueta roja o verde. También puedes usarla para complementar un look más informal, luciéndola con una chaqueta de mezclilla y jeans. ¡Las opciones son infinitas!',
    cleanBody:
      'Para mantener la bufanda de cuero azul en excelente estado, es importante cuidarla adecuadamente. Para eliminar el polvo y la suciedad, puedes utilizar un paño suave o un cepillo de cerdas suaves para limpiarla. En caso de manchas, es recomendable aplicar un limpiador de cuero suave y seguir las instrucciones del fabricante. Evita exponerla directamente a la luz del sol o a la humedad excesiva, ya que esto podría dañar el cuero. Al guardarla, puedes enrollarla suavemente y guardarla en un lugar fresco y seco.',
  },
  'bufanda-franela-aguamarina': {
    prompt: 'bufanda franela aguamarina',
    opinionBody:
      'La bufanda de franela en color aguamarina ha recibido excelentes opiniones por parte de los usuarios. Muchos destacan su suavidad y calidez, ideal para los días más fríos. Además, el color aguamarina le da un toque único y vibrante, siendo perfecto para añadir un toque de color a cualquier outfit. Sin duda, una opción popular entre aquellos que buscan comodidad y estilo.',
    combineBody:
      'La bufanda de franela en color aguamarina es extremadamente versátil a la hora de combinarla con otras prendas. Puede ser el complemento perfecto para un look casual, combinándola con jeans y una chaqueta de cuero. También puede añadir un toque de color a un outfit más formal, usándola con vestidos o trajes en tonos neutros. En definitiva, esta bufanda se adapta fácilmente a cualquier estilo y ocasión.',
    cleanBody:
      'Para mantener la bufanda de franela en color aguamarina en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Se recomienda lavarla a mano con agua fría y un detergente suave, evitando el uso de lejía. Después, se puede dejar secar al aire o utilizar una secadora a baja temperatura. Es importante no retorcerla para evitar que se deforme. Además, se recomienda guardarla en un lugar seco y protegido del sol cuando no se esté usando. Siguiendo estos consejos, la bufanda de franela aguamarina lucirá como nueva durante mucho tiempo.',
  },
  'bufanda-jersey-oro': {
    prompt: 'bufanda jersey oro',
    opinionBody:
      'La bufanda jersey oro ha recibido excelentes comentarios por parte de los usuarios. El material de jersey le brinda una suavidad y calidez inigualables, perfectas para los días más fríos. Además, su color oro añade un toque de elegancia y sofisticación a cualquier conjunto. Los usuarios destacan su comodidad, durabilidad y lo versátil que es, ya que puede combinarse con diferentes estilos y colores de prendas.',
    combineBody:
      'La bufanda jersey oro es extremadamente versátil y puede combinarse de diversas maneras. Para un look elegante y sofisticado, puedes combinarla con un abrigo negro de piel sintética, unos pantalones de cuero y unos botines negros. Si buscas un estilo más bohemio, puedes utilizarla junto con una chaqueta de punto oversize, unos jeans desgastados y unas botas marrones. Incluso puedes utilizarla como punto focal en un conjunto de colores neutros, como una chaqueta y unos pantalones grises. Las posibilidades son infinitas.',
    cleanBody:
      'Para mantener tu bufanda jersey oro en perfectas condiciones, es recomendable lavarla a mano o en ciclo delicado en la lavadora, utilizando agua fría. Si decides lavarla a mano, utiliza un detergente suave y evita retorcerla para no dañar las fibras del material. Después de lavarla, déjala secar al aire libre, evitando la exposición directa al sol. Para mantener su forma, es recomendable doblarla cuidadosamente y almacenarla en un lugar seco y limpio. Si la bufanda tiene arrugas, puedes plancharla a baja temperatura, utilizando un paño delgado o una toalla sobre ella para protegerla. Sigue estos consejos y tu bufanda jersey oro lucirá siempre perfecta.',
  },
  'bufanda-seda-rosa': {
    prompt: 'bufanda seda rosa',
    opinionBody:
      'Los usuarios que han comprado y usado la bufanda de seda rosa han quedado encantados con su calidad y su suavidad. Destacan que es un complemento elegante y sofisticado que añade un toque de glamour a cualquier outfit. Además, comentan que el color rosa es muy favorecedor y versátil, ya que se puede combinar con diferentes prendas y estilos. En resumen, las opiniones sobre la bufanda de seda rosa son muy positivas, destacando su calidad, suavidad y versatilidad.',
    combineBody:
      'La bufanda de seda rosa es un accesorio muy versátil que se puede combinar de diferentes formas. Para un look casual y relajado, puedes lucirla con jeans y una chaqueta de cuero negra, añadiendo un toque femenino y sofisticado al conjunto. También puedes combinarla con un vestido negro y unos tacones para un look más elegante y formal. Si prefieres un estilo bohemio, puedes llevarla con una blusa blanca y una falda de flores. En resumen, la bufanda de seda rosa se puede combinar con diferentes prendas y estilos, adaptándose a tus preferencias.',
    cleanBody:
      'La seda es un material delicado, por lo que es importante tener cuidado al limpiar y mantener la bufanda de seda rosa. Se recomienda lavarla a mano en agua fría con un detergente suave para prendas delicadas. Evita frotarla con fuerza y enjuágala suavemente. Después, sécala extendida sobre una toalla y a la sombra, evitando la exposición directa al sol. Para evitar que se arrugue, puedes plancharla a baja temperatura o utilizar un vaporizador de prendas. Recuerda guardarla en un lugar fresco y seco, lejos de la luz solar directa. Siguiendo estos cuidados, podrás disfrutar de tu bufanda de seda rosa por mucho tiempo.',
  },
  'bufanda-tela-turquesa': {
    prompt: 'bufanda tela turquesa',
    opinionBody:
      'Los usuarios han expresado su satisfacción con la bufanda de tela turquesa, destacando su suavidad y la calidad del material. Además, mencionan que el color turquesa es vibrante y llamativo, agregando un toque de estilo a cualquier atuendo. También han valorado positivamente su tamaño, ya que es lo suficientemente grande como para utilizarla de diferentes formas. En general, las opiniones sobre la bufanda de tela turquesa son muy favorables, recomendándola como una excelente opción para añadir un toque de color a cualquier look.',
    combineBody:
      'La bufanda de tela turquesa es extremadamente versátil y puede combinarse con una amplia variedad de prendas y colores. Puede ser el complemento perfecto para un outfit casual, combinándola con jeans y una camiseta blanca. También puede ser utilizada para agregar un toque de color a un conjunto más formal, como un vestido negro. Además, este tono de turquesa se ve especialmente bien combinado con colores neutros como el blanco, el gris y el beige. En resumen, la bufanda de tela turquesa puede ser el toque final que necesitas para completar cualquier estilo.',
    cleanBody:
      'Para mantener la bufanda de tela turquesa en óptimas condiciones, es recomendable seguir estas recomendaciones de limpieza y cuidado. En primer lugar, es importante leer las instrucciones del fabricante y seguir sus indicaciones específicas. En la mayoría de los casos, la bufanda puede lavarse a mano o en ciclo suave con agua fría, utilizando un detergente suave. Es importante evitar el uso de blanqueadores y suavizantes para preservar el color y la textura de la bufanda. A la hora de secarla, se recomienda escurrirla suavemente para eliminar el exceso de agua y luego colocarla sobre una superficie plana para secar al aire libre. Finalmente, es aconsejable guardar la bufanda de tela turquesa en un lugar seco y alejado de la luz solar directa, evitando el contacto con objetos afilados o ásperos que',
  },
  'camisa-denim-blanco': {
    prompt: 'camisa denim blanco',
    opinionBody:
      'La camisa denim blanco es una prenda versátil que destaca por su estilo moderno y atemporal. Los usuarios que han adquirido esta camisa destacan la calidad del material, que es resistente y duradero. Además, mencionan que el color blanco le da un toque fresco y elegante al conjunto, perfecto para ocasiones casuales o formales. En cuanto al ajuste, las opiniones son positivas, ya que la camisa se adapta bien al cuerpo sin perder comodidad. En resumen, quienes la han probado recomiendan la camisa denim blanco por su calidad, estilo y versatilidad.',
    combineBody:
      'La camisa denim blanco es una prenda que se puede combinar de diversas maneras para crear diferentes looks. Para un outfit casual y relajado, puedes combinarla con unos jeans azules y zapatillas blancas. Si buscas un look más sofisticado, puedes usar la camisa con unos pantalones negros y zapatos de vestir. También puedes optar por combinarla con una falda de cuero y botines para un estilo más rockero. Para un look veraniego, puedes usar la camisa abierta sobre un vestido o bikini. La versatilidad de la camisa denim blanco te permite crear múltiples combinaciones de moda.',
    cleanBody:
      'Para mantener la camisa denim blanco en óptimas condiciones, es importante seguir las indicaciones de cuidado del fabricante. Por lo general, se recomienda lavarla a máquina con agua fría y utilizar un detergente suave. Evita el uso de blanqueadores, ya que pueden dañar el color blanco. Para el secado, se aconseja colgarla en una percha y evitar la exposición directa al sol. En caso de manchas, es recomendable tratarlas de inmediato con un quitamanchas adecuado para prendas blancas. Además, es recomendable planchar la camisa a baja temperatura, preferiblemente por el reverso, para evitar dañar el material. Siguiendo estas recomendaciones, podrás disfrutar de tu camisa denim blanco por mucho tiempo.',
  },
  'camisa-denim-borgona': {
    prompt: 'camisa denim borgona',
    opinionBody:
      'Esta camisa denim borgoña ha recibido excelentes opiniones por parte de los usuarios. El material de denim le brinda una resistencia y durabilidad excepcionales, mientras que el color borgoña le agrega un toque de estilo y elegancia. Los usuarios destacan su ajuste cómodo y versátil, que se adapta tanto a ocasiones informales como a looks más formales. Además, el material de denim le confiere una textura suave al tacto, lo que la hace realmente agradable de llevar. En resumen, la camisa denim borgoña es una elección acertada para aquellos que buscan un estilo moderno con una prenda de alta calidad.',
    combineBody:
      'La camisa denim borgoña es extremadamente versátil y puede ser combinada de diversas formas para crear looks elegantes y casuales. Para un outfit casual, puedes combinarla con unos jeans oscuros y zapatillas blancas, para lograr un estilo relajado pero moderno. Si prefieres un look más formal, puedes llevarla con un pantalón de vestir en color negro, zapatos de cuero y un blazer. Además, puedes agregar accesorios como una corbata estampada o un pañuelo de bolsillo para darle un toque extra de estilo. La camisa denim borgoña es una prenda versátil y fácil de combinar, permitiéndote crear looks únicos y atractivos para cualquier ocasión.',
    cleanBody:
      'Para mantener tu camisa denim borgoña en perfectas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. Para evitar que el color se desvanezca, se recomienda lavarla del revés y en agua fría. Utiliza un detergente suave y evita el uso de blanqueadores agresivos. Al momento de secarla, extiéndela en una superficie plana para evitar que se deforme. Asimismo, evita la exposición directa al sol durante largos periodos de tiempo para evitar el deterioro del color. Si es necesario plancharla, hazlo a temperatura media y también del revés',
  },
  'camisa-denim-plata': {
    prompt: 'camisa denim plata',
    opinionBody:
      'Los usuarios que adquirieron la camisa denim en color plata están encantados con su compra. Destacan la versatilidad de esta prenda, que puede ser utilizada tanto para un look casual durante el día como para uno más elegante por la noche. Además, el material denim proporciona un ajuste cómodo y duradero. Sin duda, la camisa denim plata es una opción popular entre aquellos que buscan un toque de estilo moderno en su guardarropa.',
    combineBody:
      'La camisa denim plata es una prenda versátil que puede combinarse de diferentes maneras para crear looks únicos. Para un look casual y relajado, puedes combinarla con unos jeans oscuros o blancos y zapatillas blancas. Si buscas una opción más formal, opta por combinarla con unos pantalones chinos en color negro o beige y unos zapatos de vestir. También puedes agregar accesorios en tonos plateados para resaltar el color de la camisa. ¡Las posibilidades son infinitas!',
    cleanBody:
      'La camisa denim plata requiere de un cuidado especial para mantener su aspecto impecable. Se recomienda lavarla a mano o a máquina en agua fría con detergente suave. Evita el uso de suavizantes y blanqueadores, ya que pueden dañar el color plata. Para secarla, es preferible colgarla al aire libre o utilizar el programa de secado suave de la secadora. Si es necesario plancharla, hazlo a baja temperatura y por el reverso de la prenda. Siguiendo estos sencillos consejos, podrás disfrutar de tu camisa denim plata por mucho tiempo.',
  },
  'camisa-franela-blanco': {
    prompt: 'camisa franela blanco',
    opinionBody:
      'La camisa de franela blanco es una elección versátil y atemporal para cualquier ocasión. La mayoría de las personas coinciden en que el material de franela es suave, cálido y cómodo de llevar. Además, el color blanco ofrece un aspecto elegante y fresco, perfecto para combinar con diversos estilos y tendencias. Los usuarios también mencionan que esta camisa es muy fácil de combinar con distintas prendas, lo que la convierte en un elemento básico en el armario de cualquier persona.',
    combineBody:
      'La camisa de franela blanco se puede combinar de múltiples formas para crear looks tanto casuales como más formales. Para un look casual, puedes combinarla con unos jeans oscuros y zapatillas blancas. Si quieres darle un toque más fashion, puedes añadir una chaqueta de cuero negra y unos botines. Para un aspecto más formal, puedes combinarla con unos pantalones de vestir y zapatos en tonos neutros. También puedes agregar accesorios como una bufanda o un sombrero para completar el look.',
    cleanBody:
      'Para asegurarte de mantener tu camisa de franela blanco en óptimas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. En primer lugar, es importante leer las instrucciones de lavado del fabricante. En general, se recomienda lavarla a máquina con agua fría y utilizar un detergente suave. Evita el uso de blanqueadores o productos químicos agresivos, ya que podrían dañar el tejido. Además, es preferible secar al aire en lugar de utilizar la secadora, para evitar el encogimiento. Si es necesario, puedes planchar la camisa a baja temperatura. Con estos simples cuidados, tu camisa de franela blanco se mantendrá en perfectas condiciones durante mucho tiempo.',
  },
  'camisa-jacquard-marino': {
    prompt: 'camisa jacquard marino',
    opinionBody:
      'Los usuarios han mostrado una gran satisfacción con la camisa jacquard marino. Comentan que es una prenda de alta calidad, gracias a su material jacquard, que le da un aspecto elegante y sofisticado. Además, destacan su color marino, que brinda versatilidad y permite combinarla fácilmente con otros elementos de vestuario. Los clientes también han elogiado su ajuste cómodo y la durabilidad de la camisa. En general, se considera una excelente opción para hombres que buscan un estilo moderno y refinado.',
    combineBody:
      'La camisa jacquard marino es un básico versátil que se puede combinar de muchas maneras. Para un look más formal, puedes llevarla con un pantalón de vestir negro y zapatos de cuero. Si prefieres un estilo más casual, puedes combinarla con unos jeans y zapatillas blancas. Para agregar un toque de color, puedes optar por accesorios en tonos marrones o algunos detalles en rojo. También puedes experimentar con diferentes tipos de pantalones, como chinos o incluso bermudas, dependiendo de la ocasión. En resumen, las posibilidades son infinitas para crear estilismos únicos con esta camisa.',
    cleanBody:
      'Para mantener la camisa jacquard marino en excelente estado, es importante seguir ciertos cuidados. En cuanto a la limpieza, se recomienda leer las instrucciones de lavado específicas del fabricante, ya que puede variar según el material utilizado. Sin embargo, en general, se aconseja lavarla a máquina con agua fría y utilizar un detergente suave. Evita el uso de blanqueadores y suavizantes. Para secarla, es preferible colgarla en una percha y dejarla secar al aire. Si necesitas plancharla, asegúrate de hacerlo a baja temperatura y por el revés, para evitar dañar el tejido. Siguiendo estos sencillos consejos, podrás disfrutar de tu camisa jacquard marino en buen estado durante mucho tiempo.',
  },
  'camisa-nylon-bronce': {
    prompt: 'camisa nylon bronce',
    opinionBody:
      'Los usuarios que han adquirido esta camisa de nylon bronce han elogiado la comodidad y la durabilidad del material. Además, destacan su versatilidad para ser usada tanto en ocasiones formales como informales, gracias a su color bronce que añade un toque elegante y sofisticado. Sin duda, una elección acertada para lucir a la moda y estar cómodo al mismo tiempo.',
    combineBody:
      'Esta camisa de nylon bronce es muy fácil de combinar, ya que su color neutro se adapta a diferentes estilos y colores. Puedes llevarla con unos pantalones de vestir en tonos oscuros para un look más formal, o combinarla con unos jeans para un estilo casual y relajado. Añade complementos en tonos tierra o dorados para realzar el bronce de la camisa y crear un conjunto armonioso y moderno.',
    cleanBody:
      'Para mantener la camisa de nylon bronce en perfectas condiciones, es recomendable seguir las instrucciones de lavado del fabricante. Generalmente, se aconseja lavarla a mano con agua fría y detergente suave. Evita utilizar blanqueadores y secarla a altas temperaturas, ya que podrían dañar el material y afectar el color. Para eliminar arrugas, puedes utilizar una plancha a temperatura baja. Siguiendo estos consejos, podrás disfrutar de tu camisa de nylon bronce durante mucho tiempo.',
  },
  'camisa-saten-esmeralda': {
    prompt: 'camisa saten esmeralda',
    opinionBody:
      'Los usuarios que han adquirido la camisa de satén esmeralda destacan su elegancia y sofisticación. El material de satén le otorga un brillo y suavidad únicos, mientras que el color esmeralda aporta un toque de distinción. Además, resaltan la calidad de la prenda y la excelente relación calidad-precio. Perfecta para eventos especiales o simplemente para lucir elegante en el día a día.',
    combineBody:
      'La versatilidad de la camisa de satén esmeralda permite una amplia gama de combinaciones. Para un look formal, puedes combinarla con pantalones negros o grises y unos zapatos de tacón del mismo tono. Si buscas un estilo más casual, puedes optar por unos jeans oscuros y unas zapatillas blancas. Además, puedes añadir un blazer negro o una chaqueta de cuero para completar el conjunto. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Es importante seguir correctamente las instrucciones de cuidado de la prenda para mantenerla en perfecto estado. La camisa de satén esmeralda generalmente se recomienda lavar a mano o en ciclo suave en la lavadora, con agua fría y utilizando detergente suave. Evita el uso de blanqueadores y suavizantes de tela, ya que pueden dañar el material. Para secar, es preferible hacerlo al aire libre o utilizar la opción de secado delicado en la secadora. Para planchar, utiliza una temperatura media-baja y hazlo por el revés de la prenda. Recuerda que el satén es un tejido delicado, así que trata de evitar el roce con superficies ásperas y evita exponerlo a productos químicos agresivos. Con los cuidados adecuados, tu camisa de satén esmeralda lucirá impecable por mucho tiempo.',
  },
  'camisa-saten-turquesa': {
    prompt: 'camisa saten turquesa',
    opinionBody:
      'La camisa de satén turquesa es un verdadero hito en el mundo de la moda. Su tejido de satén le da un aspecto elegante y sofisticado, mientras que el color turquesa le agrega un toque de frescura y vitalidad. Los usuarios que han tenido la oportunidad de usar esta camisa han quedado encantados con la suavidad y la sensación lujosa del satén, además de destacar lo favorecedor que es el color turquesa en diferentes tipos de piel. Este estilo de camisa es perfecto para ocasiones especiales y eventos formales, ya que combina a la perfección con pantalones negros, faldas de tonos neutros o incluso jeans oscuros.',
    combineBody:
      'La camisa de satén turquesa es extremadamente versátil y puede combinarse de muchas formas para lograr diferentes looks. Para una apariencia elegante y formal, puedes combinarla con una falda lápiz negra y tacones altos a juego. Para un look más casual y relajado, puedes combinarla con jeans oscuros y zapatillas blancas. Si deseas agregar más color y vida a tu outfit, puedes combinarla con pantalones o faldas en tonos más vibrantes como el rosa, el amarillo o el coral. En resumen, la camisa de satén turquesa es una prenda que puedes usar en muchas ocasiones y con diferentes combinaciones, ¡las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu camisa de satén turquesa en perfectas condiciones, es importante seguir algunas pautas de limpieza y cuidado. Es recomendable lavarla a mano con agua fría y un detergente suave, evitando el uso de lejía y suavizantes. Al momento de secarla, es preferible colgarla en una percha en un lugar fresco y bien ventilado, evitando la exposición directa al sol. Si es necesario plancharla, utiliza la configuración de temperatura más baja y coloca un paño de algodón entre la plancha y la tela para',
  },
  'camisa-seda-azul': {
    prompt: 'camisa seda azul',
    opinionBody:
      'La camisa de seda azul ha recibido excelentes críticas por parte de los usuarios. Su suave y delicado material de seda proporciona una sensación de lujo y comodidad. El color azul vibrante agrega un toque de estilo y elegancia a cualquier atuendo. Los usuarios han elogiado la excelente calidad de esta camisa, así como su durabilidad. Sin duda, es una opción popular entre aquellos que buscan añadir un toque sofisticado a su guardarropa.',
    combineBody:
      'La camisa de seda azul es una prenda muy versátil que se puede combinar de diversas maneras para crear diferentes estilos. Para un look elegante y formal, puedes combinarla con pantalones de vestir negros y tacones altos. Para un estilo más casual pero sofisticado, puedes usarla con jeans oscuros y zapatillas blancas. Si deseas un atuendo más llamativo, puedes combinarla con una falda de color metálico o estampado. La camisa de seda azul es una opción ideal para cualquier ocasión, ya sea una cena elegante o una reunión informal.',
    cleanBody:
      'La camisa de seda azul requiere cuidados especiales debido a su delicado material. Se recomienda lavarla a mano con agua fría y un detergente suave específico para seda. Evita retorcer o frotar la tela para evitar daños. Es importante secarla al aire libre o colgarla en una percha para que se seque naturalmente. Si es necesario, puedes plancharla a baja temperatura, utilizando un paño delgado entre la plancha y la camisa para evitar quemaduras. Para mantener el color y la calidad de la camisa de seda azul, evita la exposición directa al sol y el roce con objetos ásperos. Sigue estos simples consejos de cuidado y tu camisa de seda azul se mantendrá en excelentes condiciones por mucho tiempo.',
  },
  'camiseta-algodon-plata': {
    prompt: 'camiseta algodon plata',
    opinionBody:
      'Nuestros clientes están encantados con la camiseta de algodón plata. Han destacado su comodidad y su suavidad, gracias al material de algodón de alta calidad. Además, el color plata le da un toque elegante y sofisticado que la hace perfecta para cualquier ocasión. Sin duda, es una prenda imprescindible en el armario de cualquier persona que busque comodidad y estilo.',
    combineBody:
      'La versatilidad de la camiseta de algodón plata hace que sea muy fácil de combinar. Puedes optar por un look casual y relajado, combinándola con unos vaqueros y zapatillas blancas. También puedes darle un toque más formal agregando unos pantalones negros y unos zapatos elegantes. Si quieres un look más divertido, prueba a combinarla con una falda de cuero o unos shorts de colores vivos. Las posibilidades son infinitas y esta camiseta será siempre el centro de atención.',
    cleanBody:
      'Para mantener tu camiseta de algodón plata en perfectas condiciones, te recomendamos lavarla a máquina en agua fría y usar un detergente suave. Evita usar blanqueador o suavizante, ya que pueden dañar el color plata. Después del lavado, déjala secar al aire libre en una percha para evitar que se deforme. Si necesitas plancharla, hazlo a baja temperatura y del revés. Siguiendo estos sencillos cuidados, tu camiseta de algodón plata se mantendrá como nueva durante mucho tiempo.',
  },
  'camiseta-cuero-oro': {
    prompt: 'camiseta cuero oro',
    opinionBody:
      'Las opiniones sobre la camiseta de cuero en color oro son muy positivas. Los usuarios destacan la calidad del material y el brillo elegante que aporta el color dorado. Comentan que es una prenda versátil y llamativa, perfecta para ocasiones especiales o para dar un toque sofisticado a cualquier look. Además, resaltan la durabilidad y resistencia del cuero, lo que garantiza una camiseta que perdurará en el tiempo.',
    combineBody:
      'La camiseta de cuero en color oro es muy versátil a la hora de combinarla. Puede crearse un look elegante y sofisticado combinándola con una falda o pantalón negro y unos tacones altos. También puede lucirse de manera más casual con unos vaqueros rotos y unas zapatillas blancas, creando un contraste de estilos muy interesante. Otra opción es combinarla con una falda plisada en tonos neutros para lograr un look moderno y femenino. Las posibilidades son infinitas, solo hay que dejar volar la imaginación.',
    cleanBody:
      'Para limpiar y mantener la camiseta de cuero en color oro, es importante seguir algunos cuidados especiales. Lo primero es evitar el contacto con líquidos y productos químicos agresivos que puedan dañar el cuero. En caso de manchas, se recomienda utilizar un paño humedecido en agua tibia y jabón neutro para limpiar la zona afectada. Es importante secar el cuero de forma natural, evitando la exposición directa al sol o fuentes de calor. Para mantener el brillo y suavidad del cuero, se puede aplicar un producto específico para este material de forma periódica. Siguiendo estos consejos, la camiseta de cuero oro se mantendrá como nueva durante mucho tiempo.',
  },
  'camiseta-denim-morado': {
    prompt: 'camiseta denim morado',
    opinionBody:
      'La camiseta denim morado ha recibido excelentes críticas por parte de los usuarios. Su material resistente y su color llamativo la convierten en una prenda única y versátil. Muchos usuarios destacan su estilo moderno y su capacidad para destacar en cualquier conjunto. Además, el denim es conocido por su durabilidad, lo que hace que esta camiseta sea una inversión a largo plazo.',
    combineBody:
      'La camiseta denim morado ofrece infinitas posibilidades de combinación. Para un look casual y relajado, puedes combinarla con unos vaqueros azules y zapatillas blancas. Si buscas un estilo más elegante, puedes optar por combinarla con una falda de cuero y tacones altos. Además, el morado es un color que se complementa muy bien con tonos neutros como el negro, el blanco y el gris, por lo que puedes jugar con diferentes accesorios y complementos para agregar un toque personal a tu conjunto.',
    cleanBody:
      'Es importante tener en cuenta que el denim puede perder color con el tiempo, por lo que es recomendable seguir algunas pautas para mantener tu camiseta denim morado en óptimas condiciones. Para evitar que el color se desvanezca, se recomienda lavarla del revés con agua fría y utilizar un detergente suave. Evita el uso de lejía y secadoras, ya que pueden dañar la tela y afectar el color. Además, es recomendable planchar la camiseta a temperatura media y guardarla en un lugar fresco y seco para evitar la formación de arrugas y mantener su aspecto impecable.',
  },
  'camiseta-nylon-bronce': {
    prompt: 'camiseta nylon bronce',
    opinionBody:
      'Los usuarios han expresado su satisfacción con la camiseta de nylon bronce, destacando su comodidad y durabilidad. Afirman que el material de nylon proporciona una excelente transpirabilidad, manteniendo la frescura durante todo el día. Además, el color bronce agrega un toque moderno y sofisticado al guardarropa. En general, los clientes están muy contentos con esta prenda y la recomiendan ampliamente.',
    combineBody:
      'La versatilidad de la camiseta de nylon bronce permite combinarla de diversas formas. Para un look casual y relajado, puedes optar por unos jeans de color azul claro y zapatillas blancas. Si deseas un atuendo más elegante, puedes combinarla con unos pantalones negros y tacones altos en tonos neutros. También puedes agregar accesorios dorados para resaltar el color bronce de la camiseta. En resumen, las posibilidades de combinación son infinitas, ¡solo deja volar tu creatividad!',
    cleanBody:
      'Para mantener la camiseta de nylon bronce en óptimas condiciones, se recomienda lavarla a máquina en ciclo suave y con agua fría. Evita usar lejía y suavizantes, ya que pueden dañar el material y alterar el color. Para secarla, es preferible colgarla en un lugar fresco y a la sombra, evitando la exposición directa al sol. Si es necesario plancharla, utiliza una temperatura baja y asegúrate de colocar un paño entre la plancha y la camiseta para evitar daños en el tejido. Siguiendo estos sencillos cuidados, podrás disfrutar de tu camiseta de nylon bronce por mucho tiempo.',
  },
  'camiseta-piel-claro': {
    prompt: 'camiseta piel claro',
    opinionBody:
      'Los usuarios que han adquirido la camiseta de piel en color claro están encantados con su compra. Destacan la suavidad y la calidad del material, así como la comodidad y el ajuste perfecto. Además, el color claro es muy versátil, ya que se puede combinar con diferentes prendas y estilos, convirtiéndola en una pieza básica en el armario de cualquier persona amante de la moda.',
    combineBody:
      'La camiseta de piel en color claro es una prenda extremadamente versátil que se puede combinar de diversas maneras. Puedes crear un look casual utilizando jeans y zapatillas blancas. Si deseas un look más sofisticado, puedes combinarla con una falda midi y tacones altos. Otra opción es usarla debajo de un blazer y acompañarla con pantalones elegantes. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu camiseta de piel en color claro en perfecto estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Recuerda que la piel es un material delicado, por lo que se recomienda utilizar un paño húmedo y jabón neutro para limpiar las manchas superficiales. Evita el uso de productos químicos agresivos y asegúrate de secarla correctamente para evitar la formación de moho. Además, es aconsejable guardarla en un lugar fresco y seco, lejos de la luz directa del sol, para evitar que se deteriore prematuramente.',
  },
  'camiseta-poliester-marron': {
    prompt: 'camiseta poliester marron',
    opinionBody:
      'La camiseta de poliéster es una elección popular debido a su durabilidad y resistencia. El color marrón añade un toque de sofisticación y versatilidad a cualquier outfit. Los usuarios destacan la comodidad de esta camiseta, gracias a la suavidad del material y a la transpirabilidad que ofrece. Además, el poliéster es conocido por ser resistente a las arrugas, por lo que esta camiseta se mantiene impecable durante todo el día. Sin duda, la camiseta de poliéster marrón es una opción elegante y práctica para cualquier ocasión.',
    combineBody:
      'La camiseta de poliéster marrón es extremadamente versátil y puede combinarse de múltiples formas. Para un look casual, puedes combinarla con unos jeans o pantalones chinos en tonos neutros como beige o gris. Si buscas un look más elegante, puedes combinarla con una falda o pantalón de vestir en tonos oscuros como negro o azul marino. Además, puedes añadir algún accesorio en tonos dorados o plateados para darle un toque de glamour. La camiseta de poliéster marrón se adapta a cualquier estilo y ocasión, convirtiéndose en una prenda imprescindible en tu armario.',
    cleanBody:
      'Para mantener tu camiseta de poliéster marrón en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a máquina con agua fría y usar un detergente suave. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar las fibras del poliéster. Una vez lavada, se recomienda secarla al aire libre o a baja temperatura en la secadora. Para eliminar arrugas, puedes planchar la camiseta a baja temperatura, siempre utilizando un paño delgado entre la plancha y la prenda para proteger el material. Siguiendo estos simples consejos, podrás disfrutar',
  },
  'camiseta-punto-electrico': {
    prompt: 'camiseta punto electrico',
    opinionBody:
      'Las opiniones sobre la camiseta de punto eléctrico son muy variadas. Muchos usuarios destacan su comodidad y suavidad gracias al tejido de punto, que se adapta perfectamente al cuerpo. Además, su diseño elegante y versátil en color negro la hace ideal para cualquier ocasión. Sin embargo, algunos comentarios mencionan que es necesario tener cuidado al lavarla, ya que el tejido puede encogerse si no se sigue correctamente las instrucciones de lavado.',
    combineBody:
      'La camiseta de punto eléctrico en color negro es una prenda muy versátil que puede combinarse de diferentes formas. Para un look informal, puedes combinarla con unos vaqueros y zapatillas deportivas. Si buscas un estilo más sofisticado, puedes combinarla con una falda midi y unos tacones. También puedes añadir capas como una chaqueta de cuero o un blazer para darle un toque más elegante. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener la camiseta de punto eléctrico en perfectas condiciones, es recomendable lavarla a mano o en ciclo delicado en la lavadora con agua fría. Evita usar lejía o productos abrasivos que puedan dañar el tejido. Para secarla, es mejor dejarla al aire libre o utilizar un programa de secado suave en la secadora. Si es necesario plancharla, utiliza baja temperatura y coloca un paño sobre la prenda para evitar dañar el punto. Con los cuidados adecuados, podrás disfrutar de esta camiseta durante mucho tiempo.',
  },
  'camiseta-punto-rojo': {
    prompt: 'camiseta punto rojo',
    opinionBody:
      'La camiseta de punto rojo ha sido muy bien recibida por los usuarios. Muchos destacan su tejido de punto de alta calidad, que le da un aspecto elegante y sofisticado. Además, el color rojo le aporta un toque vibrante y llamativo, lo que la convierte en una prenda perfecta para destacar en cualquier ocasión. Los usuarios también valoran su comodidad y ajuste, ya que se adapta perfectamente al cuerpo sin sacrificar la libertad de movimiento.',
    combineBody:
      'La camiseta de punto rojo es muy versátil y combina a la perfección con diferentes estilos y prendas. Para un look casual, puedes combinarla con unos vaqueros y zapatillas blancas. Si buscas un estilo más formal, puedes optar por combinarla con unos pantalones de vestir negros y unos zapatos de cuero. También puedes jugar con los accesorios para darle un toque personal, como una bufanda o un collar llamativo.',
    cleanBody:
      'Para mantener tu camiseta de punto rojo en buen estado, es importante seguir algunas recomendaciones. Se recomienda lavarla a mano o en ciclo delicado en la lavadora con agua fría. Utiliza un detergente suave y evita el uso de blanqueadores. Después de lavarla, es recomendable secarla al aire o a baja temperatura en la secadora. Si es posible, plancha la camiseta por el revés utilizando una temperatura baja para evitar dañar el tejido de punto. Guarda la camiseta en un lugar fresco y seco para evitar la formación de arrugas o malos olores.',
  },
  'camiseta-rayon-marino': {
    prompt: 'camiseta rayon marino',
    opinionBody:
      'Las opiniones sobre la camiseta de rayón marino son muy positivas. Los usuarios destacan la suavidad y comodidad del material de rayón, así como su alta durabilidad. Además, el color marino es muy versátil y combina fácilmente con diferentes estilos y prendas.',
    combineBody:
      'La camiseta de rayón marino es una prenda muy versátil que se puede combinar de diversas formas. Para un look casual, puedes llevarla con unos jeans y zapatillas blancas. Si quieres un outfit más elegante, puedes combinarla con unos pantalones de vestir y zapatos negros. También puedes agregar una chaqueta de cuero para darle un toque más urbano.',
    cleanBody:
      'Para limpiar y mantener la camiseta de rayón marino, es recomendable lavarla a mano o en programa delicado en la lavadora, utilizando agua fría. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el tejido. También es importante plancharla a baja temperatura, de preferencia del revés. Siguiendo estos consejos de cuidado, podrás mantener tu camiseta de rayón marino en buen estado por mucho tiempo.',
  },
  'camiseta-saten-gris': {
    prompt: 'camiseta saten gris',
    opinionBody:
      'La camiseta de satén gris es altamente valorada por su elegancia y suave textura. Los usuarios destacan su aspecto lujoso y su capacidad para realzar cualquier conjunto. Además, el tejido de satén brinda una sensación sedosa y fresca en la piel, lo que la convierte en una excelente opción para climas cálidos. En términos de color, el gris es versátil y fácil de combinar, lo que la convierte en una prenda imprescindible en cualquier guardarropa.',
    combineBody:
      'La versatilidad del gris permite una amplia gama de combinaciones para la camiseta de satén gris. Para un look elegante y sofisticado, puedes combinarla con una falda lápiz negra o unos pantalones de vestir oscuros. Si buscas un estilo más casual, puedes optar por combinarla con jeans ajustados y zapatillas blancas. Incluso puedes darle un toque moderno e interesante agregando un blazer de color vibrante. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu camiseta de satén gris en excelente estado, es importante seguir algunas recomendaciones de cuidado. Se recomienda lavarla a mano con agua fría y detergente suave. Evita retorcerla para escurrirla y sécala a la sombra, ya que la exposición directa al sol puede dañar el tejido. Además, plancha la camiseta a baja temperatura por el reverso para evitar posibles daños en la superficie de satén. Si sigues estos consejos, tu camiseta de satén gris lucirá impecable durante mucho tiempo.',
  },
  'camiseta-seda-azul': {
    prompt: 'camiseta seda azul',
    opinionBody:
      'La camiseta de seda azul es altamente elogiada por su suavidad y elegancia. Los usuarios coinciden en que el material de seda otorga una sensación lujosa y confortable al contacto con la piel. Además, el color azul resulta ser muy favorecedor y versátil, ya que puede ser combinado tanto con tonos neutros como con colores más llamativos. En general, las opiniones sobre la camiseta de seda azul son muy positivas, destacando su calidad y estilo atemporal.',
    combineBody:
      'La camiseta de seda azul es una prenda muy versátil que puede ser combinada de diferentes maneras según la ocasión. Para un look elegante y sofisticado, puedes combinarla con pantalones o faldas de color negro, beige o blanco, creando un contraste equilibrado. Si buscas un estilo más relajado y casual, puedes optar por jeans o shorts en tonos denim o blancos. Para agregar un toque de color, puedes combinarla con accesorios en tonos dorados o plateados, como brazaletes o collares.',
    cleanBody:
      'La limpieza y mantenimiento de una camiseta de seda azul requiere de ciertos cuidados especiales para conservar su apariencia y suavidad. Es recomendable lavarla a mano con agua fría y un detergente suave, evitando el uso de productos agresivos. También es importante evitar el uso de la secadora, ya que el calor puede dañar las fibras de seda. En lugar de eso, se recomienda secarla al aire libre en una superficie plana. Para eliminar arrugas, se puede utilizar una plancha a baja temperatura o vaporizarla suavemente. Siguiendo estos consejos, podrás disfrutar de tu camiseta de seda azul en perfectas condiciones por mucho tiempo.',
  },
  'camiseta-tela-beige': {
    prompt: 'camiseta tela beige',
    opinionBody:
      'La camiseta de tela beige es una prenda versátil y atemporal que se ha convertido en un básico tanto para hombres como para mujeres. Su color neutro permite combinarla fácilmente con diferentes estilos y colores, y su material suave y ligero la hace perfecta para cualquier ocasión. Los usuarios destacan su comodidad y calidad, asegurando que es una prenda duradera que se mantiene en buenas condiciones incluso después de varios lavados. Además, resaltan que su tono beige es elegante y combina con todo tipo de pantalones, faldas y shorts.',
    combineBody:
      'La camiseta de tela beige es extremadamente versátil y se puede combinar de múltiples formas. Para un look casual y relajado, puedes llevarla con unos jeans azules y zapatillas blancas. Si buscas un estilo más sofisticado, puedes optar por combinarla con una falda midi de cuero y unos tacones negros. Si prefieres un look más fresco y veraniego, puedes llevarla con unos shorts blancos y unas sandalias de tiras. Además, puedes añadir accesorios como un collar statement o una chaqueta de denim para darle un toque especial a tu outfit.',
    cleanBody:
      'Para mantener tu camiseta de tela beige en buenas condiciones, es importante seguir algunas recomendaciones de limpieza. Te recomendamos lavarla a mano o en ciclo suave en la lavadora con agua fría para evitar dañar el color y la tela. Además, es importante utilizar detergente suave y evitar el uso de lejía. Para secarla, puedes colgarla en una percha y dejar que se seque al aire libre, evitando el uso de la secadora. Si la camiseta presenta arrugas, puedes utilizar una plancha a temperatura baja para eliminarlas. Recuerda que al seguir estas instrucciones, prolongarás la vida útil de tu camiseta de tela beige y la mantendrás siempre impecable.',
  },
  'camiseta-tweed-amarillo': {
    prompt: 'camiseta tweed amarillo',
    opinionBody:
      'Esta elegante camiseta de tweed amarillo ha recibido excelentes críticas por su calidad y estilo. Los clientes destacan la suavidad y durabilidad del material de tweed, así como el bonito color amarillo que añade un toque llamativo a cualquier conjunto. Además, comentan que se ajusta de forma cómoda y favorecedora, haciendo de esta camiseta una elección versátil y chic para cualquier ocasión.',
    combineBody:
      'La camiseta de tweed amarillo combina perfectamente con diferentes estilos y prendas. Para un look más formal, puedes combinarla con una falda lápiz negra y unos tacones altos. Si buscas un estilo más casual, puedes optar por combinarla con unos pantalones vaqueros y unas zapatillas blancas. También puedes añadir accesorios dorados para realzar el color amarillo. En definitiva, esta camiseta de tweed amarillo es una prenda versátil que se puede adaptar a diversas ocasiones y estilos.',
    cleanBody:
      'Para mantener tu camiseta de tweed amarillo en perfectas condiciones, se recomienda seguir algunas pautas de limpieza y cuidado. Antes de lavarla, es importante leer las instrucciones de lavado específicas del fabricante. En general, se aconseja lavarla a mano o en ciclo delicado en la lavadora con agua fría. Evita el uso de lejía o productos químicos agresivos y sécala al aire libre para evitar dañar el tejido. Para quitar las arrugas, puedes usar una plancha a temperatura media, asegurándote de colocar un paño fino entre la camiseta y la plancha para evitar dañar el material. Con unos cuidados adecuados, tu camiseta de tweed amarillo lucirá como nueva durante mucho tiempo.',
  },
  'capa-algodon-turquesa': {
    prompt: 'capa algodon turquesa',
    opinionBody:
      'Los usuarios que han adquirido la capa de algodón turquesa han quedado encantados con su compra. Destacan la suavidad y comodidad del material de algodón, que proporciona una sensación agradable al llevarla puesta. Además, el color turquesa es muy llamativo y vibrante, lo que añade un toque de estilo y originalidad a cualquier outfit. En general, las opiniones son muy positivas, resaltando la calidad del producto y su capacidad para proteger del frío sin dejar de ser ligera y fácil de llevar.',
    combineBody:
      'La capa de algodón turquesa es una prenda perfecta para añadir un toque de color y estilo a tus looks. Puedes combinarla de diversas formas: con unos jeans y una camiseta blanca para un look casual y desenfadado, o con un vestido negro y unos tacones para un outfit más elegante y sofisticado. Además, puedes jugar con diferentes accesorios en tonos neutros o dorados para resaltar aún más el color turquesa de la capa. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu capa de algodón turquesa en buen estado, es importante seguir algunas pautas de limpieza y cuidado. Recomendamos lavarla a mano con agua fría y un detergente suave, evitando el uso de productos abrasivos. Al secarla, es mejor dejarla tender al aire en un lugar sombreado, evitando la exposición directa al sol. Si surgieran arrugas, puedes plancharla a baja temperatura, siempre utilizando un paño o una tela protectora entre la plancha y la capa. Siguiendo estas indicaciones, podrás disfrutar de tu capa de algodón turquesa durante mucho tiempo.',
  },
  'capa-encaje-blanco': {
    prompt: 'capa encaje blanco',
    opinionBody:
      'Los clientes que han adquirido esta capa de encaje blanco están encantados con su calidad y estilo. Destacan la delicadeza y elegancia del encaje, así como la manera en que resalta cualquier atuendo. Además, mencionan que es una prenda versátil que puede ser usada tanto en ocasiones formales como informales. En resumen, los usuarios elogian la belleza y versatilidad de esta capa de encaje blanco.',
    combineBody:
      'Para crear un outfit perfecto con la capa de encaje blanco, hay varias opciones. Puedes optar por combinarla con un vestido negro ajustado para lograr un contraste elegante y sofisticado. También puedes llevarla sobre una camiseta blanca básica y unos jeans, agregando un toque romántico y femenino a tu look casual. Otra alternativa es utilizarla como complemento de una blusa de manga larga y una falda midi para un estilo más formal. En definitiva, la capa de encaje blanco es una prenda muy versátil que se adapta a diferentes estilos y ocasiones.',
    cleanBody:
      'La capa de encaje blanco requiere cuidados especiales para mantener su belleza y durabilidad. Se recomienda lavarla a mano con agua fría y un detergente suave para prendas delicadas. Evita retorcerla o frotarla con fuerza, ya que esto podría dañar el encaje. Después de lavarla, colócala sobre una toalla limpia y presiónala suavemente para quitar el exceso de agua. Luego, déjala secar al aire libre, evitando la exposición directa al sol. Para guardarla, es recomendable doblarla cuidadosamente y guardarla en un lugar seco y sin humedad. Siguiendo estos consejos de limpieza y mantenimiento, tu capa de encaje blanco lucirá impecable en cada ocasión.',
  },
  'capa-encaje-marron': {
    prompt: 'capa encaje marron',
    opinionBody:
      'La capa de encaje marrón es sin duda una elección elegante y sofisticada. Muchas personas elogian su diseño único y femenino, ya que el encaje agrega un toque romántico y delicado. Además, el color marrón le da un aspecto cálido y versátil, perfecto para combinar con diferentes outfits. Sin embargo, algunos usuarios han comentado que el encaje puede ser un poco frágil y requiere cuidado al ponerse y quitarla para evitar enganches. En general, la capa de encaje marrón ha recibido opiniones positivas por su estilo y versatilidad.',
    combineBody:
      'La capa de encaje marrón es una prenda muy versátil que se puede combinar de diferentes maneras para crear looks elegantes y estilosos. Puedes usarla sobre un vestido ajustado para agregar un toque de glamour a tu outfit de noche, o combinarla con unos jeans y una blusa sencilla para un look más casual pero sofisticado. Además, puedes jugar con los accesorios para resaltar aún más el color marrón, como cinturones o bolsos en tonos similares. Esta capa también se ve muy bien con zapatos de tacón alto o botas hasta la rodilla. Las posibilidades son infinitas, ¡solo debes dejar volar tu imaginación!',
    cleanBody:
      'Para mantener tu capa de encaje marrón en óptimas condiciones, es importante seguir algunas recomendaciones de cuidado. Primero, verifica siempre las instrucciones de lavado del fabricante, ya que algunos encajes pueden ser delicados y requerir lavado a mano o en ciclo suave. En caso de manchas, es recomendable tratarlas de inmediato con un quitamanchas suave y evitar frotar el encaje para evitar daños. Si necesitas planchar la capa, hazlo a baja temperatura y utilizando una tela protectora para evitar quemaduras o marcas en el encaje. Además, al guardarla, asegúrate de que esté completamente se',
  },
  'capa-gasa-plata': {
    prompt: 'capa gasa plata',
    opinionBody:
      'La capa de gasa plata ha recibido excelentes críticas por parte de los usuarios. Muchos destacan su elegancia y versatilidad, perfecta para eventos formales o fiestas. Además, el material de gasa le da un aspecto ligero y fluido, lo que la hace muy cómoda de llevar. El color plata le añade un toque de brillo y sofisticación, convirtiéndola en una opción ideal para destacar en cualquier ocasión.',
    combineBody:
      'La capa de gasa plata es muy versátil y puede combinarse de diversas maneras. Para un look elegante y sofisticado, puedes combinarla con un vestido negro ajustado y unos tacones altos del mismo color. Si prefieres un estilo más casual pero chic, puedes usarla con unos jeans ajustados y una blusa blanca, añadiendo unos botines negros de tacón. Para un toque de glamur, combínala con un vestido de noche en tonos plateados y unos accesorios brillantes.',
    cleanBody:
      'Para limpiar y mantener correctamente una capa de gasa plata, es recomendable seguir algunas pautas. En primer lugar, es importante leer las instrucciones de cuidado que vienen con la prenda, ya que pueden tener indicaciones específicas. En general, se recomienda lavarla a mano con agua fría y un detergente suave. Evitar retorcerla o frotarla, ya que esto puede dañar el tejido. Para secarla, es preferible colgarla en una percha, evitando la exposición directa al sol. Además, es importante almacenarla en un lugar seco y alejado de la luz para evitar que se decolore.',
  },
  'capa-punto-borgona': {
    prompt: 'capa punto borgona',
    opinionBody:
      'Con qué combinar la capa de punto borgoña: Esta capa en color borgoña se puede combinar de diversas formas para lograr looks únicos y elegantes. Para un outfit casual, puedes combinarla con jeans ajustados y una camisa blanca. Si prefieres un estilo más formal, puedes llevarla sobre un vestido negro ajustado y añadir unos tacones altos. También puedes optar por combinarla con una falda midi y una blusa de seda para un look más femenino y sofisticado. Las posibilidades son infinitas, y esta capa en color borgoña sin duda se convertirá en un elemento clave en tu armario.',
    combineBody: null,
    cleanBody: null,
  },
  'capa-tafetan-verde': {
    prompt: 'capa tafetan verde',
    opinionBody:
      'Los usuarios han expresado su satisfacción con la capa de tafetán verde, elogiando su elegancia y versatilidad. Comentan que el material de tafetán le da un acabado brillante y sofisticado, mientras que el color verde añade un toque de frescura y estilo a cualquier atuendo. Además, destacan que la capa es cómoda de llevar y combina bien con una variedad de prendas y estilos.',
    combineBody:
      'Esta capa de tafetán verde es extremadamente versátil y se puede combinar con una amplia gama de prendas. Para un look elegante y sofisticado, puedes combinarla con un vestido negro ajustado y unos tacones altos. Si prefieres algo más casual pero chic, puedes usarla sobre una blusa blanca y unos jeans ajustados. Además, puedes experimentar con diferentes colores para hacerla destacar aún más, como combinarla con un top rojo brillante o un vestido estampado. Las posibilidades son infinitas.',
    cleanBody:
      'Para mantener tu capa de tafetán verde en perfectas condiciones, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda que la limpies en seco para evitar dañar el material. Si necesitas eliminar manchas o derrames leves, puedes usar un paño húmedo y suave con un poco de detergente suave. Asegúrate de no frotar demasiado fuerte para no dañar la tela. Después de limpiarla, déjala secar al aire libre y guárdala en un lugar fresco y seco. Recuerda evitar el contacto con objetos afilados que puedan enganchar o rasgar el tafetán. Siguiendo estos consejos de cuidado, podrás disfrutar de tu capa de tafetán verde por mucho tiempo.',
  },
  'capa-terciopelo-aguamarina': {
    prompt: 'capa terciopelo aguamarina',
    opinionBody:
      'Los usuarios están encantados con esta capa de terciopelo aguamarina. Destacan su suavidad y textura luxe, que proporciona un aspecto elegante y sofisticado. Además, el color aguamarina es muy llamativo y favorecedor, añadiendo un toque de color a cualquier outfit. Sin duda, es una pieza versátil y de gran calidad que recibirá halagos allá donde vayas.',
    combineBody:
      'Gracias a su tono aguamarina vibrante, esta capa de terciopelo puede ser combinada de múltiples formas para crear un look elegante y a la moda. Puedes optar por un estilo monocromático y combinarla con prendas en tonos similares, como un vestido de terciopelo o unos pantalones de seda. También puedes crear contrastes interesantes con colores complementarios, como el dorado o el blanco. En definitiva, las posibilidades son infinitas y puedes adaptarla según tu estilo y ocasión.',
    cleanBody:
      'El terciopelo es un tejido delicado y requiere cuidados especiales para mantener su aspecto impecable. Para limpiar la capa de terciopelo aguamarina, recomendamos utilizar un cepillo suave o un rodillo adhesivo para eliminar el polvo y la suciedad. Evita mojarla completamente, ya que podría dañar el tejido. En caso de manchas, es mejor acudir a un profesional de la limpieza en seco para evitar dañar el terciopelo. También es importante guardarla en un lugar oscuro y seco para prevenir el desgaste y mantener su suavidad y brillo por más tiempo.',
  },
  'capa-terciopelo-beige': {
    prompt: 'capa terciopelo beige',
    opinionBody:
      'Los clientes están encantados con la capa de terciopelo beige. La suavidad y la elegancia del material tienen una gran aceptación. Además, el color beige le da un toque sofisticado y versátil, lo que la convierte en una prenda ideal para cualquier ocasión. Los usuarios destacan la comodidad y la calidez que proporciona, convirtiéndola en una opción perfecta para los días fríos de invierno. Sin duda, la capa de terciopelo beige se ha convertido en una opción favorita entre los amantes de la moda y el estilo.',
    combineBody:
      'La capa de terciopelo beige es extremadamente versátil y se puede combinar de diversas maneras para crear looks increíbles. Para un estilo elegante y sofisticado, puedes combinarla con unos pantalones negros ajustados y unas botas altas. Si prefieres un look más casual, puedes usarla con unos jeans y unas zapatillas blancas. Además, puedes agregar accesorios dorados o plateados para completar el conjunto. Sin importar cómo decidas combinarla, la capa de terciopelo beige siempre será protagonista y añadirá un toque de estilo y glamour a tu outfit.',
    cleanBody:
      'La capa de terciopelo beige requiere de ciertos cuidados especiales para mantener su apariencia y calidad por mucho tiempo. Para limpiarla, se recomienda utilizar un cepillo de cerdas suaves para eliminar el polvo y la suciedad superficial. En caso de manchas, es importante utilizar un producto de limpieza específico para terciopelo y seguir las indicaciones del fabricante. Además, es recomendable evitar el contacto con líquidos o sustancias que puedan manchar o dañar el terciopelo. Para guardarla, es ideal colgarla en un lugar ventilado y protegido del polvo. De esta manera, podrás disfrutar de tu capa de terciopelo beige en perfectas condiciones durante mucho tiempo.',
  },
  'cazadora-brocado-verde': {
    prompt: 'cazadora brocado verde',
    opinionBody:
      'La cazadora de brocado verde ha sido muy bien recibida por los clientes. Su diseño elegante y original ha capturado la atención de aquellos que buscan destacar con estilo. El brocado le da un toque de sofisticación, mientras que el color verde añade frescura y versatilidad. Los usuarios han elogiado la calidad de los materiales utilizados, así como la comodidad y durabilidad de la prenda. En definitiva, esta cazadora se ha convertido en una opción favorita para aquellos que desean lucir a la moda y destacar en cualquier ocasión.',
    combineBody:
      'La cazadora de brocado verde se puede combinar de distintas formas para crear looks únicos y estilosos. Para un outfit casual chic, puedes combinarla con unos jeans ajustados y una camiseta blanca básica. Para un look más elegante, puedes usarla sobre un vestido negro ceñido al cuerpo y unos tacones altos. También puedes darle un toque más bohemio al combinarla con una falda estampada y unas botas camperas. En resumen, las posibilidades son infinitas, y esta cazadora de brocado verde será el complemento perfecto para cualquier conjunto.',
    cleanBody:
      'Para mantener la cazadora de brocado verde en perfectas condiciones, es importante seguir ciertos cuidados. Para la limpieza, se recomienda consultar las instrucciones del fabricante, ya que los diferentes materiales pueden requerir diferentes métodos de lavado. En general, es aconsejable lavarla a mano o en ciclo delicado en la lavadora, utilizando un detergente suave. Evita el uso de lejía o productos químicos agresivos. Para secarla, colócala en una percha y déjala secar al aire libre, evitando la exposición directa al sol. Recuerda también guardarla en un lugar fresco y seco para evitar daños. Siguiendo estos consejos, podrás disfrutar de tu cazadora de brocado verde durante mucho tiempo.',
  },
  'cazadora-cuero-marino': {
    prompt: 'cazadora cuero marino',
    opinionBody:
      'Esta cazadora de cuero marino se ha convertido en un favorito entre los usuarios. El material de cuero brinda una durabilidad excepcional, asegurando que la prenda dure mucho tiempo. El color marino es elegante y versátil, lo que la hace perfecta para cualquier ocasión. Además, el ajuste es impecable y el estilo clásico hace que sea una opción atemporal en el armario de cualquier persona. Los usuarios la recomiendan por su calidad y diseño.',
    combineBody:
      'La versatilidad de esta cazadora de cuero marino permite una amplia gama de combinaciones. Puedes combinarla con un par de jeans oscuros y una camiseta blanca para un look casual chic. Si quieres darle un toque más elegante, úsala sobre un vestido negro y añade unos tacones altos. Para un estilo más rockero, combínala con unos pantalones de cuero y una camiseta estampada. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener la cazadora de cuero marino en óptimas condiciones, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, asegúrate de leer las instrucciones del fabricante antes de realizar cualquier limpieza. Para quitar manchas pequeñas, usa un paño húmedo y un poco de jabón suave. Evita usar productos químicos fuertes que puedan dañar el cuero. Si la cazadora se ha mojado, déjala secar de forma natural y evita la exposición directa al sol. Recuerda guardarla en un lugar fresco y seco cuando no la estés usando. Con estos cuidados básicos, podrás disfrutar de tu cazadora de cuero marino durante mucho tiempo.',
  },
  'cazadora-encaje-rosa': {
    prompt: 'cazadora encaje rosa',
    opinionBody:
      'La cazadora de encaje rosa ha sido muy bien recibida por aquellos que la han adquirido. Su diseño único y femenino la convierte en una prenda perfecta para agregar un toque de estilo a cualquier outfit. Además, su material de encaje le da un toque de elegancia y sofisticación. Los usuarios destacan su comodidad y versatilidad, ya que puede ser combinada tanto con ropa casual como con outfits más formales.',
    combineBody:
      'La cazadora de encaje rosa puede ser combinada de diversas formas para crear looks únicos y originales. Para un estilo más casual, puedes combinarla con unos jeans ajustados y una camiseta básica blanca. Para un look más elegante, puedes usarla sobre un vestido negro ajustado, añadiendo unos tacones y accesorios en tonos neutros. También puedes optar por combinarla con una falda midi y una blusa de seda para lograr un look femenino y sofisticado. ¡Las posibilidades son infinitas!',
    cleanBody:
      'A la hora de limpiar y mantener tu cazadora de encaje rosa, es importante seguir ciertos cuidados para mantenerla en óptimas condiciones. Te recomendamos leer las instrucciones de cuidado que vienen con la prenda. En general, el encaje es un material delicado y se debe lavar a mano o en ciclo delicado en la lavadora, utilizando agua fría y un detergente suave. Evita el uso de blanqueadores o suavizantes. Para secarla, es preferible hacerlo al aire libre o utilizar el programa de secado delicado de la secadora. Recuerda también guardarla en un lugar fresco y seco, lejos de la luz directa del sol, para evitar la decoloración y el deterioro del encaje. Siguiendo estos simples consejos, podrás disfrutar de tu cazadora de encaje rosa por mucho tiempo.',
  },
  'cazadora-franela-esmeralda': {
    prompt: 'cazadora franela esmeralda',
    opinionBody:
      'Los usuarios que han adquirido esta cazadora de franela en color esmeralda han quedado impresionados por su estilo único y moderno. La combinación del suave y cálido material de franela con el vibrante color esmeralda hace que esta prenda sea una opción perfecta para destacar en cualquier ocasión. Además, muchos usuarios destacan la calidad de la confección, asegurando que es resistente y duradera. Sin duda, esta cazadora de franela en color esmeralda es una elección acertada para aquellos que quieren añadir un toque de color y estilo a sus outfits.',
    combineBody:
      'La cazadora de franela en color esmeralda es extremadamente versátil y se puede combinar de diversas formas para crear looks únicos y atractivos. Para un estilo casual y relajado, puedes optar por combinarla con unos jeans ajustados y unas botas de cuero. Si quieres darle un toque más chic, puedes llevarla sobre un vestido corto y unos tacones altos en color negro. También puedes jugar con los accesorios y optar por complementos en tonos neutros para resaltar el vibrante color esmeralda de la cazadora. Explora tu creatividad y seguro encontrarás numerosas opciones para lucir esta prenda de manera espectacular.',
    cleanBody:
      'La limpieza y el mantenimiento de la cazadora de franela en color esmeralda es bastante sencillo. Recomendamos seguir las instrucciones proporcionadas por el fabricante para un cuidado adecuado. En general, se recomienda lavarla a mano o en ciclo suave en la lavadora con agua fría y un detergente suave. Evita usar blanqueadores o suavizantes de telas, ya que podrían dañar el color y la textura de la franela. Para secarla, se recomienda colgarla en una percha y evitar la exposición directa al sol. Con estos sencillos cuidados, podrás disfrutar de tu cazadora de franela en color esmeralda durante mucho tiempo, manteniendo',
  },
  'cazadora-gasa-marron': {
    prompt: 'cazadora gasa marron',
    opinionBody:
      'La cazadora de gasa marrón es una prenda versátil y elegante que ha recibido excelentes críticas por parte de los usuarios. Su material liviano y transpirable la convierte en la opción perfecta para usar durante las épocas de entretiempo. Además, el color marrón es muy fácil de combinar y agrega un toque de sofisticación a cualquier outfit. Los usuarios destacan que esta cazadora es cómoda de llevar y que su diseño moderno y ajustable se adapta a diferentes estilos y cuerpos. En resumen, la cazadora de gasa marrón ha sido muy bien valorada por su calidad, estilo y versatilidad.',
    combineBody:
      'La cazadora de gasa marrón es una prenda muy versátil que se puede combinar de diferentes formas para lograr distintos estilos. Para un look casual y relajado, puedes combinarla con unos jeans ajustados y una camiseta básica blanca. Si quieres lucir más sofisticada, puedes optar por combinarla con una falda de cuero y una blusa de seda. También puedes usarla sobre un vestido estampado y añadir unos botines para crear un look boho chic. En resumen, la cazadora de gasa marrón es una pieza que se adapta a diferentes estilos y se puede combinar con una amplia gama de prendas y accesorios.',
    cleanBody:
      'Para mantener tu cazadora de gasa marrón en buen estado, es importante seguir algunas recomendaciones. En primer lugar, lee siempre las etiquetas de cuidado y lava la prenda según las indicaciones del fabricante. En general, la cazadora de gasa marrón puede ser lavada a mano o a máquina con un programa suave y agua fría. Evita usar lejía o detergentes agresivos que puedan dañar el tejido. Después de lavarla, sécala al aire libre o a baja temperatura en la secadora. Para eliminar arrugas, utiliza una plancha a baja temperatura y coloca un paño delgado entre la plancha y la cazadora para evitar dañ',
  },
  'cazadora-jacquard-gris': {
    prompt: 'cazadora jacquard gris',
    opinionBody:
      'La cazadora jacquard gris ha recibido excelentes críticas por parte de los usuarios. Su diseño elegante y moderno, combinado con la calidad del material, la convierte en una opción popular entre aquellos que buscan estar a la moda. Los comentarios destacan su comodidad y durabilidad, además de resaltar su versatilidad para combinar con diferentes estilos.',
    combineBody:
      'La cazadora jacquard gris es una prenda muy versátil que se puede combinar de diversas formas. Para un look casual, puedes usarla con jeans ajustados y una camiseta básica. Si prefieres un estilo más sofisticado, combínala con una blusa de seda y pantalones de vestir. También puedes agregar accesorios como bufandas o collares para darle un toque personalizado a tu outfit.',
    cleanBody:
      'Para mantener la cazadora jacquard gris en óptimas condiciones, es recomendable seguir las instrucciones de cuidado del fabricante. En general, se aconseja lavarla a mano o utilizar un ciclo suave en la lavadora con agua fría. Evita el uso de cloro o blanqueadores y no la expongas directamente al sol para evitar que se decolore. Además, es importante almacenarla en un lugar fresco y seco, lejos de la humedad.',
  },
  'cazadora-lana-oro': {
    prompt: 'cazadora lana oro',
    opinionBody:
      'La cazadora de lana oro ha recibido excelentes opiniones de los usuarios. Muchos destacan su estilo elegante y sofisticado, además de su alta calidad y durabilidad. El color oro añade un toque de glamour a cualquier outfit, convirtiéndola en una prenda perfecta para eventos especiales o para darle un toque de brillo a tu día a día. Sin duda, es una elección acertada para quienes buscan destacar y lucir a la moda.',
    combineBody:
      'La versatilidad de la cazadora de lana oro la convierte en una prenda fácil de combinar. Para un look casual, puedes combinarla con unos jeans y una camiseta blanca básica. Si quieres un look más formal, puedes optar por combinarla con una falda lápiz negra y una blusa blanca. Además, puedes complementarla con accesorios en tonos neutros como beige o negro para resaltar aún más el brillo del color oro. Sea cual sea tu estilo, esta cazadora será el complemento perfecto para tus outfits.',
    cleanBody:
      'Es importante seguir las instrucciones del fabricante al limpiar y mantener la cazadora de lana oro. En general, se recomienda llevarla a una tintorería especializada para garantizar su correcta limpieza y evitar daños en el tejido. Si prefieres hacerlo tú mismo, asegúrate de utilizar un detergente suave y agua fría para evitar que el color se desvanezca. Evita retorcerla al secar y déjala secar al aire libre en una superficie plana. Recuerda también guardarla en un lugar fresco y seco para evitar la formación de humedad y mantenerla en las mejores condiciones posibles.',
  },
  'cazadora-organdi-naranja': {
    prompt: 'cazadora organdi naranja',
    opinionBody:
      'La cazadora organdi naranja ha sido muy bien recibida por los usuarios que la han adquirido. El material de organdi le da un aspecto elegante y ligero, perfecto para la temporada de primavera. El color naranja le añade un toque de vitalidad y alegría. Los usuarios han destacado su comodidad y lo fácil que es combinarla con diferentes estilos de ropa.',
    combineBody:
      'La cazadora organdi naranja es una prenda versátil que se puede combinar de diferentes maneras. Para un look casual, puedes combinarla con unos jeans ajustados y unas zapatillas blancas. Si deseas un look más sofisticado, puedes usarla sobre un vestido negro y añadir unos tacones altos. También puedes optar por combinarla con colores complementarios como el azul marino o el beige.',
    cleanBody:
      'Para mantener la cazadora organdi naranja en buen estado, te recomendamos seguir las instrucciones de cuidado que vienen con la prenda. En general, es preferible lavarla a mano con agua fría y detergente suave. Evita retorcerla y déjala secar al aire libre. Si es necesario plancharla, hazlo a baja temperatura y coloca un paño entre la plancha y la prenda para evitar dañar el material. Con estos cuidados básicos, podrás disfrutar de tu cazadora organdi naranja durante mucho tiempo.',
  },
  'cazadora-piel-azul': {
    prompt: 'cazadora piel azul',
    opinionBody:
      'La cazadora de piel azul ha recibido excelentes opiniones por parte de nuestros clientes. Destacan su calidad y durabilidad, ya que la piel utilizada es de primera calidad. Además, el color azul le da un toque de estilo y modernidad, lo que la convierte en una prenda versátil que puede ser utilizada en diferentes ocasiones. En general, los usuarios están muy satisfechos con esta cazadora y la recomiendan por su diseño y confort.',
    combineBody:
      'La cazadora de piel azul combina a la perfección con distintos estilos y prendas. Para un look casual, puedes combinarla con unos jeans y una camiseta blanca. Si prefieres un estilo más formal, puedes llevarla sobre una camisa y pantalones de vestir. Además, puedes añadir complementos como bufandas o gorros en tonos neutros para completar el conjunto. En definitiva, la cazadora de piel azul es una prenda versátil que se adapta a diversas combinaciones y estilos.',
    cleanBody:
      'Para mantener la cazadora de piel azul en buen estado, es importante seguir algunos cuidados especiales. En primer lugar, se recomienda limpiarla con un paño húmedo y jabón suave en caso de manchas. Evita utilizar productos químicos agresivos que puedan dañar la piel. Además, es recomendable guardarla en un lugar seco y ventilado, lejos de la luz directa del sol. De manera periódica, puedes aplicar productos específicos para el cuidado de la piel para mantenerla hidratada y evitar que se reseque. Siguiendo estos consejos, podrás disfrutar de tu cazadora de piel azul durante mucho tiempo.',
  },
  'chaleco-cachemira-amarillo': {
    prompt: 'chaleco cachemira amarillo',
    opinionBody:
      'Los usuarios que han adquirido el chaleco de cachemira amarillo han quedado encantados con su suavidad y calidez. La cachemira es un material de alta calidad que brinda una sensación de lujo al usarlo. El color amarillo agrega un toque vibrante y alegre a cualquier conjunto. Los clientes han elogiado el ajuste perfecto y la versatilidad de esta prenda, que puede ser usada tanto en ocasiones formales como informales.',
    combineBody:
      'El chaleco de cachemira amarillo es una prenda versátil que puede combinarse de varias formas. Para un look casual y relajado, puedes combinarlo con unos jeans ajustados y una camiseta básica blanca. Si buscas un outfit más elegante, puedes usarlo sobre un vestido negro ajustado o una blusa de seda acompañada de una falda a juego. Además, el color amarillo se combina muy bien con tonos neutros como el blanco, el beige o el gris, lo cual te brinda muchas opciones para crear estilismos únicos.',
    cleanBody:
      'La cachemira requiere un cuidado especial para mantenerla en excelentes condiciones. Se recomienda lavar a mano el chaleco de cachemira amarillo con agua fría y un detergente suave para prendas delicadas. Evita retorcer o frotar enérgicamente la prenda, ya que esto puede dañar las fibras delicadas de cachemira. Después del lavado, sécalo en posición horizontal para evitar estirar la prenda. Para mantener su suavidad y evitar la formación de bolitas, puedes utilizar un cepillo de cachemira para cepillar suavemente la superficie. Guárdalo en un lugar fresco y seco, preferiblemente en una bolsa de algodón, lejos de la luz directa del sol y de la humedad. Sigue estas recomendaciones y tu chaleco de cachemira amarillo lucirá impecable durante mucho tiempo.',
  },
  'chaleco-encaje-aguamarina': {
    prompt: 'chaleco encaje aguamarina',
    opinionBody:
      'El chaleco de encaje en color aguamarina ha recibido excelentes comentarios por su elegante y sofisticado diseño. Los usuarios destacan su delicado detalle de encaje, que le da un toque femenino y romántico al outfit. Además, el color aguamarina agrega frescura y vitalidad, convirtiéndolo en una opción perfecta para ocasiones especiales o para darle un toque único a cualquier look casual. Sin duda, este chaleco es altamente recomendado por su calidad y estilo.',
    combineBody:
      'El chaleco de encaje en color aguamarina es extremadamente versátil y puede combinar con una amplia gama de prendas y estilos. Para un look más elegante y formal, puedes combinarlo con una blusa o un vestido en tonos neutros como blanco, gris o negro. Para un look más casual pero igualmente chic, puedes usarlo sobre una camiseta básica y jeans. Además, complementa muy bien con accesorios en tonos dorados o plateados, como pulseras o pendientes. ¡Las posibilidades de combinación son infinitas!',
    cleanBody:
      'Para mantener el chaleco de encaje en color aguamarina en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarlo a mano con agua fría y un detergente suave, evitando retorcerlo para evitar que se deforme el encaje. Si es necesario, se puede planchar a baja temperatura, siempre utilizando un paño fino por encima del encaje para protegerlo. Por último, se recomienda guardar el chaleco encaje en un lugar fresco y seco, lejos de la luz directa del sol, y evitar el contacto con cremas o perfumes que puedan mancharlo. Siguiendo estos sencillos pasos, podrás disfrutar de tu chaleco de encaje en color aguamarina por mucho tiempo.',
  },
  'chaleco-saten-negro': {
    prompt: 'chaleco saten negro',
    opinionBody:
      'El chaleco de satén negro es una prenda elegante y versátil que ha recibido excelentes opiniones por parte de quienes lo han utilizado. Su material de satén le otorga un aspecto sofisticado y su color negro le añade un toque de elegancia. Muchos usuarios han destacado la calidad de su confección y lo cómodo que resulta. Además, su diseño ajustable permite un perfecto ajuste al cuerpo. Sin duda, el chaleco de satén negro es una excelente elección para eventos formales y ocasiones especiales.',
    combineBody:
      'El chaleco de satén negro es una prenda versátil que se puede combinar de múltiples formas, dependiendo de la ocasión y el estilo que se quiera lograr. Para un look formal y elegante, se puede combinar con una camisa blanca y un traje a juego. Para un estilo más casual pero sofisticado, puede lucir fantástico con una camisa de botones de color llamativo y unos pantalones chinos. También se puede combinar con una falda o unos jeans para un look más desenfadado pero chic. Las posibilidades son infinitas, lo importante es jugar con diferentes texturas y colores para crear outfits únicos y estilosos.',
    cleanBody:
      'El chaleco de satén negro requiere algunos cuidados especiales para mantener su apariencia impecable. Se recomienda seguir las instrucciones de lavado indicadas en la etiqueta del fabricante. En general, se aconseja lavar a mano con agua fría y un detergente suave. Evitar retorcer o frotar vigorosamente para no dañar las fibras delicadas del satén. Para el secado, es preferible colgarlo en una percha y dejar que se seque al aire. Si es necesario plancharlo, utilizar la temperatura más baja y preferiblemente hacerlo por el reverso del chaleco. Siguiendo estas recomendaciones, podrás disfrutar de un chaleco de satén negro en perfecto estado durante mucho tiempo.',
  },
  'chaleco-seda-esmeralda': {
    prompt: 'chaleco seda esmeralda',
    opinionBody:
      'Los usuarios que han adquirido este chaleco de seda en color esmeralda han quedado enamorados de su elegancia y versatilidad. Destacan la suavidad y delicadeza del material, así como su brillo y acabado impecable. Además, el color esmeralda le añade un toque de sofisticación y originalidad, convirtiéndolo en una pieza única en cualquier outfit.',
    combineBody:
      'Este chaleco de seda en color esmeralda es muy versátil y puede combinarse de diferentes maneras. Para un look elegante y formal, puede ser el complemento perfecto para usar sobre una camisa blanca y pantalones negros. Si quieres un look más casual pero aún sofisticado, puedes combinarlo con unos jeans de tono claro y una blusa de seda en colores neutros. También puedes añadir accesorios dorados para resaltar aún más el tono esmeralda del chaleco.',
    cleanBody:
      'Para mantener el chaleco de seda en color esmeralda en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Se recomienda lavarlo a mano, utilizando agua tibia y un detergente suave para prendas delicadas. Evitar retorcer o frotar en exceso la prenda para evitar dañar las fibras de seda. Además, es importante secarlo al aire libre, evitando la exposición directa al sol. Si es necesario plancharlo, utilizar una temperatura baja y colocar un paño fino entre el chaleco y la plancha para proteger el tejido de seda. Siguiendo estos consejos, podrás disfrutar de tu chaleco de seda esmeralda durante mucho tiempo.',
  },
  'chaleco-terciopelo-claro': {
    prompt: 'chaleco terciopelo claro',
    opinionBody:
      'Los usuarios se muestran encantados con este chaleco de terciopelo claro. Destacan su suavidad y elegancia, además de su gran versatilidad. Muchos comentan que es perfecto para ocasiones especiales, aportando un toque sofisticado a cualquier conjunto. Otros destacan su gran comodidad y lo bien que sienta al cuerpo. Sin duda, este chaleco de terciopelo claro es una elección acertada para lucir a la moda y sentirse único.',
    combineBody:
      'El chaleco de terciopelo claro es una prenda muy versátil, por lo que se puede combinar de diferentes formas. Para un look más formal y elegante, puedes combinarlo con una camisa blanca o negra y pantalones de vestir en tonos oscuros. Para un look más casual, puedes optar por una camiseta básica y jeans ajustados. También puedes jugar con accesorios como un collar llamativo o una corbata de seda. Sea cual sea tu elección, este chaleco de terciopelo claro añadirá un toque de estilo y clase a tu outfit.',
    cleanBody:
      'Para mantener el chaleco de terciopelo claro en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, es recomendable cepillar suavemente el terciopelo con un cepillo de cerdas suaves para eliminar cualquier resto de polvo. En caso de manchas, se puede utilizar un paño ligeramente humedecido con agua tibia y jabón neutro para tratarlas suavemente. Se recomienda evitar el lavado a máquina y el uso de productos químicos agresivos. Para el almacenamiento, es preferible colgarlo en un perchero en lugar de doblarlo, para evitar que se formen pliegues en el terciopelo. Siguiendo estas sencillas recomendaciones, podrás disfrutar de tu chaleco de terciopelo claro durante mucho tiempo.',
  },
  'chaqueta-brocado-borgona': {
    prompt: 'chaqueta brocado borgona',
    opinionBody:
      'La chaqueta de brocado borgoña ha recibido excelentes comentarios por parte de los usuarios. Su diseño elegante y sofisticado ha sido muy elogiado, así como la calidad del material utilizado en su confección. Los usuarios destacan que es una prenda versátil que se puede usar en diferentes ocasiones, ya sea para eventos especiales o para darle un toque de estilo a un look casual. Además, el color borgoña es muy favorecedor y se adapta a diferentes tonos de piel. En general, quienes han adquirido esta chaqueta están muy satisfechos con su compra y la recomiendan ampliamente.',
    combineBody:
      'La chaqueta de brocado borgoña es una prenda que se puede combinar de diversas formas. Para un look elegante y sofisticado, puedes llevarla con una blusa de seda en tonos neutros, unos pantalones negros y unos tacones altos. Si prefieres un estilo más casual y relajado, puedes combinarla con una camiseta básica blanca, unos jeans ajustados y unas zapatillas deportivas. También puedes agregar accesorios dorados o plateados para darle un toque de brillo y glamour. La versatilidad de esta chaqueta te permite adaptarla a diferentes estilos y ocasiones, convirtiéndola en una prenda imprescindible en tu armario.',
    cleanBody:
      'Para mantener la chaqueta de brocado borgoña en excelentes condiciones, es importante seguir algunas recomendaciones. En primer lugar, verifica las instrucciones de cuidado que vienen en la etiqueta del producto, ya que diferentes materiales pueden tener requerimientos de limpieza específicos. En general, se recomienda lavarla a mano con agua fría y un detergente suave. Evita utilizar productos agresivos o frotar demasiado fuerte, ya que esto puede dañar el tejido o los detalles de brocado. Si es posible, es preferible secarla al aire libre en lugar de usar la secadora. Para',
  },
  'chaqueta-cuero-negro': {
    prompt: 'chaqueta cuero negro',
    opinionBody:
      'La chaqueta de cuero negro es un clásico que nunca pasa de moda y es considerada un elemento básico en el armario de cualquier persona. Los usuarios que han comprado esta chaqueta elogian su estilo atemporal y versatilidad, ya que puede ser usada tanto en ocasiones casuales como formales. Además, mencionan su durabilidad, ya que el cuero de alta calidad garantiza que la chaqueta resistirá el paso del tiempo.',
    combineBody:
      'La chaqueta de cuero negro es increíblemente versátil y puede combinarse con una amplia variedad de prendas y estilos. Para un look casual y moderno, puedes combinarla con unos jeans ajustados y una camiseta blanca. Si buscas un estilo más sofisticado, puedes usarla sobre un vestido ajustado y agregar unos tacones altos. También puedes optar por combinarla con una falda midi y una blusa elegante para un look más elegante y femenino. Las posibilidades son infinitas, ¡sólo deja volar tu imaginación!',
    cleanBody:
      'El cuero negro es un material duradero, pero requiere de un cuidado adecuado para mantenerlo en óptimas condiciones. Para limpiar la chaqueta de cuero negro, puedes utilizar un paño húmedo y un poco de jabón suave para quitar cualquier mancha o suciedad. Evita sumergir la chaqueta en agua y nunca uses detergentes agresivos o productos químicos abrasivos. Además, es importante mantener la chaqueta alejada de la luz solar directa y almacenarla en una percha o en una funda de tela transpirable para evitar que se deforme. Recuerda también aplicar un acondicionador de cuero cada cierto tiempo para mantenerlo hidratado y flexible. Con los cuidados adecuados, tu chaqueta de cuero negro lucirá como nueva durante mucho tiempo.',
  },
  'chaqueta-cuero-verde': {
    prompt: 'chaqueta cuero verde',
    opinionBody:
      'La chaqueta de cuero verde ha recibido excelentes opiniones por parte de los usuarios. Su diseño moderno y elegante la convierte en una prenda versátil que puede ser usada tanto en ocasiones casuales como formales. El cuero de alta calidad garantiza durabilidad y resistencia, mientras que el color verde le da un toque único y llamativo. Además, los usuarios destacan lo cómoda que resulta al llevarla puesta, lo que la convierte en una opción perfecta para cualquier temporada del año.',
    combineBody:
      'La chaqueta de cuero verde ofrece infinidad de posibilidades a la hora de combinarla. Para un look casual y desenfadado, puedes llevarla con jeans y una camiseta blanca, añadiendo un toque de estilo con unas zapatillas deportivas. Si prefieres un outfit más formal, puedes combinarla con una blusa o camisa estampada y unos pantalones de vestir, completando el look con unos zapatos de tacón. Incluso puedes darle un toque más atrevido y combinarla con prendas estampadas o de colores llamativos para crear un look audaz y original.',
    cleanBody:
      'Para mantener tu chaqueta de cuero verde en buen estado, es importante seguir algunas recomendaciones. En primer lugar, asegúrate de leer las instrucciones de cuidado del fabricante, ya que cada chaqueta puede requerir un cuidado específico. En general, evita exponerla a la luz directa del sol o a fuentes de calor intensas, ya que esto puede dañar el cuero. Para limpiarla, utiliza un paño suave ligeramente humedecido en agua tibia y jabón neutro, realizando movimientos suaves y circulares. Si la chaqueta tiene manchas difíciles de eliminar, es recomendable acudir a un profesional especializado en limpieza de cuero. Finalmente, asegúrate de guardarla en un lugar seco y protegido del polvo cuando no la estés usando, preferiblemente en una funda de tela transpirable. Siguiendo estos consejos, podr',
  },
  'chaqueta-gasa-aguamarina': {
    prompt: 'chaqueta gasa aguamarina',
    opinionBody:
      'La chaqueta de gasa aguamarina ha recibido excelentes críticas por parte de nuestros clientes. Su tejido ligero y fluido proporciona una sensación de frescura y comodidad, perfecta para climas cálidos. Además, el color aguamarina brinda un toque de elegancia y sofisticación, convirtiendo esta chaqueta en un elemento imprescindible en cualquier guardarropa. Los usuarios han elogiado su versatilidad, ya que puede utilizarse tanto para eventos formales como para looks más casuales y relajados.',
    combineBody:
      'La chaqueta de gasa aguamarina es una prenda muy versátil que puede combinarse de diversas formas. Para un look elegante, puedes utilizarla sobre un vestido o un conjunto de falda y blusa en tonos neutros como el blanco o el negro. Si prefieres un estilo más informal, puedes combinarla con unos vaqueros y una camiseta básica en colores claros. También puedes darle un toque de color contrastante utilizando accesorios en tonos dorados o plateados.',
    cleanBody:
      'Para mantener tu chaqueta de gasa aguamarina en óptimas condiciones, te recomendamos seguir las siguientes instrucciones de limpieza. Primero, verifica la etiqueta de cuidado para asegurarte de seguir las indicaciones específicas del fabricante. En la mayoría de los casos, la gasa es un tejido delicado que debe lavarse a mano o en ciclo suave con agua fría. Evita el uso de blanqueadores y suavizantes, ya que podrían dañar la prenda. Para secarla, es preferible colgarla en un lugar ventilado y evitar la exposición directa al sol. Si es necesario plancharla, utiliza la temperatura más baja y coloca un paño de algodón entre la plancha y la chaqueta para evitar posibles daños en el tejido.',
  },
  'chaqueta-jacquard-rojo': {
    prompt: 'chaqueta jacquard rojo',
    opinionBody:
      'La chaqueta jacquard rojo es altamente valorada por los usuarios debido a su diseño único y llamativo. El material jacquard le da un toque de elegancia y sofisticación, mientras que el color rojo resalta y aporta un toque de pasión a cualquier atuendo. Los clientes destacan la calidad de la chaqueta y lo bien que se ajusta al cuerpo, además de su versatilidad para combinar con diferentes estilos.',
    combineBody:
      'La chaqueta jacquard rojo puede ser combinada de diversas maneras para crear diferentes looks. Para un estilo elegante y formal, puedes combinarla con una falda o pantalón negro y una blusa blanca. Si quieres lucir más casual, puedes optar por llevarla con unos jeans ajustados y una camiseta básica en tonos neutros. Incluso puedes utilizarla como capa sobre un vestido negro para agregarle un toque de color y estilo.',
    cleanBody:
      'Para mantener la chaqueta jacquard rojo en las mejores condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se sugiere leer las instrucciones específicas del fabricante en cuanto a lavado y secado. Sin embargo, en la mayoría de los casos, se recomienda lavarla a mano con agua fría y un detergente suave. Evita retorcerla o frotarla demasiado fuerte, ya que esto puede dañar el tejido. Para secarla, se aconseja colgarla en un lugar con buena ventilación y lejos de la luz directa del sol. También es recomendable mantenerla guardada en un lugar fresco y seco cuando no la estés utilizando.',
  },
  'chaqueta-lino-turquesa': {
    prompt: 'chaqueta lino turquesa',
    opinionBody:
      'Los usuarios han quedado encantados con la chaqueta de lino turquesa. Destacan su frescura y comodidad, perfecta para los días calurosos de verano. Además, el color turquesa le da un toque único y elegante, convirtiéndola en una prenda versátil para diferentes ocasiones. La calidad del lino también ha sido muy bien valorada, ya que es un material duradero y resistente.',
    combineBody:
      'La chaqueta de lino turquesa es una prenda muy versátil que puede combinarse de diversas formas. Para un look casual, puedes combinarla con unos jeans desgastados y unas zapatillas blancas, creando un estilo relajado pero con personalidad. Si buscas algo más formal, puedes usarla sobre un vestido o una falda midi en tonos neutros, como el blanco o el beige, y complementar el conjunto con unos zapatos de tacón en tonos metalizados.',
    cleanBody:
      'El lino es un material natural que requiere de ciertos cuidados para mantener su apariencia y durabilidad. Para limpiar la chaqueta de lino turquesa, es recomendable lavarla a mano con agua fría y un detergente suave. Evita retorcerla para eliminar el exceso de agua y déjala secar al aire libre, preferiblemente en un lugar con sombra. Si necesitas plancharla, hazlo mientras todavía esté ligeramente húmeda utilizando un ajuste de temperatura media-baja para evitar dañar las fibras del lino. Recuerda siempre leer las instrucciones de cuidado específicas de la prenda antes de realizar cualquier tipo de limpieza.',
  },
  'chaqueta-organdi-plata': {
    prompt: 'chaqueta organdi plata',
    opinionBody:
      'Los usuarios que han adquirido la chaqueta de organdi plata han expresado su satisfacción con la calidad y el diseño del producto. Destacan la elegancia que aporta su color plateado y el uso del material de organdi, el cual le da un aspecto ligero y delicado. Además, mencionan que es una prenda versátil que se puede usar tanto en ocasiones formales como en eventos más informales. En resumen, las opiniones son positivas y resaltan la belleza y la versatilidad de esta chaqueta.',
    combineBody:
      'La chaqueta de organdi plata es una prenda versátil que se puede combinar de diferentes formas. Para un look elegante, se puede llevar con un vestido o una falda de color negro, creando un contraste sofisticado. Si se busca un estilo más casual, se puede combinar con unos vaqueros y una camiseta blanca, añadiendo así un toque de glamour a un outfit más informal. Además, se puede complementar con accesorios plateados o en tonos neutros para resaltar aún más su color y material.',
    cleanBody:
      'Para mantener la chaqueta de organdi plata en buen estado, es importante seguir algunas recomendaciones. Se recomienda lavarla a mano con agua fría y un detergente suave, evitando el uso de productos químicos fuertes que puedan dañar el material. Es preferible no utilizar la secadora, sino dejarla secar al aire libre en una superficie plana. Además, se debe evitar el contacto con objetos que puedan enganchar o rasgar el organdi y se recomienda guardarla en un lugar fresco y seco, preferiblemente colgada, para evitar arrugas o deformaciones en el tejido.',
  },
  'chaqueta-piel-plata': {
    prompt: 'chaqueta piel plata',
    opinionBody:
      'La chaqueta de piel plateada es sin duda una elección audaz y elegante para aquellos que buscan destacar en cualquier ocasión. Los usuarios han destacado la excelente calidad del material, así como su durabilidad. Además, mencionan que esta chaqueta es versátil y puede ser combinada con diferentes estilos y prendas.',
    combineBody:
      'La chaqueta de piel plateada es muy versátil y puede ser combinada de diversas formas. Para un look sofisticado, puedes combinarla con unos pantalones negros ajustados y una blusa blanca. Si buscas un estilo más casual, puedes optar por unos vaqueros rasgados y una camiseta de colores neutros. Además, puedes experimentar combinándola con faldas o vestidos para un look más femenino y atrevido.',
    cleanBody:
      'La limpieza y el mantenimiento de una chaqueta de piel plateada es crucial para preservar su belleza y durabilidad. Se recomienda utilizar un paño húmedo y suave para limpiar las manchas superficiales. Además, es importante utilizar productos específicos para el cuidado de la piel, como cremas o sprays hidratantes, para mantenerla en buen estado. Es recomendable evitar la exposición directa al sol y guardarla en un lugar fresco y seco cuando no se esté utilizando. Con estos cuidados, tu chaqueta de piel plateada lucirá impecable por mucho tiempo.',
  },
  'chaqueta-sintetica-gris': {
    prompt: 'chaqueta sintetica gris',
    opinionBody:
      'La chaqueta sintética gris ha recibido excelentes opiniones por parte de los usuarios que la han adquirido. Su material sintético le brinda una gran resistencia y durabilidad, asegurando que te acompañará durante mucho tiempo. Además, su color gris es versátil y combina fácilmente con cualquier tipo de outfit. Los usuarios destacan que es una prenda muy cómoda y abrigada, perfecta para los días fríos. Sin duda, la chaqueta sintética gris se ha convertido en una opción muy popular entre aquellos que buscan estilo y funcionalidad.',
    combineBody:
      'La chaqueta sintética gris es una prenda versátil que se puede combinar de múltiples formas. Para un look casual y urbano, puedes combinarla con unos vaqueros negros y una camiseta básica blanca. Si prefieres un estilo más elegante, puedes llevarla con una falda lápiz negra y una blusa de seda. Para darle un toque más sofisticado, puedes añadir unos zapatos de tacón y unos accesorios de tonos plateados. En definitiva, la chaqueta sintética gris es una prenda que se adapta a cualquier estilo y te permite crear distintos outfits según la ocasión.',
    cleanBody:
      'Para limpiar y mantener tu chaqueta sintética gris en buen estado, es recomendable seguir algunas pautas. Primero, antes de limpiarla, asegúrate de revisar las instrucciones de cuidado proporcionadas por el fabricante. En general, puedes lavarla a mano o a máquina en un programa suave y con agua fría. Evita usar blanqueadores o secadoras de ropa, ya que podrían dañar el material. Para secarla, lo mejor es colgarla en un lugar con buena ventilación y alejada de la luz solar directa. Por último, si notas alguna mancha, puedes intentar eliminarla con un paño húmedo y un poco de detergente suave. Recuerda siempre realizar pruebas en una zona pequeña y discreta antes de aplicar cualquier producto de limpieza en toda la chaqueta. Con',
  },
  'chaqueta-tafetan-azul': {
    prompt: 'chaqueta tafetan azul',
    opinionBody:
      'Esta chaqueta de Tafetán azul ha recibido excelentes críticas por su estilo moderno y sofisticado. Los usuarios la elogian por su gran calidad de material y confección impecable. Además, su color azul brinda un toque elegante y versátil que se adapta a diferentes ocasiones. Sin duda, es una prenda que no puede faltar en el armario de cualquier persona amante de la moda.',
    combineBody:
      'Esta chaqueta de Tafetán azul es extremadamente versátil y puede combinarse de múltiples formas. Para un look casual chic, se puede llevar con unos jeans ajustados y una camiseta blanca. Si se busca un look más formal, puede combinarse con una falda lápiz y una blusa de seda. Además, los accesorios en tonos plateados o dorados complementarán perfectamente este atuendo. La chaqueta de Tafetán azul es una prenda muy versátil que brinda infinitas opciones de combinación.',
    cleanBody:
      'Es importante seguir las instrucciones de cuidado proporcionadas por el fabricante al limpiar y mantener la chaqueta de Tafetán azul. En general, se recomienda lavarla a mano con agua fría y detergente suave. Evita el uso de lejía y secarla a máquina, ya que esto puede dañar el material. Además, se aconseja colgarla en una percha o plegarla cuidadosamente en un lugar con buena ventilación para evitar arrugas. Siguiendo estos consejos, podrás mantener tu chaqueta de Tafetán azul en perfectas condiciones por mucho tiempo.',
  },
  'chaqueta-tafetan-verde': {
    prompt: 'chaqueta tafetan verde',
    opinionBody:
      'Los usuarios que han comprado la chaqueta tafetán verde han quedado encantados con su estilo y calidad. Destacan la suavidad y brillo del tejido de tafetán, así como su tono verde vibrante. Además, mencionan que es una prenda versátil, perfecta para combinar en diferentes ocasiones.',
    combineBody:
      'La chaqueta de tafetán verde es una pieza muy fácil de combinar. Puedes crear un look elegante y sofisticado al combinarla con un vestido negro y zapatos de tacón. Si buscas un outfit más casual, puedes usarla con unos jeans ajustados y una camiseta blanca. También puedes agregar accesorios dorados para darle un toque de glamour.',
    cleanBody:
      'Para mantener tu chaqueta de tafetán verde en perfectas condiciones, es recomendable seguir las instrucciones de lavado del fabricante. Por lo general, se recomienda lavarla en seco para evitar dañar el delicado tejido de tafetán. Si necesitas limpiar pequeñas manchas, puedes utilizar un paño húmedo y jabón suave. Recuerda guardarla en un lugar fresco y seco, lejos de la luz directa del sol, para evitar que se decolore.',
  },
  'chaqueta-tela-electrico': {
    prompt: 'chaqueta tela electrico',
    opinionBody:
      'Los usuarios han quedado impresionados con la chaqueta de tela eléctrico, destacando su increíble capacidad para ofrecer calor en condiciones extremas. Además, el material de alta calidad garantiza durabilidad y resistencia al desgaste. Los colores disponibles también han sido muy apreciados, ya que permiten a los usuarios elegir el que mejor se adapte a su estilo personal.',
    combineBody:
      'La versatilidad de la chaqueta de tela eléctrico hace que sea fácil de combinar con diferentes prendas y estilos. Para un look casual y deportivo, puedes combinarla con unos jeans y zapatillas. Si prefieres un estilo más elegante, puedes optar por combinarla con pantalones de vestir y zapatos de cuero. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para garantizar una larga vida útil a tu chaqueta de tela eléctrico, es importante seguir algunas recomendaciones de limpieza y mantenimiento. Se recomienda leer las instrucciones del fabricante para asegurarse de utilizar el método de limpieza adecuado. En general, se recomienda lavar a mano con agua fría y detergente suave. Evita el uso de blanqueador y plancha. Siempre es bueno almacenarla en un lugar seco y protegido del sol cuando no esté en uso. De esta manera, podrás disfrutar de tu chaqueta de tela eléctrico durante mucho tiempo.',
  },
  'corbata-artificial-esmeralda': {
    prompt: 'corbata artificial esmeralda',
    opinionBody:
      'Los usuarios que han adquirido la corbata artificial esmeralda han quedado gratamente sorprendidos por su alta calidad y apariencia real. El material utilizado brinda una textura suave y resistente, lo que garantiza que la corbata se mantenga impecable durante todo el día. Además, el color esmeralda agrega un toque de elegancia y sofisticación a cualquier conjunto. Sin duda, esta corbata esmeralda artificial es una excelente elección para aquellos que buscan lucir con estilo y originalidad en cualquier ocasión.',
    combineBody:
      'La corbata artificial esmeralda es extremadamente versátil y puede combinarse de diferentes maneras para lograr diversos estilos. Para eventos formales, se recomienda combinarla con un traje negro o azul marino, ya que creará un contraste audaz y elegante. Si deseas un atuendo más casual, puedes combinarla con una camisa blanca y pantalones chinos en tonos neutros. También puedes agregar un blazer o chaqueta en tonos oscuros para complementar el look. En definitiva, la corbata artificial esmeralda es una elección atemporal que añadirá un toque de estilo y distinción a cualquier conjunto.',
    cleanBody:
      'Mantener tu corbata artificial esmeralda en perfecto estado es bastante sencillo. Para eliminar manchas y suciedad, te recomendamos utilizar un paño húmedo y un poco de detergente suave. Frota suavemente la zona afectada y luego aclara con agua limpia. Evita utilizar productos químicos agresivos o sumergir la corbata en agua, ya que esto podría dañar el material. Además, cuando no la uses, es importante guardar la corbata en un lugar fresco y seco, preferiblemente colgada o enrollada en un organizador de corbatas para evitar arrugas. Siguiendo estos sencillos consejos, podrás mantener tu corbata artificial esmeralda en perfectas condiciones durante mucho tiempo.',
  },
  'corbata-cachemira-oro': {
    prompt: 'corbata cachemira oro',
    opinionBody:
      'La corbata de cachemira en color oro ha recibido excelentes opiniones por parte de nuestros usuarios. La combinación del lujoso material de cachemira con el vibrante color dorado añade un toque de sofisticación a cualquier conjunto. Los usuarios destacan la suavidad y calidad de la cachemira, así como la durabilidad de la corbata. Sin duda, este accesorio es perfecto para ocasiones especiales y eventos formales.',
    combineBody:
      'La corbata de cachemira en color oro es extremadamente versátil a la hora de combinarla con el resto del atuendo. Puede ser la pieza central de un look elegante y audaz, combinando con un traje negro o azul oscuro. Para un estilo más relajado, puedes combinarla con una camisa blanca y unos pantalones chinos en tonos neutros. También se ve increíble con un traje de tweed marrón o verde oscuro. Sea cual sea tu elección, la corbata de cachemira oro siempre añadirá un toque de glamour a tu look.',
    cleanBody:
      'La corbata de cachemira en color oro requiere de cuidados especiales para mantenerla en óptimas condiciones. Para eliminar manchas superficiales, se recomienda utilizar un paño húmedo y suave frotando suavemente sobre la área afectada. Evita el uso de productos químicos agresivos, ya que podrían dañar la delicada fibra de cachemira. Se recomienda también guardar la corbata en un lugar donde no esté expuesta a la luz solar directa o a la humedad. Además, es importante evitar doblarla con fuerza para evitar la formación de arrugas permanentes. Sigue estos sencillos consejos de limpieza y mantenimiento y tu corbata de cachemira oro lucirá impecable durante mucho tiempo.',
  },
  'corbata-denim-gris': {
    prompt: 'corbata denim gris',
    opinionBody:
      'La corbata denim gris es una opción elegante y moderna para cualquier ocasión. Los usuarios han destacado su calidad y durabilidad, asegurando que se mantiene en perfectas condiciones a lo largo del tiempo. Además, su diseño versátil permite combinarla con distintos estilos de vestimenta, convirtiéndola en un accesorio imprescindible en el guardarropa de cualquier hombre sofisticado. Sin duda, la corbata denim gris es una elección acertada para aquellos que buscan destacar con estilo y originalidad.',
    combineBody:
      'La corbata denim gris es muy versátil y se puede combinar con una amplia variedad de colores y estilos. Para un look más formal, se puede utilizar con una camisa blanca y un traje oscuro, creando un contraste elegante y sofisticado. Si se busca un estilo más casual, se puede combinar con una camisa de rayas y unos pantalones chinos en tonos neutros. Para agregar un toque de color, se puede optar por un pañuelo de bolsillo en tonos azules o rojos. En definitiva, la corbata denim gris es una pieza clave para crear looks modernos y con estilo.',
    cleanBody:
      'La corbata denim gris requiere de un cuidado especial para mantenerla en óptimas condiciones. Es recomendable evitar derrames de líquidos sobre ella, ya que el denim puede absorber manchas con facilidad. En caso de que se ensucie, se puede utilizar un paño húmedo con agua tibia y jabón suave para limpiarla suavemente. Es importante evitar el uso de componentes químicos fuertes o frotar enérgicamente, ya que podrían dañar la tela. Para el almacenamiento, es recomendable colgarla en un gancho o enrollarla suavemente y guardarla en una caja de corbatas para evitar arrugas. Siguiendo estos consejos, podrás disfrutar de tu corbata denim gris en perfectas condiciones durante mucho tiempo.',
  },
  'corbata-encaje-marron': {
    prompt: 'corbata encaje marron',
    opinionBody:
      'La corbata de encaje marrón es altamente valorada por su estilo elegante y sofisticado. Muchos usuarios elogian la calidad del encaje utilizado en esta corbata, ya que agrega un toque de originalidad y distinción a cualquier atuendo. Además, el color marrón es muy versátil y puede combinarse fácilmente con una variedad de trajes y camisas. En resumen, los usuarios están encantados con esta corbata de encaje marrón por su diseño único y su capacidad para realzar cualquier look.',
    combineBody:
      'La corbata de encaje marrón es una opción excelente para complementar numerosos estilos y ocasiones. Se puede combinar de forma elegante con un traje negro o gris, agregando ese toque de sofisticación y originalidad. También puede ser una excelente elección para un atuendo más casual, combinándola con una camisa blanca y un pantalón de color neutro. Incluso se puede usar con un estilo más creativo, combinándola con colores más atrevidos para crear un contraste de moda. En definitiva, la corbata de encaje marrón es una prenda versátil que puede adaptarse a diferentes looks con facilidad.',
    cleanBody:
      'Para mantener la corbata de encaje marrón en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a mano con agua fría y jabón suave, evitando frotar demasiado para no dañar el encaje. Después de lavarla, se debe secar colgada en un lugar fresco y sin exposición directa al sol para evitar que se deforme. Si es necesario plancharla, se debe hacer a baja temperatura y utilizando un paño de algodón para proteger el encaje. Siguiendo estos cuidados, la corbata de encaje marrón se mantendrá en excelente estado y lista para lucir en cualquier ocasión.',
  },
  'corbata-jacquard-azul': {
    prompt: 'corbata jacquard azul',
    opinionBody:
      'La corbata jacquard azul es una opción elegante y sofisticada para cualquier ocasión. Los usuarios que han adquirido este producto destacan su excelente calidad y diseño. El material jacquard utilizado en esta corbata le brinda un aspecto único y distinguido. Además, el color azul es altamente versátil y combina fácilmente con diferentes estilos de vestimenta. En general, las opiniones sobre esta corbata son muy positivas, ya que brinda un toque de distinción y estilo a cualquier conjunto.',
    combineBody:
      'La corbata jacquard azul es una pieza versátil que puede combinarse de múltiples formas. Para un look clásico y elegante, esta corbata queda perfecta con un traje negro o gris oscuro. Si buscas un estilo más moderno y casual, puedes combinarla con una camisa blanca y un pantalón chino en color beige. Para agregar un toque de color, puedes optar por un pañuelo de bolsillo en tonos complementarios como el gris o el azul marino. En resumen, la corbata jacquard azul es una elección segura que se adapta a diferentes outfits y estilos.',
    cleanBody:
      'Para garantizar una larga vida útil a tu corbata jacquard azul, es importante seguir algunos consejos de limpieza y mantenimiento. Para eliminar manchas o suciedad leve, puedes utilizar un paño húmedo y frotar suavemente sobre la zona afectada. En caso de manchas difíciles, se recomienda llevar la corbata a una tintorería especializada. Evita doblarla de forma brusca y opta por colgarla en un organizador de corbatas para evitar que se arrugue. Además, es importante guardarla en un lugar fresco y seco, alejada de la luz directa del sol, para evitar que el color se desvanezca. Siguiendo estos simples pasos, podrás mantener tu corbata jacquard azul en perfectas condiciones por mucho tiempo.',
  },
  'corbata-jacquard-celeste': {
    prompt: 'corbata jacquard celeste',
    opinionBody:
      'Los usuarios que han adquirido la corbata Jacquard celeste la describen como una opción elegante y sofisticada para cualquier ocasión. El tejido Jacquard le da un toque de distinción, mientras que el color celeste añade frescura y vitalidad al conjunto. Los clientes resaltan la calidad de los materiales y la atención al detalle en la confección de esta corbata. Sin duda, es una elección acertada para lucir un look refinado y destacar en cualquier evento.',
    combineBody:
      'La corbata Jacquard celeste es extremadamente versátil y se puede combinar de varias formas. Para un look más formal, puedes lucirla con un traje azul marino y una camisa blanca. Este conjunto clásico añadirá elegancia y sofisticación a tu imagen. Para un estilo más casual, puedes combinarla con un pantalón chino beige y una camisa a rayas en tonos neutros. Este contraste de colores le dará un toque moderno y desenfadado a tu look. No olvides complementar el conjunto con unos zapatos de cuero marrón para un acabado impecable.',
    cleanBody:
      'Para mantener tu corbata Jacquard celeste en óptimas condiciones, es recomendable seguir algunos consejos de limpieza y cuidado. En primer lugar, evita derramar líquidos sobre la corbata. En caso de manchas, utiliza un paño húmedo para tratarlas suavemente. No apliques fuertes frotaciones, ya que podrías dañar el tejido. Si necesitas limpiarla en profundidad, es aconsejable llevarla a una tintorería especializada. Además, para evitar arrugas, es importante desatar siempre el nudo de la corbata después de utilizarla y colgarla en una percha para que se airee y mantenga su forma original. Siguiendo estos simples cuidados, tu corbata Jacquard celeste lucirá impecable por mucho tiempo.',
  },
  'corbata-jersey-negro': {
    prompt: 'corbata jersey negro',
    opinionBody:
      'La corbata de jersey negro es un accesorio muy versátil y elegante que definitivamente debe estar en el armario de cualquier hombre sofisticado. El material de jersey le da un aspecto moderno y casual, perfecto para ocasiones informales o más relajadas. Además, el color negro es clásico y atemporal, lo que la convierte en una opción segura y elegante para cualquier outfit. Los usuarios que han adquirido esta corbata destacan su comodidad, ya que el material de jersey es suave y flexible, lo que la hace muy fácil de ajustar y de llevar durante todo el día. Además, su estilo versátil permite combinarla con una amplia variedad de prendas.',
    combineBody:
      'La corbata de jersey negro es extremadamente versátil y puede combinarse con una gran variedad de prendas para crear looks elegantes y modernos. Por ejemplo, puedes lucirla con una camisa blanca y un traje gris para conseguir un estilo clásico y sobrio. Si deseas un look más casual pero igualmente sofisticado, puedes combinarla con una camisa de denim y unos pantalones chinos. También puedes agregar un toque de color combinándola con una camisa de tonos azules o con estampados sutiles. En resumen, la corbata de jersey negro es una opción segura y elegante que se adapta fácilmente a cualquier estilo y ocasión.',
    cleanBody:
      'Para mantener tu corbata de jersey negro en perfectas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. Lo primero es evitar derramar líquidos sobre ella y protegerla de manchas que puedan ser difíciles de eliminar. En caso de que la corbata se ensucie, es recomendable lavarla a mano con agua fría y un detergente suave. Evita retorcerla para eliminar el exceso de agua y sécala al aire libre, evitando exponerla directamente a la luz solar. Si necesitas plancharla, utiliza una temperatura baja y colócala sobre un paño húmedo para evitar dañar el material. Siguiendo estos s',
  },
  'corbata-lana-morado': {
    prompt: 'corbata lana morado',
    opinionBody:
      'La corbata de lana morado es sin duda una opción elegante y sofisticada para cualquier ocasión. Su material de lana le brinda un aspecto y una textura únicos, lo que la convierte en una elección popular entre aquellos que buscan destacar con estilo. Los usuarios han elogiado su suavidad y calidad, ya que la lana es conocida por ser duradera y de alta calidad. Además, el color morado agrega un toque de originalidad, ideal para aquellos que desean desafiar lo convencional y destacar en cualquier evento. En general, las opiniones sobre la corbata de lana morado son muy positivas, destacando su estilo, calidad y versatilidad.',
    combineBody:
      'La corbata de lana morado es una pieza versátil que puede combinarse de múltiples formas. Para looks formales, se puede combinar con un traje negro o gris oscuro, agregando un toque de color y sofisticación al conjunto. También puede ser una opción interesante para combinar con una camisa blanca y un chaleco a juego, creando un look elegante pero con un toque de originalidad. Para looks más casuales, se puede combinar con una camisa de rayas en tonos neutros y un pantalón de vestir o jeans oscuros. En definitiva, las posibilidades son infinitas a la hora de combinar la corbata de lana morado, lo que la convierte en una elección versátil y audaz.',
    cleanBody:
      'La corbata de lana morado requiere cuidados especiales para mantener su aspecto y calidad a lo largo del tiempo. Lo ideal es limpiarla en seco para evitar daños en el tejido de lana. En caso de manchas, se recomienda usar un paño húmedo con agua tibia y jabón suave, evitando frotar enérgicamente para no dañar la fibra de lana. Es importante evitar el contacto con líquidos que puedan causar manchas permanentes, como el vino o el aceite. Para el almacen',
  },
  'corbata-nylon-amarillo': {
    prompt: 'corbata nylon amarillo',
    opinionBody:
      'La corbata de nylon amarillo ha recibido excelentes comentarios por parte de nuestros usuarios. Su material de nylon la hace resistente y duradera, perfecta para aquellos que buscan una corbata de calidad. El color amarillo le da un toque vibrante y llamativo, siendo ideal para eventos más informales o para agregar un toque de color a un conjunto más sobrio. Además, la corbata de nylon amarillo es fácil de ajustar y mantener su forma durante todo el día, lo que hace que sea una opción muy popular entre nuestros clientes.',
    combineBody:
      'La corbata de nylon amarillo es extremadamente versátil y puede combinarse de diferentes formas para lograr múltiples looks. Si deseas un estilo elegante y sobrio, puedes combinarla con un traje negro o gris oscuro y una camisa blanca. Para un look más audaz y moderno, puedes optar por combinarla con un traje azul marino y una camisa a rayas en tonos azules o blancos. Si quieres un look más casual, puedes combinarla con una camisa de manga corta en tonos neutros y pantalones chinos de color beige. ¡Las posibilidades son infinitas!',
    cleanBody:
      'La corbata de nylon amarillo es muy fácil de limpiar y mantener. Para eliminar las manchas o derrames, simplemente utiliza un paño húmedo con agua tibia y jabón suave para frotar suavemente la zona afectada. Evita utilizar productos químicos fuertes o frotar con demasiada fuerza, ya que podrían dañar el material de nylon. Si necesitas planchar la corbata, utiliza una plancha a baja temperatura y coloca un paño de algodón sobre la corbata para evitar dañarla. Recuerda guardarla en un lugar seguro para evitar que se arrugue o se dañe. Con estos sencillos cuidados, tu corbata de nylon amarillo se mantendrá en perfectas condiciones por mucho tiempo.',
  },
  'corbata-piel-amarillo': {
    prompt: 'corbata piel amarillo',
    opinionBody:
      'Las opiniones sobre la corbata de piel amarillo son muy positivas. Los usuarios destacan su aspecto elegante y sofisticado, así como la suavidad y durabilidad del material. Además, el color amarillo vibrante agrega un toque de originalidad y estilo a cualquier atuendo. Sin duda, es una excelente opción para aquellos que buscan destacar con un toque de color en sus conjuntos formales.',
    combineBody:
      'La corbata de piel amarillo es muy versátil a la hora de combinarla. Puedes crear conjuntos elegantes y clásicos combinándola con un traje negro o gris, que permitirá que el color amarillo destaque aún más. También puedes optar por un look casual y moderno combinándola con una camisa blanca y un blazer azul marino. Para añadir un toque de contraste, puedes agregar accesorios en tonos neutros como cinturones y zapatos en color marrón.',
    cleanBody:
      'Para mantener la corbata de piel amarillo en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, evita derramar líquidos sobre ella y protégela de la humedad. Si se ensucia, puedes limpiarla suavemente con un paño húmedo y un poco de jabón neutro. Si la mancha es persistente, es recomendable acudir a una tintorería especializada en prendas de piel para su limpieza profesional. Asimismo, es importante guardarla correctamente en un lugar seco y alejado de la luz directa del sol para evitar su deterioro.',
  },
  'corbata-piel-naranja': {
    prompt: 'corbata piel naranja',
    opinionBody:
      'La corbata de piel en color naranja es una opción audaz y llamativa para aquellos hombres que desean destacar en cualquier ocasión. Su material de piel le confiere un aspecto sofisticado y elegante, mientras que el color naranja le añade un toque de frescura y originalidad.',
    combineBody: 'Con qué combinar la corbata de piel en color naranja',
    cleanBody:
      'Si se prefiere un look más informal, esta corbata puede ser usada con una camisa de cuadros o rayas en tonos neutros, creando un contraste de colores y patrones. Incluso se puede combinar con un suéter de cuello alto y unos pantalones chinos para un look más relajado pero estiloso.',
  },
  'corbata-punto-azul': {
    prompt: 'corbata punto azul',
    opinionBody:
      'La corbata de punto azul ha recibido excelentes comentarios por parte de los usuarios. El diseño de punto añade un toque de elegancia y sofisticación, mientras que el color azul brinda frescura y versatilidad. Los usuarios destacan la calidad del material y su durabilidad, lo que la convierte en una excelente opción para lucir impecable en cualquier ocasión. Además, muchos mencionan que el punto azul es ideal para añadir un toque de estilo a trajes formales y casuales.',
    combineBody:
      'La corbata de punto azul es extremadamente versátil y puede ser combinada de diversas maneras. Para un estilo clásico y elegante, puedes combinarla con un traje gris o negro, una camisa blanca y unos zapatos de cuero. Si prefieres un look más casual y moderno, puedes optar por combinarla con una camisa de denim y un pantalón chino beige. Además, este tono de azul se ve increíblemente bien con colores como el gris, el blanco, el negro y el rosa pálido, lo que te permite crear combinaciones únicas y originales.',
    cleanBody:
      'Para mantener tu corbata de punto azul en perfectas condiciones, es importante seguir algunos consejos de limpieza y cuidado. Si la corbata sufre alguna mancha, es recomendable tratarla de inmediato con un paño húmedo y jabón neutro. Evita lavarla en lavadora, ya que el agua y la fricción pueden dañar el tejido. En su lugar, puedes llevarla a una tintorería especializada para su limpieza en seco. Para conservar su forma, evita doblarla de manera excesiva y guárdala en un organizador de corbatas o colgada en una percha. Siguiendo estos simples consejos, podrás disfrutar de tu corbata de punto azul durante mucho tiempo.',
  },
  'corbata-punto-verde': {
    prompt: 'corbata punto verde',
    opinionBody:
      'La corbata de punto verde es el accesorio perfecto para aquellos que buscan agregar un toque de color a sus outfits. Su material de punto le da un aspecto único y elegante. Los usuarios han elogiado su estilo moderno y versatilidad, ya que puede ser usado tanto en eventos formales como informales. Además, el color verde es muy favorecedor y agrega un toque de frescura a cualquier conjunto. Sin duda, la corbata de punto verde es una elección acertada para aquellos que desean destacarse con estilo en cualquier ocasión.',
    combineBody:
      'La corbata de punto verde es un accesorio versátil que se puede combinar de muchas maneras. Para un look clásico y elegante, puedes combinarla con una camisa blanca y un traje negro. Si buscas algo más casual pero igualmente chic, puedes usarla con una camisa de rayas en tonos neutros y un pantalón chino beige. Para agregar un toque de color adicional, puedes complementarla con una chaqueta azul marino. En resumen, la corbata de punto verde se puede combinar de múltiples formas y siempre agregará un toque de estilo a tu outfit.',
    cleanBody:
      'Para mantener tu corbata de punto verde en óptimas condiciones, es importante seguir algunas instrucciones de limpieza y cuidado. En primer lugar, evita derramar líquidos sobre ella y protégela de manchas. Si necesitas limpiarla, te recomendamos llevarla a una tintorería profesional para garantizar su correcto cuidado. Si prefieres hacerlo en casa, puedes utilizar un paño húmedo y suave para eliminar manchas superficiales. Nunca utilices lavadora ni secadora, ya que podrían dañar el material. Por último, es recomendable guardarla en un lugar oscuro y libre de humedad, preferiblemente colgada en un gancho o enrollada en papel de seda. Siguiendo estos sencillos consejos, podrás disfrutar de tu corbata de punto verde por mucho tiempo.',
  },
  'corbata-vaquera-gris': {
    prompt: 'corbata vaquera gris',
    opinionBody:
      'La corbata vaquera gris ha sido muy bien recibida por aquellos que buscan un toque moderno y casual en su vestimenta. Su material de mezclilla le da un aspecto único y atractivo, mientras que el color gris le brinda versatilidad para combinar con diferentes trajes. Los usuarios destacan que es una excelente opción para eventos informales y lucir elegante sin perder ese toque desenfadado.',
    combineBody:
      'La corbata vaquera gris puede ser una opción interesante para combinar con camisas de tonos claros, como blanco o azul pastel. También se puede optar por un conjunto de pantalón y chaqueta en colores neutros, como el negro o el gris oscuro, para crear un contraste interesante. Si deseas un estilo aún más informal, puedes combinarla con una camiseta de manga larga y unos jeans ajustados para un look urbano y moderno.',
    cleanBody:
      'Para mantener tu corbata vaquera gris en buen estado, es recomendable seguir las instrucciones de lavado que vienen indicadas en la etiqueta. En general, se sugiere lavarla a mano con agua fría y un detergente suave. Evita retorcer o frotar en exceso, ya que podrías dañar el tejido. Para secarla, lo mejor es colgarla en un lugar fresco y bien ventilado, evitando la exposición directa al sol. Recuerda plancharla a baja temperatura y con vapor, si es necesario, para eliminar arrugas. Siguiendo estos consejos, podrás disfrutar de tu corbata vaquera gris por mucho tiempo.',
  },
  'falda-encaje-morado': {
    prompt: 'falda encaje morado',
    opinionBody:
      'Los usuarios han quedado encantados con la falda de encaje morado. Destacan la calidad del material y cómo resalta el color. Además, el diseño elegante y femenino ha recibido elogios por parte de quienes la han adquirido. Sin duda, es una prenda que llama la atención y realza cualquier outfit.',
    combineBody:
      'Esta falda de encaje morado es realmente versátil y puede combinar con una gran variedad de prendas y colores. Para un look sofisticado, puedes combinarla con una blusa blanca y tacones negros. Si prefieres un estilo más casual, quedará genial con una camiseta estampada y zapatillas blancas. No tengas miedo de experimentar y mezclar distintas prendas para lograr un conjunto único y a la moda.',
    cleanBody:
      'Para mantener la falda de encaje morado en perfectas condiciones, te recomendamos seguir las instrucciones de lavado del fabricante. En la mayoría de los casos, se aconseja lavarla a mano con agua fría y detergente suave. Evita retorcerla y secarla al sol directamente para evitar que se dañe el color o la forma del encaje. Al guardarlo, es importante colgarlo en una percha para evitar arrugas. Recuerda tratar cualquier mancha de inmediato para evitar que se adhiera al tejido.',
  },
  'falda-jacquard-rojo': {
    prompt: 'falda jacquard rojo',
    opinionBody:
      'Los usuarios que han adquirido la falda jacquard rojo han quedado encantados con su calidad y diseño. Destacan la suavidad del material y lo cómoda que resulta al llevarla puesta. Además, el color rojo intenso de la falda aporta un toque de elegancia y estilo a cualquier conjunto. Sin duda, es una prenda perfecta para lucir en ocasiones especiales o para darle un toque sofisticado a un look casual.',
    combineBody:
      'La versatilidad de la falda jacquard rojo permite combinarla de diversas formas para crear diferentes estilos. Si buscas un look más formal, puedes combinarla con una blusa blanca y unos tacones negros. Para un look más casual, puedes optar por una camiseta básica de color neutro y unas zapatillas blancas. Si quieres destacar el color rojo de la falda, puedes añadir complementos en tonos dorados o negros. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu falda jacquard rojo en perfecto estado, te recomendamos seguir estas sencillas pautas de limpieza y cuidado. Lávala a mano con agua fría y un detergente suave, evitando frotar en exceso para evitar dañar el tejido. Si prefieres lavarla a máquina, utiliza el programa para prendas delicadas y colócala dentro de una bolsa de malla para protegerla. Para secarla, es mejor dejarla al aire libre en una superficie plana, evitando la exposición directa al sol. Recuerda plancharla a baja temperatura, preferiblemente del revés, para evitar dañar el tejido. Siguiendo estos consejos, disfrutarás de tu falda jacquard rojo durante mucho tiempo.',
  },
  'falda-lana-electrico': {
    prompt: 'falda lana electrico',
    opinionBody:
      'Los usuarios han destacado la comodidad y calidez de esta falda de lana en color eléctrico. Su material de alta calidad garantiza una sensación suave y agradable al tacto, además de mantener el calor en los días fríos. Además, su color eléctrico la convierte en una prenda llamativa y moderna, perfecta para destacar en cualquier ocasión.',
    combineBody:
      'Esta falda de lana en color eléctrico puede ser combinada de múltiples formas para crear diferentes estilos. Puedes optar por una blusa o camisa blanca para un look elegante y minimalista. Para un toque más audaz, puedes combinarla con una chaqueta de cuero en tonos oscuros. También puedes jugar con los contrastes y combinarla con una camisa estampada o una sudadera de colores vivos para un look más casual y divertido.',
    cleanBody:
      'Para que tu falda de lana eléctrico conserve su aspecto y calidad por más tiempo, es recomendable lavarla a mano con agua fría y un detergente suave. Evita retorcerla o frotarla con fuerza para evitar dañar las fibras de la lana. Es importante secarla en posición horizontal para evitar deformaciones. Además, puedes cepillarla suavemente con un cepillo de cerdas suaves para eliminar cualquier pelo o pelusa acumulada. Recuerda guardarla en un lugar adecuado, protegida del polvo y la humedad, para conservarla en óptimas condiciones.',
  },
  'falda-lana-morado': {
    prompt: 'falda lana morado',
    opinionBody:
      'Las opiniones sobre la falda de lana morado son muy favorables entre los usuarios. Han destacado la calidad de la lana utilizada, que proporciona una sensación de calidez y suavidad al usarla. Además, el color morado es muy llamativo y permite crear combinaciones de outfits originales. En general, los usuarios están satisfechos con esta prenda y la recomiendan como una opción elegante y cómoda para los días fríos.',
    combineBody:
      'La falda de lana morado es una prenda versátil que puede ser combinada de diferentes formas. Debido a su color llamativo, puede ser el centro de atención en un outfit. Se puede combinar con una blusa blanca o negra para lograr un contraste elegante, o con colores similares como tonos de morado más claros o violeta. Además, se pueden añadir accesorios en tonos neutros como beige o gris, o incluso atreverse con estampados florales o geométricos para un look más divertido.',
    cleanBody:
      'Para mantener la falda de lana morado en buen estado, es importante seguir las indicaciones de cuidado en la etiqueta de la prenda. Se recomienda lavarla a mano en agua fría con detergente suave para prendas delicadas. Evitar el uso de blanqueadores o productos químicos agresivos que puedan dañar la lana. Es aconsejable secar la falda en posición horizontal para evitar que se deforme. Además, se puede utilizar un cepillo suave para peinar la lana y evitar la formación de pelusas. Siguiendo estos cuidados, la falda de lana morado lucirá como nueva durante mucho tiempo.',
  },
  'falda-lino-beige': {
    prompt: 'falda lino beige',
    opinionBody:
      'La falda de lino beige es una elección popular entre las aficionadas a la moda debido a su versatilidad y comodidad. Muchas personas opinan que el lino es un material ligero y transpirable, ideal para los días calurosos de verano. Además, el color beige es atemporal y fácil de combinar, lo que la convierte en una prenda básica en cualquier armario. Los usuarios también destacan la calidad y durabilidad del tejido de lino, lo que hace que esta falda sea una inversión segura a largo plazo.',
    combineBody:
      'La falda de lino beige es extremadamente versátil y puede combinarse de muchas maneras diferentes. Para un look casual y relajado, puedes combinarla con una camiseta blanca básica y sandalias de cuero. Si buscas un estilo más elegante, puedes optar por una blusa de seda en tonos neutros y unos tacones altos. Otra opción es combinarla con una chaqueta de jean y zapatillas para un look urbano y moderno. Como el color beige es neutral, tienes infinitas posibilidades de combinación con diferentes estilos y colores en tu armario.',
    cleanBody:
      'Para limpiar y mantener tu falda de lino beige en óptimas condiciones, es importante seguir algunas recomendaciones. En primer lugar, verifica la etiqueta de cuidado de la prenda para seguir las instrucciones del fabricante. Por lo general, el lino puede lavarse a máquina con agua fría, pero es recomendable utilizar un programa delicado y una bolsa de lavado para proteger la tela. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar el tejido. Para secar, se recomienda colgar la falda en una percha y dejarla secar al aire libre. En caso de arrugas, puedes plancharla a temperatura media, preferiblemente cuando aún esté un poco húmeda. Recuerda evitar el exceso de calor y utilizar un paño de algodón entre la plancha y la falda para protegerla. Siguiendo estos cuidados simples, podrás disfrutar de tu fal',
  },
  'falda-punto-esmeralda': {
    prompt: 'falda punto esmeralda',
    opinionBody:
      'Los usuarios han quedado encantados con esta falda de punto en color esmeralda. Destacan su suave textura y lo cómoda que es de llevar. Además, el color esmeralda le da un toque elegante y sofisticado, perfecto para cualquier ocasión. También resaltan la calidad del material y lo bien que se adapta al cuerpo, realzando la figura. Sin duda, una prenda imprescindible en el armario.',
    combineBody:
      'La falda de punto en color esmeralda es muy versátil y se puede combinar de diferentes formas. Para un look casual, puedes llevarla con una camiseta blanca básica y zapatillas deportivas. Si buscas algo más elegante, combínala con una blusa de seda en tonos neutros y unos tacones. También puedes añadir complementos como un cinturón o una bufanda en tonos marrones para darle un toque más original. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu falda de punto en color esmeralda en buen estado, te recomendamos seguir estas recomendaciones de limpieza. Lávala a mano con agua fría y un detergente suave, evitando el uso de lejía o productos químicos agresivos. Sécala al aire libre, evitando la exposición directa al sol para evitar la decoloración del color esmeralda. Para mantener su forma original, es aconsejable guardarla en un lugar oscuro y sin doblarla. Siguiendo estos consejos, disfrutarás de tu falda por mucho más tiempo.',
  },
  'falda-punto-rojo': {
    prompt: 'falda punto rojo',
    opinionBody:
      'Con qué combinar la falda de punto rojo: Esta falda de punto rojo se puede combinar de diversas maneras. Para un look casual puedes optar por una camisa blanca y unas zapatillas blancas. Si buscas un look más elegante, combínala con una blusa negra y unos tacones altos. También puedes agregar accesorios dorados para resaltar aún más el color de la falda.',
    combineBody: null,
    cleanBody: null,
  },
  'falda-seda-oro': {
    prompt: 'falda seda oro',
    opinionBody:
      'Nuestra falda de seda en color oro ha recibido excelentes críticas de nuestros clientes. El material de seda le da a la falda un aspecto elegante y lujoso, perfecto para ocasiones especiales o eventos formales. Además, el color oro agrega un toque sofisticado y glamuroso a cualquier look. Los usuarios destacan la suavidad y ligereza del tejido, lo que proporciona comodidad durante todo el día. Sin duda, esta falda de seda en color oro recibe elogios por su estilo y calidad.',
    combineBody:
      'La versatilidad de la falda de seda oro permite combinarla de diferentes maneras para adaptarse a cualquier ocasión. Para un look elegante y refinado, puedes combinarla con una blusa de seda en tonos neutros como el blanco o el negro. Si deseas darle un toque más festivo, puedes usarla con una blusa en tonos dorados o con estampados llamativos. Para un estilo más casual pero sofisticado, puedes combinarla con una camiseta de algodón en color blanco y unos zapatos de tacón bajo. En definitiva, la falda de seda oro puede ser un gran protagonista en cualquier conjunto, solo debes jugar con los complementos adecuados.',
    cleanBody:
      'Para mantener tu falda de seda oro en perfectas condiciones, es importante seguir algunos consejos de limpieza y cuidado. La seda es un material delicado que requiere un tratamiento especial. Se recomienda lavar a mano en agua fría con un detergente suave. Evita retorcer o frotar la falda, ya que esto puede dañar las fibras de seda. Después de lavar, sécala al aire libre, evitando la exposición directa al sol. Si es necesario plancharla, utiliza una temperatura baja y coloca un paño fino entre la plancha y la falda para protegerla. Al guardarla, colócala en una bolsa de tela transpirable para evitar que se arrugue o se ensucie. Siguiendo estos consejos, podrás disfrutar de tu fal',
  },
  'falda-tela-negro': {
    prompt: 'falda tela negro',
    opinionBody:
      'El estilo clásico de la falda de tela negro ha sido apreciado por generaciones. Su versatilidad y elegancia la convierten en una prenda imprescindible en el armario de cualquier mujer. Muchas personas destacan la comodidad y la calidad del material, ya que la tela negra es suave y resistente. Además, su color neutro permite combinarla fácilmente con diferentes prendas y accesorios.',
    combineBody:
      'La falda de tela negro es una prenda muy versátil que puede combinarse de múltiples formas. Para un look elegante y sofisticado, puedes combinarla con una blusa blanca y unos tacones altos negros. Si prefieres un estilo más casual, puedes optar por una camiseta a rayas y unas zapatillas blancas. También puedes darle un toque más edgy con una camiseta de bandas y unas botas de cuero. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu falda de tela negro en óptimas condiciones, es importante seguir algunas recomendaciones de cuidado. Lo ideal es lavarla en agua fría utilizando detergente suave. Evita utilizar lejía o productos químicos que puedan dañar el color. Si es posible, lava la falda a mano para evitar el desgaste. Después de lavarla, déjala secar al aire libre y evita exponerla directamente al sol, ya que esto puede desvanecer el color. Si es necesario, plancha la falda a baja temperatura y evita planchar directamente sobre el estampado o adornos. Con los cuidados adecuados, tu falda de tela negro lucirá como nueva por mucho tiempo.',
  },
  'gabardina-artificial-beige': {
    prompt: 'gabardina artificial beige',
    opinionBody:
      'La gabardina artificial beige es una prenda de vestir versátil y muy popular entre los amantes de la moda. Los usuarios destacan su comodidad y ligereza, ideal para cualquier ocasión. Además, el material artificial proporciona una buena resistencia al agua y a las arrugas, lo que la convierte en una opción perfecta para los días lluviosos. Sin duda, es una prenda imprescindible en cualquier guardarropa.',
    combineBody:
      'La gabardina artificial beige es una prenda neutra que se puede combinar con una gran variedad de colores y estilos. Para un look casual, puedes llevarla con unos jeans y una camiseta blanca. Si prefieres un estilo más elegante, puedes combinarla con un vestido negro y unos tacones. Además, puedes jugar con accesorios como bufandas o sombreros para darle un toque personalizado.',
    cleanBody:
      'Para mantener tu gabardina artificial beige en buen estado, es importante seguir algunas pautas de limpieza y cuidado. En primer lugar, es recomendable leer las instrucciones de lavado del fabricante, ya que algunos materiales artificiales pueden requerir limpieza en seco. Si se puede lavar en casa, es aconsejable hacerlo a baja temperatura y con un detergente suave. Evita utilizar suavizantes, ya que pueden afectar la impermeabilidad del tejido. Finalmente, cuelga la gabardina para que se seque al aire libre, evitando la exposición directa al sol.',
  },
  'gabardina-gasa-verde': {
    prompt: 'gabardina gasa verde',
    opinionBody:
      'La gabardina de gasa verde es un absoluto acierto para aquellos que buscan una prenda elegante y ligera. Los usuarios destacan su suave textura y su caída fluida, que le confiere un aspecto sofisticado. Además, su color verde es muy versátil y combina perfectamente con múltiples estilos y tonalidades de piel. Sin duda, es una opción imprescindible para darle un toque de estilo único a cualquier outfit.',
    combineBody:
      'La gabardina de gasa verde se destaca por su versatilidad y su capacidad para complementar todo tipo de looks. Puedes combinarla con unos jeans ajustados y una camiseta blanca para un look casual y desenfadado. Si buscas un estilo más elegante, úsala sobre un vestido o una falda lápiz en tonos neutros. También puedes agregar accesorios dorados o beige para realzar el color verde de la gabardina. ¡Las posibilidades son infinitas!',
    cleanBody:
      'La gabardina de gasa verde debe ser tratada con cuidado para mantener su aspecto impecable. Se recomienda lavarla a mano usando agua fría y un detergente suave. Evita retorcerla para eliminar el exceso de agua y cuélgala para secar al aire libre. No la expongas directamente a la luz solar para evitar la decoloración. Si es necesario plancharla, utiliza una temperatura baja y coloca un paño delgado sobre la tela para protegerla. Siguiendo estos consejos, podrás disfrutar de tu gabardina de gasa verde durante mucho tiempo.',
  },
  'gabardina-organdi-claro': {
    prompt: 'gabardina organdi claro',
    opinionBody:
      'La gabardina organdi claro es una prenda muy versátil y elegante que ha recibido excelentes opiniones por parte de los usuarios. Su material ligero y traslúcido le aporta un aspecto sofisticado, ideal para ocasiones especiales o para darle un toque de distinción a cualquier look. Además, su color claro permite combinarla fácilmente con diferentes prendas y añadir un toque de luminosidad a cualquier conjunto.',
    combineBody:
      'La gabardina organdi claro es una prenda que se puede combinar de múltiples formas para lograr distintos estilos. Si quieres crear un look más formal, puedes combinarla con un vestido o un traje en tonos neutros, como el blanco, el beige o el negro. Para un estilo más casual, puedes llevarla con unos jeans y una blusa básica. Además, puedes añadir complementos en tonos pastel o metálicos para darle un toque de color o brillo a tu conjunto.',
    cleanBody:
      'Para mantener la gabardina organdi claro en buen estado, es importante seguir las instrucciones de cuidado indicadas por el fabricante. En general, es recomendable lavarla a mano o en ciclo delicado en la lavadora, utilizando agua fría y un detergente suave. Evita retorcerla para no dañar el material y déjala secar al aire libre, evitando la exposición directa al sol. Además, es recomendable guardarla en un lugar fresco y seco, lejos de la luz intensa, para preservar su color y evitar que se arrugue.',
  },
  'gabardina-piel-celeste': {
    prompt: 'gabardina piel celeste',
    opinionBody:
      'La gabardina de piel celeste ha recibido excelentes críticas por parte de los usuarios. Su material de piel le brinda un aspecto elegante y sofisticado, mientras que su color celeste añade un toque de frescura y originalidad. Los usuarios destacan la alta calidad de la confección y la durabilidad de la gabardina, además de su diseño versátil que se adapta tanto a ocasiones formales como informales. Sin duda, la gabardina de piel celeste es una opción muy popular entre quienes buscan destacar con estilo y clase.',
    combineBody:
      'La gabardina de piel celeste es una prenda muy versátil que puede combinar con una amplia gama de colores y estilos. Para crear un look elegante y sofisticado, puedes combinarla con pantalones negros y una blusa blanca. Si prefieres un estilo más casual, puedes utilizarla con jeans ajustados y una camiseta estampada. Además, la gabardina de piel celeste también se ve muy bien con vestidos o faldas para un look más femenino. No temas experimentar y jugar con diferentes combinaciones y accesorios, ya que esta prenda se adapta fácilmente a diferentes estilos y ocasiones.',
    cleanBody:
      'Para mantener la gabardina de piel celeste en óptimas condiciones, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable utilizar un cepillo suave o un paño húmedo para eliminar cualquier suciedad o polvo acumulado en la superficie de la gabardina. En caso de manchas, es importante tratarlas de inmediato utilizando un limpiador suave y siguiendo las instrucciones del fabricante. Es recomendable guardar la gabardina en un lugar oscuro y fresco, preferiblemente colgada en una percha para evitar que se deforme. Además, es recomendable aplicar regularmente un acondicionador de cuero para mantener la suavidad y flexibilidad de la piel. Siguiendo estos simples consejos, podrás disfrutar de tu gabardina de piel celeste durante mucho tiempo.',
  },
  'gabardina-punto-marron': {
    prompt: 'gabardina punto marron',
    opinionBody:
      'La gabardina de punto marrón es un clásico atemporal que no pasa de moda. Su material de punto le da un toque de comodidad y versatilidad, permitiéndote lucir elegante sin sacrificar la comodidad. Los usuarios destacan su durabilidad y resistencia, además de su capacidad para adaptarse a diferentes climas. El color marrón es muy versátil, ya que combina fácilmente con una amplia variedad de colores y estilos. Sin duda, la gabardina de punto marrón es una opción segura y elegante para cualquier ocasión.',
    combineBody:
      'La gabardina de punto marrón es una prenda muy versátil que se puede combinar de diversas maneras. Para un look casual, puedes usarla con unos jeans y una camiseta blanca básica. Si buscas algo más formal, puedes combinarla con pantalones de vestir en tonos neutros y una camisa de vestir. Para un outfit más chic, prueba con una falda midi y un top estampado. No temas experimentar con diferentes accesorios como bufandas, sombreros o zapatos de colores para añadir un toque de estilo personal. La gabardina de punto marrón te ofrece infinitas posibilidades de combinación para lucir siempre elegante y a la moda.',
    cleanBody:
      'La gabardina de punto marrón es una prenda que requiere cuidados especiales para mantenerla en buen estado. Para limpiarla, se recomienda lavarla a mano con agua fría y un detergente suave. Evita usar blanqueadores o productos agresivos que puedan dañar el tejido. Si es posible, es preferible colgarla en lugar de doblarla para evitar que se deformen las fibras del tejido de punto. Para mantener el color marrón vibrante, evita exponerla directamente al sol por largos períodos de tiempo. Si notas alguna mancha, trata de limpiarla de inmediato antes de que se fije en el tejido. Siguiendo estos consejos de limpieza y',
  },
  'gabardina-tafetan-morado': {
    prompt: 'gabardina tafetan morado',
    opinionBody:
      'La gabardina de tafetán morado ha recibido excelentes críticas por su calidad y estilo. Los usuarios destacan su durabilidad, resistencia al agua y su elegante acabado en color morado. Además, la combinación de la tela de tafetán con el color morado agrega un toque de sofisticación y originalidad a cualquier outfit.',
    combineBody:
      'Esta gabardina de tafetán morado es extremadamente versátil a la hora de combinarla. Puedes usarla para crear un look casual con jeans y una camiseta blanca, o llevarla sobre un vestido negro para un conjunto más elegante. También puedes combinarla con pantalones de cuero y botas altas para un estilo urbano y vanguardista. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu gabardina de tafetán morado en óptimas condiciones, es importante seguir algunos pasos de limpieza y cuidado. Recomendamos leer las etiquetas de cuidado y seguir las instrucciones del fabricante. En general, se recomienda lavar en agua fría y utilizar un detergente suave. Evita el uso de productos químicos agresivos y secar al aire libre. Si es necesario planchar, utiliza una temperatura baja y coloca un paño entre la plancha y la gabardina para proteger el material. Con los cuidados adecuados, tu gabardina de tafetán morado lucirá como nueva durante mucho tiempo.',
  },
  'gabardina-tela-bronce': {
    prompt: 'gabardina tela bronce',
    opinionBody:
      'La gabardina de tela bronce ha recibido críticas positivas por su elegante aspecto y versatilidad. Los usuarios destacan su material duradero y resistente, perfecto para protegerse de las inclemencias del tiempo. Además, el color bronce le da un toque sofisticado y único, convirtiéndola en una prenda perfecta para ocasiones especiales o para darle un toque de estilo a cualquier look casual. Los usuarios también valoran su comodidad y ajuste, haciendo de esta gabardina una opción ideal para lucir a la moda sin comprometer el confort.',
    combineBody:
      'La gabardina de tela bronce es extremadamente versátil a la hora de combinarla con otras prendas. Gracias a su color neutro y elegante, se puede usar tanto con colores cálidos como fríos. Para un look elegante, se puede combinar con pantalones negros y una blusa blanca o para darle un toque sofisticado, se puede combinar con una falda lápiz y una camisa de seda. Para un estilo más casual, se puede optar por unos jeans ajustados y una camiseta básica. Además, se puede complementar con accesorios de tonos dorados o plateados para resaltar aún más su brillo y elegancia.',
    cleanBody:
      'Para mantener la gabardina de tela bronce en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, se recomienda leer y seguir las instrucciones de lavado que vienen con la prenda. Generalmente, se aconseja lavarla a mano o en ciclo suave en la lavadora, utilizando agua fría. Para evitar dañar el color o el material, es recomendable no utilizar lejía ni suavizante. Después del lavado, se debe colgar la gabardina para que se seque al aire, evitando la exposición directa al sol. También se puede utilizar una plancha a baja temperatura para quitar cualquier arruga. Siguiendo estos simples pasos, la gabardina de tela bronce se mantendrá impec',
  },
  'gabardina-vaquera-negro': {
    prompt: 'gabardina vaquera negro',
    opinionBody:
      'Las opiniones sobre la gabardina vaquera negro son muy positivas. Los usuarios destacan su estilo moderno y versátil, perfecto para complementar cualquier tipo de look. Además, resaltan la durabilidad del material vaquero, que proporciona resistencia y comodidad al mismo tiempo. Sin duda, la gabardina vaquera negro se ha convertido en una prenda imprescindible en el armario de cualquier amante de la moda.',
    combineBody:
      'La gabardina vaquera negro es una prenda muy versátil que puede combinarse de diversas formas. Para un look casual, puede ser ideal con unos pantalones vaqueros azules y una camiseta básica blanca. Para una ocasión más formal, se puede combinar con una blusa de seda en tono neutro y unos pantalones de vestir. También queda genial con vestidos y faldas, aportando un toque desenfadado pero sofisticado.',
    cleanBody:
      'Para limpiar y mantener la gabardina vaquera negro, es recomendable seguir algunas pautas. Primero, es importante leer las instrucciones de lavado específicas de cada prenda, ya que los materiales pueden variar. En general, se recomienda lavar la gabardina en agua fría y utilizar detergentes suaves. Evitar el uso de lejía y secar al aire libre, evitando fuentes directas de calor. Además, se puede planchar a baja temperatura si es necesario. Siguiendo estos consejos, podrás mantener tu gabardina vaquera negro en perfecto estado por mucho tiempo.',
  },
  'guantes-algodon-borgona': {
    prompt: 'guantes algodon borgona',
    opinionBody:
      'Los guantes de algodón borgoña son altamente valorados por su suavidad y comodidad. Los usuarios destacan que brindan una gran sensación de confort y protección durante los días fríos. Además, el color borgoña agrega un toque de estilo y elegancia a cualquier conjunto. Sin duda, son una excelente opción para mantener las manos abrigadas sin sacrificar el estilo.',
    combineBody:
      'Los guantes de algodón borgoña son extremadamente versátiles y se pueden combinar con una gran variedad de prendas. Para un look casual, puedes usarlos con jeans y una chaqueta de cuero negra. Si quieres un estilo más sofisticado, combínalos con una falda midi y un suéter de punto. También lucirán geniales con un abrigo camel y pantalones de vestir. No importa la ocasión, estos guantes aportarán un toque de color y estilo a cualquier conjunto.',
    cleanBody:
      'La clave para mantener los guantes de algodón borgoña en perfectas condiciones es lavarlos a mano con agua tibia y jabón suave. Evita usar blanqueadores y secarlos a altas temperaturas, ya que podrían perder color y forma. Es recomendable secarlos al aire libre, evitando la exposición directa al sol. Además, es importante guardarlos en un lugar seco y protegido para evitar el deterioro. Siguiendo estos consejos de limpieza y cuidado, podrás disfrutar de tus guantes de algodón borgoña por mucho tiempo.',
  },
  'guantes-cachemira-verde': {
    prompt: 'guantes cachemira verde',
    opinionBody:
      'Los guantes de cachemira verde son altamente valorados por su combinación de estilo y comodidad. Los usuarios destacan la suavidad y calidez del material de cachemira, que proporciona un agradable tacto en la piel. Además, el color verde es muy versátil y se adapta fácilmente a diferentes outfits, convirtiendo a estos guantes en un accesorio imprescindible en el armario de cualquier persona que busque estar a la moda durante el invierno.',
    combineBody:
      'Los guantes de cachemira verde pueden combinarse con una amplia gama de colores y estampados. Para un look elegante y sofisticado, se pueden combinar con un abrigo negro o gris y unos pantalones ajustados. También quedan muy bien con prendas en tonos neutros como el blanco o el beige. Si se busca un estilo más llamativo, se pueden usar junto a una chaqueta estampada o un vestido de colores vibrantes. En definitiva, los guantes de cachemira verde son una elección atemporal que añadirá un toque de estilo a cualquier conjunto.',
    cleanBody:
      'Para mantener los guantes de cachemira verde en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Primero, es importante lavarlos a mano con agua fría y un detergente suave, evitando el uso de lejía o suavizantes. Después, se deben secar en posición horizontal, alejados de la luz solar directa. Para conservar su suavidad y forma, se pueden guardar en una bolsa de tela o en un cajón, protegidos de la humedad y de posibles roces con otros objetos. Con estos sencillos cuidados, los guantes de cachemira verde lucirán como nuevos durante mucho más tiempo.',
  },
  'guantes-encaje-marino': {
    prompt: 'guantes encaje marino',
    opinionBody:
      'Los guantes encaje marino han recibido excelentes opiniones por parte de los usuarios. Su diseño elegante y sofisticado los convierte en el complemento perfecto para ocasiones especiales. Además, su material de encaje de alta calidad proporciona una sensación suave y cómoda en las manos. Los usuarios destacan su versatilidad para combinar con diferentes estilos de vestimenta, convirtiéndose en un accesorio imprescindible en el armario de cualquier fashionista.',
    combineBody:
      'Los guantes encaje marino se pueden combinar de múltiples formas para crear looks únicos y sofisticados. Por ejemplo, puedes combinarlos con un vestido negro ajustado para lograr un look elegante y sensual. Si buscas un estilo más romántico, puedes combinarlos con una blusa blanca de manga larga y una falda midi de color pastel. Para un look más casual, puedes combinarlos con unos jeans y una camisa blanca. No importa la ocasión, los guantes encaje marino siempre añadirán un toque de feminidad y elegancia a tu outfit.',
    cleanBody:
      'Para mantener tus guantes encaje marino en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, se recomienda lavarlos a mano con agua fría y detergente suave. Evita retorcerlos y déjalos secar al aire libre, alejados de la luz directa del sol. Para mantener el encaje en buen estado, evita el contacto con superficies rugosas o ásperas, y guárdalos en un lugar fresco y seco. Si es necesario plancharlos, utiliza una temperatura baja y coloca un paño delgado entre el encaje y la plancha para evitar daños. Siguiendo estos cuidados, tus guantes encaje marino lucirán como nuevos durante mucho tiempo.',
  },
  'guantes-encaje-rosa': {
    prompt: 'guantes encaje rosa',
    opinionBody:
      'Los guantes encaje rosa son una elección única y elegante para cualquier ocasión. Los usuarios han elogiado su suavidad y comodidad, además de su ajuste perfecto. El encaje añade un toque romántico y femenino que muchas personas adoran. Además, el color rosa agrega un toque delicado y dulce a cualquier atuendo. Sin duda, los guantes encaje rosa son una opción popular entre aquellos que buscan un accesorio sofisticado y con estilo.',
    combineBody:
      'Los guantes encaje rosa pueden combinarse de diversas maneras para crear looks increíbles. Para un estilo romántico, puedes combinarlos con un vestido blanco o crema, tacones y accesorios dorados. Si deseas un look más audaz, puedes usarlos con una falda de cuero negra, una blusa blanca y botines negros. También puedes combinarlos con un vestido negro y zapatos de tacón del mismo color para un look elegante y sofisticado. En resumen, los guantes encaje rosa pueden adaptarse a diferentes estilos y ser el complemento perfecto para cualquier ocasión.',
    cleanBody:
      'Al ser de encaje, los guantes encaje rosa requieren de un cuidado especial para mantener su belleza y durabilidad. Es recomendable lavarlos a mano con agua fría y un detergente suave. Evita retorcerlos o frotarlos con fuerza para evitar daños en el encaje. Después de lavarlos, sécalos al aire libre, evitando la luz directa del sol para evitar que se decoloren. Para evitar las arrugas, guárdalos en un lugar seguro y plano. Siempre revisa las instrucciones de cuidado específicas del fabricante antes de realizar cualquier limpieza. Con un mantenimiento adecuado, tus guantes encaje rosa lucirán impecables por mucho tiempo.',
  },
  'guantes-franela-beige': {
    prompt: 'guantes franela beige',
    opinionBody:
      'Los guantes de franela beige son altamente valorados por su suavidad y calidez. Los usuarios han elogiado la comodidad que brindan, así como su capacidad para mantener las manos abrigadas en los días fríos. Además, el color beige es muy versátil y combina fácilmente con diferentes estilos de ropa. Sin duda, estos guantes son una excelente opción para quienes buscan estilo y funcionalidad.',
    combineBody:
      'Los guantes de franela beige son un accesorio elegante que puede combinar fácilmente con una amplia gama de colores y estilos. Para un look casual, puedes usarlos con jeans y una chaqueta de cuero. Si prefieres un estilo más sofisticado, combínalos con un abrigo largo de color neutro, como el gris o el negro. Además, los guantes beige también lucen bien con tonos más claros, como el blanco o el beige, creando un look monocromático y refinado.',
    cleanBody:
      'Para mantener los guantes de franela beige en buen estado, es importante seguir algunas pautas de limpieza y mantenimiento. Recomendamos lavarlos a mano con agua tibia y un detergente suave, evitando frotar en exceso para no dañar la franela. Después de lavarlos, déjalos secar al aire libre en una superficie plana. Para mantener su suavidad, puedes cepillar suavemente la franela después del secado. Además, es importante guardar los guantes en un lugar seco y protegido para evitar que se ensucien o se dañen. Siguiendo estos consejos, podrás disfrutar de tus guantes de franela beige por mucho más tiempo.',
  },
  'guantes-jersey-verde': {
    prompt: 'guantes jersey verde',
    opinionBody:
      'Los usuarios han quedado encantados con los guantes jersey verde. Destacan su suavidad y comodidad gracias al material de jersey utilizado. Además, el color verde agrega un toque de estilo y originalidad a cualquier conjunto. Los guantes también son elásticos, lo que los hace ajustables a diferentes tamaños de manos. Sin duda, una excelente elección para mantenerse abrigado y a la moda durante los días fríos.',
    combineBody:
      'Los guantes jersey verde son una opción versátil que combina perfectamente con diferentes estilos y prendas. Puedes llevarlos con abrigos y chaquetas en tonos neutros como negro, gris o beige para crear un contraste llamativo. Para un look más casual, úsalos con una sudadera y jeans. Si quieres un estilo más elegante, combínalos con un abrigo largo y pantalones de vestir. Los guantes verdes también pueden ser el acento de color ideal en conjuntos totalmente negros o blancos. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tus guantes jersey verde en buen estado, te recomendamos seguir estos consejos de limpieza y cuidado. Lávalos a mano con agua fría y un detergente suave. Evita el uso de lejía y suavizante, ya que pueden dañar el tejido. No los retuerzas ni los exprimas, simplemente déjalos secar al aire libre. Para mantener su forma original, guárdalos en un lugar seco y sin apretar. Si sigues estas recomendaciones, tus guantes mantendrán su suavidad y color vibrante durante mucho tiempo.',
  },
  'guantes-lino-naranja': {
    prompt: 'guantes lino naranja',
    opinionBody:
      'Los guantes de lino naranja son una opción popular entre aquellos que buscan un accesorio llamativo y elegante. Los usuarios destacan la suavidad y la comodidad del lino, que proporciona una sensación liviana y transpirable. Además, el color naranja vibrante agrega un toque de estilo a cualquier conjunto. Los usuarios también mencionan que los guantes de lino naranja son duraderos y fáciles de cuidar. En general, las opiniones sobre este producto son muy positivas, destacando su calidad y apariencia.',
    combineBody:
      'Los guantes de lino naranja son una excelente elección para agregar un toque de color y estilo a tu outfit. Puedes combinarlos con prendas en tonos neutros como blanco, negro o gris para crear un contraste llamativo. También puedes optar por colores complementarios como el azul marino o el verde oliva para lograr un aspecto armonioso. Los guantes de lino naranja también lucen muy bien con estampados florales o tropicales, ya que añaden un toque divertido y fresco. En definitiva, las posibilidades de combinación con estos guantes son amplias y te permiten experimentar con diferentes estilos y tendencias.',
    cleanBody:
      'Para mantener tus guantes de lino naranja en buen estado, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, asegúrate de leer las instrucciones de lavado del fabricante para conocer las recomendaciones específicas. En la mayoría de los casos, los guantes de lino se pueden lavar a máquina en un ciclo suave con agua fría. Sin embargo, es mejor evitar el uso de blanqueadores o detergentes agresivos que puedan dañar las fibras del lino. Después de lavarlos, es importante dejarlos secar al aire libre, evitando la exposición directa al sol. Para mantener su suavidad y evitar arrugas, puedes planchar los guantes a baja temperatura. Siguiendo estos sencillos pasos, podrás disfrutar de tus guantes de l',
  },
  'guantes-organdi-amarillo': {
    prompt: 'guantes organdi amarillo',
    opinionBody:
      'Los usuarios que han adquirido los guantes de organdi amarillo han quedado encantados con su diseño elegante y suave. El material de organdi les proporciona una sensación ligera y transpirable, perfecta para usar en ocasiones especiales. El color amarillo les añade un toque de alegría y originalidad, convirtiéndolos en el accesorio ideal para complementar cualquier atuendo. Sin duda, los guantes de organdi amarillo son una elección confiable y estilosa.',
    combineBody:
      'Los guantes de organdi amarillo son extremadamente versátiles y pueden combinarse con una amplia gama de prendas y accesorios. Para un look elegante y retro, puedes combinarlos con un vestido de corte clásico en tonos neutros como el blanco o el negro. Si deseas darles un toque moderno y divertido, prueba combinarlos con prendas en colores vibrantes como el verde o el azul. Además, puedes añadir accesorios llamativos como un collar o unos pendientes en tonos dorados para realzar el brillo del amarillo. Las posibilidades son infinitas, ¡solo debes dejar volar tu imaginación!',
    cleanBody:
      'Los guantes de organdi amarillo requieren un cuidado especial para mantener su apariencia impecable. Siempre es recomendable leer las instrucciones específicas del fabricante, pero en general, puedes seguir estos consejos para limpiarlos y mantener su calidad. Primero, asegúrate de quitar cualquier mancha inmediatamente frotándola suavemente con un paño húmedo y jabón suave. Evita frotar fuertemente, ya que podrías dañar el material delicado. Luego, déjalos secar al aire libre, preferiblemente en una superficie plana y lejos de la luz directa del sol. Si es necesario plancharlos, utiliza una temperatura baja y coloca un paño delgado sobre ellos para evitar daños. Siguiendo estos sencillos pasos, podrás disfrutar de tus',
  },
  'guantes-piel-borgona': {
    prompt: 'guantes piel borgona',
    opinionBody:
      'Los guantes de piel borgoña son altamente valorados por su elegancia y sofisticación. Los usuarios destacan la calidad de la piel, que es suave y duradera, así como su ajuste perfecto. Además, el color borgoña agrega un toque de estilo y distinción a cualquier conjunto. Sin duda, estos guantes son una excelente opción para los amantes de la moda.',
    combineBody:
      'Los guantes de piel borgoña son una pieza versátil que se puede combinar con distintos estilos y colores. Para un look clásico y sofisticado, combínalos con un abrigo negro y unos pantalones de vestir. Si quieres añadir un toque de color, puedes optar por combinarlos con un vestido blanco o con tonos neutros como el gris o el beige. También quedan muy bien con prendas en tonos oscuros como el azul marino o el verde botella.',
    cleanBody:
      'Para mantener tus guantes de piel borgoña en perfecto estado, es recomendable seguir algunos consejos de limpieza y mantenimiento. Primero, asegúrate de limpiarlos regularmente con un paño o cepillo suave para eliminar el polvo y la suciedad. Si se manchan, utiliza un producto específico para limpieza de cuero y sigue las instrucciones del fabricante. Luego, aplica un acondicionador de cuero para mantener la suavidad y flexibilidad de la piel. Por último, guárdalos en un lugar fresco y seco, evitando la exposición directa al sol o a fuentes de calor. Con estos cuidados, tus guantes de piel borgoña lucirán impecables temporada tras temporada.',
  },
  'guantes-punto-amarillo': {
    prompt: 'guantes punto amarillo',
    opinionBody:
      'Los guantes de punto amarillos son muy populares entre aquellos que buscan combinar funcionalidad y estilo en sus accesorios de invierno. La mayoría de las opiniones destacan la suavidad y calidez del material de punto, que mantiene las manos protegidas del frío sin sacrificar la comodidad. Además, el color amarillo brillante agrega un toque de alegría y energía a cualquier conjunto. Los usuarios también aprecian la elasticidad de estos guantes, que se adaptan perfectamente a la forma de las manos. En resumen, los guantes de punto amarillos son una opción popular entre quienes buscan una opción funcional y moderna para mantener las manos calientes durante los meses de invierno.',
    combineBody:
      'Los guantes de punto amarillos son un accesorio versátil que se puede combinar con una amplia variedad de conjuntos. Para un look casual y relajado, puedes combinarlos con vaqueros y una chaqueta de mezclilla. Si quieres un estilo más elegante y sofisticado, puedes combinarlos con un abrigo de color neutro y pantalones negros. También lucen geniales con vestidos o faldas, añadiendo un toque de color y diversión a tu atuendo. No temas experimentar y jugar con diferentes colores y estampados, ya que el color amarillo de los guantes puede servir como un punto focal llamativo en tu conjunto. En definitiva, los guantes de punto amarillos son una opción versátil que se adapta a diferentes estilos y ocasiones.',
    cleanBody:
      'Para mantener tus guantes de punto amarillos en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Lo primero es consultar las instrucciones de lavado del fabricante, ya que algunas prendas requieren un tipo específico de cuidado. En general, los guantes de punto suelen ser delicados y se recomienda lavarlos a mano con agua fría y detergente suave. Evita retorcerlos o frotarlos en exceso para no deformarlos. Después de lavarlos',
  },
  'guantes-rayon-naranja': {
    prompt: 'guantes rayon naranja',
    opinionBody:
      'Los guantes de rayón naranja son muy populares entre los usuarios debido a su comodidad y durabilidad. Muchos clientes han elogiado la suavidad del material y lo cómodos que son de usar durante largos períodos de tiempo. Además, el color naranja vibrante es muy atractivo y hace que estos guantes destaquen. Los usuarios también han destacado la resistencia de los guantes de rayón naranja, ya que pueden soportar el desgaste diario sin perder su forma ni calidad. En general, las opiniones sobre los guantes de rayón naranja son muy positivas, lo que los convierte en una excelente opción para proteger sus manos mientras se mantiene elegante.',
    combineBody:
      'Los guantes de rayón naranja son una excelente opción para darle un toque de color y estilo a cualquier atuendo. Pueden combinarse de muchas formas diferentes para lograr looks únicos y llamativos. Por ejemplo, puedes combinarlos con un abrigo oscuro para crear un contraste audaz y sofisticado. Si buscas un look más casual, puedes combinar los guantes de rayón naranja con una chaqueta de mezclilla y jeans. También puedes agregar un toque de elegancia al combinarlos con un traje negro durante eventos formales. Los guantes de rayón naranja son extremadamente versátiles y se adaptan a cualquier estilo y ocasión, por lo que las posibilidades de combinación son infinitas.',
    cleanBody:
      'La limpieza y el mantenimiento adecuados de los guantes de rayón naranja son esenciales para garantizar su durabilidad y apariencia. Para limpiarlos, se recomienda seguir las instrucciones específicas del fabricante, ya que algunos guantes pueden ser lavables a máquina, mientras que otros requieren limpieza en seco. Es importante evitar el uso de productos químicos abrasivos o lejía, ya que pueden dañar el tejido de rayón. Para el mantenimiento diario, se recomienda guardar los guantes de rayón naranja en un lugar fresco y seco, lejos',
  },
  'guantes-saten-azul': {
    prompt: 'guantes saten azul',
    opinionBody:
      'Los usuarios están encantados con estos guantes de satén azul. El material suave y sedoso brinda una gran comodidad al usarlos, y el color azul es vibrante y llamativo. Además, destacan la buena calidad de los guantes, asegurando que son duraderos y resistentes. Sin duda, una excelente opción para lucir elegante y sofisticado.',
    combineBody:
      'Estos guantes de satén azul son la pieza perfecta para complementar cualquier atuendo. Puedes combinarlos con vestidos de noche o trajes formales para darle un toque de elegancia. También lucen espectaculares con conjuntos más casuales, como jeans y blusas. No importa la ocasión, estos guantes serán el accesorio ideal para realzar tu estilo y agregar un toque de glamour.',
    cleanBody:
      'Para mantener tus guantes de satén azul en perfectas condiciones, es importante tomar algunas precauciones. Lávalos a mano con agua fría y detergente suave. Evita retorcerlos o frotarlos con fuerza para evitar daños en el tejido. Después de lavarlos, déjalos secar al aire libre en un lugar fresco y evita exponerlos directamente al sol. Una vez secos, guárdalos en un lugar seco y limpio, preferiblemente dentro de una bolsa o caja para protegerlos del polvo y la humedad. Siguiendo estos consejos, podrás disfrutar de tus guantes de satén azul durante mucho tiempo.',
  },
  'guantes-seda-naranja': {
    prompt: 'guantes seda naranja',
    opinionBody:
      'Los guantes de seda naranja son altamente valorados por su elegancia y sofisticación. Los usuarios destacan la suavidad y delicadeza del material, así como su excelente ajuste a la mano. Además, el color naranja añade un toque vibrante y llamativo a cualquier conjunto, convirtiéndolos en un accesorio versátil y moderno.',
    combineBody:
      'Estos guantes de seda naranja combinan a la perfección con una amplia variedad de estilos y tonos. Para un look elegante y formal, puedes combinarlos con un abrigo negro y un vestido de tonos neutros. Si prefieres un estilo más llamativo, opta por combinarlos con prendas en colores contrastantes como azul marino o verde esmeralda. También se pueden utilizar para aportar un toque de color a un conjunto básico en tonos neutros.',
    cleanBody:
      'Para mantener los guantes de seda naranja en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarlos a mano con agua fría y un detergente suave. Evita retorcerlos para eliminar el exceso de agua y en lugar de eso, presiona suavemente para secarlos. Además, es recomendable guardarlos en un lugar fresco y oscuro, lejos de la luz solar directa para evitar que el color se desvanezca. Siguiendo estos consejos, podrás disfrutar de tus guantes de seda naranja durante mucho tiempo.',
  },
  'guantes-tejido-oro': {
    prompt: 'guantes tejido oro',
    opinionBody:
      'Los guantes tejidos en color oro están ganando cada vez más popularidad debido a su aspecto elegante y lujoso. Los usuarios están encantados con su diseño único y el toque especial que le dan a cualquier atuendo. Además de su atractivo estético, las opiniones destacan la calidad del material utilizado en su fabricación, que proporciona comodidad y calidez en cada uso. Los guantes tejido oro son una elección audaz y sofisticada que no pasa desapercibida.',
    combineBody:
      'Estos guantes en color oro son la pieza perfecta para elevar cualquier conjunto. Puedes combinarlos con un look monocromático en tonos neutros para que destaquen aún más. También lucen maravillosos junto a prendas en tonos oscuros como el negro o el azul marino, creando un contraste llamativo y elegante. Si quieres un estilo más arriesgado, úsalos con estampados en colores vivos para un look audaz y original. Deja volar tu creatividad y experimenta para lograr combinaciones únicas y elegantes.',
    cleanBody:
      'Para mantener tus guantes tejidos en color oro en perfectas condiciones, es importante seguir algunos cuidados simples. Recuerda leer las instrucciones de cuidado en la etiqueta antes de proceder. En general, es recomendable lavar los guantes a mano con agua fría y un detergente delicado. Evita frotar demasiado fuerte para no dañar las fibras. Después del lavado, dóblalos cuidadosamente y déjalos secar al aire libre. Evita exponerlos directamente a la luz solar para evitar decoloración. Siguiendo estos consejos, tus guantes tejido oro se mantendrán hermosos y listos para usar en cada ocasión.',
  },
  'jersey-gasa-beige': {
    prompt: 'jersey gasa beige',
    opinionBody:
      'El jersey de gasa beige es altamente valorado por su elegancia y ligereza. Los usuarios destacan su suavidad y comodidad al llevarlo puesto. Además, el material de gasa proporciona un aspecto delicado y femenino. Su color beige es versátil y fácil de combinar, convirtiéndolo en una prenda básica en cualquier guardarropa.',
    combineBody:
      'El jersey de gasa beige es una opción ideal para combinar con diferentes prendas y estilos. Puedes crear un look sofisticado combinándolo con una falda midi de color neutro y tacones altos. Si prefieres un estilo más casual, puedes usarlo con unos jeans ceñidos y zapatillas blancas. También puedes agregar accesorios en tonos dorados para resaltar el color beige del jersey.',
    cleanBody:
      'Para mantener el jersey de gasa beige en buen estado, es importante seguir unas pautas de limpieza adecuadas. Se recomienda lavarlo a mano con agua fría y un detergente suave. Evita retorcer o frotar en exceso para evitar daños en la gasa. Después de lavarlo, es aconsejable secarlo en posición horizontal para evitar deformaciones. Además, es recomendable guardar el jersey en un lugar oscuro y protegido del sol para evitar decoloraciones.',
  },
  'jersey-jacquard-azul': {
    prompt: 'jersey jacquard azul',
    opinionBody:
      'Con qué combinar el jersey jacquard azul: Este jersey jacquard azul es extremadamente versátil y se puede combinar de múltiples formas. Para un look casual y relajado, puedes combinarlo con unos jeans de color blanco o denim. Si buscas un estilo más elegante, puedes optar por una falda lápiz negra y unos tacones altos. Además, puedes añadir accesorios dorados para darle un toque de glamour. Sin duda alguna, este jersey azul se convertirá en una pieza clave en tus conjuntos de temporada.',
    combineBody: null,
    cleanBody: null,
  },
  'jersey-saten-negro': {
    prompt: 'jersey saten negro',
    opinionBody:
      'Con qué combinar el jersey de satén negro: El jersey de satén negro es una prenda versátil que se puede combinar de diversas formas. Para un look formal, puedes combinarlo con una falda lápiz y tacones altos. Si deseas un look más casual pero elegante, úsalo con unos jeans ajustados y botines. También puedes añadir accesorios dorados o plateados para resaltar el tono oscuro del jersey. En resumen, el jersey de satén negro es una prenda básica que se puede combinar de múltiples maneras para lograr diferentes estilos.',
    combineBody: null,
    cleanBody: null,
  },
  'jersey-tejido-azul': {
    prompt: 'jersey tejido azul',
    opinionBody:
      'Los usuarios han elogiado la comodidad y suavidad de este jersey tejido azul. El material de alta calidad garantiza una prenda duradera que se mantiene en buen estado con el tiempo. Además, el color azul añade un toque de elegancia y versatilidad, siendo fácil de combinar con diferentes outfits.',
    combineBody:
      'Este jersey tejido azul es una opción perfecta para crear outfits casuales y elegantes. Puedes combinarlo con unos jeans oscuros y sneakers blancos para un look relajado pero chic. Si deseas un estilo más sofisticado, combínalo con una falda midi plisada y botines negros. Además, puedes añadir accesorios dorados para darle un toque de glamour adicional.',
    cleanBody:
      'Para mantener este jersey tejido azul en óptimas condiciones, es recomendable lavarlo a mano con agua fría y un detergente suave. Evita frotar demasiado fuerte para evitar dañar las fibras del tejido. Después del lavado, sécalo en posición horizontal para evitar que se deforme. Si es necesario, puedes plancharlo a baja temperatura. Recuerda seguir siempre las instrucciones de cuidado del fabricante para mantener tu prenda en perfecto estado durante mucho tiempo.',
  },
  'jersey-tela-negro': {
    prompt: 'jersey tela negro',
    opinionBody:
      'El jersey de tela negro es una opción muy popular entre los usuarios. Muchos afirman que es una prenda versátil y atemporal que se adapta a cualquier ocasión. Además, destacan la comodidad que ofrece gracias a la suavidad de su tela. Algunos mencionan que el color negro ayuda a lucir más elegante y sofisticado. En general, las opiniones sobre este jersey son muy positivas, considerándolo como una pieza básica que no puede faltar en el armario.',
    combineBody:
      'La versatilidad del jersey de tela negro permite combinarlo con una amplia variedad de prendas y estilos. Para un look casual, puedes llevarlo con unos jeans y zapatillas blancas. Si deseas un outfit más formal, puedes combinarlo con pantalones de vestir y unos zapatos de cuero. Combina muy bien con faldas y botas altas para un estilo más femenino. Si quieres un toque de color, agregale accesorios llamativos, como una bufanda roja o un bolso de colores vibrantes. Las posibilidades son infinitas a la hora de combinar el jersey de tela negro, lo cual lo convierte en una prenda muy versátil para cualquier ocasión.',
    cleanBody:
      'Para mantener el jersey de tela negro en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarlo a mano o en ciclo suave en la lavadora, utilizando agua fría. Evita el uso de blanqueadores y suavizantes, ya que pueden afectar la calidad de la tela. Para secarlo, es preferible colgarlo en una percha y evitar retorcerlo para que no pierda su forma. En cuanto al planchado, se recomienda hacerlo a temperatura baja o media, y preferiblemente del revés para evitar que se dañe el color. Siguiendo estas instrucciones de limpieza y mantenimiento, podrás disfrutar de tu jersey de tela negro durante mucho tiempo.',
  },
  'leggings-denim-esmeralda': {
    prompt: 'leggings denim esmeralda',
    opinionBody:
      'Los leggings denim esmeralda son una opción única y llamativa para darle un toque de estilo a tus outfits. Los usuarios que han probado estos leggings destacan su comodidad y lo favorecedores que son para la figura. Además, el color esmeralda agrega un toque de sofisticación y originalidad, causando sensación allá donde vayas. Sin duda, los leggings denim esmeralda son una elección acertada para lucir a la moda y sentirte cómoda al mismo tiempo.',
    combineBody:
      'Los leggings denim esmeralda son muy versátiles y se pueden combinar de múltiples formas. Para un look casual y relajado, puedes combinarlos con una camiseta blanca básica y unas zapatillas blancas. Si quieres un look más elegante, úsalos con una blusa de seda en tonos neutros y unos zapatos de tacón en color negro. Para un look más atrevido, combínalos con una chaqueta de cuero y unas botas negras. Las posibilidades son infinitas y te aseguramos que llamarás la atención con este color tan vibrante.',
    cleanBody:
      'Para mantener tus leggings denim esmeralda en perfecto estado, es importante seguir algunas pautas de cuidado. Recuerda lavarlos del revés, preferiblemente a mano o en ciclo delicado en la lavadora. Utiliza detergente suave y evita el uso de suavizante. No los expongas al sol directo para evitar que el color se desvanezca. Si tienes alguna mancha, trata de limpiarla con un quitamanchas antes de lavarlos. Además, cuando no los uses, guárdalos en un lugar oscuro y seco, lejos de la luz directa. Siguiendo estas recomendaciones, tus leggings denim esmeralda se mantendrán impecables y listos para lucir en cualquier ocasión.',
  },
  'leggings-encaje-azul': {
    prompt: 'leggings encaje azul',
    opinionBody:
      'Los leggings encaje azul son una prenda muy versátil y estilizada que ha ganado popularidad entre las mujeres. Su material en encaje le da un toque femenino y elegante, perfecto para ocasiones especiales o simplemente para lucir a la moda en el día a día. Las opiniones de las usuarias destacan su comodidad, ajuste y su capacidad de realzar la figura. Además, el color azul es muy favorecedor y fácil de combinar, lo que lo convierte en una opción ideal para agregar un toque de color a cualquier conjunto.',
    combineBody:
      'A la hora de combinar los leggings encaje azul, las posibilidades son infinitas. Puedes optar por un look casual y relajado combinándolos con una camiseta oversized y zapatillas blancas. Si deseas un estilo más elegante, puedes llevarlos con un top ajustado o una blusa de seda, acompañados de tacones altos. Además, los leggings encaje azul se adaptan muy bien a cualquier tono de gris, blanco, negro o incluso rosado claro, por lo que puedes jugar con diferentes combinaciones según tu estilo y ocasión.',
    cleanBody:
      'Para garantizar que tus leggings encaje azul se mantengan en perfecto estado, es importante seguir las instrucciones de cuidado del fabricante. Por lo general, se recomienda lavarlos a mano con agua fría y un detergente suave. Evita frotarlos demasiado fuerte para no dañar el encaje. Es preferible no utilizar secadora, ya que el calor puede deformar la prenda. En su lugar, déjalos secar al aire libre. Si es necesario plancharlos, utiliza una temperatura baja y protege el encaje con un paño delgado. Siguiendo estos consejos, tus leggings encaje azul se mantendrán en óptimas condiciones durante mucho tiempo.',
  },
  'leggings-gasa-marron': {
    prompt: 'leggings gasa marron',
    opinionBody:
      'Los leggings de gasa en color marrón son una elección perfecta para aquellas mujeres que deseen agregar un toque de elegancia y sofisticación a sus conjuntos. Su tejido ligero y fluido los convierte en una opción cómoda y fresca para los días más calurosos. Las opiniones de quienes los han utilizado destacan su comodidad y versatilidad, ya que se pueden combinar con una amplia variedad de prendas y estilos. Además, el color marrón es muy favorecedor y se adapta a cualquier tono de piel, lo que los convierte en una elección segura y versátil.',
    combineBody:
      'Los leggings de gasa marrón se pueden combinar de múltiples formas para crear estilismos únicos y con estilo. Para un look casual y relajado, puedes combinarlos con una camiseta básica blanca y unas zapatillas blancas. Si quieres un look más elegante, puedes optar por combinarlos con una camisa de seda en tonos neutros y unos zapatos de tacón. Si buscas un look boho chic, puedes combinarlos con una blusa estampada y unas sandalias de cuero. En definitiva, estos leggings en color marrón son una prenda versátil que se adapta a cualquier estilo y ocasión.',
    cleanBody:
      'Para mantener tus leggings de gasa marrón en buen estado, es recomendable seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlos a mano con agua fría y detergentes suaves. Evita utilizar lejía o productos químicos agresivos que puedan dañar el tejido. Es importante secarlos en posición horizontal y evitar el uso de la secadora. Para eliminar las arrugas, puedes utilizar una plancha a baja temperatura, siempre teniendo cuidado de no quemar el tejido. Siguiendo estas recomendaciones, tus leggings de gasa marrón se mantendrán en perfectas condiciones durante mucho tiempo.',
  },
  'leggings-jacquard-azul': {
    prompt: 'leggings jacquard azul',
    opinionBody:
      'Los leggings jacquard azul son una opción popular entre las mujeres que buscan comodidad y estilo. El diseño jacquard crea una textura única que añade un toque especial a cualquier conjunto. Muchas usuarias destacan la suavidad y elasticidad del material, que se adapta perfectamente al cuerpo sin perder su forma original. Además, el color azul es versátil y se puede combinar fácilmente con diferentes colores y estampados. En general, quienes han probado los leggings jacquard azul se muestran satisfechas con su calidad y diseño.',
    combineBody:
      'Los leggings jacquard azul ofrecen múltiples posibilidades de combinación en tu vestuario. Puedes crear looks tanto casuales como elegantes con esta prenda versátil. Para un conjunto casual, puedes combinar los leggings con una camiseta básica blanca y zapatillas deportivas. Si deseas un look más elegante, puedes optar por una blusa de seda en tonos neutros y tacones altos. Para añadir un toque de color, puedes utilizar accesorios como una bufanda o un bolso en tonos contrastantes. Sea cual sea tu estilo, los leggings jacquard azul serán el complemento perfecto.',
    cleanBody:
      'Para mantener tus leggings jacquard azul en óptimas condiciones, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlos a mano o en ciclo suave en la lavadora con agua fría. Evita usar lejía o suavizante, ya que pueden dañar el material y desvanecer el color. Para secarlos, es ideal colgarlos en un lugar fresco y ventilado, evitando la exposición directa al sol. Si es necesario plancharlos, utiliza la temperatura más baja y coloca un paño fino entre el leggings y la plancha para evitar cualquier daño. Siguiendo estas pautas de cuidado, tus leggings jacquard azul se mantendrán como nuevos durante mucho tiempo.',
  },
  'leggings-jacquard-blanco': {
    prompt: 'leggings jacquard blanco',
    opinionBody:
      'Los leggings jacquard blancos son una prenda versátil y cómoda que se adapta a cualquier estilo. Muchas personas han elogiado su suavidad y elasticidad, lo que los convierte en una excelente opción para hacer ejercicio o para relajarse en casa. Además, su diseño jacquard añade un toque elegante y sofisticado, que los hace perfectos para ocasiones más formales. En cuanto a la calidad del material, los usuarios han señalado que es duradero y resistente al desgaste. En general, los usuarios están muy satisfechos con los leggings jacquard blanco, considerándolos una excelente adquisición para el armario.',
    combineBody:
      'Los leggings jacquard blanco son extremadamente versátiles y se pueden combinar de muchas maneras. Para un look casual pero elegante, puedes combinarlos con una blusa holgada y unos tacones altos. Si prefieres un look más informal, puedes optar por una camiseta gráfica y uns zapatillas blancas. Para un outfit de noche, combínalos con una blusa de seda y unos tacones de color negro. Además, puedes agregar accesorios como un cinturón o una bufanda para darle un toque personalizado a tu look. Las opciones son infinitas, así que no tengas miedo de experimentar y divertirte con tu estilo.',
    cleanBody:
      'Para mantener tus leggings jacquard blanco en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Antes de lavarlos, asegúrate de leer las instrucciones de cuidado en la etiqueta. En general, se recomienda lavarlos a máquina en agua fría y usando un detergente suave. Evita usar blanqueadores para evitar dañar el color blanco. Para secarlos, es mejor colgarlos o tenderlos en una posición horizontal para evitar que se deformen. Si es necesario, puedes plancharlos a baja temperatura, pero asegúrate de poner un paño entre el leggings y la plancha para evitar quemaduras. Siguiendo estos consejos, tus leggings jacquard blanco se mantendrán impecables',
  },
  'leggings-jacquard-borgona': {
    prompt: 'leggings jacquard borgona',
    opinionBody:
      'Los leggings jacquard borgona son altamente elogiados por su diseño elegante y moderno. Su material de alta calidad proporciona una sensación suave y cómoda al usarlos. Los usuarios mencionan que son muy versátiles, ya que pueden ser usados tanto para actividades deportivas como para ocasiones más casuales. Además, el color borgoña le da un toque de estilo único que resalta cualquier atuendo.',
    combineBody:
      'Los leggings jacquard borgona son muy fáciles de combinar. Puedes crear un look casual y relajado al combinarlos con una camiseta básica y zapatillas blancas. Para eventos más formales, puedes usarlos con un blazer y unos tacones elegantes. Si deseas un look más deportivo, puedes combinarlos con una sudadera con capucha y zapatillas deportivas. La versatilidad del color borgoña te permite crear múltiples estilos adaptados a cualquier ocasión.',
    cleanBody:
      'Para mantener los leggings jacquard borgona en óptimas condiciones, te recomendamos lavarlos a mano con agua fría y un detergente suave. Evita usar lejía o productos químicos agresivos, ya que pueden dañar el tejido. Después de lavarlos, déjalos secar al aire libre, evitando la exposición directa al sol. Si es necesario, puedes plancharlos a baja temperatura para eliminar cualquier arruga. Siguiendo estos sencillos consejos, podrás disfrutar de tus leggings jacquard borgona durante mucho tiempo sin perder su calidad y color original.',
  },
  'leggings-lana-rosa': {
    prompt: 'leggings lana rosa',
    opinionBody:
      'Los leggings de lana rosa son una elección popular entre las amantes de la moda y el confort. Muchas personas elogian la suavidad y calidez de este material, especialmente en los meses más fríos. Además, el color rosa agrega un toque femenino y delicado a cualquier outfit. Las usuarias también destacan la versatilidad de estos leggings, ya que se pueden combinar fácilmente con diferentes prendas y estilos. En general, las opiniones son muy positivas y recomiendan estos leggings para mantenerse cómoda y a la moda durante todo el invierno.',
    combineBody:
      'A la hora de combinar leggings de lana rosa, las posibilidades son múltiples. Para un look casual y relajado, puedes optar por una sudadera oversize y zapatillas blancas. Si buscas un estilo más chic, puedes combinarlos con una blusa blanca y un blazer negro, agregando unos tacones para darle un toque elegante. Para un outfit más deportivo, puedes usarlos con una camiseta gráfica y zapatillas deportivas. También puedes optar por agregar una chaqueta de cuero o una bufanda en tonos neutros para darle un contraste interesante. No dudes en jugar con diferentes prendas y accesorios para crear looks únicos y originales con tus leggings de lana rosa.',
    cleanBody:
      'Para mantener tus leggings de lana rosa en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, es recomendable lavarlos a mano con agua fría y un detergente suave. Evita retorcerlos y secarlos en posición horizontal para evitar que pierdan su forma. Si prefieres lavarlos a máquina, utiliza un ciclo suave y colócalos en una bolsa de lavado para protegerlos. Para el secado, es preferible que los dejes al aire libre en un lugar sombreado y alejado del sol directo. Realiza un planchado suave a baja temperatura si es necesario pero asegúrate de leer las instrucciones de cuidado del fabricante. Siguiendo estas recomendaciones, tus leggings de lana rosa',
  },
  'leggings-lana-turquesa': {
    prompt: 'leggings lana turquesa',
    opinionBody:
      'Los leggings de lana en color turquesa han sido muy populares entre aquellos que buscan comodidad y estilo. Los usuarios están encantados con la suavidad y calidez que brinda el material de lana, lo que los convierte en la opción perfecta para los días fríos. Además, el color turquesa agrega un toque de originalidad y frescura a cualquier atuendo. Los usuarios también destacan que estos leggings son muy versátiles, ya que se pueden combinar con una variedad de prendas y colores. En general, los usuarios están muy satisfechos con la calidad y apariencia de los leggings de lana turquesa.',
    combineBody:
      'Los leggings de lana en color turquesa pueden combinarse de muchas formas diferentes. Para un look casual pero elegante, puedes combinarlos con un suéter o una blusa en tonos neutros como el blanco, el negro o el gris. Si buscas un look más vibrante, puedes combinarlos con prendas en colores similares, por ejemplo, una blusa o una chaqueta en tonos azules o verdes. También puedes optar por contrastar el turquesa con colores más llamativos, como el rojo o el amarillo. Para completar el look, puedes añadir accesorios como bufandas o gorros que complementen el color turquesa de los leggings. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Mantener tus leggings de lana turquesa en buen estado no requiere de muchos cuidados especiales. Lo primero que debes hacer es leer las instrucciones de lavado que vienen con la prenda, ya que algunos leggings de lana pueden requerir lavados a mano o en lavadora en un ciclo suave. En general, es recomendable lavarlos en agua fría o tibia con un detergente suave. Evita usar lejía o suavizantes, ya que pueden dañar las fibras de lana. Es preferible secar los leggings en posición horizontal para evitar deformaciones. Si es necesario, puedes plancharlos a baja temperatura, asegurándote de protegerlos con un paño o una',
  },
  'leggings-nylon-morado': {
    prompt: 'leggings nylon morado',
    opinionBody:
      'Los leggings de nylon morado son una opción perfecta para aquellos que buscan comodidad y estilo en una prenda. Los usuarios han elogiado su suave y elástico material de nylon, que se adapta fácilmente al cuerpo sin perder su forma original. Además, el color morado añade un toque de elegancia y originalidad a cualquier look. Los usuarios también han destacado la durabilidad de estos leggings, ya que resisten el desgaste y mantienen su color vibrante incluso después de múltiples lavados. En general, los leggings de nylon morado han recibido críticas positivas por su calidad, confort y estilo.',
    combineBody:
      'Los leggings de nylon morado son una prenda versátil que se puede combinar de varias formas para lograr diferentes estilos. Para un look casual y relajado, puedes combinarlos con una camiseta blanca suelta y zapatillas deportivas. Si prefieres un look más elegante, puedes optar por una blusa de seda en tonos neutros y tacones altos. Para ocasiones más formales, puedes combinar los leggings morados con una blazer y botines de tacón. También puedes añadir accesorios como collares o pulseras en tonos plateados o dorados para darle un toque extra de sofisticación. La versatilidad del color morado te permite combinar estos leggings con una amplia gama de colores y estilos.',
    cleanBody:
      'Para mantener tus leggings de nylon morado en óptimas condiciones, es importante seguir algunas pautas de limpieza y cuidado. El nylon es un material resistente, pero es necesario lavarlo con cuidado para evitar dañarlo. Se recomienda lavar los leggings a mano con agua fría y un detergente suave. Evita usar lejía o productos químicos agresivos que puedan alterar el color o textura del nylon. Después de lavarlos, es importante secarlos al aire libre para evitar el uso de secadora, ya que el calor excesivo puede dañar el material. Además, es recomendable evitar planchar los leggings de nylon, ya que el calor directo también puede afectar su apariencia. Siguiendo',
  },
  'leggings-nylon-rosa': {
    prompt: 'leggings nylon rosa',
    opinionBody:
      'Los leggings de nylon rosa son una elección popular entre las amantes de la moda y el estilo. Su material de nylon ofrece una sensación suave y elástica, lo que los hace cómodos de usar durante todo el día. Además, el color rosa aporta un toque femenino y vibrante a cualquier conjunto.',
    combineBody: 'Con qué combinar leggings de nylon rosa:',
    cleanBody:
      'Si deseas destacar aún más el color rosa de los leggings, puedes añadir complementos en tonos complementarios como el blanco, el gris o el negro. Un suéter gris oversized con botas negras será una opción acogedora y moderna. Para un toque más audaz, puedes agregar detalles en tonos metálicos como plata o dorado.',
  },
  'leggings-poliester-gris': {
    prompt: 'leggings poliester gris',
    opinionBody:
      'Los leggings de poliéster gris son una excelente opción para cualquier ocasión. Su material resistente y elástico proporciona comodidad y durabilidad. Los usuarios han elogiado la suavidad de la tela y la versatilidad del color gris, que combina fácilmente con cualquier outfit. Además, destacan que estos leggings no se decoloran ni se deforman con el tiempo, lo que garantiza que mantendrán su aspecto impecable durante mucho tiempo.',
    combineBody:
      'Los leggings de poliéster gris son extremadamente versátiles y se pueden combinar de muchas maneras. Para un look casual y relajado, puedes usarlos con una camiseta holgada y zapatillas deportivas. Si buscas algo más elegante, puedes combinarlos con una blusa suelta y tacones altos. Para un look más deportivo, puedes optar por una sudadera con capucha y zapatillas de deporte. Sin importar el estilo que elijas, estos leggings se adaptan perfectamente a cualquier outfit.',
    cleanBody:
      'La limpieza y el mantenimiento de los leggings de poliéster gris es muy sencillo. Puedes lavarlos a máquina en agua fría con detergente suave. Evita usar lejía o suavizante de telas, ya que pueden dañar la tela de poliéster. Para secarlos, es mejor colgarlos al aire libre o en una percha, evitando la exposición directa al sol. Si es necesario plancharlos, asegúrate de utilizar una temperatura baja. Siguiendo estas instrucciones, podrás disfrutar de tus leggings de poliéster gris en excelente estado durante mucho tiempo.',
  },
  'leggings-sintetica-marron': {
    prompt: 'leggings sintetica marron',
    opinionBody:
      'Los leggings sintéticos marrones son una opción muy popular entre las mujeres que buscan comodidad y estilo. Las opiniones son muy positivas, ya que este tipo de leggings son muy elásticos y se adaptan perfectamente al cuerpo, ofreciendo una sensación de suavidad y libertad de movimiento. Además, el color marrón es muy versátil, por lo que se pueden combinar fácilmente con diferentes prendas y estilos. Si estás buscando una prenda versátil y cómoda para tu guardarropa, los leggings sintéticos marrones son una excelente elección.',
    combineBody:
      'Los leggings sintéticos marrones son una prenda muy versátil que se puede combinar de diferentes formas y estilos. Puedes usarlos con una camiseta básica blanca y unas zapatillas deportivas para crear un look casual y relajado. Si quieres un look más elegante, puedes combinarlos con una blusa de seda y unos zapatos de tacón. También puedes combinarlos con un suéter oversized y unas botas para un look más invernal. El color marrón es muy fácil de combinar, por lo que las posibilidades son infinitas.',
    cleanBody:
      'Para limpiar y mantener los leggings sintéticos marrones, es importante seguir las instrucciones de cuidado del fabricante. En la mayoría de los casos, se recomienda lavarlos a máquina en ciclo suave con agua fría y utilizar detergente suave. Evita usar suavizante de telas, ya que puede afectar la elasticidad de los leggings. Después de lavarlos, sécalos al aire libre o en una secadora a baja temperatura. Para evitar que se formen bolitas en la tela, evita frotarlos o lavarlos junto con prendas ásperas. Si sigues estas recomendaciones de cuidado, tus leggings sintéticos marrones se mantendrán en perfectas condiciones durante mucho tiempo.',
  },
  'leggings-tela-verde': {
    prompt: 'leggings tela verde',
    opinionBody:
      'Los leggings de tela verde han sido muy populares entre quienes buscan agregar un toque de color a sus outfits diarios. Muchos usuarios destacan la comodidad de este tipo de leggings, gracias al material elástico que se adapta perfectamente al cuerpo. Además, el color verde es elegido especialmente por quienes desean darle un aspecto fresco y natural a su apariencia. Sin duda, los leggings de tela verde son una excelente opción para aquellos que quieren lucir a la moda y disfrutar de la comodidad al mismo tiempo.',
    combineBody:
      'La versatilidad de los leggings de tela verde permite combinarlos de diversas maneras. Para un look casual y relajado, puedes combinarlos con una camiseta blanca y zapatillas blancas. Si buscas algo más elegante, puedes optar por un top negro ajustado y tacones negros. Para un estilo más bohemio, puedes agregar una blusa de encaje en tonos tierra y unas sandalias con trenzas. Sin importar el estilo que elijas, los leggings de tela verde añadirán un toque de frescura y originalidad a tu atuendo.',
    cleanBody:
      'Para garantizar la durabilidad y belleza de tus leggings de tela verde, es importante tener en cuenta algunos consejos de limpieza y mantenimiento. Siempre es recomendable seguir las instrucciones de lavado indicadas en la etiqueta del producto. En general, se recomienda lavarlos a mano con agua fría y un detergente suave. Evita usar suavizantes de tela, ya que pueden hacer que el color se desvanezca. Después de lavarlos, es importante secarlos al aire libre, evitando la exposición directa al sol. Si sigues estos consejos, tus leggings de tela verde se mantendrán en óptimas condiciones y lucirán como nuevos durante mucho tiempo.',
  },
  'leggings-tweed-azul': {
    prompt: 'leggings tweed azul',
    opinionBody:
      'Los leggings de tweed azul son una elección trendy y versátil para cualquier ocasión. Este material ofrece una textura única que se destaca y agrega estilo a tu outfit. Además, el color azul es elegante y combina fácilmente con diferentes tonos y estampados. Los usuarios destacan la comodidad de estos leggings y su capacidad para estilizar la figura. Sin duda, una excelente opción para lucir a la moda.',
    combineBody:
      'Para armar un look sofisticado con los leggings de tweed azul, puedes combinarlos con una blusa blanca y una chaqueta de cuero negra. Este contraste de colores genera un impacto visual interesante. También puedes optar por una camisa de rayas en tonos neutros y unos tacones altos para dar un toque más elegante. Si buscas un estilo más casual, una sudadera oversized y zapatillas blancas son una excelente opción.',
    cleanBody:
      'Para mantener tus leggings de tweed azul en buen estado, es importante seguir algunas recomendaciones de limpieza. Primero, lee las instrucciones de cuidado en la etiqueta del producto. En general, se recomienda lavarlos a mano o en ciclo delicado en la lavadora con agua fría. Evita el uso de blanqueadores y suavizantes. Para secarlos, cuelga los leggings en una percha para evitar que se deformen. Si es necesario, puedes plancharlos a baja temperatura. Siguiendo estos cuidados, tus leggings de tweed azul se mantendrán en óptimas condiciones por mucho tiempo.',
  },
  'media-denim-naranja': {
    prompt: 'media denim naranja',
    opinionBody:
      'Las medias de denim naranja son una opción audaz y original para añadir un toque de color a cualquier conjunto. Los usuarios han comentado que el material de denim utilizado es de alta calidad, ofreciendo una gran durabilidad y resistencia. Además, el color naranja vibrante es perfecto para destacar y llamar la atención. Muchos han elogiado cómo estas medias pueden transformar por completo un look básico, agregando estilo y personalidad. Sin embargo, algunos advierten que el color puede ser un poco difícil de combinar, por lo que es importante elegir el resto de la ropa y accesorios con cuidado.',
    combineBody:
      'Las medias de denim naranja se pueden combinar de varias maneras para crear looks únicos y llamativos. Para un estilo desenfadado, puedes optar por un par de shorts vaqueros y una camiseta blanca, dejando que las medias sean el centro de atención. Si prefieres un look más sofisticado, puedes combinarlas con una falda midi de color neutro y una blusa elegante. Además, puedes añadir accesorios en tonos dorados o plateados para complementar el conjunto. Recuerda que el color naranja es bastante llamativo, por lo que es importante equilibrar el resto de los elementos del look para obtener un resultado armonioso.',
    cleanBody:
      'Para mantener tus medias de denim color naranja en buen estado, es importante seguir algunas pautas de limpieza y cuidado. Siempre lee las instrucciones de lavado en la etiqueta antes de proceder. En general, se recomienda lavarlas en agua fría con un detergente suave y sin lejía. Evita frotarlas en exceso para evitar dañar el color y el material. Tras lavarlas, acláralas bien y colócalas en posición horizontal para secar, evitando la exposición directa al sol. Una vez secas, guárdalas en un lugar fresco y seco para evitar que se estiren o se deformen. Siguiendo estos consejos de limpieza y mantenimiento, tus medias de denim naranja lucirán im',
  },
  'media-franela-marron': {
    prompt: 'media franela marron',
    opinionBody:
      'Los usuarios que han adquirido la media franela marrón han quedado encantados con su calidad y comodidad. El material suave y cálido proporciona un agradable confort durante todo el día. Además, su color marrón oscuro es versátil y fácil de combinar con diferentes prendas de vestir. Sin duda, una excelente opción para mantener tus pies abrigados y lucir un estilo casual y chic.',
    combineBody:
      'La media franela marrón es perfecta para combinar con varias prendas y estilos. Puedes lucirla con unos pantalones vaqueros o unos leggins negros para un look casual y relajado. Si buscas un estilo más elegante, puedes combinarlas con una falda plisada y una blusa de tonos neutros. Asimismo, puedes agregar algún accesorio en tonos dorados o marrones para completar el conjunto. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tus medias franela marrón en buen estado, es recomendable lavarlas a mano o en ciclo delicado en la lavadora con agua fría. Evita el uso de lejía o productos químicos fuertes, ya que pueden dañar el material. Es importante secarlas al aire libre o a baja temperatura en la secadora para evitar que se deformen. También puedes utilizar una bolsa de lavado para protegerlas durante el lavado. Siguiendo estos sencillos cuidados, tus medias franela marrón se mantendrán suaves, duraderas y listas para ser usadas en todo momento.',
  },
  'media-jersey-borgona': {
    prompt: 'media jersey borgona',
    opinionBody:
      'El jersey de media borgoña ha recibido excelentes opiniones de los usuarios. La combinación de su suave y cálido material, junto con su hermoso color borgoña, lo convierten en una opción elegante y confortable para cualquier ocasión. Los usuarios han elogiado el ajuste perfecto y la calidad de la confección, así como la durabilidad del material. Además, el color borgoña es muy versátil y combina fácilmente con una variedad de prendas y estilos.',
    combineBody:
      'El jersey de media borgoña es muy versátil y se puede combinar de diferentes maneras para crear looks únicos. Para un look casual y relajado, puedes combinarlo con unos jeans y zapatillas blancas. Si buscas un look más elegante, puedes combinarlo con una falda lápiz y tacones negros. Para un look más sofisticado, puedes combinarlo con unos pantalones negros y unos botines de tacón. Incluso puedes usarlo como capa sobre un vestido o una blusa para agregar un toque de estilo y calidez a tu outfit.',
    cleanBody:
      'Para mantener el jersey de media borgoña en perfectas condiciones, se recomienda seguir las instrucciones de cuidado indicadas en la etiqueta. En general, es preferible lavarlo a mano o en ciclo delicado en la lavadora con agua fría y detergente para prendas delicadas. Evita usar lejía y secarlo en secadora, ya que esto podría dañar el material. Después del lavado, es importante secarlo en una superficie plana y evitar retorcerlo para evitar deformaciones. Si necesitas quitar arrugas, puedes plancharlo a baja temperatura o utilizar vapor. Siguiendo estas recomendaciones, podrás disfrutar de tu jersey de media borgoña por mucho tiempo.',
  },
  'media-jersey-morado': {
    prompt: 'media jersey morado',
    opinionBody:
      'El jersey morado de media ha recibido excelentes críticas por parte de los usuarios. Su diseño elegante y moderno lo convierte en una opción perfecta para cualquier ocasión. El material utilizado en su fabricación garantiza su durabilidad y comodidad. Además, el color morado añade un toque de estilo único y sofisticado. Sin duda alguna, este jersey es una elección acertada para quienes buscan lucir a la moda y sentirse cómodos al mismo tiempo.',
    combineBody:
      'El jersey morado de media es una prenda versátil que puede combinarse de diversas formas. Para un look casual y relajado, puedes combinarlo con unos jeans oscuros y zapatillas blancas. Si deseas un estilo más elegante, puedes lucirlo con unos pantalones negros y unos zapatos de tacón. Para agregar un toque más atrevido, puedes combinarlo con una falda estampada y unas botas altas. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener el jersey morado de media en perfecto estado, es importante seguir algunas recomendaciones de limpieza y mantenimiento. Se aconseja lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo para escurrirlo y sécalo a la sombra. Si prefieres lavarlo a máquina, utiliza un programa delicado y colócalo dentro de una bolsa de lavado para protegerlo. Además, es recomendable plancharlo a baja temperatura y almacenarlo en un lugar fresco y seco. Siguiendo estas indicaciones, podrás disfrutar de tu jersey morado de media por mucho tiempo.',
  },
  'media-organdi-aguamarina': {
    prompt: 'media organdi aguamarina',
    opinionBody:
      'El vestido de media organdi aguamarina ha sido muy bien recibido por nuestras usuarias. Muchas destacan la ligereza y frescura del material, perfecto para eventos de primavera o verano. Además, el color aguamarina es el favorito de muchas, ya que aporta luminosidad y realza el tono de piel. En cuanto al ajuste, varias mencionan que el corte favorece la silueta y resalta las curvas. En resumen, las opiniones sobre el vestido de media organdi aguamarina son muy positivas, destacando la calidad, comodidad y estilo elegante que ofrece.',
    combineBody:
      'El vestido de media organdi aguamarina es una prenda versátil que se puede combinar de diferentes maneras. Para un look más sofisticado, recomendamos combinarlo con tacones altos y unos accesorios dorados. Si buscas un estilo más casual, puedes añadir unas zapatillas blancas y una chaqueta de denim. Además, el color aguamarina combina muy bien con tonos neutros como el blanco, negro o gris, por lo que puedes optar por unos zapatos en esos colores y un bolso a juego. En definitiva, las opciones de combinación con el vestido de media organdi aguamarina son infinitas, ¡solo depende de tu estilo y creatividad!',
    cleanBody:
      'Para mantener el vestido de media organdi aguamarina en las mejores condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Recomendamos lavarlo a mano con agua fría y un detergente suave. Evita el uso de suavizantes y no lo retuerzas al escurrirlo. Después de lavarlo, sécalo al aire libre en una superficie plana, evitando la exposición directa al sol para evitar que se decolore. Si deseas plancharlo, asegúrate de hacerlo a baja temperatura o utilizando una tela protectora para evitar dañar el material. Siguiendo estos consejos, podrás disfrutar de tu',
  },
  'media-punto-esmeralda': {
    prompt: 'media punto esmeralda',
    opinionBody:
      'Las medias de punto en color esmeralda son una opción elegante y vanguardista para complementar cualquier atuendo. Muchos usuarios coinciden en que este tono de verde intenso agrega un toque de frescura y sofisticación a sus outfits. Además, destacan la calidad y durabilidad del material utilizado en la fabricación de estas medias, lo que las convierte en una opción confiable para lucir a la moda en cualquier ocasión.',
    combineBody:
      'La versatilidad de las medias de punto en color esmeralda permite combinarlas de diversas maneras. Pueden ser el complemento ideal para un vestido negro, creando un contraste llamativo y elegante. También se pueden combinar con una falda o pantalón en tonos neutros como gris o beige, para agregar un toque de color sin restarle protagonismo al conjunto. Para un look más atrevido, se pueden combinar con un conjunto estampado en tonos verdes y azules, creando un estilo audaz y moderno.',
    cleanBody:
      'Para asegurar que tus medias de punto en color esmeralda se mantengan en óptimas condiciones, es importante seguir algunos consejos de limpieza. Se recomienda lavarlas a mano con agua fría y un detergente suave, evitando el uso de blanqueadores o productos químicos agresivos. Es importante secarlas al aire libre, evitando la exposición directa al sol. Para prolongar su vida útil, se aconseja guardarlas en un lugar fresco y seco, evitando el contacto con objetos afilados que puedan dañar el tejido. Siguiendo estas recomendaciones, podrás disfrutar de tus medias de punto esmeralda por mucho tiempo.',
  },
  'media-seda-marino': {
    prompt: 'media seda marino',
    opinionBody:
      'Las medias de seda en color marino son muy populares entre las mujeres que buscan una prenda elegante y sofisticada para complementar sus outfits. Los usuarios destacan la suavidad y comodidad del material, que brinda una sensación de lujo y delicadeza. Además, el color marino es muy versátil y combina perfectamente con una amplia gama de colores, lo que permite utilizar estas medias en diferentes ocasiones y estilos. En general, las opiniones son positivas, resaltando la calidad y belleza de estas medias de seda en color marino.',
    combineBody:
      'La media de seda en color marino es una pieza clave en el armario de cualquier mujer. Esta prenda se puede combinar de múltiples formas, ya sea para lucir un look elegante y formal o para dar un toque de estilo a un outfit más casual. Por ejemplo, se puede combinar con un vestido negro ajustado y unos zapatos de tacón alto para una ocasión formal, o con una falda plisada y una blusa blanca para un look más casual pero sofisticado. También puedes jugar con tonos complementarios, como el gris o el blanco, para crear combinaciones armoniosas. La versatilidad del color marino te permitirá crear looks muy variados con esta media de seda.',
    cleanBody:
      'Para mantener tus medias de seda en color marino en perfecto estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavar las medias a mano con agua fría y un detergente suave. Evita retorcerlas o frotarlas con fuerza, ya que esto podría dañar el tejido de seda. Después de lavarlas, sécalas al aire libre, evitando la exposición directa al sol. Si es necesario plancharlas, utiliza la temperatura más baja y coloca un paño entre la plancha y las medias para proteger el tejido. Almacenalas en un lugar fresco y seco, alejadas de la luz directa del sol y de elementos que puedan causarles enganches o desgastes. Siguiendo estos consejos',
  },
  'media-tafetan-turquesa': {
    prompt: 'media tafetan turquesa',
    opinionBody:
      'Las opiniones sobre estas medias de tafetán turquesa son muy positivas. Los usuarios destacan la suavidad y la comodidad que proporcionan, así como el ajuste perfecto que obtienen al usarlas. Además, el color turquesa brillante y llamativo es muy apreciado, ya que agrega un toque de estilo y originalidad a cualquier conjunto. Sin duda, estas medias de tafetán turquesa son una excelente elección para lucir atractiva y elegante en cualquier ocasión.',
    combineBody:
      'Estas medias de tafetán turquesa son extremadamente versátiles a la hora de combinarlas. Puedes optar por un look más casual y juvenil usándolas con una falda plisada o un vestido floral, añadiendo una camiseta blanca y unos zapatos deportivos. Si prefieres un estilo más elegante y sofisticado, puedes combinarlas con un vestido de noche en negro o blanco, unos tacones altos y un clutch a juego. En definitiva, las posibilidades de combinación con estas medias son infinitas, lo que te permitirá crear looks únicos y deslumbrantes.',
    cleanBody:
      'Para mantener estas medias de tafetán turquesa en perfecto estado, se recomienda seguir algunas pautas de limpieza y cuidado. En primer lugar, es importante lavarlas a mano con agua fría y un detergente suave, evitando el uso de lejía y suavizantes. Después de lavarlas, es recomendable secarlas al aire libre, evitando la exposición directa al sol. Es importante tener cuidado al ponerlas y quitarlas, evitando engancharlas con joyas o uñas afiladas, ya que esto podría dañar el tejido. Siguiendo estos consejos de limpieza y cuidado, tus medias de tafetán turquesa se mantendrán impecables y podrás disfrutar de ellas durante mucho tiempo.',
  },
  'pantalones-encaje-plata': {
    prompt: 'pantalones encaje plata',
    opinionBody:
      'Los pantalones de encaje plata son una elección audaz y llamativa para cualquier ocasión. La combinación del encaje y el color plata crea un aspecto elegante y sofisticado que seguramente llamará la atención. Las opiniones de quienes han probado estos pantalones son en su mayoría positivas, destacando la alta calidad y la comodidad que ofrecen. Muchos usuarios también elogian cómo realzan su figura y les dan un toque de glamour. En definitiva, los pantalones de encaje plata son una elección segura para aquellos que desean lucir a la moda y destacar entre la multitud.',
    combineBody:
      'Los pantalones de encaje plata son una prenda versátil que se puede combinar de diferentes maneras para lograr distintos estilos. Para un look elegante y formal, puedes combinarlos con una blusa de seda en tonos neutros y unos tacones altos. Si buscas un aspecto más casual y divertido, puedes optar por una camiseta gráfica y unas zapatillas blancas. Para una opción más audaz y arriesgada, puedes combinarlos con una chaqueta de cuero y unos botines negros. Sea cual sea tu estilo, los pantalones de encaje plata te permiten crear looks únicos y originales.',
    cleanBody:
      'Cuando se trata de limpiar y mantener los pantalones de encaje plata, es importante seguir ciertos cuidados. Recomendamos lavarlos a mano con agua fría y un detergente suave. Evita retorcerlos y secarlos al sol directo, ya que esto puede dañar el encaje y hacer que el color se desvanezca. Si es posible, coloca los pantalones en una bolsa de lavado antes de ponerlos en la lavadora. También es importante leer las instrucciones de cuidado específicas del fabricante, ya que algunas prendas de encaje requieren un cuidado diferente. Con los cuidados adecuados, tus pantalones de encaje plata lucirán impecables y',
  },
  'pantalones-lino-bronce': {
    prompt: 'pantalones lino bronce',
    opinionBody:
      'Los pantalones de lino en color bronce son una elección elegante y sofisticada para cualquier ocasión. Su tejido ligero y transpirable los hace perfectos para los meses de verano, ya que te mantendrán fresco y cómodo durante todo el día. Además, el color bronce agrega un toque de estilo y distinción a tu look. Los usuarios han destacado la calidad y durabilidad de estos pantalones, así como la versatilidad para combinar con diferentes prendas y estilos. Sin duda, los pantalones de lino bronce son una opción perfecta si buscas un look sofisticado y moderno.',
    combineBody:
      'Los pantalones de lino en color bronce ofrecen muchas posibilidades al momento de combinarlos. Puedes optar por una camisa blanca o de colores neutros para un look más clásico y elegante. Si quieres añadir un toque de color, puedes combinarlos con una camisa en tonos pastel o con estampados sutiles. Para obtener un look más casual, puedes usarlos con una camiseta básica y unas zapatillas blancas. Además, puedes completar tu outfit con complementos en tonos tierra o dorados para resaltar el color bronce de los pantalones. La versatilidad de los pantalones de lino bronce te permitirá crear looks para diferentes ocasiones y estilos.',
    cleanBody:
      'Mantener tus pantalones de lino bronce en buen estado es fundamental para que luzcan siempre impecables. Asegúrate de seguir las instrucciones de lavado indicadas en la etiqueta del producto. Por lo general, se recomienda lavarlos a mano o en ciclo suave en la lavadora con agua fría. Es importante utilizar un detergente suave o específico para prendas delicadas para evitar dañar las fibras del lino. Evita el uso de blanqueadores y suavizantes, ya que pueden afectar la calidad y color del tejido. Al terminar el lavado, es recomendable secar los pantalones al aire libre, evitando la exposición',
  },
  'pantalones-lino-marron': {
    prompt: 'pantalones lino marron',
    opinionBody:
      'Los pantalones de lino marrón son una opción excelente para aquellos que buscan comodidad y estilo durante los días cálidos de verano. Muchos usuarios los elogian por su suavidad y transpirabilidad, lo que los convierte en una prenda ideal para mantenerse fresco en climas calurosos. Además, el color marrón ofrece versatilidad y la posibilidad de combinarlos con una amplia variedad de outfits.',
    combineBody:
      'Los pantalones de lino marrón pueden combinarse de diversas formas para crear looks elegantes y casuales. Para un outfit de oficina, puedes combinarlos con una camisa blanca y unos zapatos de vestir. Si prefieres un estilo más relajado, puedes optar por una camiseta neutra y unas zapatillas blancas. También puedes añadir un toque de color con accesorios, como una bufanda o un cinturón en tonos llamativos.',
    cleanBody:
      'Para mantener tus pantalones de lino marrón en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarlos a mano o en ciclo delicado con agua fría para evitar que se encojan. Evita utilizar suavizantes de telas, ya que pueden dañar las fibras de lino. Si es necesario, plancha los pantalones a temperatura baja mientras están todavía húmedos. Almacenarlos en un lugar fresco y seco ayudará a evitar arrugas y deformaciones en el tejido.',
  },
  'pantalones-punto-claro': {
    prompt: 'pantalones punto claro',
    opinionBody:
      'Los pantalones de punto claro son una elección popular entre aquellos que buscan comodidad y estilo. Los usuarios han elogiado la suavidad y flexibilidad del material de punto, que proporciona una sensación de comodidad durante todo el día. Además, el color claro agrega un toque de frescura y versatilidad a cualquier conjunto. Las opiniones positivas destacan la calidad de la confección y la durabilidad de los pantalones, lo que los convierte en una inversión a largo plazo.',
    combineBody:
      'Los pantalones de punto claro son extremadamente versátiles y se pueden combinar con una amplia variedad de prendas y estilos. Para un look casual y relajado, puedes combinarlos con una camiseta básica y zapatillas blancas. Para un look más elegante, opta por combinarlos con una blusa o una camisa de seda y unos zapatos de tacón. También puedes agregar una chaqueta o una blazer para completar el conjunto. Los colores neutros, como el blanco, el negro o el beige, son opciones seguras para combinar con los pantalones de punto claro.',
    cleanBody:
      'Para mantener los pantalones de punto claro en buen estado, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlos a máquina en agua fría y usar un detergente suave. Evita usar blanqueador, ya que puede dañar el color claro del tejido. Trata las manchas de inmediato, utilizando un quitamanchas suave y siguiendo las indicaciones del producto. Para secarlos, es preferible colgarlos o extenderlos sobre una superficie plana en lugar de usar la secadora, lo que puede provocar que el tejido se encoja o se dañe. Finalmente, plancha los pantalones a baja temperatura si es necesario, prestando atención a las recomendaciones del fabricante.',
  },
  'pantalones-tejido-beige': {
    prompt: 'pantalones tejido beige',
    opinionBody:
      'Los pantalones tejido beige son una elección popular entre los amantes de la moda. Su suave y cómodo material, combinado con su elegante color beige, los convierte en una prenda versátil y fácil de combinar. Los usuarios han elogiado su ajuste perfecto y su capacidad para realzar la figura. Además, la resistencia del tejido hace que estos pantalones sean duraderos y fáciles de mantener, lo cual es muy apreciado por los clientes.',
    combineBody:
      'Los pantalones tejido beige son una prenda que se puede combinar de diversas formas. Para un look casual, puedes combinarlos con una camiseta blanca y unas zapatillas deportivas. Si buscas un estilo más formal, puedes optar por una blusa elegante y unos tacones altos. Además, los accesorios dorados o plateados aportarán un toque de sofisticación. En épocas más frías, puedes añadir un blazer o una chaqueta de cuero para completar el conjunto.',
    cleanBody:
      'Para mantener los pantalones tejido beige en buen estado, se recomienda seguir algunas pautas de limpieza y mantenimiento. Es conveniente leer las instrucciones de cuidado específicas del fabricante antes de lavar los pantalones. En general, se sugiere lavarlos a máquina con agua fría y utilizar un detergente suave. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar el tejido. Para secarlos, es preferible colgarlos al aire libre o usar un secador a baja temperatura. Además, es importante realizar un planchado suave a baja temperatura para evitar marcas o arrugas en el tejido beige.',
  },
  'pantalones-tejido-verde': {
    prompt: 'pantalones tejido verde',
    opinionBody:
      'Los pantalones de tejido verde son extremadamente populares entre aquellos que buscan agregar un toque de estilo y originalidad a su guardarropa. Además de ser cómodos y versátiles, los pantalones de tejido verde también brindan un aspecto fresco y contemporáneo. Los usuarios han elogiado la calidad del material, destacando su suavidad y durabilidad. Los pantalones de tejido verde también han recibido comentarios positivos por su ajuste favorecedor y su capacidad para realzar la figura de quienes los usan. Con su amplia gama de colores y estilos, los pantalones de tejido verde se han convertido en una opción popular para cualquier ocasión.',
    combineBody:
      'Los pantalones de tejido verde son una prenda versátil que se puede combinar de muchas formas diferentes. Para un look casual y relajado, puedes optar por combinarlos con una camiseta blanca y unas zapatillas deportivas en colores neutros. Si quieres un estilo más elegante, puedes combinar los pantalones de tejido verde con una blusa de seda en tonos neutros y unos tacones altos. También puedes darle un toque más atrevido usando accesorios en tonos dorados o metálicos. Los pantalones de tejido verde son una opción perfecta para agregar un toque de color a cualquier conjunto.',
    cleanBody:
      'Para mantener tus pantalones de tejido verde en óptimas condiciones, es importante seguir algunas pautas de cuidado. Te recomendamos lavarlos siguiendo las instrucciones en la etiqueta de cuidado. En general, se recomienda lavarlos a mano o en ciclo suave en la lavadora con agua fría. Evita usar lejía o productos químicos fuertes que puedan dañar el tejido. Para el secado, lo mejor es colgarlos al aire libre o secarlos al aire en posición horizontal para evitar deformaciones. Si es necesario plancharlos, utiliza una temperatura baja o media y coloca un paño entre la plancha y los pantalones para evitar dañar el tejido. Con los cuid',
  },
  'pijama-algodon-oro': {
    prompt: 'pijama algodon oro',
    opinionBody:
      'La pijama de algodón en color oro es una opción elegante y confortable para la hora de dormir. Los usuarios que han adquirido este producto destacan la suavidad y frescura del tejido de algodón, que proporciona una sensación agradable al contacto con la piel. Además, el color oro agrega un toque de lujo y sofisticación al conjunto. Los comentarios positivos también mencionan la durabilidad del pijama, que se mantiene en buen estado lavado tras lavado. En resumen, los usuarios coinciden en que la pijama de algodón oro ofrece comodidad y estilo.',
    combineBody:
      'La pijama de algodón en color oro es una prenda versátil que se puede combinar con diferentes accesorios y calzado para crear looks tanto casuales como más formales. Para un look relajado, se puede complementar con unas zapatillas de estar por casa o unas pantuflas cómodas. Si se desea un estilo más elegante, se puede combinar con unos zapatos de salón en tonos neutros o dorados. Además, se puede añadir un kimono o una bata ligera en tonos similares para completar el conjunto. En resumen, la pijama de algodón oro se adapta a diferentes estilos y permite crear looks variados según la ocasión.',
    cleanBody:
      'Para mantener la pijama de algodón oro en buen estado, se recomienda seguir las instrucciones de lavado del fabricante. En general, se sugiere lavarla a máquina con agua fría y utilizar un detergente suave. Es importante evitar el uso de blanqueadores o productos químicos agresivos que puedan alterar el color del tejido. Después de lavarla, se recomienda secarla al aire libre o a baja temperatura en la secadora para evitar que se encoja. Además, se aconseja plancharla a temperatura media o utilizar un ajuste específico para prendas de algodón. Siguiendo estos consejos de limpieza y mantenimiento, la pijama de algodón oro se mantendrá en perfecto estado durante mucho tiempo.',
  },
  'pijama-denim-gris': {
    prompt: 'pijama denim gris',
    opinionBody:
      'Los usuarios que han adquirido el pijama denim gris han quedado satisfechos con su compra. Destacan la comodidad que brinda el material denim y el suave color gris. Muchos mencionan que es perfecto para descansar en casa, ya que es ligero y fresco. Además, el diseño moderno y casual del pijama denim gris lo convierte en una prenda versátil que puede ser usada tanto para dormir como para relajarse en el sofá.',
    combineBody:
      'El pijama denim gris es muy fácil de combinar debido a su tonalidad neutra. Puedes crear un look relajado combinándolo con unas zapatillas blancas y una chaqueta de punto para esos días fríos. También puedes agregar un toque de estilo añadiendo unos accesorios en tonos plateados o negros. Si buscas un estilo más informal, puedes complementarlo con unas sandalias y un bolso de tela. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu pijama denim gris en perfectas condiciones, te recomendamos seguir las instrucciones de cuidado proporcionadas por el fabricante. Por lo general, se recomienda lavar en agua fría y utilizar un detergente suave. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el material. Para mantener el color gris vibrante, puedes añadir un poco de vinagre blanco durante el lavado. Recuerda secarlo al aire libre y planchar a temperatura baja si es necesario. Siguiendo estos consejos, tu pijama denim gris lucirá como nuevo por mucho tiempo.',
  },
  'pijama-lana-rojo': {
    prompt: 'pijama lana rojo',
    opinionBody:
      'Los usuarios destacan la comodidad y calidez que brinda esta pijama de lana en color rojo. Además, el material de lana es apreciado por su suavidad y capacidad de mantener el calor corporal durante las noches más frías. Sin duda, este pijama de lana rojo es una elección acertada para aquellos que buscan una prenda cómoda y abrigadora para descansar.',
    combineBody:
      'Para crear un conjunto perfecto con esta pijama de lana rojo, puedes combinarla con unas zapatillas de casa en tonos neutros, como blanco o gris. Además, puedes añadir una bata o una manta de lana a juego para crear un look armonioso y acogedor. Este conjunto te mantendrá cómodo y elegante en tus momentos de relajación en casa.',
    cleanBody:
      'Para mantener tu pijama de lana rojo en perfecto estado, es importante seguir algunas recomendaciones. Lávala a mano o en ciclo suave en la lavadora con agua fría y utiliza un detergente suave. Evita retorcerla y sécala al aire libre para evitar deformaciones. Recuerda no plancharla directamente y almacenarla en un lugar seco y ventilado. Siguiendo estos cuidados, podrás disfrutar de tu pijama de lana rojo durante mucho tiempo.',
  },
  'pijama-punto-blanco': {
    prompt: 'pijama punto blanco',
    opinionBody:
      'El pijama de punto blanco es todo lo que necesitas para disfrutar de noches de descanso y confort. Los usuarios coinciden en que su material de punto es suave y cómodo, perfecto para relajarse después de un largo día. Además, el color blanco le da un toque de elegancia y pureza. Sin duda, es una excelente opción para aquellos que buscan comodidad y estilo en sus prendas de dormir.',
    combineBody:
      'El pijama de punto blanco combina a la perfección con cualquier tipo de calzado y accesorios. Puedes optar por unas zapatillas de casa cómodas para completar el look relajado. Si quieres llevarlo fuera de casa, puedes combinarlo con unas sandalias o incluso zapatillas deportivas para lograr un estilo más casual. En cuanto a accesorios, un gorro de dormir o una mascarilla facial a juego le darán un toque divertido y original.',
    cleanBody:
      'Eliminar las manchas y mantener tu pijama de punto blanco en buen estado es muy sencillo. Puedes lavarlo a mano o a máquina en un ciclo suave con agua fría. Utiliza un detergente suave y evita el uso de lejía para evitar dañar el color blanco. Es recomendable secarlo al aire libre o a baja temperatura en la secadora. Para evitar la formación de bolitas en el tejido, puedes darle la vuelta al pijama antes de lavarlo. Sigue estos sencillos consejos de limpieza y disfrutarás de tu pijama punto blanco durante mucho tiempo.',
  },
  'pijama-saten-marino': {
    prompt: 'pijama saten marino',
    opinionBody:
      'Los usuarios opinan que el pijama de satén marino es una prenda sumamente cómoda y elegante. El material de satén le da un toque de glamour, mientras que el color marino le añade un aire sofisticado. Además, destacan que el satén es suave al tacto y se siente agradable en la piel, lo que garantiza una experiencia de descanso placentera. En general, las opiniones son muy positivas y recomiendan este pijama para quienes buscan comodidad y estilo durante la noche.',
    combineBody:
      'El pijama de satén marino puede combinarse de diferentes formas para crear looks tanto para la noche como para el día. Para un look más lujoso y elegante, puedes combinarlo con una bata de satén del mismo color. Si buscas un estilo más relajado, puedes optar por combinarlo con una sudadera oversize y zapatillas deportivas para crear un outfit de loungewear. También puedes utilizar la parte superior del pijama como una blusa y combinarla con una falda o pantalón de vestir para lograr un look más formal. Las posibilidades son infinitas, ¡sólo depende de tu creatividad!',
    cleanBody:
      'Para mantener tu pijama de satén marino en óptimas condiciones, es recomendable seguir algunas pautas de cuidado básicas. En primer lugar, se recomienda lavar a mano con agua fría y detergente suave para prendas delicadas. Evita el uso de lejía o productos que puedan dañar el material. Si prefieres utilizar la lavadora, selecciona un programa de lavado suave y utiliza una bolsa de lavado para proteger el pijama. Para secarlo, es mejor colgarlo en una percha y evitar la exposición directa al sol. Si necesitas plancharlo, utiliza una temperatura baja y coloca un paño fino sobre el satén para evitar dañarlo. Siguiendo estas simples recomendaciones, mantendrás tu pijama de satén marino en perfectas condiciones durante mucho tiempo.',
  },
  'pijama-tafetan-plata': {
    prompt: 'pijama tafetan plata',
    opinionBody:
      'El pijama de tafetán plata ha recibido excelentes críticas por parte de los usuarios. Muchos destacan su suavidad y comodidad, afirmando que es perfecto para descansar durante la noche. Además, el color plata le da un toque elegante y sofisticado, haciendo que sea una prenda versátil que puede utilizarse también como conjunto para estar en casa.',
    combineBody:
      'El pijama de tafetán plata es una prenda muy fácil de combinar. Puede utilizarse con zapatillas de casa en tonos neutros o incluso con tacones para crear un look más arreglado. Si se quiere utilizar como conjunto para estar en casa, se puede combinar con una bata en un tono similar o en blanco. También puede ser complementado con joyas sencillas para añadir un toque de elegancia.',
    cleanBody:
      'Para limpiar y mantener el pijama de tafetán plata, es recomendable seguir las instrucciones de lavado y cuidado del fabricante. Generalmente, este tipo de prendas puede lavarse a mano o a máquina con agua fría y un detergente suave. Es importante evitar usar lejía y secar al aire libre o a baja temperatura para evitar que el color se desgaste. Además, se recomienda planchar a baja temperatura o utilizar vapor para eliminar arrugas si es necesario.',
  },
  'pijama-terciopelo-bronce': {
    prompt: 'pijama terciopelo bronce',
    opinionBody:
      'El pijama de terciopelo bronce ha recibido excelentes opiniones por parte de los usuarios. Muchos destacan su suavidad y comodidad, ya que el terciopelo proporciona una sensación de lujo mientras se duerme. Además, el color bronce hace que este pijama sea elegante y sofisticado. Los clientes también elogian la calidad del material, que se mantiene en perfectas condiciones incluso después de varios lavados. Sin duda, el pijama de terciopelo bronce es una excelente elección para quienes buscan estilo y confort durante la noche.',
    combineBody:
      'El pijama de terciopelo bronce es una prenda versátil que se puede combinar de diferentes formas. Para un look elegante en casa, puedes combinarlo con unas zapatillas de terciopelo a juego. Si prefieres un estilo más casual, puedes añadir una chaqueta de punto en color neutro y unos pantuflas cómodas. Para una ocasión especial, puedes combinarlo con unas sandalias de tacones bajos y unos pendientes dorados. El pijama de terciopelo bronce te da la oportunidad de crear outfits únicos y atractivos, tanto dentro como fuera de casa.',
    cleanBody:
      'Para mantener tu pijama de terciopelo bronce en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Siempre lee y sigue las instrucciones de lavado en la etiqueta del producto. En general, se recomienda lavar a mano o a máquina en un programa para prendas delicadas utilizando agua fría. Evita el uso de suavizantes y secadoras, ya que podrían dañar el terciopelo. Para quitar manchas, utiliza un detergente suave y frota suavemente con un cepillo suave. Una vez limpio, deja secar al aire libre y plancha a baja temperatura si es necesario. Siguiendo estos consejos, tu pijama de terciopelo bronce se mantendrá como nuevo durante mucho más tiempo.',
  },
  'pollera-denim-lima': {
    prompt: 'pollera denim lima',
    opinionBody:
      'La pollera denim en color Lima es una opción fresca y moderna para lucir en cualquier ocasión. Los usuarios que la han adquirido destacan la calidad del material, su ajuste perfecto y lo versátil que resulta en distintos outfits. Además, el color Lima le da un toque vibrante y llamativo, convirtiéndola en una prenda especialmente popular para la temporada de primavera-verano. Sin duda, la pollera denim Lima es una elección acertada para quienes buscan estilo y comodidad.',
    combineBody:
      'La pollera denim Lima es una prenda muy versátil que puede combinarse de diferentes formas para crear looks únicos y a la moda. Para un outfit casual y relajado, puedes utilizarla con una camiseta blanca básica y zapatillas blancas o deportivas. Si prefieres un estilo más sofisticado, puedes combinarla con una blusa de seda en tonos neutros y sandalias de tacón alto. Para un look boho chic, puedes optar por una camisa estampada y unas botas camperas. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu pollera denim Lima en óptimas condiciones, es recomendable seguir algunas pautas de cuidado. Primero, es importante leer las instrucciones de lavado que vienen en la etiqueta del producto. En general, se recomienda lavarla del revés y a mano o en ciclo delicado en la lavadora, utilizando detergente suave. Evita usar blanqueadores o productos químicos agresivos. Para secarla, lo mejor es colgarla al aire libre o en un lugar fresco y evitar la exposición directa al sol. Con estos cuidados, tu pollera denim Lima se mantendrá como nueva por mucho tiempo.',
  },
  'pollera-encaje-plata': {
    prompt: 'pollera encaje plata',
    opinionBody:
      'Esta pollera de encaje plateada ha recibido opiniones positivas por parte de nuestras usuarias. Muchas destacan su elegancia y versatilidad, convirtiéndola en una prenda perfecta para ocasiones especiales. La combinación del encaje y el color plateado le otorgan un toque sofisticado y glamoroso.',
    combineBody:
      'La pollera de encaje plata es una prenda muy versátil que puede ser combinada de varias maneras. Para un look más formal, puedes combinarla con una blusa de seda en tonos neutros y unos zapatos de tacón alto. Si buscas un outfit más casual, puedes combinarla con una camiseta básica y unas zapatillas blancas. También puedes agregar accesorios en tonos plateados para completar el look.',
    cleanBody:
      'Para mantener la pollera de encaje plata en buen estado, es recomendable seguir las instrucciones de lavado que vienen en la etiqueta del producto. En general, se recomienda lavar a mano o en ciclo delicado en la lavadora con agua fría y detergente suave. Es importante no retorcer ni frotar bruscamente la prenda. Después del lavado, se puede colgar para que se seque al aire libre, evitando la exposición directa al sol. En caso de manchas, es recomendable tratarlas de inmediato con un quitamanchas suave.',
  },
  'pollera-franela-marron': {
    prompt: 'pollera franela marron',
    opinionBody:
      'La pollera de franela marrón es una prenda muy versátil y cómoda que ha recibido excelentes comentarios por parte de nuestros clientes. Destacan la calidad del material y su suavidad al contacto con la piel. Además, el color marrón es muy elegante y fácil de combinar, lo que la convierte en una opción ideal para cualquier ocasión. Sin duda, es una elección acertada para lucir a la moda y sentirse cómoda a la vez.',
    combineBody:
      'El color marrón de la pollera de franela permite una amplia gama de combinaciones. Para un look casual, puedes combinarla con una camiseta básica blanca y zapatillas deportivas. Si deseas un outfit más elegante, puedes optar por una blusa de seda en tonos neutros, tacones y accesorios dorados. Incluso puedes usarla con una chaqueta de cuero y botas altas para un look más rebelde y sofisticado. Las posibilidades son infinitas, y con la pollera de franela marrón siempre lucirás a la moda.',
    cleanBody:
      'Para mantener la pollera de franela marrón en óptimas condiciones, se recomienda seguir algunas pautas de limpieza. Se sugiere lavarla a mano con agua fría y un detergente suave. Evita usar blanqueadores y suavizantes que puedan dañar el tejido. Después de lavarla, es importante secarla al aire libre y evitar la exposición directa al sol para evitar que pierda color. También se sugiere plancharla a temperatura baja y guardarla en un lugar fresco y seco para evitar arrugas y malos olores. Siguiendo estos consejos, disfrutarás de tu pollera de franela marrón por mucho tiempo.',
  },
  'pollera-lana-lima': {
    prompt: 'pollera lana lima',
    opinionBody:
      '-Los usuarios que han adquirido esta pollera de lana en color lima destacan su comodidad y suavidad. Además, mencionan que es una prenda versátil que puede ser utilizada tanto en ocasiones formales como informales. El color lima le da un toque de frescura y originalidad, siendo perfecta para destacar en cualquier evento.',
    combineBody:
      '-La pollera de lana en color lima combina a la perfección con una amplia variedad de colores y estilos. Puedes optar por combinarla con una blusa blanca para un look clásico y elegante, o arriesgarte con un top estampado para un look más moderno y divertido. También puedes añadir accesorios en tonos neutros como negro o beige para complementar y resaltar aún más este vibrante color.',
    cleanBody:
      '-Para mantener tu pollera de lana en color lima en perfectas condiciones, es recomendable seguir estas indicaciones de cuidado. Limpia la prenda a mano o en ciclo delicado en la lavadora, utilizando agua tibia y un detergente suave. Es importante no retorcerla y secarla a la sombra, evitando la exposición directa al sol. Para eliminar pelusas, utiliza un rodillo quitapelusas o una esponja especial para prendas de lana. Siguiendo estos sencillos consejos, tu pollera de lana en color lima se mantendrá como nueva durante mucho tiempo.',
  },
  'pollera-poliester-marron': {
    prompt: 'pollera poliester marron',
    opinionBody:
      'La pollera de poliéster marrón ha recibido opiniones positivas en general. Los usuarios destacan su comodidad y versatilidad, ya que se puede usar en diferentes ocasiones y estilos. Además, el poliéster es un material duradero y resistente, lo que la convierte en una prenda de larga duración. En cuanto al color marrón, muchos comentan que es fácil de combinar con diferentes tonalidades, lo que la convierte en una opción ideal para cualquier guardarropa.',
    combineBody:
      'La versatilidad del color marrón hace que la pollera de poliéster sea fácil de combinar con diferentes prendas y colores. Para un look casual, se puede combinar con una camiseta blanca y zapatillas blancas o negras. Para un estilo más elegante, se puede combinar con una blusa o camisa en tonos neutros como blanco, beige o negro, y agregar unos zapatos de tacón. También se puede jugar con la combinación de estampados y texturas utilizando una blusa estampada o un suéter de punto. Las posibilidades son infinitas, lo importante es encontrar el estilo que mejor se adapte a cada persona.',
    cleanBody:
      'La pollera de poliéster marrón es fácil de mantener y limpiar. La mayoría de las veces, se puede lavar a máquina en ciclo suave con agua fría o tibia. Es recomendable utilizar detergente suave y evitar el uso de suavizantes, ya que pueden afectar la calidad del poliéster. Para secarla, se recomienda colgarla en una percha o dejarla secar al aire libre, evitando la exposición directa al sol. En caso de manchas difíciles, se puede tratar con un quitamanchas suave antes de lavarla. Siguiendo estas sencillas instrucciones, la pollera de poliéster marrón mantendrá su aspecto impecable durante mucho tiempo.',
  },
  'pollera-punto-gris': {
    prompt: 'pollera punto gris',
    opinionBody:
      'La pollera punto gris ha recibido excelentes opiniones por parte de quienes han tenido la oportunidad de usarla. Su material de punto le brinda una sensación de comodidad y suavidad al contacto con la piel, y su color gris resulta versátil y fácil de combinar. Además, su corte y ajuste favorecen la silueta, lo que la convierte en una prenda muy favorecedora. Sin duda, la pollera punto gris es una elección acertada para lucir elegante y sentirse cómoda a la vez.',
    combineBody:
      'La pollera punto gris se puede combinar de diversas formas para lograr diferentes looks. Para un estilo casual y relajado, puedes combinarla con una camiseta básica blanca y unas zapatillas deportivas. Si prefieres un look más sofisticado, puedes usarla con una blusa de seda en tonos neutros y unos tacones elegantes. Esta pollera también queda genial con una chaqueta de cuero y botas altas para un estilo más roquero. En resumen, la pollera punto gris es tan versátil que se adapta a cualquier estilo y ocasión.',
    cleanBody:
      'Para mantener la pollera punto gris en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, se recomienda leer y seguir las instrucciones de lavado que vienen en la etiqueta de la prenda. En general, las prendas de punto requieren un lavado suave a mano o en ciclo delicado en la lavadora, utilizando agua fría o templada y un detergente suave. Es importante evitar el uso de blanqueadores y secar la prenda al aire libre, preferiblemente sobre una toalla plana para que no pierda su forma. Finalmente, es recomendable guardar la pollera punto gris doblada en un lugar seco y ventilado para evitar la aparición de arrugas o hongos. Siguiendo estos sencillos consejos, podrás disfrutar de tu pollera punto gris por mucho tiempo.',
  },
  'pollera-punto-plata': {
    prompt: 'pollera punto plata',
    opinionBody:
      'Los usuarios han expresado su satisfacción con esta pollera de punto plata. Destacan su comodidad y su versatilidad, ya que puede ser usada tanto en ocasiones formales como informales. Además, el material de punto le da un toque elegante y a la vez casual, convirtiéndola en una prenda imprescindible en el armario.',
    combineBody:
      'La pollera de punto plata es muy fácil de combinar. Puedes optar por un look más formal añadiendo una blusa en tonos neutros y unos tacones. Si prefieres un estilo más casual, puedes llevarla con una camiseta blanca y unas zapatillas deportivas. También puedes añadir algunos accesorios en tonos plateados para resaltar el color de la pollera y completar tu outfit.',
    cleanBody:
      'Para mantener la pollera de punto plata en buen estado, se recomienda lavarla a mano con agua fría y utilizando un detergente suave. Es importante evitar el uso de lejía y secarla horizontalmente para evitar que se deforme. Para mantener su brillo, se puede planchar a baja temperatura. Recuerda guardarla en un lugar fresco y seco para evitar la formación de arrugas.',
  },
  'pollera-rayon-esmeralda': {
    prompt: 'pollera rayon esmeralda',
    opinionBody:
      'Los usuarios que han adquirido esta pollera de rayón en color esmeralda han quedado enamorados de su suavidad y confort. Además, destacan su caída fluida y su elegante brillo, que la hacen perfecta para ocasiones especiales. Los colores vibrantes y la calidad del material han sido muy elogiados, convirtiéndola en una prenda muy valorada por quienes la han adquirido.',
    combineBody:
      'La pollera de rayón en color esmeralda es extremadamente versátil a la hora de combinar. Para un look formal, puede ser combinada con una blusa blanca y unos zapatos de tacón alto en tonos neutros como el negro o el beige. Para un estilo más casual, una camiseta básica en negro o blanco junto con unas zapatillas blancas o negras lucirán perfectas. Además, los accesorios dorados complementarán muy bien este conjunto, realzando el color esmeralda de la pollera.',
    cleanBody:
      'Para mantener la pollera de rayón en color esmeralda en óptimas condiciones, se recomienda lavarla a mano con agua fría y detergente suave. Se debe evitar el uso de lejía, ya que puede dañar el color. Es importante secarla al aire libre y evitar la exposición directa al sol, que puede hacer que el color se desgaste. Para plancharla, se debe usar una temperatura baja y colocar un paño entre la plancha y la tela para evitar posibles daños. Siguiendo estos cuidados, la pollera de rayón esmeralda se mantendrá intacta por mucho tiempo.',
  },
  'pollera-seda-bronce': {
    prompt: 'pollera seda bronce',
    opinionBody:
      'Esta hermosa pollera de seda color bronce ha recibido excelentes comentarios de los usuarios que la han probado. Su material de seda le otorga una textura suave y fluida que resalta la feminidad de quien la lleva puesta. Además, su color bronce agrega un toque de elegancia y sofisticación a cualquier outfit. Los usuarios aseguran que es una prenda versátil y fácil de combinar, perfecta tanto para ocasiones formales como informales.',
    combineBody:
      'La pollera de seda color bronce es una prenda muy versátil que puede combinarse de diversas formas. Para un look elegante y sofisticado, puedes combinarla con una blusa de seda en tonos neutros, como blanco o beige, y unos tacones altos. Si prefieres un estilo más casual, puedes optar por una camiseta básica de algodón en tonos claros, unas zapatillas blancas y una chaqueta de denim. Para completar el outfit, puedes agregar algunos accesorios en tonos dorados, como pulseras o un collar.',
    cleanBody:
      'Para mantener la pollera de seda color bronce en óptimas condiciones, es importante seguir algunas recomendaciones de cuidado. Es recomendable lavarla a mano con agua fría y un detergente suave. Evita retorcerla o frotarla demasiado, ya que puede dañar la delicada fibra de seda. Una vez lavada, es recomendable secarla al aire libre, evitando la exposición directa al sol. Si es necesario plancharla, utiliza una temperatura baja y coloca un paño protector sobre la prenda para evitar posibles quemaduras. Guarda la pollera de seda en un lugar fresco y seco, preferiblemente en una bolsa de tela para protegerla del polvo y la humedad. Siguiendo estos simples consejos, podrás disfrutar de tu pollera de seda color bronce por mucho tiempo.',
  },
  'pollera-tejido-electrico': {
    prompt: 'pollera tejido electrico',
    opinionBody:
      'La pollera de tejido eléctrico ha ganado popularidad entre las amantes de la moda debido a su textura única y moderna. Los usuarios destacan su comodidad y versatilidad, ya que se adapta a diferentes estilos y ocasiones. Además, el tejido eléctrico le da a la pollera un aspecto elegante y sofisticado, atrayendo la atención de quienes la usan. Sin embargo, algunos opinan que puede ser un poco más delicada debido al tejido especial, por lo que recomiendan tener cuidado al lavarla o plancharla.',
    combineBody:
      'La pollera de tejido eléctrico es una prenda perfecta para crear looks modernos y vanguardistas. Puedes combinarla con una blusa de seda en tonos neutros para obtener un estilo elegante y minimalista. Si quieres algo más audaz, puedes agregar una chaqueta de cuero o una camiseta gráfica. Para el calzado, unas botas altas o unos tacones de punta serán ideales para completar el look. En cuanto a los accesorios, opta por piezas simples pero llamativas, como unos pendientes largos o una pulsera statement.',
    cleanBody:
      'La pollera de tejido eléctrico requiere ciertos cuidados especiales para mantener su apariencia impecable. Es recomendable lavarla a mano con agua fría o a baja temperatura en la lavadora, utilizando un ciclo de centrifugado suave. Evita retorcerla para evitar deformaciones en el tejido. Para el secado, es preferible dejarla al aire libre o usar una secadora con temperatura baja. En cuanto al planchado, coloca un paño húmedo sobre la prenda y utiliza una plancha a baja temperatura para evitar dañar el tejido eléctrico. Con estos sencillos cuidados, tu pollera de tejido eléctrico se mantendrá en perfecto estado por mucho tiempo.',
  },
  'pollera-tejido-morado': {
    prompt: 'pollera tejido morado',
    opinionBody:
      'Las opiniones sobre la pollera de tejido morado son muy positivas. Los usuarios destacan la calidad del tejido, que es suave y cómodo al mismo tiempo. Además, el color morado proporciona un toque de elegancia y sofisticación al outfit. Muchas personas elogian el ajuste perfecto de la pollera, que realza la figura sin ser demasiado ajustada. En general, es un producto que ha recibido excelentes comentarios por su diseño, comodidad y estilo.',
    combineBody:
      'La pollera de tejido morado es muy versátil y se puede combinar de diferentes maneras. Para un look casual y relajado, puedes combinarla con una camiseta blanca básica y unas zapatillas blancas. Si deseas un outfit más elegante, puedes optar por una blusa de seda en tonos neutros, como el blanco o el beige, y complementarlo con unos zapatos de tacón en color nude. Para un estilo más arriesgado y llamativo, puedes combinarla con una blusa estampada en tonos contrastantes, como el amarillo o el azul. En definitiva, las opciones para combinar esta pollera son infinitas y te permitirán lucir siempre a la moda.',
    cleanBody:
      'Para mantener tu pollera de tejido morado en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y mantenimiento. En primer lugar, siempre lee las instrucciones de cuidado y lavado que vienen en la etiqueta del producto. Si la pollera es lavable a máquina, utiliza un ciclo suave y agua fría para evitar dañar el tejido. En caso de que sea necesario plancharla, hazlo a baja temperatura y con un paño protector para evitar quemaduras o marcas en el tejido. Recuerda siempre almacenarla en un lugar fresco y seco, colgada o doblada suavemente. Siguiendo estos consejos, tu pollera de tejido morado se verá como nueva durante mucho tiempo.',
  },
  'pollera-tejido-verde': {
    prompt: 'pollera tejido verde',
    opinionBody:
      'La pollera tejido verde ha recibido excelentes críticas por parte de los usuarios. Muchos elogian su suave y cómodo material, perfecto para lucir elegante y a la moda. Además, destacan su color vibrante y versátil, ideal para combinar con diferentes outfits. Sin duda, es una prenda que no puede faltar en tu armario.',
    combineBody:
      'Esta pollera tejido verde es extremadamente versátil y se puede combinar de muchas maneras. Para un look casual, puedes llevarla con una camiseta blanca y zapatillas blancas para un estilo fresco y relajado. Si buscas un look más elegante, combínala con una blusa negra y tacones altos para lucir sofisticada y glamorosa. Además, puedes jugar con accesorios en tonos dorados o plateados para darle un toque extra de estilo.',
    cleanBody:
      'Para mantener tu pollera tejido verde en perfecto estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a mano o en ciclo suave en la lavadora, con agua fría y utilizando detergente suave. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el tejido y el color. Para secarla, es preferible colgarla en un lugar fresco y ventilado. Si es necesario plancharla, asegúrate de hacerlo a baja temperatura y por el reverso de la prenda. Siguiendo estos consejos, podrás disfrutar de tu pollera tejido verde durante mucho tiempo.',
  },
  'pollera-tela-lima': {
    prompt: 'pollera tela lima',
    opinionBody:
      'La pollera de tela lima ha recibido excelentes opiniones por parte de quienes la han adquirido. Su diseño vibrante y fresco es perfecto para lucir en los días soleados. Además, su material de alta calidad garantiza comodidad y durabilidad. Sin duda, es una elección acertada para lucir a la moda y sentirse cómoda en cualquier ocasión.',
    combineBody:
      'La pollera de tela lima es una prenda versátil que se puede combinar de diversas formas. Puedes crear un look casual y relajado combinándola con una camiseta blanca y unas zapatillas blancas. Si buscas un estilo más elegante, puedes optar por una blusa de color neutro y unos zapatos de tacón en tonos neutros o metálicos. No tengas miedo de experimentar, la pollera de tela lima se adapta a diferentes estilos y te permitirá lucir increíble en cualquier ocasión.',
    cleanBody:
      'Para mantener tu pollera de tela lima en perfecto estado, es importante seguir algunas recomendaciones de limpieza. Primero, siempre revisa las instrucciones de cuidado en la etiqueta de la prenda. La mayoría de las veces, la pollera de tela lima se puede lavar a mano o a máquina con agua fría y detergente suave. Evita el uso de blanqueadores y suavizantes, ya que podrían deteriorar el color y la calidad de la tela. Al finalizar el lavado, deja secar al aire libre o utiliza la opción de secado a baja temperatura en la secadora. No planches la pollera a altas temperaturas, ya que podría dañar la tela. Siguiendo estos consejos, podrás disfrutar de tu pollera de tela lima por mucho más tiempo.',
  },
  'pollera-terciopelo-morado': {
    prompt: 'pollera terciopelo morado',
    opinionBody:
      'Los usuarios han quedado encantados con la pollera de terciopelo morado. Destacan la suavidad y la elegancia que aporta este material, así como el vibrante color morado. Además, resaltan la calidad de la prenda y la comodidad al llevarla puesta. Sin duda, una opción perfecta para lucir sofisticada y atractiva en cualquier ocasión.',
    combineBody:
      'La versatilidad de la pollera de terciopelo morado hace que sea muy fácil de combinar. Puedes crear un look elegante y formal combinándola con una blusa de seda en tonos neutros, como el blanco o el beige. Si buscas un estilo más casual, puedes optar por una camiseta básica en tonos claros y completar el conjunto con unas zapatillas blancas o unas botas negras. No temas experimentar, ya que el morado es un color que se puede combinar con muchos otros y crear looks únicos y llamativos.',
    cleanBody:
      'El terciopelo requiere un cuidado especial para mantener su aspecto y calidad. Para la limpieza de la pollera de terciopelo morado, se recomienda seguir las instrucciones de la etiqueta. En general, es mejor optar por la limpieza en seco, ya que el terciopelo puede ser delicado y sensible al agua. Además, es importante cepillar el terciopelo regularmente con un cepillo de cerdas suaves para eliminar el polvo y evitar que se acumule. Al guardar la pollera, es recomendable colgarla en una percha para evitar arrugas y mantener su forma. Con estos cuidados adecuados, tu pollera de terciopelo morado se mantendrá en excelente estado y podrás disfrutar de ella durante mucho tiempo.',
  },
  'pollera-vaquera-electrico': {
    prompt: 'pollera vaquera electrico',
    opinionBody:
      'La pollera vaquera eléctrico ha recibido excelentes opiniones de nuestros clientes. El material de denim utilizado brinda durabilidad y comodidad, mientras que el color eléctrico agrega un toque audaz a cualquier look. Los usuarios elogian su ajuste perfecto y su estilo moderno, convirtiéndola en una opción versátil para cualquier ocasión.',
    combineBody:
      'La pollera vaquera eléctrico es una prenda llamativa que se puede combinar de varias maneras. Para un look casual y relajado, puedes combinarla con una camiseta básica blanca y zapatillas deportivas. Si quieres un estilo más elegante, puedes optar por una blusa de seda en tonos neutros y tacones altos. Sea cual sea tu elección, esta pollera añadirá un toque de estilo vanguardista a tu outfit.',
    cleanBody:
      'Para mantener la pollera vaquera eléctrico en perfectas condiciones, te recomendamos seguir estas instrucciones de limpieza. Lava la prenda del revés con agua fría y utiliza un detergente suave. Evita el uso de blanqueadores y suavizantes, ya que pueden dañar el color eléctrico. Después de lavarla, sécala al aire libre o a máquina a baja temperatura. Asimismo, plancharla del revés y a baja temperatura ayudará a mantener su forma original. Siguiendo estas precauciones, disfrutarás de tu pollera vaquera eléctrico durante mucho tiempo.',
  },
  'publish-your-products': {
    prompt: 'publish your products',
    opinionBody: null,
    combineBody: null,
    cleanBody: null,
  },
  'remera-artificial-bronce': {
    prompt: 'remera artificial bronce',
    opinionBody:
      'Con qué combinar la remera artificial bronce: Esta remera artificial bronce es muy versátil y se puede combinar de varias maneras. Para un look casual, puedes combinarla con unos jeans oscuros y zapatillas blancas. Si buscas un outfit más elegante, puedes optar por combinarla con una falda lápiz negra y tacones altos. Además, puedes agregar accesorios dorados para resaltar aún más el color bronce de la remera. ¡Las posibilidades son infinitas!',
    combineBody: null,
    cleanBody: null,
  },
  'remera-gasa-azul': {
    prompt: 'remera gasa azul',
    opinionBody:
      'La remera de gasa azul ha recibido excelentes opiniones por parte de aquellos que la han probado. Muchos destacan su comodidad y ligereza, perfecta para los días calurosos de verano. Además, su color azul le da un toque fresco y vibrante que resalta cualquier look. Sin duda, es una prenda versátil y elegante que ha conquistado a los amantes de la moda.',
    combineBody:
      'La remera de gasa azul es tan versátil que puedes combinarla de múltiples formas. Para un estilo casual y relajado, puedes llevarla con unos jeans ajustados y zapatillas blancas. Si prefieres un look más sofisticado, combínala con una falda midi de color neutro y sandalias de tacón. Para darle un toque glamuroso, añade accesorios dorados. Sea cual sea tu elección, la remera de gasa azul será el complemento perfecto para cualquier conjunto.',
    cleanBody:
      'La remera de gasa azul requiere de cuidados especiales para mantenerla en óptimas condiciones. Te recomendamos lavarla a mano con agua fría y utilizar detergente suave. Evita retorcerla para evitar que se deformen las fibras. Después de lavarla, déjala secar al aire libre, alejada de la luz solar directa para evitar la decoloración. Para eliminar arrugas, utiliza una plancha a temperatura baja. Siguiendo estos consejos, podrás mantener tu remera de gasa azul luciendo como nueva por mucho tiempo.',
  },
  'remera-jacquard-celeste': {
    prompt: 'remera jacquard celeste',
    opinionBody:
      'La remera jacquard celeste ha recibido excelentes críticas por parte de los usuarios que la han adquirido. Su diseño en jacquard le da un toque de estilo único, mientras que su color celeste lo hace perfecto para cualquier ocasión. Además, el material utilizado en su fabricación es de alta calidad, lo que garantiza su durabilidad y comodidad. Sin duda, esta opción es muy recomendada por su excelente relación calidad-precio.',
    combineBody:
      'La remera jacquard celeste es muy versátil y fácil de combinar. Su color neutro permite crear outfits para diversas ocasiones. Puede ser utilizada con pantalones vaqueros para un look casual y relajado, o combinada con una falda o pantalón negro para una ocasión más formal. Para darle un toque extra de estilo, puedes añadir accesorios en tonos plateados o dorados. La remera jacquard celeste es una prenda clave en cualquier armario.',
    cleanBody:
      'Para mantener tu remera jacquard celeste en perfectas condiciones, es recomendable seguir algunas pautas. Se recomienda lavarla a mano con agua fría y detergente suave, evitando el uso de lejía. También es importante evitar retorcerla al secarla, simplemente déjala secar al aire libre. Para evitar la formación de arrugas, se puede planchar la prenda a baja temperatura. Con estos cuidados adecuados, tu remera jacquard celeste se mantendrá como nueva durante mucho tiempo.',
  },
  'remera-lino-morado': {
    prompt: 'remera lino morado',
    opinionBody:
      'Los usuarios han quedado encantados con esta remera de lino morado. Destacan la suavidad y frescura del material, ideal para los días calurosos de verano. Además, el color morado agrega un toque de originalidad y estilo a cualquier outfit. Sin duda, es una prenda que no puede faltar en el guardarropa.',
    combineBody:
      'Esta versátil remera de lino morado puede ser combinada de muchas formas. Para un look casual, puedes combinarla con jeans y zapatillas blancas. Si quieres algo más elegante, puedes optar por pantalones negros o falda midi en tonos neutros. Los accesorios dorados resaltarán aún más el color morado de la remera. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener esta remera de lino morado en buen estado, es importante seguir algunas recomendaciones. Se recomienda lavarla a mano con agua fría y detergente suave. Evita usar cloro o blanqueadores que puedan afectar el color. Al secarla, es mejor hacerlo al aire libre o a temperatura baja en la secadora. Además, es aconsejable plancharla a baja temperatura para evitar dañar las fibras de lino. Siguiendo estos consejos, tu remera de lino morado lucirá como nueva por mucho tiempo.',
  },
  'remera-punto-marron': {
    prompt: 'remera punto marron',
    opinionBody:
      'La remera de punto marrón es una opción clásica y versátil para cualquier ocasión. Los usuarios destacan su suavidad y comodidad, ya que el material de punto permite una gran elasticidad y un ajuste perfecto. Además, el color marrón es muy favorecedor y fácil de combinar, por lo que es una prenda muy valorada en el armario de cualquier persona. Sin duda, una opción segura y elegante.',
    combineBody:
      'La remera de punto marrón es una prenda básica que se puede combinar de diversas maneras. Para un look casual y relajado, puedes llevarla con unos jeans azules y zapatillas blancas. Si buscas un estilo más sofisticado, puedes combinarla con una falda midi de tonos neutros y unos tacones. En los días más frescos, puedes agregar una chaqueta de cuero negra para darle un toque de estilo. Sin importar cómo decidas combinarla, la remera de punto marrón siempre será un acierto.',
    cleanBody:
      'Para mantener tu remera de punto marrón en buen estado, es recomendable lavarla a mano o en ciclo delicado en la lavadora, utilizando agua fría o tibia. Evita el uso de suavizantes o detergentes agresivos, ya que pueden dañar las fibras del tejido. Para secarla, colócala en una superficie plana y déjala secar al aire. Evita el uso de la secadora, ya que puede encoger la prenda. Si es necesario, puedes plancharla a baja temperatura. Siguiendo estos sencillos consejos, podrás disfrutar por mucho tiempo de tu remera de punto marrón en su estado óptimo.',
  },
  'remera-punto-plata': {
    prompt: 'remera punto plata',
    opinionBody:
      'La remera punto plata es altamente valorada por su estilo moderno y elegante. Los usuarios destacan su versatilidad para combinar con diferentes prendas y su suavidad gracias al material de punto utilizado. Además, el color plata le da un toque de sofisticación que la hace perfecta para ocasiones especiales.',
    combineBody:
      'La remera punto plata es muy versátil y se puede combinar de diferentes formas. Para un look casual, puedes combinarla con unos jeans y zapatillas blancas para un estilo relajado pero chic. Si buscas algo más sofisticado, puedes combinarla con una falda plisada y tacones altos. También puedes agregar un blazer negro para darle un toque más formal. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu remera punto plata en buen estado, es importante seguir las instrucciones de cuidado de la etiqueta. Por lo general, se recomienda lavarla a mano con agua fría y detergente suave para evitar que pierda su forma y color. Evita retorcerla al secarla, en su lugar, déjala secar al aire libre sobre una superficie plana. Si es necesario, puedes plancharla a baja temperatura con vapor. Siguiendo estos consejos, tu remera punto plata se mantendrá como nueva por mucho más tiempo.',
  },
  'remera-sintetica-amarillo': {
    prompt: 'remera sintetica amarillo',
    opinionBody:
      'Los usuarios que han adquirido esta remera sintética amarilla han expresado su satisfacción con su compra. Destacan la comodidad que ofrece el material sintético, que permite una gran movilidad y transpirabilidad. Además, el color amarillo brinda un toque de energía y alegría a cualquier conjunto. Los clientes también han elogiado la durabilidad de la prenda, ya que conserva su forma y color incluso después de múltiples lavados. En resumen, la remera sintética amarilla ha recibido excelentes opiniones por su confort, estilo y durabilidad.',
    combineBody:
      'La remera sintética amarilla es una prenda versátil que puede combinarse de diversas formas. Para un look casual y relajado, puedes usarla con unos jeans ajustados y zapatillas blancas. Si prefieres un estilo más elegante, puedes combinarla con una falda midi a rayas en tonos neutros y unos tacones negros. Para darle un toque desenfadado, puedes llevarla con unos shorts de denim y unas sandalias de colores. En definitiva, la remera sintética amarilla se adapta a diferentes estilos y ocasiones, permitiéndote crear conjuntos únicos y llenos de personalidad.',
    cleanBody:
      'La remera sintética amarilla requiere cuidados especiales para mantenerla en buen estado. Se recomienda lavarla a mano o en ciclo delicado en la lavadora, utilizando agua fría y detergente suave. Evita el uso de suavizantes, ya que pueden dañar las fibras sintéticas. Si es necesario, puedes plancharla a baja temperatura, siempre y cuando le des la vuelta y uses un paño para proteger el tejido. Para el almacenamiento, es preferible colgarla en un lugar fresco y seco, evitando la exposición directa a la luz solar. Siguiendo estos consejos, podrás disfrutar de tu remera sintética amarilla durante mucho tiempo, manteniendo su color y forma intactos.',
  },
  'remera-tafetan-aguamarina': {
    prompt: 'remera tafetan aguamarina',
    opinionBody:
      'Esta remera de tafetán aguamarina ha recibido excelentes opiniones por parte de los usuarios. Su material de alta calidad brinda una sensación suave y sedosa al tacto, a la vez que proporciona una apariencia elegante y refinada. El color aguamarina es muy favorecedor y añade un toque de frescura a cualquier outfit. Los usuarios destacan la comodidad y versatilidad de esta remera, ya que es perfecta para lucir tanto en ocasiones formales como informales. Además, su ajuste perfecto realza la figura y resalta las curvas de manera favorecedora. La remera de tafetán aguamarina ha conquistado a las personas que buscan estilo, confort y originalidad en una prenda.',
    combineBody:
      'La remera de tafetán aguamarina es una prenda versátil que se puede combinar de diversas formas para crear diferentes looks. Para un look casual y fresco, puedes combinarla con unos jeans ajustados y unas zapatillas blancas. Si prefieres un estilo más elegante, puedes llevarla con una falda midi de color neutro y unos tacones altos. Para un toque más sofisticado, puedes añadir accesorios en tonos dorados o plata. Si quieres un look más llamativo, puedes combinarla con una falda estampada y accesorios de colores contrastantes. La remera de tafetán aguamarina es una prenda versátil que te permitirá crear outfits únicos y llenos de estilo.',
    cleanBody:
      'Para mantener la remera de tafetán aguamarina en perfectas condiciones, es necesario seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a mano con agua fría y detergente suave. Evita utilizar productos blanqueadores o frotarla enérgicamente para no dañar el material. Después de lavarla, es importante secarla a la sombra y evitar la exposición directa al sol',
  },
  'remera-vaquera-negro': {
    prompt: 'remera vaquera negro',
    opinionBody:
      'La remera vaquera negro se ha convertido en un clásico imprescindible en el armario de cualquier persona. Su diseño versátil y atemporal la hace perfecta para combinar con diferentes estilos y ocasiones. Además, el material vaquero le otorga durabilidad y resistencia, lo que la convierte en una prenda de alta calidad. Los usuarios destacan su comodidad y lo favorecedor que resulta el color negro, que ayuda a estilizar la figura. Sin duda, una opción muy recomendada.',
    combineBody:
      'La remera vaquera negro es extremadamente versátil y puede combinarse de diversas formas para crear looks únicos. Para un estilo casual y relajado, puedes combinarla con unos pantalones de mezclilla en tonos claros y unas zapatillas blancas. Si buscas un look más sofisticado, puedes combinarla con una falda de cuero y unos tacones altos, logrando un contraste interesante entre lo casual y lo elegante. Además, puedes agregar accesorios como un collar statement o una chaqueta de cuero para darle un toque aún más especial.',
    cleanBody:
      'La remera vaquera negro requiere de ciertos cuidados para mantener su aspecto impecable. Para lavarla, lo ideal es darle la vuelta y utilizar un detergente suave en agua fría para evitar que se destiña o pierda intensidad su color. Se recomienda no utilizar secadora, sino colgarla en una percha y dejarla secar al aire libre. Es importante evitar el uso de blanqueadores o productos químicos agresivos. Para mantenerla en buen estado, se aconseja guardarla en un lugar fresco y seco, lejos de la luz directa del sol. Si sigues estos simples consejos, tu remera vaquera negro se mantendrá como nueva durante mucho tiempo.',
  },
  'remera-vaquera-oro': {
    prompt: 'remera vaquera oro',
    opinionBody:
      'La remera vaquera oro se ha convertido en una tendencia muy popular entre los amantes de la moda. Su combinación de tela vaquera y color oro la hacen única y llamativa. Muchos usuarios han expresado su satisfacción con este producto, destacando su estilo moderno y versatilidad para combinar con diferentes prendas. Además, resaltan la calidad del material y la comodidad que brinda al usarla. Definitivamente, la remera vaquera oro es una opción ganadora para lucir a la moda.',
    combineBody:
      'La remera vaquera oro es una prenda muy versátil que puede combinarse de diversas formas para lograr diferentes estilos. Para un look casual y relajado, se puede combinar con unos jeans azules y zapatillas blancas. Si buscas un estilo más sofisticado, puedes optar por una falda midi negra y zapatos de tacón alto. Para una ocasión más formal, puedes combinarla con un blazer negro y pantalones de vestir. Sea cual sea tu estilo, la remera vaquera oro agrega un toque de brillo y estilo a tus outfits.',
    cleanBody:
      'Para mantener la remera vaquera oro en buen estado, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a mano o en ciclo suave en la lavadora con agua fría para preservar el color y evitar que se decolore. Evita el uso de lejía o detergentes agresivos que puedan dañar el tejido. Para secarla, es mejor hacerlo al aire libre o en secadora a baja temperatura. Si es necesario plancharla, utiliza una temperatura baja y coloca un paño entre la plancha y la prenda para evitar dañarla. Siguiendo estas simples instrucciones, podrás disfrutar de tu remera vaquera oro por mucho tiempo.',
  },
  'sandalias-denim-borgona': {
    prompt: 'sandalias denim borgona',
    opinionBody:
      'Las sandalias denim borgoña son extremadamente populares entre los amantes de la moda. Muchas personas elogian su estilo moderno y versátil, ya que combinan a la perfección con una variedad de outfits. Los usuarios también destacan la comodidad y durabilidad del material denim, que proporciona un ajuste cómodo y resistente al desgaste. Además, el color borgoña agrega un toque de elegancia a cualquier look, convirtiendo estas sandalias en una elección atemporal.',
    combineBody:
      'Las sandalias denim borgoña ofrecen innumerables posibilidades de combinación. Puedes crear un look casual y relajado combinándolas con unos jeans ajustados y una camiseta blanca básica. Para un estilo más sofisticado, puedes optar por un vestido negro o una falda midi en tonos neutros. También puedes agregar un toque de color coordinando estas sandalias con una falda estampada o unos pantalones cortos en tonos complementarios. No importa el estilo que elijas, las sandalias denim borgoña añadirán un toque de estilo único a tu outfit.',
    cleanBody:
      'Para mantener las sandalias denim borgoña en óptimas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, asegúrate de leer las instrucciones del fabricante antes de realizar cualquier limpieza. En general, puedes limpiar suavemente la superficie con un cepillo suave o un paño húmedo. Si hay manchas difíciles de eliminar, puedes usar un limpiador específico para tejidos denim. Evita sumergir las sandalias en agua, ya que podría dañar el material. Para mantener el color borgoña vibrante, guárdalas en un lugar fresco y seco cuando no las estés usando. Recuerda también evitar la exposición prolongada al sol, ya que esto puede desvanecer el color. Siguiendo estas sencillas medidas, podrás disfrutar de tus sandalias denim borgoña durante mucho tiempo.',
  },
  'sandalias-lino-plata': {
    prompt: 'sandalias lino plata',
    opinionBody:
      'Las sandalias de lino en color plata son una opción elegante y versátil para lucir en diferentes ocasiones. Los usuarios han destacado la comodidad que brindan gracias al material suave y transpirable del lino. Además, el color plata añade un toque de brillo y sofisticación a cualquier look. Las opiniones sobre estas sandalias coinciden en resaltar su estilo moderno y su capacidad para combinar con diferentes prendas.',
    combineBody:
      'Las sandalias de lino en color plata pueden combinarse de diversas formas para lograr looks tanto casuales como más formales. Para un outfit casual de verano, son ideales con un vestido corto de estampado floral o una falda midi de colores vibrantes. Si buscas un look más elegante, puedes combinarlas con pantalones de tela en tonos neutros y una blusa de seda en color blanco o negro. Además, este tipo de sandalias están muy de moda para eventos nocturnos, y pueden lucir espectaculares con un vestido de noche plateado o negro.',
    cleanBody:
      'Para mantener tus sandalias de lino en color plata en buen estado, es importante seguir algunos consejos de limpieza y cuidado. Para quitar el polvo o la suciedad superficial, puedes utilizar un cepillo suave o un paño ligeramente húmedo. Evita sumergir las sandalias por completo en agua, ya que el lino puede deformarse o perder su color. En caso de manchas difíciles, puedes usar un detergente suave diluido en agua y frotar suavemente con un cepillo de cerdas suaves. Para mantener la forma de las sandalias, es recomendable guardarlas en un lugar seco y protegidas con papel o una bolsa para evitar que se deformen.',
  },
  'sandalias-punto-azul': {
    prompt: 'sandalias punto azul',
    opinionBody:
      'Las sandalias de punto azul son altamente valoradas por su comodidad y estilo. Los usuarios destacan la suavidad del material y la capacidad de adaptarse a la forma del pie, brindando una sensación de frescura en los días calurosos. Además, mencionan que el color azul le da un toque moderno y versátil que combina fácilmente con diferentes outfits. En general, quienes las han probado se muestran satisfechos y las recomiendan como una excelente opción para el verano.',
    combineBody:
      'Las sandalias de punto azul son una elección perfecta para completar cualquier tipo de look. Puedes combinarlas con unos jeans ajustados y una camiseta blanca para un estilo casual y relajado. Si buscas algo más elegante, puedes optar por un vestido estampado en tonos neutros y accesorios dorados. Para un look más femenino, puedes utilizar una falda midi y una blusa de color pastel. No importa si prefieres un estilo informal o más sofisticado, las sandalias de punto azul se adaptarán a cualquier conjunto.',
    cleanBody:
      'Para mantener las sandalias de punto azul en óptimas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, es recomendable limpiarlas regularmente con un paño húmedo para eliminar la suciedad acumulada. Si es necesario, puedes utilizar un detergente suave diluido en agua. Evita sumergirlas completamente en agua y no utilices productos químicos agresivos que puedan dañar el tejido de punto. Para mantener su forma y evitar deformaciones, es aconsejable guardarlas en un lugar fresco y seco cuando no las estés utilizando. Siguiendo estos simples consejos, podrás disfrutar de tus sandalias de punto azul durante mucho tiempo.',
  },
  'sandalias-punto-verde': {
    prompt: 'sandalias punto verde',
    opinionBody:
      'Las sandalias punto verde son una opción versátil y moderna para cualquier ocasión. Los usuarios destacan la comodidad que brindan gracias a su material de punto, que se adapta perfectamente al pie. Además, su color verde vibrante agrega un toque de frescura y alegría a cualquier conjunto. Sin duda, las opiniones sobre estas sandalias son muy positivas, ya que combinan estilo y confort a la perfección.',
    combineBody:
      'Las sandalias punto verde son ideales para crear looks frescos y llamativos. Puedes combinarlas con una falda midi estampada y una blusa blanca para un estilo boho-chic. Si prefieres un look más casual, prueba con unos jeans ajustados y una camiseta a rayas. También puedes optar por un vestido negro elegante para una ocasión más formal. En definitiva, las posibilidades de combinación son infinitas, ya que el color verde es muy versátil y se adapta a diferentes estilos y colores.',
    cleanBody:
      'Para mantener las sandalias punto verde en excelente estado, es recomendable seguir algunos consejos de limpieza y cuidado. Si están hechas de material de punto, lo ideal es lavarlas a mano con agua fría y un detergente suave. Evita retorcerlas para escurrirlas y en su lugar, déjalas secar al aire libre. Para mantener el color verde brillante, evita exponerlas al sol directo por largos períodos de tiempo. Además, verifica que el material no se haya desgastado y recuerda guardarlas en un lugar fresco y seco cuando no las uses. Siguiendo estos consejos, tus sandalias punto verde se mantendrán impecables y listas para lucir en cualquier ocasión.',
  },
  'sombrero-algodon-marino': {
    prompt: 'sombrero algodon marino',
    opinionBody:
      'Este sombrero de algodón marino ha recibido excelentes críticas por su comodidad y estilo. Los usuarios destacan la calidad del material, que es suave y transpirable, perfecto para los días calurosos. Además, el color marino le da un toque elegante y versátil, ideal para combinar con diferentes outfits.',
    combineBody:
      'Este sombrero de algodón marino es muy versátil y puede combinarse con diferentes prendas y estilos. Puedes usarlo con un vestido veraniego de flores para un look boho chic, o con unos jeans y una camiseta blanca para un estilo más casual. También queda genial con un traje de baño y un caftán en la playa. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener tu sombrero de algodón marino en buen estado, te recomendamos seguir algunos cuidados básicos. Si se ensucia, puedes limpiarlo suavemente con agua tibia y jabón neutro. Evita frotar con fuerza para no dañar el tejido. Déjalo secar al aire libre, pero evita la luz directa del sol. Siempre guárdalo en un lugar seco y protegido, preferiblemente en una caja o bolsa para evitar que se deforme o se ensucie.',
  },
  'sombrero-artificial-plata': {
    prompt: 'sombrero artificial plata',
    opinionBody:
      'El sombrero artificial plata ha recibido excelentes valoraciones por parte de los usuarios. Muchos destacan su brillo y elegancia, que lo convierten en el accesorio perfecto para ocasiones especiales. Además, su material sintético de alta calidad garantiza durabilidad y resistencia. Sin duda, este sombrero es una opción popular para quienes buscan destacar con estilo y sofisticación.',
    combineBody:
      'El sombrero artificial plata es extremadamente versátil y puede combinarse con diversos estilos y prendas de vestir. Para un look elegante y sofisticado, puedes combinarlo con un vestido negro ajustado y unos zapatos de tacón alto. Si prefieres un estilo más casual, puedes llevarlo con unos jeans, una blusa blanca y unas zapatillas blancas. Además, no olvides añadir algunos accesorios plateados a tu conjunto para completar el look con armonía.',
    cleanBody:
      'Para mantener tu sombrero artificial plata en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. Para eliminar el polvo y la suciedad, puedes usar un cepillo suave o un paño húmedo. Evita mojar completamente el sombrero, ya que podría dañar el material. Si notas alguna mancha persistente, puedes probar a frotarla suavemente con un detergente suave y un paño húmedo. Después de limpiarlo, déjalo secar al aire libre en una superficie plana. Recuerda almacenarlo en un lugar fresco y seco, lejos de la luz directa del sol, para evitar que se decolore o se dañe. Siguiendo estos sencillos consejos, podrás disfrutar de tu sombrero artificial plata durante mucho tiempo.',
  },
  'sombrero-brocado-marron': {
    prompt: 'sombrero brocado marron',
    opinionBody:
      'Los usuarios que han adquirido este sombrero brocado marrón destacan su elegancia y estilo único. El material brocado le da un toque sofisticado, mientras que el color marrón resalta cualquier look. Además, su forma y tamaño se adaptan perfectamente a diferentes tipos de rostros, brindando comodidad a quienes lo usan.',
    combineBody:
      'El sombrero brocado marrón es extremadamente versátil y puede combinarse de diversas formas. Para un look casual, puedes usarlo con unos jeans ajustados, una camisa blanca y una chaqueta de cuero. Si buscas algo más elegante, combínalo con un vestido midi estampado y unos tacones altos. También puedes agregar accesorios dorados para resaltar aún más su estilo.',
    cleanBody:
      'Para mantener tu sombrero brocado marrón en óptimas condiciones, es importante seguir algunos cuidados. Para eliminar el polvo y la suciedad, utiliza un cepillo suave o un paño húmedo y pásalo suavemente sobre la superficie. Evita mojarlo en exceso y nunca lo metas en la lavadora. Además, guárdalo en un lugar fresco y seco para evitar deformaciones.',
  },
  'sombrero-denim-oro': {
    prompt: 'sombrero denim oro',
    opinionBody:
      'El sombrero denim oro es una elección perfecta para aquellos que deseen agregar un toque de estilo único a su outfit. Los usuarios que lo han probado coinciden en que su diseño moderno y sofisticado es ideal para completar cualquier look. Además, destacan su material de alta calidad, que proporciona durabilidad y confort durante su uso. Sin duda, el sombrero denim oro es una opción versátil y elegante que no puede faltar en tu colección de accesorios.',
    combineBody:
      'A la hora de combinar el sombrero denim oro, las posibilidades son infinitas. Su color dorado le da un toque de glamour que puede complementar tanto un look casual como uno más formal. Para un estilo urbano y moderno, puedes combinarlo con jeans ajustados, una camiseta blanca y una chaqueta de cuero. Si buscas un look más sofisticado, úsalo con un vestido negro y unos tacones altos. No importa la ocasión, el sombrero denim oro será el complemento perfecto para realzar tu estilo personal.',
    cleanBody:
      'Para mantener tu sombrero denim oro en perfectas condiciones, es importante seguir algunos cuidados básicos. Siempre que sea posible, utiliza un cepillo suave para eliminar el polvo y mantenerlo limpio. En caso de manchas, puedes utilizar un paño húmedo con agua tibia y jabón suave para limpiar la superficie cuidadosamente. Evita sumergirlo completamente en agua y nunca utilices productos químicos agresivos. Almacenarlo en un lugar fresco y seco, y evitar exponerlo prolongadamente a la luz directa del sol, ayudará a preservar la calidad y el color del sombrero denim oro a largo plazo. Con un poco de atención y cuidado, seguirás luciendo este elegante accesorio durante mucho tiempo.',
  },
  'sombrero-encaje-beige': {
    prompt: 'sombrero encaje beige',
    opinionBody:
      'El sombrero de encaje beige es una elección elegante y sofisticada para completar cualquier conjunto. Los usuarios han elogiado su diseño delicado y femenino, destacando la belleza del encaje y el color beige. Además, la calidad del material ha recibido comentarios positivos, ya que el encaje es suave al tacto y resistente. Muchos han afirmado que este sombrero es perfecto para ocasiones especiales o para darle un toque de estilo a un look casual. En resumen, las opiniones sobre el sombrero encaje beige son muy favorables, destacando su diseño, calidad y versatilidad.',
    combineBody:
      'El sombrero de encaje beige es muy versátil y se puede combinar de diferentes maneras para crear looks únicos. Para un estilo romántico y femenino, puedes combinarlo con un vestido blanco o pastel y sandalias de tacón. Si buscas un look más boho o casual, puedes combinarlo con unos jeans desgastados, una camiseta básica y unas botas o zapatillas blancas. También puedes usarlo para darle un toque de elegancia a un conjunto de pantalones y blusa. En resumen, el sombrero encaje beige combina bien con una amplia gama de prendas y estilos, permitiéndote crear looks tanto formales como informales.',
    cleanBody:
      'Para mantener tu sombrero de encaje beige en buen estado, es importante seguir algunas pautas de limpieza y cuidado. En primer lugar, evita mojarlo para evitar dañar el encaje y deformar la forma del sombrero. En caso de manchas superficiales, puedes intentar retirarlas suavemente con un paño húmedo y un detergente suave. Recuerda no frotar demasiado fuerte para no dañar el material. Si el sombrero es plegable, guárdalo enrollado en un lugar seco y ventilado para evitar la acumulación de humedad y malos olores. Por último, es recomendable protegerlo del sol directo para evitar la decoloración del color beige',
  },
  'sombrero-punto-amarillo': {
    prompt: 'sombrero punto amarillo',
    opinionBody:
      'Los usuarios que han adquirido este sombrero de punto amarillo destacan su gran calidad y confort. Comentan que es perfecto para protegerse del sol durante los días cálidos, gracias a su diseño ligero y transpirable. Además, el color amarillo vibrante agrega un toque de estilo y alegría a cualquier outfit. Sin duda, es un accesorio imprescindible para lucir a la moda y sentirse cómodo al mismo tiempo.',
    combineBody:
      'Este sombrero de punto amarillo es muy versátil y puede combinarse con diversas prendas y estilos. Para un look casual de verano, puedes combinarlo con un vestido o una falda corta en tonos neutros como blanco o beige. Si buscas un estilo más bohemio, puedes añadir accesorios como collares largos o pulseras étnicas. Además, puedes lucirlo con jeans y una camiseta básica para un outfit relajado pero con estilo.',
    cleanBody:
      'Para mantener tu sombrero de punto amarillo en buen estado, te recomendamos seguir algunas precauciones. Siempre es importante leer las instrucciones de cuidado del fabricante, pero generalmente puedes lavarlo a mano con agua tibia y detergente suave. Evita utilizar productos químicos agresivos y secarlo al sol directo, preferiblemente colócalo en una superficie plana para que recupere su forma original. Recuerda también evitar doblarlo demasiado para evitar deformaciones. Siguiendo estas sencillas recomendaciones, podrás disfrutar de tu sombrero de punto amarillo durante mucho tiempo.',
  },
  'sombrero-punto-lima': {
    prompt: 'sombrero punto lima',
    opinionBody:
      'El sombrero de punto en color Lima ha recibido excelentes reseñas por parte de los usuarios. Muchos destacan su comodidad y versatilidad, ya que se adapta perfectamente a diferentes estilos y ocasiones. Además, el material de punto brinda una sensación cálida y suave al tacto, lo que lo convierte en una opción ideal para protegerte del frío durante los días más frescos. Sin duda, este sombrero en color Lima se ha convertido en un accesorio imprescindible en el guardarropa de muchos fashionistas.',
    combineBody:
      'El sombrero de punto en color Lima es extremadamente versátil y puede ser combinado con una amplia variedad de prendas y estilos. Para un look casual y relajado, puedes combinarlo con unos jeans ajustados, una camiseta básica y unas zapatillas blancas. Si quieres lucir más elegante, puedes optar por llevarlo con un vestido midi de estampado floral y unas sandalias de tacón. Además, su color vibrante complementa a la perfección otros tonos neutros como el blanco, el gris y el negro. ¡Las opciones son infinitas!',
    cleanBody:
      'Para mantener tu sombrero de punto en color Lima en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Para comenzar, es aconsejable lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo o frotarlo en exceso para evitar que se deforme. Después de lavarlo, colócalo sobre una toalla en una superficie plana para que se seque al aire de forma natural. Por último, es recomendable guardar el sombrero en un lugar seguro y seco, donde no esté expuesto directamente a la luz solar o al calor excesivo. Si sigues estos sencillos pasos, podrás disfrutar de tu sombrero de punto en color Lima por mucho tiempo.',
  },
  'sombrero-seda-celeste': {
    prompt: 'sombrero seda celeste',
    opinionBody:
      'El sombrero de seda en color celeste es una elección muy elegante y sofisticada. Su material de seda le da un aspecto delicado y lujoso, y el color celeste añade un toque de frescura y sutileza. Las personas que han utilizado este sombrero destacan su comodidad y su capacidad para complementar cualquier outfit. Además, resaltan la calidad de la seda y su durabilidad, lo que lo convierte en una excelente inversión en moda. Sin duda, el sombrero de seda celeste es una elección elegante y versátil para cualquier ocasión.',
    combineBody:
      'El sombrero de seda en color celeste es muy versátil y se puede combinar de diversas formas para crear diferentes looks. Para un estilo elegante y formal, puedes combinarlo con un vestido o traje en tonos neutros, como el blanco o el negro. Si buscas un look más casual y divertido, puedes combinarlo con una blusa de rayas y unos jeans. También puedes añadir accesorios en tonos plateados o dorados para darle un toque de brillo. En definitiva, el sombrero de seda celeste puede adaptarse a cualquier estilo y ser un complemento perfecto para realzar tu outfit.',
    cleanBody:
      'El sombrero de seda en color celeste requiere de cuidados especiales para mantenerse en óptimas condiciones. Para limpiarlo, es recomendable utilizar un paño suave y agua tibia con un poco de detergente suave. Frota suavemente el material de seda y evita retorcerlo para evitar dañarlo. Después, déjalo secar al aire libre, evitando la luz directa del sol. Para mantenerlo en buen estado, guárdalo en un lugar fresco y seco, protegido de la humedad y de la luz solar directa. Evita doblarlo en exceso para evitar que se deformen las formas. Siguiendo estos sencillos consejos de limpieza y mantenimiento, podrás disfrutar de tu sombrero de seda celeste durante mucho tiempo.',
  },
  'sombrero-sintetica-marron': {
    prompt: 'sombrero sintetica marron',
    opinionBody:
      'Este sombrero sintético en color marrón ha recibido opiniones muy positivas de nuestros clientes. Destacan su durabilidad y resistencia, gracias al material de alta calidad utilizado en su fabricación. Además, su color marrón combina fácilmente con diferentes estilos y outfits, convirtiéndolo en una opción versátil para cualquier ocasión. Es un accesorio elegante y sofisticado que ha recibido elogios por su diseño y acabado impecable.',
    combineBody:
      'El sombrero sintético marrón es una pieza que puede complementar muchos looks. Combínalo con un abrigo largo en tonos tierra para crear un outfit otoñal y sofisticado. También puedes usarlo con una blusa blanca, unos jeans ajustados y unas botas altas para un estilo más casual pero elegante. Si buscas un look más boho, pruébalo con un vestido camisero y unas sandalias de plataforma. Las posibilidades son infinitas, ya que el color marrón es muy versátil y fácil de combinar.',
    cleanBody:
      'Para mantener tu sombrero sintético marrón en buen estado, te recomendamos seguir algunos consejos de limpieza y cuidado. Primero, asegúrate de utilizar un cepillo suave o un paño húmedo para eliminar el polvo y la suciedad. Evita usar productos químicos agresivos que puedan dañar el material sintético. Si el sombrero tiene manchas, puedes utilizar un detergente suave mezclado con agua tibia y frotar suavemente con un paño. Recuerda dejarlo secar al aire libre y evitar la exposición directa al sol. Guarda tu sombrero en un lugar fresco y seco para evitar que se deforme. Siguiendo estos simples pasos, podrás disfrutar de tu sombrero sintético marrón por mucho tiempo.',
  },
  'sombrero-tejido-amarillo': {
    prompt: 'sombrero tejido amarillo',
    opinionBody:
      'El sombrero tejido amarillo es una opción elegante y llamativa para complementar tus outfits de verano. Los usuarios destacan su estilo único y su capacidad para agregar un toque de color a cualquier look. Además, su material tejido brinda una sensación fresca y ligera, perfecta para los días soleados. Sin duda, este sombrero se ha convertido en un favorito entre aquellos que buscan destacar con estilo en cualquier ocasión.',
    combineBody:
      'El sombrero tejido amarillo se puede combinar de múltiples formas para crear looks versátiles y llenos de estilo. Si deseas un look casual para el día a día, puedes combinarlo con una camiseta blanca, unos jeans ajustados y unas zapatillas blancas. Para ocasiones más formales, puedes optar por combinarlo con un vestido midi floreado y unas sandalias de tacón. También se verá genial con un traje de baño y un pareo en tonos neutros para una tarde en la playa. Las posibilidades son infinitas con este sombrero amarillo tejido.',
    cleanBody:
      'Para mantener en buen estado tu sombrero tejido amarillo, es importante seguir algunas recomendaciones de limpieza y cuidado. Si el sombrero es de materiales naturales como la paja, puedes limpiarlo suavemente con un paño húmedo y dejar que se seque al aire libre. Evita sumergirlo completamente en agua, ya que esto podría deformarlo. Si el sombrero tiene manchas, utiliza un cepillo de cerdas suaves y jabón neutro para limpiarlas. Recuerda no retorcer el sombrero al secarlo para evitar dañar su estructura. Almacenarlo en un lugar fresco y seco cuando no lo uses ayudará a mantenerlo en excelente estado durante más tiempo. Asegúrate de cuidar bien de tu sombrero tejido amarillo para poder disfrutar de su estilo y belleza en todas tus aventuras.',
  },
  'sombrero-tela-azul': {
    prompt: 'sombrero tela azul',
    opinionBody:
      'Los usuarios que han adquirido este sombrero de tela azul han destacado su comodidad y estilo. Además, el material de alta calidad proporciona durabilidad y resistencia. También han elogiado su versatilidad, ya que puede adaptarse a diferentes ocasiones y estilos de vestuario.',
    combineBody:
      'Este sombrero de tela azul puede ser el complemento perfecto para numerosos conjuntos. Para un look casual y veraniego, puedes combinarlo con una camiseta blanca, unos jeans y unas sandalias. Si buscas un estilo más elegante, puedes lucirlo con un vestido floral y unos tacones. Además, puedes añadir un toque boho combinándolo con una blusa suelta, unos shorts y unas botas.',
    cleanBody:
      'Para mantener tu sombrero de tela azul en óptimas condiciones, es importante seguir unos sencillos pasos. Si el sombrero tiene manchas, puedes limpiarlo frotándolas suavemente con agua y jabón neutro. Luego, déjalo secar al aire libre, evitando la exposición directa al sol. Si es necesario, puedes utilizar un cepillo de cerdas suaves para eliminar el polvo y la suciedad. Recuerda guardar el sombrero en un lugar fresco y seco, protegido de la humedad. De esta manera, podrás disfrutar de tu sombrero de tela azul durante mucho tiempo.',
  },
  'sudadera-encaje-aguamarina': {
    prompt: 'sudadera encaje aguamarina',
    opinionBody:
      'La sudadera encaje aguamarina ha recibido excelentes críticas por su estilo único y femenino. El encaje le agrega un toque elegante y sofisticado, mientras que el color aguamarina brinda frescura y alegría. Los usuarios elogian la comodidad y suavidad del material, perfecto para lucir a la moda sin renunciar al confort. Sin duda, esta sudadera es una elección acertada para aquellas mujeres que buscan destacar con un look original y trendy.',
    combineBody:
      'La versatilidad de la sudadera encaje aguamarina permite combinarla de diversas formas para lograr diferentes estilos. Para un look casual y relajado, puedes combinarla con unos jeans ajustados y zapatillas blancas. Si quieres un look más elegante, puedes llevarla con una falda midi en tonos neutros y unos tacones altos. Además, puedes complementarla con accesorios en tonos dorados para darle un toque sofisticado. Sea cual sea tu elección, la sudadera encaje aguamarina es sin duda una prenda versátil que se adapta a diferentes ocasiones.',
    cleanBody:
      'Para mantener la sudadera encaje aguamarina en óptimas condiciones, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarla a mano con agua fría y utilizar detergentes suaves. Evita el uso de suavizantes o productos abrasivos que puedan dañar el encaje. Al secarla, es preferible dejarla al aire libre en una superficie plana para evitar deformaciones. Si es necesario plancharla, utiliza una temperatura baja y emplea un paño delgado o una toalla entre la plancha y la prenda para proteger el encaje. Siguiendo estos sencillos consejos de limpieza y mantenimiento, podrás disfrutar de tu sudadera encaje aguamarina por mucho tiempo y en perfectas condiciones.',
  },
  'sudadera-seda-azul': {
    prompt: 'sudadera seda azul',
    opinionBody:
      'La sudadera de seda azul es un producto muy bien valorado por los usuarios. Su material de alta calidad, la suavidad y el brillo que aporta la seda, hacen de esta prenda una opción elegante y cómoda. Además, el color azul añade un toque de frescura y estilo. Los usuarios destacan la combinación perfecta entre comodidad y sofisticación que ofrece esta sudadera.',
    combineBody:
      'Esta sudadera de seda azul es muy versátil y puede combinarse de diferentes maneras. Para un look casual, puedes combinarla con unos vaqueros y zapatillas blancas. Si buscas un estilo más formal, puedes combinarla con una falda midi y tacones. También puedes añadir complementos en tonos plateados o dorados para darle un toque elegante. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para limpiar y mantener tu sudadera de seda azul, es importante seguir algunas recomendaciones. Lo ideal es lavarla a mano con agua fría y un detergente suave. Evita retorcerla y sécala en una superficie plana, sin colgarla. Si necesitas plancharla, utiliza una plancha a baja temperatura y coloca un paño fino entre la plancha y la sudadera para evitar daños en el tejido. Con estos cuidados, tu sudadera de seda azul se mantendrá en perfectas condiciones durante mucho tiempo.',
  },
  'sudadera-seda-marron': {
    prompt: 'sudadera seda marron',
    opinionBody:
      'La sudadera de seda marrón ha recibido excelentes opiniones por parte de los usuarios. Su material suave y lujoso brinda una sensación de comodidad incomparable. El color marrón le da un toque sofisticado y versátil, permitiendo combinarla con una gran variedad de prendas. Además, su diseño moderno y elegante la convierte en una opción ideal para lucir a la moda en cualquier ocasión.',
    combineBody:
      'La sudadera de seda marrón se puede combinar de diversas maneras para crear estilos únicos. Para un look casual, puedes combinarla con unos jeans ajustados y zapatillas blancas. Si buscas un estilo más formal, puedes usarla con unos pantalones de vestir y unos zapatos de tacón. Además, puedes añadir accesorios como un bolso de mano o una bufanda estampada para darle un toque extra de estilo.',
    cleanBody:
      'Para asegurar una larga vida útil a tu sudadera de seda marrón, es importante seguir las instrucciones de cuidado adecuadas. Se recomienda lavarla a mano con agua fría y utilizar detergente suave para prendas delicadas. Evita retorcerla o frotarla en exceso para evitar daños en el material. Además, es importante secarla al aire libre, evitando la exposición directa al sol. Si es necesario plancharla, utiliza la opción de baja temperatura y coloca un paño por encima para proteger la seda. Siguiendo estas recomendaciones, podrás disfrutar de tu sudadera de seda marrón en perfectas condiciones por mucho tiempo.',
  },
  'sudadera-seda-morado': {
    prompt: 'sudadera seda morado',
    opinionBody:
      'Los usuarios que han adquirido esta sudadera de seda en color morado han destacado su suavidad y comodidad. Además, mencionan que su material de seda le otorga un aspecto elegante y sofisticado. También valoran la durabilidad de esta prenda y cómo el color morado resalta y agrega estilo a su look. En definitiva, las opiniones son positivas y recomiendan esta sudadera a aquellos que buscan un toque de lujo en su vestimenta.',
    combineBody:
      'La versatilidad del color morado de esta sudadera de seda permite combinarla con una amplia gama de colores y estampados. Para un look elegante y formal, puedes combinarla con pantalones negros o grises y zapatos de tacón. Si buscas un look más casual, puedes optar por combinarla con jeans oscuros y zapatillas blancas. Además, puedes añadir accesorios como una bufanda o un bolso en tonos neutros para completar tu outfit.',
    cleanBody:
      'La seda es un material delicado, por lo que es importante tener cuidado al lavar y mantener esta sudadera. Se recomienda lavarla a mano con agua fría y utilizar un detergente suave para prendas delicadas. Evita retorcer la prenda y secarla al sol directo, ya que esto puede dañar las fibras de seda. Para mantener el color morado vibrante, es aconsejable evitar su exposición prolongada al sol. Asimismo, es recomendable guardarla en un lugar fresco y seco, lejos de la humedad y de posibles roces que puedan dañar su tejido. Siguiendo estos cuidados, podrás disfrutar de tu sudadera de seda morado por mucho tiempo.',
  },
  'sudadera-tela-verde': {
    prompt: 'sudadera tela verde',
    opinionBody:
      'La sudadera de tela verde se ha convertido en una de las favoritas entre aquellos que buscan comodidad y estilo. Los usuarios destacan su suavidad y lo cómoda que es al llevarla puesta. Además, el color verde es muy llamativo y le da un toque único a cualquier outfit. Sin duda, una prenda que no puede faltar en tu armario.',
    combineBody:
      'La versatilidad de la sudadera de tela verde permite combinarla de diversas formas. Puedes optar por crear un look casual y relajado al combinarla con unos jeans y zapatillas blancas. Si prefieres algo más sofisticado, puedes añadir unos pantalones negros y unas botas a juego. Incluso puedes agregar accesorios en tonos neutros para destacar aún más el color verde de la sudadera. Las posibilidades son infinitas.',
    cleanBody:
      'Para mantener tu sudadera de tela verde en buen estado, es importante seguir algunas recomendaciones de cuidado. Se recomienda lavarla a mano o en ciclo suave en la lavadora, utilizando agua fría. Evita el uso de cloro o productos blanqueadores que puedan dañar el color. Para secarla, lo mejor es dejarla al aire libre para evitar el encogimiento. Si necesitas plancharla, utiliza una temperatura baja y coloca un paño o tela delgada sobre la sudadera para protegerla. Sigue estos sencillos consejos y tu sudadera de tela verde se mantendrá como nueva por mucho tiempo.',
  },
  'sudadera-vaquera-azul': {
    prompt: 'sudadera vaquera azul',
    opinionBody:
      'La sudadera vaquera azul es una prenda versátil y de moda que ha ganado popularidad en los últimos años. Los usuarios destacan su comodidad y estilo casual. Además, el material vaquero le da un toque único y duradero. Su color azul es muy llamativo y se puede combinar fácilmente con otros colores.',
    combineBody:
      'La sudadera vaquera azul se puede combinar de muchas formas diferentes. Para un look casual, puedes combinarla con unos jeans negros y unas zapatillas blancas. Si prefieres un estilo más elegante, puedes llevarla con una falda de cuero y unos botines. Además, puedes complementarla con accesorios como una gorra o una mochila.',
    cleanBody:
      'Para mantener tu sudadera vaquera azul en buen estado, es recomendable seguir algunas pautas de cuidado. En primer lugar, es importante leer las instrucciones de lavado en la etiqueta del producto. En general, se recomienda lavarla del revés y con agua fría para evitar que se decolore. Además, es aconsejable evitar el uso de suavizantes y secarla al aire libre para evitar que se encoja. En cuanto al planchado, se recomienda hacerlo del revés y a baja temperatura para evitar dañar el material vaquero.',
  },
  'sueter-cachemira-azul': {
    prompt: 'sueter cachemira azul',
    opinionBody:
      'Los usuarios que han adquirido el suéter de cachemira azul han quedado fascinados con su suavidad y confort. La calidad del material de cachemira crea una sensación de lujo en cada uso. Además, el color azul añade un toque de elegancia y versatilidad al suéter, convirtiéndolo en una prenda perfecta para cualquier ocasión. Sin duda, este suéter de cachemira azul es una elección acertada para quienes buscan estilo y comodidad.',
    combineBody:
      'El suéter de cachemira azul puede combinarse de múltiples maneras para crear looks sofisticados y modernos. Para un look casual, puedes combinarlo con unos jeans ajustados y botas negras. Si buscas un estilo más elegante, opta por una falda plisada y zapatos de tacón en tonos neutros. También puedes combinarlo con pantalones de vestir y mocasines para un look de oficina. Sea cual sea tu estilo, el suéter de cachemira azul será el complemento perfecto para realzar tu outfit.',
    cleanBody:
      'La cachemira es un material delicado y requiere cuidados especiales para mantenerlo en óptimas condiciones. Para limpiar tu suéter de cachemira azul, es recomendable lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo y sécalo en posición horizontal sobre una toalla para que no pierda su forma. Para mantener la suavidad de la cachemira, puedes almacenarlo en una bolsa de tela transpirable y evitar exponerlo directamente a la luz solar. Siguiendo estos consejos, tu suéter de cachemira azul lucirá siempre impecable y te brindará años de uso.',
  },
  'sueter-encaje-turquesa': {
    prompt: 'sueter encaje turquesa',
    opinionBody:
      'El suéter encaje turquesa ha recibido excelentes críticas por parte de los usuarios. Su diseño elegante y femenino, combinado con el color turquesa vibrante, lo convierten en una opción perfecta para lucir a la moda. Además, el material de encaje añade un toque de sofisticación y transparencia sutil. Muchos usuarios destacan que es una prenda versátil que se puede combinar tanto con jeans como con faldas, y es ideal para ocasiones especiales o eventos formales.',
    combineBody:
      'El suéter encaje turquesa es un verdadero favorito para crear looks atrevidos y elegantes. Puedes combinarlo con unos jeans ajustados para un look casual chic, o con una falda plisada para un toque más femenino. Para resaltar aún más el color turquesa, puedes añadir accesorios dorados, como pulseras o aros. Además, podrías crear un contraste sorprendente combinándolo con un pantalón negro o blanco. Sin duda, este suéter encaje turquesa es una prenda versátil que te permitirá experimentar con diferentes estilos.',
    cleanBody:
      'Para mantener tu suéter encaje turquesa en perfecto estado, es importante seguir algunas pautas de cuidado. Primero, asegúrate de leer las instrucciones de lavado en la etiqueta del suéter. En general, se recomienda lavarlo a mano con agua fría y un detergente suave para prendas delicadas. Evita retorcer o frotar demasiado el suéter para evitar dañar el encaje. Después de lavarlo, déjalo secar en posición horizontal para evitar que se deforme. Recuerda también guardar el suéter en un lugar seco y ventilado, lejos de la luz directa del sol, para evitar que se decolore. Siguiendo estas simples recomendaciones, podrás disfrutar de tu suéter encaje turquesa por mucho tiempo.',
  },
  'sueter-franela-bronce': {
    prompt: 'sueter franela bronce',
    opinionBody:
      'Este suéter de franela bronce ha recibido excelentes críticas por parte de los usuarios. La combinación de la suavidad y calidez de la franela con el elegante color bronce hace que este suéter sea una elección perfecta para mantenerse abrigado y lucir a la moda durante los días fríos. Los usuarios elogian la calidad del material y su durabilidad, además de destacar lo versátil que es para combinar con diferentes estilos de vestimenta.',
    combineBody:
      'La versatilidad del suéter de franela bronce permite una amplia gama de opciones a la hora de combinarlo. Para un look casual, puedes usarlo con jeans y botas de cuero. Si deseas un look más elegante, puedes combinarlo con pantalones de vestir y zapatos de tacón. También puedes agregar algunos accesorios dorados para realzar el color bronce del suéter. En definitiva, este suéter es la prenda ideal para crear looks tanto informales como sofisticados.',
    cleanBody:
      'Para mantener este suéter en óptimas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Se recomienda lavarlo a mano con agua fría y un detergente suave. Evita el uso de blanqueadores y no lo frotes con fuerza para evitar dañar las fibras de la franela. Después de lavarlo, sécalo en posición horizontal para evitar que se deforme. Si es necesario plancharlo, hazlo a temperatura baja y preferentemente con un paño protector. Siguiendo estos cuidados, tu suéter de franela bronce se mantendrá en excelentes condiciones por mucho tiempo.',
  },
  'sueter-piel-azul': {
    prompt: 'sueter piel azul',
    opinionBody:
      'El suéter de piel azul ha recibido excelentes opiniones por parte de nuestros clientes. Su estilo moderno y sofisticado es perfecto para cualquier ocasión, ya sea para una salida casual o para una reunión de negocios. El material de piel le da un toque de elegancia y durabilidad, y el color azul añade un toque de frescura a tu atuendo. Además, su diseño ajustado y cálido te mantendrá abrigado durante los días más fríos. ¡Sin duda, es una opción que vale la pena invertir!',
    combineBody:
      'Para combinar el suéter de piel azul, te sugerimos optar por colores neutros como el blanco, el gris o el negro. Esto permitirá que el suéter sea el protagonista de tu atuendo y le dará un toque de elegancia y sofisticación. Puedes combinarlo con unos pantalones de mezclilla y unos zapatos de cuero en tonos tierra para un look casual, o con una falda lápiz y tacones altos para una ocasión más formal. ¡No olvides añadir algunos accesorios como un collar o una bufanda para completar tu conjunto!',
    cleanBody:
      'Para mantener tu suéter de piel azul en óptimas condiciones, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, es recomendable leer las instrucciones de cuidado del fabricante para asegurarte de seguir los pasos adecuados. Generalmente, los suéteres de piel requieren de cuidados especiales, como limpieza en seco o el uso de productos específicos para piel. Evita lavarlo en casa, ya que esto puede dañar el material. Asimismo, es importante almacenarlo en un lugar seco y fresco, lejos de la luz solar directa para evitar decoloración. Si notas alguna mancha, es mejor acudir a un especialista en limpieza de piel para evitar daños irreparables. Siguiendo estos consejos, podrás disfrutar de tu suéter de piel azul por mucho tiempo.',
  },
  'sueter-punto-marino': {
    prompt: 'sueter punto marino',
    opinionBody:
      'Este suéter de punto en color marino ha recibido excelentes críticas por parte de nuestros clientes. Su tejido de punto garantiza una gran comodidad y calidez, perfecto para los días fríos de invierno. Además, el color marino le da un toque elegante y versátil, ideal para combinar con diferentes estilos y ocasiones.',
    combineBody:
      'Este suéter de punto en color marino combina a la perfección con una amplia variedad de prendas y colores. Puedes usarlo con unos pantalones vaqueros oscuros para un look casual y urbano, o con una falda de tonos neutros para un estilo más formal. También puedes añadir accesorios en colores como blanco, gris o negro para resaltar aún más el color marino del suéter.',
    cleanBody:
      'Para limpiar y mantener este suéter de punto color marino, es recomendable lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo para eliminar el exceso de agua y déjalo secar en posición horizontal. Además, es importante guardar el suéter en un lugar seco y alejado de la luz solar directa para evitar que se decolore. Con los cuidados adecuados, podrás disfrutar de este suéter durante mucho tiempo.',
  },
  'sueter-saten-verde': {
    prompt: 'sueter saten verde',
    opinionBody:
      'Los usuarios han quedado encantados con este suéter satén verde por su hermoso color y suave textura. Han elogiado su calidad y la comodidad que brinda al usarlo. Además, destacan su versatilidad, ya que puede ser utilizado tanto para ocasiones más formales como informales. Sin duda, es una prenda que se destaca en cualquier guardarropa.',
    combineBody:
      'Este suéter satén verde es muy fácil de combinar debido a su color versátil. Puede ser utilizada tanto con jeans para un look casual y relajado, como con pantalones de vestir para un look más elegante. Además, se puede combinar con faldas o shorts, dependiendo de la ocasión. Para añadir un toque más sofisticado, se recomienda combinarlo con accesorios dorados o plateados.',
    cleanBody:
      'Para mantener este suéter satén verde en óptimas condiciones, es importante seguir las instrucciones de lavado del fabricante. En general, se recomienda lavarlo a mano con agua fría y detergente suave. Evitar el uso de blanqueadores o productos químicos fuertes. Al secarlo, se sugiere colgarlo en una percha para que recupere su forma original. Además, es importante evitar el contacto con objetos afilados para no dañar el tejido satén. Siguiendo estos consejos, podrás disfrutar de este suéter durante mucho tiempo.',
  },
  'sueter-tafetan-blanco': {
    prompt: 'sueter tafetan blanco',
    opinionBody:
      'Este suéter de tafetán blanco ha recibido excelentes opiniones por parte de nuestros clientes. La calidad del material es destacable, ya que el tafetán es conocido por ser duradero y resistente. Además, el color blanco brinda un aspecto elegante y sofisticado. Los usuarios han destacado lo versátil que resulta este suéter, ya que se puede combinar fácilmente con diferentes estilos y ocasiones. En general, las opiniones coinciden en que es una prenda muy favorecedora y de gran calidad.',
    combineBody:
      'Este suéter de tafetán blanco es una prenda muy versátil que puedes combinar de diferentes formas. Para un look casual, te recomendamos usarlo con unos jeans ajustados y unas zapatillas blancas. Si quieres un look más elegante, puedes combinarlo con una falda midi de color negro y unos tacones altos. Para los días más fríos, puedes añadir una chaqueta de cuero negra encima. También puedes jugar con los accesorios, como un collar llamativo o unos pendientes grandes, para darle un toque especial a tu outfit. Las posibilidades son infinitas, ¡solo depende de tu creatividad!',
    cleanBody:
      'Para mantener tu suéter de tafetán blanco en óptimas condiciones, es importante seguir algunas recomendaciones. Se recomienda lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo o frotarlo con fuerza, ya que el tafetán es un material delicado. Para secarlo, lo mejor es dejarlo extendido en una superficie plana, evitando la exposición directa al sol. En caso de que necesite ser planchado, utiliza una plancha a baja temperatura y coloca un paño fino encima del suéter para evitar dañarlo. Si sigues estas instrucciones, tu suéter de tafetán blanco se mantendrá como nuevo durante mucho tiempo.',
  },
  'sueter-tejido-claro': {
    prompt: 'sueter tejido claro',
    opinionBody:
      'El suéter tejido claro es una prenda que nunca pasa de moda. Su material suave y cómodo permite disfrutar de una sensación de calidez sin sacrificar el estilo. Muchos usuarios han expresado su satisfacción con este tipo de suéter, destacando su versatilidad para combinar con diferentes looks. Además, el color claro aporta luminosidad y frescura, perfecto para resaltar en cualquier ocasión.',
    combineBody:
      'El suéter tejido claro es una elección segura para combinar con diferentes prendas. Para un look casual, puedes llevarlo con unos jeans desgastados y unas zapatillas blancas. Si buscas un estilo más sofisticado, puedes optar por combinarlo con una falda midi y unos botines de tacón. También puedes añadirle accesorios como una bufanda de colores o un sombrero para darle un toque personal.',
    cleanBody:
      'Para mantener tu suéter tejido claro en buen estado, es importante seguir algunos cuidados básicos. En primer lugar, evita lavarlo en agua caliente, ya que puede dañar las fibras del tejido. En su lugar, opta por utilizar agua fría o a temperatura ambiente. Además, es recomendable lavarlo a mano o en ciclo delicado en la lavadora, utilizando un detergente suave. Después de lavarlo, colócalo en una superficie plana para que se seque naturalmente, evitando exponerlo directamente al sol. Si es necesario, plancha con vapor a baja temperatura. Siguiendo estas recomendaciones, tu suéter tejido claro lucirá impecable durante mucho tiempo.',
  },
  'sueter-tweed-verde': {
    prompt: 'sueter tweed verde',
    opinionBody:
      'El suéter tweed verde es una opción elegante y sofisticada para cualquier ocasión. Sus materiales de alta calidad y su color llamativo lo convierten en una prenda de vestir versátil y atractiva. Los usuarios destacan su comodidad y estilo, y mencionan que es perfecto para combinar con pantalones o faldas en tonos neutros para crear un look elegante y moderno.',
    combineBody:
      'El suéter tweed verde puede ser combinado de diversas formas para lograr looks increíbles. Puedes optar por llevarlo con unos pantalones negros ajustados y unos tacones para una ocasión formal, o usarlo con unos jeans y zapatillas blancas para un estilo más casual y relajado. Además, puedes complementarlo con accesorios dorados para resaltar su elegancia y darle un toque de glamour adicional.',
    cleanBody:
      'Para mantener tu suéter tweed verde en óptimas condiciones, es importante seguir algunas pautas de limpieza y cuidado. Recomendamos lavarlo a mano con agua fría y un detergente suave, evitando el uso de lejía. Es importante secarlo en posición horizontal, lejos de la luz directa del sol, para evitar que se deforme. Además, se recomienda plancharlo a baja temperatura y guardar en un lugar fresco y seco para mantener su forma y color por más tiempo.',
  },
  'sueter-vaquera-azul': {
    prompt: 'sueter vaquera azul',
    opinionBody:
      'Este suéter vaquera azul ha recibido excelentes críticas y comentarios positivos por parte de los usuarios. Su diseño clásico y versátil lo convierten en una pieza imprescindible en cualquier guardarropa. El material utilizado es de alta calidad, lo que garantiza su durabilidad y comodidad. Además, su color azul atractivo y llamativo añade un toque de estilo a cualquier conjunto. Sin duda, este suéter vaquera azul es una elección acertada para aquellos que buscan combinar moda y funcionalidad.',
    combineBody:
      'El suéter vaquera azul se puede combinar de muchas formas diferentes para crear diversos looks. Para un estilo casual y relajado, puedes combinarlo con unos jeans oscuros y unas zapatillas blancas. Si prefieres un look más sofisticado, puedes llevarlo con una falda de cuero negra y unos botines. También puedes optar por combinarlo con pantalones chinos y mocasines para un estilo más elegante y formal. Las posibilidades son infinitas, ya que el color azul del suéter vaquera permite jugar con una amplia gama de colores y estilos.',
    cleanBody:
      'Para mantener tu suéter vaquera azul en excelentes condiciones, es recomendable seguir algunas pautas de cuidado. En primer lugar, es importante lavarlo a mano o a máquina con agua fría y un detergente suave. Evita usar lejía o productos químicos agresivos que puedan dañar el tejido. Después del lavado, es aconsejable secarlo al aire libre, evitando la exposición directa al sol. Para eliminar cualquier arruga, puedes utilizar una plancha a baja temperatura y colocar un paño sobre el suéter para protegerlo. Siguiendo estos sencillos pasos, tu suéter vaquera azul se mantendrá en perfectas condiciones durante mucho tiempo.',
  },
  'tacones-algodon-oro': {
    prompt: 'tacones algodon oro',
    opinionBody:
      'Los tacones de algodón oro son una opción elegante y sofisticada para lucir en cualquier ocasión. Los usuarios destacan la comodidad que proporciona el material de algodón, permitiendo que los pies respiren durante largas jornadas. El color oro aporta un toque de glamour y estilo a cualquier outfit. Además, estos tacones son altamente versátiles, ya que se pueden combinar con una amplia variedad de prendas y colores.',
    combineBody:
      'Los tacones de algodón oro se pueden combinar de múltiples formas para crear looks impactantes. Para un estilo elegante y formal, se pueden combinar con un vestido negro o una falda lápiz en tonos neutros. Para un look más casual pero igualmente sofisticado, se pueden combinar con unos jeans oscuros y una blusa blanca. Si se busca un estilo más arriesgado, se pueden combinar con prendas en tonos vibrantes como el azul eléctrico o el rosa fucsia. Los accesorios dorados como pulseras o clutchs complementarán perfectamente este tipo de calzado.',
    cleanBody:
      'Para mantener los tacones de algodón oro en perfectas condiciones, es recomendable seguir algunas pautas de limpieza y cuidado. Primero, se debe retirar el polvo y la suciedad con un paño suave. En caso de manchas, se puede utilizar un detergente suave y agua tibia para frotar suavemente la zona afectada. Es importante no sumergir los tacones en agua ni utilizar productos abrasivos. Para mantener el brillo del material, se puede aplicar un poco de crema hidratante para cuero o un producto específico para calzado dorado. Finalmente, se recomienda guardar los tacones en una bolsa o caja individual para evitar que se rayen o dañen en el ropero.',
  },
  'tacones-artificial-electrico': {
    prompt: 'tacones artificial electrico',
    opinionBody:
      'Los tacones artificiales eléctricos son una opción innovadora y moderna para todas aquellas amantes de la moda y los zapatos de tacón alto. Su diseño único y novedoso ha generado opiniones positivas entre las usuarias que buscan destacar y lucir elegantes en cualquier ocasión. El material utilizado en su fabricación ofrece comodidad y durabilidad, brindando una experiencia única al caminar. Además, su color vibrante y llamativo agrega un toque de originalidad a cualquier outfit.',
    combineBody:
      'A la hora de combinar tus tacones artificiales eléctricos, las posibilidades son infinitas. Estos zapatos se adaptan tanto a outfits formales como informales, permitiéndote lucir espectacular en cualquier ocasión. Si buscas un look más sofisticado, puedes combinarlos con un vestido elegante o un traje de dos piezas. Para un estilo más casual, puedes optar por llevarlos con unos jeans ajustados y una blusa de colores llamativos. Sin duda, los tacones artificiales eléctricos serán el centro de atención de cualquier conjunto.',
    cleanBody:
      'Para asegurar la vida útil de tus tacones artificiales eléctricos y mantenerlos en óptimas condiciones, es importante realizar una correcta limpieza y mantenimiento. Para limpiar el material exterior, se recomienda utilizar un paño húmedo o una esponja suave con agua y jabón neutro. Evita sumergirlos completamente en agua y recuerda secarlos por completo antes de guardarlos. En cuanto al sistema eléctrico, es importante no mojarlo ni exponerlo a líquidos, ya que podría dañarse. Guarda tus tacones en un lugar seco y protegido de la luz solar directa. Siguiendo estos consejos, tus tacones artificiales eléctricos se mantendrán impecables y listos para lucir en cualquier momento.',
  },
  'tacones-artificial-lima': {
    prompt: 'tacones artificial lima',
    opinionBody:
      'Los tacones artificiales en color lima son una opción versátil y llamativa para completar tus looks. Muchas personas han quedado sorprendidas por la comodidad y la calidad de estos zapatos. Además, el material utilizado ofrece resistencia y durabilidad, lo que los convierte en una excelente inversión. Los usuarios también destacan el increíble color lima, que agrega un toque de frescura y originalidad a cualquier outfit. En resumen, los tacones artificiales en color lima son una opción elegante y moderna que no pasará desapercibida.',
    combineBody:
      'A la hora de combinar los tacones artificiales en color lima, las posibilidades son infinitas. Para un look audaz y atrevido, puedes combinarlos con un vestido negro ajustado y accesorios en tonos plata. Si buscas un estilo más casual pero igualmente llamativo, puedes optar por unos jeans de color oscuro, una camiseta blanca y una chaqueta de cuero en negro. También puedes jugar con colores complementarios como el fucsia o el morado para crear un contraste impactante. En definitiva, los tacones artificiales en color lima son el accesorio perfecto para añadir un toque vibrante y sofisticado a cualquier conjunto.',
    cleanBody:
      'Para mantener tus tacones artificiales en color lima en perfecto estado, es importante seguir algunos consejos de limpieza y mantenimiento. Primero, asegúrate de tener un cepillo de cerdas suaves para eliminar el polvo y la suciedad de la superficie. Si son impermeables, puedes utilizar un paño húmedo con agua y jabón neutro para limpiarlos. En caso de manchas difíciles, puedes hacer una pasta con bicarbonato de sodio y agua y aplicarla sobre la mancha, dejándola actuar durante unos minutos y luego limpiando con agua. No olvides secarlos al aire libre y guardarlos en un lugar fresco y seco, lejos de la luz directa del sol. Siguiendo estos sencillos consejos, podrás disfrutar de tus tacones',
  },
  'tacones-brocado-morado': {
    prompt: 'tacones brocado morado',
    opinionBody:
      'Los tacones de brocado morado son una elección perfecta para añadir un toque de elegancia y sofisticación a cualquier atuendo. Los usuarios han elogiado la calidad de los materiales utilizados en la fabricación de estos tacones, así como su comodidad y ajuste perfecto. También han destacado el hermoso color morado del brocado, que añade un toque de estilo único. En cuanto al diseño, muchos usuarios han mencionado que estos tacones son versátiles y se pueden combinar fácilmente tanto con vestidos formales como con conjuntos más informales. En general, los tacones de brocado morado han recibido excelentes críticas y son altamente recomendados por los usuarios.',
    combineBody:
      'Los tacones de brocado morado son una opción muy versátil y se pueden combinar de varias formas para crear diferentes estilos. Para un look elegante y sofisticado, puedes combinarlos con un vestido negro o con tonos neutros como el blanco o el gris. También lucen increíbles con pantalones de tela o faldas en tonos oscuros como el negro o el azul marino. Si deseas darle un toque más casual, puedes combinarlos con jeans ajustados y una blusa o una camiseta informal. Además, estos tacones quedan muy bien con accesorios plateados o dorados, que resaltarán aún más su hermoso color morado. Existe una amplia gama de opciones para combinar estos tacones de brocado morado, lo que los convierte en una pieza versátil y elegante para cualquier ocasión.',
    cleanBody:
      'Para garantizar la durabilidad y el buen aspecto de tus tacones de brocado morado, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, asegúrate de leer las instrucciones de cuidado que vienen con los zapatos, ya que cada fabricante puede tener recomendaciones específicas. En general, se recomienda evitar mojar demasiado los tacones y utilizar un paño suave y limpio para',
  },
  'tacones-brocado-verde': {
    prompt: 'tacones brocado verde',
    opinionBody:
      'Los tacones de brocado verde son una opción elegante y sofisticada para cualquier ocasión. Muchos usuarios han elogiado la calidad del material y lo bien que se ajustan al pie. Además, el color verde del brocado agrega un toque de originalidad y estilo a cualquier conjunto. Los usuarios también destacan la comodidad de estos tacones, incluso después de llevarlos durante largos periodos de tiempo. En general, las opiniones son muy positivas y se consideran una excelente elección para lucir elegante y a la moda.',
    combineBody:
      'Los tacones de brocado verde son muy versátiles y se pueden combinar de diferentes formas. Para un look elegante y formal, puedes combinarlos con un vestido negro o jugar con la paleta de colores y optar por un vestido en tonos dorados o beige. Si buscas un estilo más casual, puedes combinarlos con unos jeans ajustados y una blusa blanca para un look más sofisticado y casual a la vez. También puedes combinarlos con faldas o pantalones de cualquier color que complemente el verde del brocado. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener los tacones de brocado verde en buen estado, es importante seguir algunos consejos de limpieza y cuidado. Primero, asegúrate de cepillarlos suavemente con un cepillo de cerdas suaves para eliminar el polvo y la suciedad acumulados. Si hay alguna mancha, puedes utilizar un paño húmedo y un poco de jabón suave para limpiarla. Evita sumergir los tacones en agua ya que eso podría dañar el material. Además, es recomendable guardar los tacones en un lugar fresco y seco, lejos de la luz directa del sol, para evitar decoloración. Siguiendo estos consejos, tus tacones de brocado verde mantendrán su belleza y lucirán como nuevos por mucho tiempo.',
  },
  'tacones-encaje-azul': {
    prompt: 'tacones encaje azul',
    opinionBody:
      'Los tacones de encaje azul son una elección elegante y sofisticada para cualquier ocasión. Su diseño único y femenino atrae miradas y se convierte en el elemento estrella de cualquier outfit. Las opiniones sobre este tipo de calzado suelen ser muy positivas, ya que suelen ser cómodos, versátiles y resistentes. Además, su color azul vibrante resalta y añade un toque de originalidad a cualquier conjunto. Sin duda, los tacones de encaje azul son la elección perfecta para aquellas mujeres que desean lucir hermosas y a la moda.',
    combineBody:
      'Los tacones de encaje azul son extremadamente versátiles y ofrecen muchas opciones de combinación. Puedes lucirlos con un vestido blanco para un look elegante y romántico, o con unos jeans ajustados y una blusa de seda para un estilo más casual pero sofisticado. También puedes combinarlos con prendas en tonos neutros como el negro o el beige, para que el calzado se convierta en el punto focal del conjunto. No importa la ocasión, los tacones de encaje azul siempre aportarán un toque de estilo único y personalidad.',
    cleanBody:
      'Para mantener tus tacones de encaje azul en perfecto estado, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, asegúrate de leer las instrucciones del fabricante para conocer las recomendaciones específicas. Siempre es recomendable limpiarlos con un paño suave y húmedo para eliminar cualquier suciedad superficial. Si tienen manchas difíciles, utiliza un detergente suave y agua tibia para limpiarlas. Evita sumergirlos completamente en agua y nunca los metas en la lavadora. Para mantener el encaje en buen estado, guárdalos en un lugar fresco y seco, preferiblemente en una bolsa de tela. Recuerda también protegerlos con un spray impermeabilizante para evitar manchas y arrugas innecesarias. Sigui',
  },
  'tacones-gasa-beige': {
    prompt: 'tacones gasa beige',
    opinionBody:
      'Los tacones de gasa beige son una opción elegante y sofisticada para cualquier ocasión. Los usuarios han comentado que su estilo femenino y delicado los hace perfectos para eventos formales o fiestas. Además, destacan su comodidad gracias al material de gasa, que se adapta al pie y permite una mayor flexibilidad.',
    combineBody:
      'Estos tacones de gasa beige son extremadamente versátiles a la hora de combinar. Puedes llevarlos con un vestido de cóctel en tonos neutros para lucir un look clásico y refinado. También puedes optar por combinarlos con unos pantalones ajustados y una blusa elegante para un estilo más moderno y casualmente sofisticado. Incluso puedes utilizarlos con un par de jeans y una camiseta blanca para darle un toque chic a un outfit sencillo.',
    cleanBody:
      'Para mantener tus tacones de gasa beige en buen estado, es importante seguir algunos cuidados básicos. Si se ensucian, puedes limpiarlos con un paño húmedo y jabón suave. Evita sumergirlos en agua y asegúrate de secarlos completamente antes de guardarlos. También es recomendable utilizar un cepillo suave para eliminar el polvo y la suciedad de la superficie. Para proteger el material de gasa, es aconsejable aplicar un spray repelente al agua antes de usarlos por primera vez.',
  },
  'tacones-jacquard-plata': {
    prompt: 'tacones jacquard plata',
    opinionBody:
      'Los tacones jacquard plata han recibido excelentes críticas por parte de nuestros clientes. Su diseño elegante y sofisticado los convierte en el complemento perfecto para ocasiones especiales. El material jacquard le da un toque de exclusividad y su color plata es muy versátil, combina fácilmente con diferentes estilos y colores. Además, su comodidad es destacable, permitiendo que puedas lucir hermosa sin sacrificar tu bienestar. Sin duda, los tacones jacquard plata son una elección acertada para añadir glamour y estilo a tu look.',
    combineBody:
      'Los tacones jacquard plata pueden ser combinados de diversas formas para crear looks impactantes. Para una ocasión formal, puedes combinarlos con un vestido negro clásico, para resaltar el brillo de los tacones y añadir un toque de elegancia. Si buscas algo más casual, puedes combinarlos con unos jeans ajustados y una blusa blanca, creando un contraste llamativo entre lo formal y lo informal. Además, puedes experimentar con otros colores, como el rojo o el azul marino, para crear combinaciones audaces y modernas. Los tacones jacquard plata te permiten jugar con tu creatividad y destacar en cualquier evento.',
    cleanBody:
      'Para mantener los tacones jacquard plata en óptimas condiciones, es recomendable seguir algunos consejos de limpieza y cuidado. En primer lugar, es importante cepillar suavemente los tacones con un cepillo de cerdas suaves para eliminar el polvo y la suciedad acumulada. Si se presentan manchas, se puede utilizar un paño húmedo con agua tibia y un poco de detergente suave para frotar suavemente la zona afectada. Es recomendable evitar el uso de productos químicos fuertes, ya que pueden dañar el material jacquard y el color plata. Por último, se recomienda guardar los tacones en una bolsa especial para evitar arañazos y mantenerlos lejos de la luz solar directa. Siguiendo estos simples consejos,',
  },
  'tacones-jersey-gris': {
    prompt: 'tacones jersey gris',
    opinionBody:
      'Los tacones de jersey gris han recibido excelentes críticas por parte de los usuarios. Muchos han destacado la comodidad que brindan gracias a su material suave y flexible. Además, el color gris les da un toque elegante y versátil, lo que los convierte en la elección perfecta para diversas ocasiones. Su diseño moderno y sofisticado también ha sido muy valorado por quienes los han adquirido.',
    combineBody:
      'Los tacones de jersey gris son extremadamente versátiles a la hora de combinarlos. Puedes optar por lucirlos con un vestido negro para lograr un look elegante y clásico, o con unos jeans y una camiseta blanca para un estilo más casual pero sofisticado. También puedes experimentar con colores más llamativos como el rojo o el amarillo para crear combinaciones audaces y atrevidas. Sea cual sea tu elección, estos tacones seguramente serán el complemento perfecto para tus outfits.',
    cleanBody:
      'Para mantener tus tacones de jersey gris en excelente estado, es importante seguir algunos consejos de limpieza y cuidado. Primero, asegúrate de limpiar cualquier mancha inmediatamente con un paño húmedo y suave. Evita el uso de productos químicos agresivos y opta por limpiadores suaves y específicos para este tipo de material. Además, es recomendable guardar los tacones en una bolsa de tela o caja para protegerlos del polvo y evitar roces o rasguños. Recuerda también rotar su uso para evitar un desgaste excesivo en una sola zona. Siguiendo estos simples pasos, tus tacones de jersey gris se mantendrán impecables por mucho tiempo.',
  },
  'tacones-lana-blanco': {
    prompt: 'tacones lana blanco',
    opinionBody:
      'Los tacones de lana blanco son una elección elegante y sofisticada para cualquier ocasión. Su material de lana proporciona una sensación de calidez y comodidad, al tiempo que añade un toque de textura única. Los usuarios que han adquirido estos tacones han elogiado su estilo versátil que se adapta tanto a looks de día como de noche. Además, destacan la calidad de los materiales utilizados y la durabilidad de los tacones. Sin duda, los tacones de lana blanco son una elección acertada para aquellos que buscan un calzado llamativo y elegante.',
    combineBody:
      'Los tacones de lana blanco se pueden combinar de múltiples formas para crear looks llamativos y llenos de estilo. Para un look sofisticado de invierno, puedes combinarlos con una falda lápiz de cuero negro y un abrigo largo del mismo tono. Si buscas un look más casual pero igualmente elegante, puedes combinarlos con unos vaqueros negros ajustados y una blusa de seda beige. Además, estos tacones también se pueden combinar con vestidos o faldas de colores neutros como gris o nude, para crear un look más suave y femenino. En resumen, los tacones de lana blanco se adaptan fácilmente a diferentes estilos y colores, lo que los convierte en una opción versátil para cualquier guardarropa.',
    cleanBody:
      'Para mantener tus tacones de lana blanco en buen estado, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable cepillar suavemente los tacones con un cepillo de cerdas suaves para eliminar el polvo y la suciedad acumulada. Si hay manchas en el material de lana, se puede utilizar un paño humedecido con agua tibia y un poco de detergente suave para frotar suavemente la zona afectada. Sin embargo, es importante evitar sumergir los tacones en agua y no utilizar productos químicos fuertes que puedan dañar el material. Una vez',
  },
  'tacones-piel-lima': {
    prompt: 'tacones piel lima',
    opinionBody:
      'Los tacones de piel en color lima son una opción vibrante y llamativa para complementar cualquier conjunto. La opinión general sobre este tipo de calzado es positiva, ya que su material de piel garantiza durabilidad y comodidad. Además, el color lima brinda un toque de originalidad y estilo único. Los usuarios destacan que estos tacones son ideales para ocasiones especiales y eventos donde se busca destacar y lucir elegante. Sin embargo, es importante recordar que el gusto por este tono puede variar, por lo que es recomendable tener en cuenta el resto de la vestimenta al combinarlos.',
    combineBody:
      'Los tacones de piel en color lima ofrecen múltiples posibilidades de combinación. Para un look audaz y llamativo, puedes combinarlos con prendas en tonos neutros como negro, blanco o gris, permitiendo que los tacones sean los protagonistas del conjunto. También puedes optar por un atuendo en tonos tierra para lograr un contraste interesante. Si prefieres algo más arriesgado, pruébalos con estampados florales o tropicales para un look veraniego y fresco. No temas experimentar con diferentes combinaciones y descubrir tu propio estilo.',
    cleanBody:
      'Para mantener tus tacones de piel en color lima en buen estado, es importante tener en cuenta algunos cuidados básicos. En primer lugar, asegúrate de protegerlos adecuadamente antes de usarlos por primera vez, aplicando un protector de cuero para prevenir manchas y daños. Si se ensucian, limpia suavemente con un paño húmedo y jabón neutro, evitando mojar demasiado el cuero. Si es necesario, puedes utilizar un cepillo de cerdas suaves para retirar suciedad o polvo acumulado. Para conservar la forma de los tacones, guárdalos en su caja original o utiliza un separador de calzado. Recuerda hidratar periódicamente el cuero con un acondicionador específico. Siguiendo estos cuidados, podrás disfrutar de tus tacones de',
  },
  'tacones-punto-azul': {
    prompt: 'tacones punto azul',
    opinionBody:
      'Los tacones de punto azul son uno de los favoritos entre los amantes de la moda. Las opiniones de quienes los han usado suelen ser muy positivas. Muchos destacan su comodidad y su estilo elegante. El material de punto les da un toque único y los hace ideales tanto para ocasiones formales como informales. Además, el color azul es versátil y combina fácilmente con diferentes outfits.',
    combineBody:
      'Los tacones de punto azul son muy versátiles a la hora de combinarlos. Puedes llevarlos con jeans ajustados y una blusa de seda para un look casual pero sofisticado. También van perfectos con vestidos o faldas en tonos neutros como el blanco, gris o negro. Si quieres un toque más arriesgado, puedes combinarlos con un outfit total denim. En definitiva, los tacones de punto azul son una apuesta segura para añadir un toque de estilo a cualquier conjunto.',
    cleanBody:
      'Para mantener los tacones de punto azul en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, debes evitar exponerlos a la humedad y la lluvia. Si se ensucian, puedes limpiarlos con un paño húmedo y un poco de jabón suave. Además, es recomendable guardarlos en una bolsa de tela o caja para protegerlos de posibles golpes o rozaduras. Si se han deformado debido al uso, puedes rellenarlos con papel de seda o usar hormas para ayudar a mantener su forma. En resumen, cuidar adecuadamente los tacones de punto azul garantizará que luzcan impecables en cada ocasión.',
  },
  'tacones-sintetica-claro': {
    prompt: 'tacones sintetica claro',
    opinionBody:
      'Los tacones de material sintético y color claro son una opción cada vez más popular entre las amantes de la moda. Muchas usuarias destacan su versatilidad y comodidad, ya que se adaptan a diferentes ocasiones y outfits. Además, el material sintético ofrece durabilidad y resistencia, lo que garantiza que los tacones conserven su apariencia y forma por mucho tiempo. En cuanto al color claro, se destaca su elegancia y la capacidad de combinar con una amplia variedad de prendas. Si buscas una opción que combine estilo y comodidad, los tacones de material sintético color claro son sin duda una excelente elección.',
    combineBody:
      'Los tacones de material sintético y color claro son una pieza versátil que se puede combinar de múltiples formas. Si buscas un look elegante y sofisticado, puedes combinarlos con un vestido de cocktail en tonos pastel o neutros. Si prefieres un estilo más casual pero a la moda, puedes llevarlos con unos jeans de corte recto y una blusa estampada. También puedes darle un toque chic a tu outfit de oficina combinándolos con un traje sastre en tono claro. En resumen, los tacones de material sintético color claro son la elección perfecta para agregar estilo y sofisticación a cualquier look.',
    cleanBody:
      'Para mantener tus tacones de material sintético color claro en perfectas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, asegúrate de limpiar cualquier mancha o suciedad de la superficie de los tacones con un paño húmedo y detergente suave. Evita el uso de productos abrasivos o químicos que puedan dañar el material. Para mantener su forma, se recomienda guardar los tacones en una bolsa de tela o caja individual, evitando que se aplasten o se deformen. Además, es importante evitar exponer los tacones a la luz directa del sol o a altas temperaturas, ya que esto puede hacer que el color se desvanezca. Siguiendo estos',
  },
  'tacones-sintetica-lima': {
    prompt: 'tacones sintetica lima',
    opinionBody:
      'Con qué combinar tacones sintéticos color lima: Los tacones sintéticos en color lima son una opción atrevida y vibrante para incorporar en cualquier outfit. Recomendamos combinarlos con colores neutros como el blanco, el negro o el gris para lograr un contraste elegante y sofisticado. También se pueden combinar con tonos más llamativos como el fucsia, el amarillo o el naranja para crear un look divertido y lleno de energía. Además, los estampados florales o geométricos en tonos pastel son una excelente opción para añadir frescura y originalidad al conjunto. No olvides jugar con los accesorios para resaltar aún más tus tacones sintéticos en color lima, como un bolso en tono metálico o joyería en colores complementarios.',
    combineBody: null,
    cleanBody: null,
  },
  'tacones-tafetan-electrico': {
    prompt: 'tacones tafetan electrico',
    opinionBody:
      'Los tacones de tafetán eléctrico son una opción elegante y llamativa para cualquier ocasión. Los usuarios han elogiado su diseño único y sofisticado, así como la comodidad que brindan a pesar de la altura del tacón. Además, el material de tafetán agrega un toque de lujo a estos zapatos, haciendo que destaquen aún más. Sin duda, los tacones de tafetán eléctrico son una elección audaz y atractiva para las amantes de la moda.',
    combineBody:
      'Los tacones de tafetán eléctrico ofrecen una amplia variedad de opciones de styling. Su llamativo color eléctrico los convierte en el punto focal de cualquier atuendo. Para un look elegante y sofisticado, se pueden combinar con un vestido negro ajustado o un traje pantalón a juego. También se pueden mezclar con prendas más informales como jeans oscuros y una blusa de seda para lograr un estilo moderno y chic. En definitiva, los tacones de tafetán eléctrico se pueden combinar de múltiples maneras para crear outfits únicos y con personalidad.',
    cleanBody:
      'Para mantener tus tacones de tafetán eléctrico en buen estado, es importante seguir algunos cuidados básicos. Para eliminar el polvo y la suciedad, puedes utilizar un cepillo suave o un paño limpio y seco. En caso de manchas superficiales, puedes utilizar un producto de limpieza específico para el material de tafetán. Sin embargo, lo más recomendable es evitar el contacto con líquidos para prevenir daños en el tejido. Además, es recomendable guardar los tacones en una bolsa de tela o caja para protegerlos de la humedad y el polvo cuando no los estés utilizando. Con estos cuidados simples, podrás mantener tus tacones de tafetán eléctrico en perfectas condiciones durante más tiempo.',
  },
  'tacones-tela-marino': {
    prompt: 'tacones tela marino',
    opinionBody:
      'Los tacones de tela en color marino son una elección elegante y versátil para cualquier ocasión. Los materiales de tela proporcionan comodidad y flexibilidad al caminar, mientras que el color marino agrega un toque de sofisticación a cualquier conjunto. Los usuarios han elogiado la calidad de estos tacones, así como su durabilidad y diseño atemporal. Con su estilo clásico y acabados de alta calidad, estos tacones de tela marino seguramente serán una adición favorecedora a tu guardarropa.',
    combineBody:
      'La versatilidad de los tacones de tela en color marino hace que sean fáciles de combinar con diversos atuendos. Para un look casual pero elegante, puedes combinarlos con unos jeans ajustados y una blusa blanca. También puedes lucirlos con vestidos de colores pastel para eventos más formales o con trajes de oficina en tonos neutros. Los tacones de tela marino también se ven fantásticos con faldas o pantalones en tonos oscuros como negro o gris, añadiendo un toque de color y estilo a tu conjunto.',
    cleanBody:
      'Para mantener tus tacones de tela marino en buen estado, es importante seguir algunas pautas de cuidado. Primero, asegúrate de evitar el contacto con líquidos y sustancias que puedan manchar o dañar el material. Si tus tacones se ensucian, puedes usar un cepillo suave para eliminar la suciedad superficial o aplicar un limpiador específico para tela. Para un mantenimiento regular, puedes utilizar aerosoles protectores para evitar el desgaste y protegerlos de posibles manchas. Además, es recomendable guardar tus tacones en una bolsa de tela o caja protectora cuando no los estés usando, para evitar posibles roces o daños. Siguiendo estos consejos, podrás disfrutar de tus tacones de tela marino durante mucho tiempo.',
  },
  'tacones-tela-rosa': {
    prompt: 'tacones tela rosa',
    opinionBody:
      'Los tacones de tela rosa son una opción elegante y femenina para cualquier ocasión. Muchas personas han quedado encantadas con este tipo de calzado, destacando la suavidad y comodidad del material. Además, el color rosa agrega un toque delicado y romántico a cualquier outfit. Sin duda, los tacones de tela rosa son una elección acertada para lucir sofisticada y a la moda.',
    combineBody:
      'Los tacones de tela rosa son versátiles y se pueden combinar con una gran variedad de prendas. Para un look elegante y formal, puedes combinarlos con un vestido o traje en tonos neutros como el blanco, el negro o el gris. Si buscas un look más casual y divertido, puedes combinarlos con jeans ajustados y una blusa de colores claros. Además, puedes agregar accesorios en tonos plateados o dorados para darle un toque de glamour a tu outfit.',
    cleanBody:
      'Para mantener tus tacones de tela rosa en perfecto estado, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, asegúrate de cepillarlos suavemente después de cada uso para eliminar el polvo y la suciedad. En caso de manchas, utiliza un paño húmedo con jabón neutro para limpiar la zona afectada. Evita sumergir los tacones en agua y nunca los metas en la lavadora. Por último, guárdalos en un lugar fresco y seco para evitar la formación de moho. Siguiendo estos consejos, tus tacones de tela rosa se mantendrán impecables y listos para lucir en cualquier ocasión.',
  },
  'tacones-tweed-oro': {
    prompt: 'tacones tweed oro',
    opinionBody:
      'Los tacones de tweed oro son una elección sofisticada y elegante para cualquier ocasión. Su material de tweed agrega un toque de textura y estilo único. Los usuarios han expresado su satisfacción con la calidad de estos tacones, destacando su comodidad y durabilidad. Además, el color oro les brinda un aspecto lujoso y llamativo, atrayendo miradas y generando halagos. Sin duda, una opción versátil y chic para agregar a tu colección de zapatos.',
    combineBody:
      'Los tacones de tweed oro son una excelente elección para agregar un toque de elegancia a tu atuendo. Combínalos con un vestido negro ajustado para un look clásico y sofisticado. También puedes usarlos con unos jeans ajustados y una blusa blanca para crear un contraste moderno y chic. Si buscas un conjunto más formal, combínalos con una falda lápiz de color neutro y una blusa de seda. Completa tu look con accesorios dorados para resaltar el color de los tacones. ¡No hay límites para la versatilidad de estos tacones de tweed oro!',
    cleanBody:
      'Para mantener tus tacones de tweed oro en perfecto estado, es importante seguir algunos consejos de limpieza y cuidado. Primero, cepilla suavemente el tweed con un cepillo de cerdas suaves para eliminar cualquier suciedad o polvo. Si hay manchas, utiliza un paño húmedo y jabón suave para limpiarlas suavemente. Evita sumergir los tacones en agua o utilizar productos químicos fuertes, ya que podrían dañar el tejido. Asegúrate de dejarlos secar completamente antes de guardarlos. Para mantener su forma, rellénalos con papel o utiliza hormas para zapatos. Recuerda guardarlos en un lugar fresco y seco, lejos de la luz directa del sol. Con un cuidado adecuado, tus tacones de tweed oro lucirán impecables y te acompañarán en tus ocasiones especiales',
  },
  'top-algodon-verde': {
    prompt: 'top algodon verde',
    opinionBody:
      'Este top de algodón verde ha recibido excelentes críticas por parte de los usuarios que lo han adquirido. Su tejido de algodón 100% garantiza comodidad y transpirabilidad, convirtiéndolo en la elección perfecta para los días cálidos. Además, su color verde vibrante es muy favorecedor y versátil, permitiendo combinarlo con una gran variedad de prendas.',
    combineBody:
      'La versatilidad del top de algodón verde lo convierte en una prenda muy fácil de combinar. Puedes optar por un look casual y fresco combinándolo con unos jeans blancos y zapatillas deportivas. Si prefieres un estilo más sofisticado, puedes llevarlo con una falda midi beige y sandalias de tacón. Incluso puedes utilizarlo como una capa adicional sobre un vestido negro ceñido para agregar un toque de color y originalidad.',
    cleanBody:
      'Para mantener el top de algodón verde en óptimas condiciones, es recomendable lavarlo a mano o en ciclo suave en la lavadora con agua fría. Utiliza un detergente suave y evita el uso de blanqueadores. Para secarlo, es preferible colgarlo en una percha en un lugar fresco y sin exposición directa al sol. Si es necesario plancharlo, hazlo a baja temperatura y evita planchar directamente sobre estampados o detalles. Siguiendo estos simples cuidados, podrás disfrutar de tu top de algodón verde por mucho tiempo.',
  },
  'top-cuero-azul': {
    prompt: 'top cuero azul',
    opinionBody:
      'El top de cuero azul es un verdadero favorito entre los amantes de la moda. Muchos usuarios comentan que el material de cuero le da a este top un aspecto elegante y sofisticado, además de ofrecer una excelente calidad y durabilidad. El color azul añade un toque de frescura y originalidad, siendo una opción perfecta para destacar en cualquier ocasión. Sin duda, es una compra muy recomendada para aquellos que buscan lucir a la vanguardia de la moda.',
    combineBody:
      'Este top de cuero azul ofrece una gran versatilidad a la hora de combinarlo con otros elementos del vestuario. Puedes crear un look más casual y relajado combinándolo con unos jeans de mezclilla y unas zapatillas blancas. Si prefieres un estilo más elegante, puedes combinarlo con una falda lápiz negra y unos tacones altos. También puedes optar por darle un toque más sofisticado agregando accesorios dorados o plateados. Las posibilidades son infinitas, así que no dudes en experimentar y encontrar tu propio estilo con este fabuloso top de cuero azul.',
    cleanBody:
      'Para asegurar que tu top de cuero azul se mantenga en óptimas condiciones, es importante seguir algunos cuidados específicos. Para su limpieza, recomiendo utilizar un paño húmedo y suave con un poco de jabón neutro. Evita colocarlo en la lavadora o sumergirlo en agua, ya que esto puede dañar el cuero. Además, es fundamental mantenerlo alejado de la luz directa del sol y de fuentes de calor excesivas, ya que esto puede provocar que el color se desgaste o se decolore. Siempre que no esté en uso, guárdalo en un lugar fresco y seco, preferiblemente en una bolsa de tela para protegerlo de posibles raspaduras. Siguiendo estos sencillos consejos, podrás disfrutar de tu top de cuero azul durante mucho tiempo.',
  },
  'top-denim-blanco': {
    prompt: 'top denim blanco',
    opinionBody:
      'El top denim blanco es una prenda versátil y elegante que ha ganado popularidad en los últimos años. Los usuarios han elogiado su estilo moderno y fresco, así como su comodidad gracias al material de denim suave y elástico. Además, el color blanco es perfecto para lucir un look veraniego y sofisticado. Muchos compradores también han destacado la calidad de construcción del top, que garantiza una larga vida útil.',
    combineBody:
      'El top denim blanco es muy fácil de combinar con diferentes prendas y accesorios. Para un look casual y relajado, puedes combinarlo con unos jeans ajustados y unas zapatillas blancas. Si buscas un estilo más sofisticado, opta por una falda midi de color pastel y unos tacones nude. También puedes combinarlo con unos pantalones de cuero negro para un look más audaz y rockero. Completa el conjunto con algunos accesorios como un collar statement y estarás lista para cualquier ocasión.',
    cleanBody:
      'Para mantener tu top denim blanco en perfectas condiciones, es importante seguir algunas pautas de limpieza y cuidado. La mayoría de los tops de denim blanco se pueden lavar a máquina en un ciclo suave con agua fría. Usa un detergente suave y evita el uso de lejía, ya que puede dañar el color y el material. Para secar, lo mejor es colgar el top en una percha para evitar que se deformen los hombros. Si tienes alguna mancha, puedes tratarla con un quitamanchas antes de lavarlo. Recuerda plancharlo a baja temperatura si es necesario, y guárdalo en un lugar fresco y seco para evitar arrugas y decoloración. Con estos cuidados, tu top denim blanco se mantendrá en perfecto estado durante mucho tiempo.',
  },
  'top-encaje-blanco': {
    prompt: 'top encaje blanco',
    opinionBody:
      'Este Top Encaje Blanco ha recibido excelentes críticas por su diseño elegante y sofisticado. El encaje de alta calidad le da un toque femenino y delicado, mientras que el color blanco lo hace perfecto para cualquier ocasión. Además, los usuarios destacan lo cómodo que es y lo bien que se ajusta al cuerpo. Sin duda, es una prenda que no puede faltar en el armario de cualquier mujer.',
    combineBody:
      'El Top Encaje Blanco se puede combinar de muchas formas para crear diferentes looks. Para un estilo elegante y formal, puedes combinarlo con una falda lápiz negra y unos tacones altos. Si quieres un look más casual pero chic, puedes combinarlo con unos jeans ajustados y unas zapatillas blancas. También puedes añadirle un blazer negro para completar el look. En definitiva, las posibilidades de combinación son infinitas y seguro que encontrarás la opción perfecta para cada ocasión.',
    cleanBody:
      'Para mantener el Top Encaje Blanco en perfectas condiciones, es importante seguir algunas recomendaciones de limpieza. La mayoría de las prendas de encaje deben lavarse a mano con agua fría y detergente suave. Es importante evitar el uso de lejía o productos químicos agresivos. Una vez lavado, se recomienda secarlo en posición horizontal para evitar deformaciones. Además, para evitar enganches y desgarros, es recomendable guardar el top encaje blanco en una bolsa de tela o colgarlo en una percha en un lugar fresco y seco. Siguiendo estos consejos, podrás disfrutar de tu top encaje blanco durante mucho tiempo.',
  },
  'top-gasa-gris': {
    prompt: 'top gasa gris',
    opinionBody:
      'El top de gasa gris ha sido muy popular entre nuestros clientes, quienes destacan su elegancia y versatilidad. El material de gasa le da un aspecto ligero y aireado, perfecto para los días más calurosos. Además, el color gris es ideal para combinar con diferentes outfits, tanto formales como informales. Sin duda, este top es una excelente opción si buscas un toque sofisticado y femenino en tu guardarropa.',
    combineBody:
      'El top de gasa gris es una prenda muy versátil que puedes combinar de diversas formas. Para un look casual, puedes utilizarlo con jeans ajustados y unas zapatillas blancas, creando un estilo fresco y moderno. Si prefieres un look más elegante, puedes combinarlo con una falda lápiz negra y unos tacones altos, logrando un estilo sofisticado y chic. No importa cómo decidas combinarlo, este top será el protagonista de cualquier outfit y te hará lucir siempre a la moda.',
    cleanBody:
      'Para mantener el top de gasa gris en perfectas condiciones, te recomendamos seguir estas indicaciones. Lávalo a mano con agua fría y un detergente suave, evitando el uso de productos químicos agresivos. Si es necesario, puedes remojarlo durante algunos minutos antes de lavarlo para eliminar cualquier mancha. Después del lavado, déjalo secar al aire libre sin retorcerlo ni someterlo a altas temperaturas. Si deseas plancharlo, utiliza una temperatura baja y coloca un paño fino sobre el top para proteger el tejido de la gasa. Al seguir estos consejos, podrás disfrutar de tu top de gasa gris durante mucho tiempo y lucir siempre impecable.',
  },
  'top-gasa-morado': {
    prompt: 'top gasa morado',
    opinionBody:
      'Este top de gasa morado ha sido muy bien recibido por nuestros clientes. Han destacado la suavidad y comodidad del material, así como el ajuste perfecto que ofrece. Además, el color morado ha sido elogiado por su intensidad y lo favorecedor que resulta en cualquier ocasión. Sin duda, es una elección acertada para quienes buscan un look elegante y sofisticado.',
    combineBody:
      'El top de gasa morado es una prenda versátil que se puede combinar de diferentes formas. Para un look casual, puedes llevarlo con unos jeans ajustados y unas zapatillas blancas. Si prefieres un estilo más formal, puedes combinarlo con una falda midi de color negro y unos tacones a juego. También puedes añadirle accesorios dorados para resaltar aún más su elegancia.',
    cleanBody:
      'Para mantener el top de gasa morado en perfecto estado, te recomendamos lavarlo a mano con agua fría y un detergente suave. Evita retorcerlo o frotarlo en exceso para evitar dañar el tejido. Después, déjalo secar al aire libre, evitando la exposición directa al sol. Para mantener su color vibrante, es recomendable guardarlo en un lugar oscuro y alejado de la luz solar. Así podrás disfrutar de este hermoso top durante mucho tiempo.',
  },
  'top-jacquard-marron': {
    prompt: 'top jacquard marron',
    opinionBody:
      'El top jacquard marrón ha recibido excelentes comentarios de los usuarios. Su diseño sofisticado y elegante lo convierte en una prenda imprescindible en el armario de cualquier fashionista. El material jacquard le brinda un toque de distinción y calidad, además de ser muy cómodo de llevar. El color marrón es versátil y fácil de combinar, lo que lo convierte en una opción ideal tanto para eventos formales como para ocasiones más casuales. Sin duda, el top jacquard marrón es una pieza que no puede faltar en tu colección de moda.',
    combineBody:
      'El top jacquard marrón es una prenda versátil que se puede combinar de diversas formas. Para un look elegante, puedes combinarlo con una falda lápiz de cuero negro y tacones altos. Si prefieres un estilo más casual, puedes optar por unos jeans ajustados y botines negros. También puedes agregar accesorios dorados o plateados para agregar un toque de brillo a tu outfit. En resumen, las posibilidades de combinación son infinitas con el top jacquard marrón, lo que lo convierte en una prenda clave para crear diferentes looks.',
    cleanBody:
      'Para mantener el top jacquard marrón en óptimas condiciones, es recomendable seguir las instrucciones de cuidado del fabricante. Generalmente, se recomienda lavarlo a mano con agua fría y detergente suave. Evita retorcerlo o frotarlo con fuerza para evitar dañar el material jacquard. Para secarlo, es mejor colgarlo en una percha en un lugar fresco y bien ventilado. Evita exponerlo directamente a la luz solar intensa para evitar la decoloración del color marrón. Siguiendo estos consejos simples, podrás disfrutar del top jacquard marrón durante mucho tiempo y mantenerlo como nuevo.',
  },
  'top-organdi-gris': {
    prompt: 'top organdi gris',
    opinionBody:
      'El top organdi gris ha recibido excelentes críticas por parte de nuestros usuarios. Su material ligero y transparente, conocido como organdi, le da un aire sofisticado y elegante. Muchos clientes destacan la comodidad y versatilidad de este top, ya que es perfecto tanto para ocasiones casuales como formales. Además, el color gris combina fácilmente con otros tonos, convirtiéndolo en una prenda imprescindible en cualquier armario.',
    combineBody:
      'El top organdi gris es muy versátil a la hora de combinarlo. Puedes crear un look elegante si lo llevas con una falda lápiz de color negro o gris oscuro. Si buscas un estilo más casual, combínalo con unos jeans ajustados y unas zapatillas blancas. También puedes optar por añadir un toque de color con accesorios en tonos vivos, como un bolso rojo o unos zapatos de color llamativo. Sea cual sea tu elección, el top organdi gris se adapta a distintos estilos y ocasiones.',
    cleanBody:
      'Para mantener el top organdi gris en perfectas condiciones, es recomendable lavarlo a mano con agua fría y un detergente suave. Evita el uso de lejía y no lo frotes con fuerza para evitar dañar el delicado material. Después de lavarlo, es importante secarlo en plano para evitar deformaciones. Si requiere planchado, utiliza la temperatura más baja y coloca una tela fina entre la plancha y el top para proteger el organdi. Siguiendo estas indicaciones, podrás disfrutar de tu top organdi gris durante mucho tiempo manteniendo su aspecto impecable.',
  },
  'top-piel-azul': {
    prompt: 'top piel azul',
    opinionBody:
      'El top de piel azul es altamente valorado por su calidad y estilo moderno. Los usuarios destacan la suavidad y durabilidad del material de piel, que proporciona un ajuste cómodo y elegante. Además, el color azul vibrante agrega un toque de frescura y originalidad a cualquier outfit. Sin duda, es una elección perfecta para lucir sofisticada y a la moda.',
    combineBody:
      'El top de piel azul es extremadamente versátil y se puede combinar de diversas maneras para crear looks impresionantes. Para un estilo casual y relajado, puedes combinarlo con unos jeans desgastados y unas zapatillas blancas. Si buscas una apariencia más formal, puedes optar por una falda lápiz negra y unos tacones altos. Agrega algunos accesorios plateados para completar el conjunto y estarás lista para cualquier ocasión.',
    cleanBody:
      'Para asegurar la durabilidad y el buen aspecto del top de piel azul, es importante seguir unas pautas adecuadas de limpieza y mantenimiento. Se recomienda utilizar un paño húmedo y suave para limpiar cualquier mancha superficial. Si es necesario, puedes usar un limpiador específico para piel. Evita el contacto con líquidos corrosivos y mantén el top alejado de la luz solar directa para evitar que se decolore. Además, es recomendable guardar el top en una funda protectora para evitar posibles daños. Con los cuidados adecuados, podrás disfrutar de este elegante top de piel azul durante mucho tiempo.',
  },
  'top-piel-plata': {
    prompt: 'top piel plata',
    opinionBody:
      'El top de piel plateado ha recibido excelentes opiniones por parte de los usuarios. Su diseño moderno y elegante lo convierte en una prenda perfecta para ocasiones especiales o para darle un toque de estilo a cualquier outfit. El material de piel le proporciona un aspecto lujoso, mientras que el color plateado le agrega un brillo sutil y sofisticado. Además, la calidad de la confección y los acabados hacen que este top sea duradero y resistente al paso del tiempo.',
    combineBody:
      'El top de piel plateado es una pieza versátil que se puede combinar de diferentes maneras para crear looks únicos y llamativos. Puedes combinarlo con unos jeans ajustados y tacones altos para un look casual pero elegante. Si quieres darle un toque más formal, puedes combinarlo con una falda lápiz y unos zapatos de tacón. Si prefieres un estilo más relajado, puedes combinarlo con unos pantalones anchos y unas zapatillas blancas. ¡Las posibilidades son infinitas!',
    cleanBody:
      'Para mantener el top de piel plateado en perfecto estado, es importante seguir algunas precauciones. Para limpiarlo, se recomienda utilizar un cepillo suave o un paño húmedo para eliminar el polvo y la suciedad. Evita el uso de productos químicos agresivos que puedan dañar la piel. Además, es aconsejable guardar el top en un lugar fresco y seco para evitar que se deteriore. Si notas alguna mancha difícil, es mejor acudir a un especialista en limpieza de cuero para evitar dañar el material.',
  },
  'top-sintetica-verde': {
    prompt: 'top sintetica verde',
    opinionBody:
      'El top sintético verde ha recibido excelentes opiniones por parte de los usuarios. Su material sintético garantiza una alta durabilidad y resistencia, lo que lo convierte en una opción ideal para lucir a la moda sin sacrificar comodidad. Además, el color verde vibrante es perfecto para agregar un toque de frescura y estilo a cualquier look. Los clientes destacan su ajuste perfecto, su tacto suave y lo favorecedor que resulta en diferentes tipos de cuerpo. Sin duda, el top sintético verde es una elección segura para lucir a la moda y destacar en cualquier ocasión.',
    combineBody:
      'El top sintético verde es extremadamente versátil a la hora de combinarlo con otras prendas. Para un look casual y fresco, puedes optar por combinarlo con unos jeans desgastados y unas zapatillas blancas. Si deseas un outfit más elegante, puedes llevarlo con una falda midi plisada y unos tacones altos. Además, el color verde va muy bien con tonos neutros como el blanco, el negro y el gris, por lo que puedes experimentar con diferentes combinaciones. No temas añadir accesorios como collares o pulseras para completar el look. Con el top sintético verde, las posibilidades son infinitas.',
    cleanBody:
      'Mantener el top sintético verde en perfectas condiciones es muy sencillo. Para limpiarlo, puedes utilizar un paño húmedo y jabón suave. Frota suavemente la superficie del top y acláralo con agua tibia. Evita frotar enérgicamente para no dañar el material. Si el top tiene alguna mancha persistente, puedes utilizar un quitamanchas suave. Recuerda no utilizar blanqueadores o detergentes agresivos, ya que podrían dañar la tela sintética. Para mantener su forma y evitar pliegues, es recomendable guardarlo en un lugar plano o colgarlo en una percha. Si sigues estos sencillos consejos de limpieza y mantenimiento, podrás disfrutar de tu top sintético verde durante mucho tiempo.',
  },
  'top-tela-blanco': {
    prompt: 'top tela blanco',
    opinionBody:
      'El top de tela blanca es una elección perfecta para aquellos que buscan una prenda versátil y fácil de combinar. Su suave material brinda comodidad y frescura durante todo el día. Los usuarios elogian su ajuste favorecedor y su capacidad para realzar cualquier tipo de figura. Además, destacan que el color blanco es un clásico que nunca pasa de moda, lo que lo convierte en una inversión segura. Sin duda, el top de tela blanca es una opción elegante y chic para cualquier ocasión.',
    combineBody:
      'El top de tela blanca es realmente versátil y se puede combinar de muchas formas diferentes. Para un look fresco y casual, puedes optar por jeans ajustados y zapatillas blancas. Si buscas una opción más elegante, puedes combinarlo con una falda midi de color pastel y tacones altos. Para un estilo bohemio, puedes usarlo con pantalones anchos y sandalias de cuero. Sin importar cómo lo combines, el top de tela blanca siempre añade un toque de sofisticación y estilo a cualquier outfit.',
    cleanBody:
      'Para mantener tu top de tela blanca en las mejores condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Siempre verifica las instrucciones de lavado en la etiqueta del producto, ya que algunos tops pueden requerir lavado a mano o en seco. Si lo lavas a máquina, utiliza un ciclo suave y agua fría para evitar que se decolore. Evita el uso de blanqueadores y plancha el top a baja temperatura si es necesario. Siempre almacena tu top de tela blanca en un lugar fresco y seco para evitar manchas y arrugas.',
  },
  'top-vaquera-azul': {
    prompt: 'top vaquera azul',
    opinionBody:
      'La top vaquera azul es una prenda extremadamente versátil que no puede faltar en tu guardarropa. Su diseño elegante y casual la convierten en una opción perfecta tanto para eventos informales como para ocasiones más formales. Los usuarios que han adquirido esta prenda destacan su comodidad y durabilidad, así como su perfecto ajuste al cuerpo. Sin duda alguna, la top vaquera azul es una elección acertada para lucir a la moda y destacar en cualquier ocasión.',
    combineBody:
      'La top vaquera azul es una prenda muy fácil de combinar, ya que su tonalidad neutral permite crear diferentes estilos y looks. Si buscas un conjunto casual, puedes combinarla con unos pantalones vaqueros en tonos claros o oscuros, añadiendo unos sneakers o unas sandalias planas. Para un estilo más elegante, puedes optar por combinarla con unos pantalones de tela en tonos neutros y agregar unos tacones. Además, puedes complementar tu outfit con accesorios como collares o pulseras para agregar un toque personalizado.',
    cleanBody:
      'Para mantener la top vaquera azul en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Se recomienda lavarla a mano con agua fría y utilizar un detergente suave. Evita utilizar productos blanqueadores o fuertes, ya que pueden dañar el color y el tejido. Para secarla, es mejor colgarla en una percha en un lugar fresco y con buena ventilación, evitando la exposición directa al sol. Si es necesario plancharla, utiliza una temperatura baja o media y coloca un paño por encima para proteger el tejido. Siguiendo estos consejos, podrás disfrutar de tu top vaquera azul durante mucho tiempo sin perder su estilo ni calidad.',
  },
  'traje-algodon-bronce': {
    prompt: 'traje algodon bronce',
    opinionBody:
      'El traje de algodón en color bronce es una opción elegante y moderna para lucir en cualquier ocasión. Los usuarios que han adquirido este tipo de producto destacan la calidad del material y su comodidad al usarlo. Además, el color bronce le añade un toque de sofisticación y originalidad al conjunto. Sin duda, es una elección acertada para aquellos que buscan destacar con estilo y distinción.',
    combineBody:
      'El traje de algodón en color bronce ofrece muchas posibilidades de combinación. Para un look más formal o de negocios, puedes combinarlo con una camisa blanca y corbata en tonos cálidos como el dorado o el marrón. Si prefieres un estilo más casual, puedes optar por una camiseta negra o gris oscuro y unos zapatos de cuero en color marrón. Además, puedes añadir accesorios en tonos neutros o en contraste como una cartera o un reloj en color negro.',
    cleanBody:
      'Para mantener tu traje de algodón en color bronce en perfectas condiciones, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable leer las instrucciones del fabricante y seguir las indicaciones de lavado. En general, se recomienda lavar a mano con agua fría y utilizar detergente suave para prendas delicadas. Evita utilizar productos o blanqueadores agresivos que puedan dañar el color. Además, es aconsejable secar el traje al aire libre en un lugar fresco y evitar la exposición directa al sol. Por último, es importante planchar a baja temperatura y utilizar un paño entre la plancha y el traje para evitar posibles daños. Siguiendo estas recomendaciones, asegurarás la durabilidad y el aspecto impecable de tu traje de algodón en color bronce.',
  },
  'traje-cachemira-marron': {
    prompt: 'traje cachemira marron',
    opinionBody:
      'Los usuarios que han adquirido un traje de cachemira marrón destacan la alta calidad y suavidad del material. Además, resaltan el hecho de que el color marrón resulta elegante y versátil, ya que puede ser utilizado tanto para eventos formales como informales. También comentan que el traje de cachemira marrón es altamente duradero y resistente al desgaste, lo que lo convierte en una excelente inversión a largo plazo.',
    combineBody:
      'El traje de cachemira marrón se puede combinar de diversas maneras para lograr diferentes estilos. Para un look clásico y elegante, se recomienda combinarlo con una camisa blanca y corbata en tonos oscuros. Si se busca un estilo más casual pero sofisticado, se puede optar por llevarlo con una camisa de rayas en tonos tierra y una chaqueta de cuero marrón. Además, este traje también queda perfecto con accesorios en tonos neutros como zapatos negros o marrones y cinturón de cuero a juego.',
    cleanBody:
      'Para mantener en buen estado un traje de cachemira marrón, es recomendable seguir algunos consejos de limpieza y mantenimiento. En primer lugar, es importante evitar exponerlo demasiado al sol, ya que los rayos UV pueden dañar el color. A la hora de limpiarlo, se aconseja llevarlo a la tintorería para un lavado en seco, ya que la cachemira es un material delicado que requiere un cuidado especial. Además, se recomienda guardar el traje en una funda de tela transpirable para protegerlo del polvo y la humedad cuando no se esté utilizando. Siguiendo estos sencillos pasos, el traje de cachemira marrón se mantendrá en óptimas condiciones durante mucho tiempo.',
  },
  'traje-gasa-rosa': {
    prompt: 'traje gasa rosa',
    opinionBody:
      'El traje de gasa rosa es sin duda una elección elegante y femenina para cualquier ocasión. Los usuarios destacan la suavidad y la ligereza del material, lo que lo convierte en una opción cómoda y fácil de llevar. Además, el color rosa es muy favorecedor para todo tipo de tonos de piel, brindando un aspecto radiante y juvenil. En general, los usuarios están encantados con este traje y lo consideran una pieza imprescindible en su guardarropa.',
    combineBody:
      'El traje de gasa rosa combina a la perfección con una amplia variedad de prendas y accesorios. Para un look romántico y sofisticado, puedes combinarlo con tacones altos en tonos neutros y accesorios en dorado o plateado. Si prefieres un estilo más casual, puedes optar por zapatillas blancas y una chaqueta de mezclilla. También puedes añadir un toque de elegancia con una bolsa de mano en tonos pastel. En resumen, las opciones son infinitas para crear outfits únicos y versátiles con este hermoso traje.',
    cleanBody:
      'Para mantener tu traje de gasa rosa en las mejores condiciones, te recomendamos seguir las siguientes indicaciones. Antes de lavarlo, asegúrate de leer las instrucciones de cuidado en la etiqueta. En la mayoría de los casos, la gasa es un material delicado que requiere lavado a mano o en ciclo suave en la lavadora. Utiliza agua fría y un detergente suave. Evita retorcer o frotar el tejido para evitar daños. Después del lavado, es importante secarlo al aire libre o en un lugar fresco y alejado de la luz directa del sol. Si es necesario plancharlo, utiliza una temperatura baja o media y coloca una tela delgada entre la plancha y la gasa para evitar quemaduras. Siguiendo estos consejos, lograrás mantener la belleza y la calidad de tu traje de gasa rosa durante mucho tiempo.',
  },
  'traje-jersey-turquesa': {
    prompt: 'traje jersey turquesa',
    opinionBody:
      'Los usuarios han quedado encantados con el traje jersey turquesa gracias a su diseño moderno y versátil. El material utilizado, en este caso un suave tejido de punto, brinda una comodidad excepcional. Además, el color turquesa da un toque de elegancia y frescura al conjunto. Sin duda, un must-have en el armario de cualquier fashionista.',
    combineBody:
      'Este traje jersey turquesa es una prenda muy versátil que se puede combinar de diversas maneras. Para un look casual pero sofisticado, recomendamos combinarlo con unos jeans negros y botines de cuero. Si prefieres un estilo más formal, puedes llevarlo con una falda lápiz de color negro y zapatos de tacón. También puedes añadir accesorios en tonos dorados para darle un toque glamuroso a tu outfit.',
    cleanBody:
      'Para mantener el traje jersey turquesa en perfectas condiciones, es importante seguir algunas recomendaciones. Se recomienda lavarlo a mano con agua fría y utilizar un detergente suave. Evita retorcerlo para escurrir y sécalo en posición horizontal para evitar que se deforme. Además, plancha a temperatura baja y evita el uso de cloro o blanqueadores. Siguiendo estos consejos, tu traje jersey turquesa lucirá impecable por mucho tiempo.',
  },
  'traje-jersey-verde': {
    prompt: 'traje jersey verde',
    opinionBody:
      'Los usuarios han elogiado el traje jersey verde por su increíble comodidad y ajuste perfecto. El material de alta calidad asegura durabilidad y resistencia, lo que lo convierte en una opción ideal para cualquier ocasión. Además, su color verde vibrante agrega un toque de estilo y sofisticación a cualquier outfit.',
    combineBody:
      'El traje jersey verde es extremadamente versátil y se puede combinar de varias maneras. Para un look elegante, se puede combinar con una camisa blanca y zapatos de vestir negros. Si se desea un look más casual, se puede combinar con una camiseta básica y zapatillas blancas. También se puede agregar un toque de originalidad combinándolo con accesorios en tonos neutros.',
    cleanBody:
      'Para mantener el traje jersey verde en perfectas condiciones, se recomienda lavarlo a mano o en ciclo suave en la lavadora con agua fría. Es importante evitar el uso de lejía o productos químicos agresivos. Después del lavado, se debe secar al aire libre, evitando la exposición directa al sol. Para mantener la forma original y eliminar las arrugas, se puede planchar a baja temperatura. Con estos cuidados, el traje jersey verde lucirá impecable durante mucho tiempo.',
  },
  'traje-lino-azul': {
    prompt: 'traje lino azul',
    opinionBody:
      'El traje de lino azul es altamente valorado por su elegancia y comodidad. Los usuarios destacan la frescura y ligereza del material de lino, ideal para climas cálidos. Además, el color azul añade un toque sofisticado y versátil al conjunto. Los clientes elogian la calidad de los acabados y la durabilidad del traje, haciendo que sea una elección popular para eventos formales o informales.',
    combineBody:
      'El traje de lino azul se puede combinar de diversas formas para adaptarse a diferentes ocasiones. Para un look formal, se recomienda combinarlo con una camisa blanca y una corbata a juego en tonos azules o grises. Para un estilo más casual, se puede optar por una camiseta de rayas horizontales en tonos neutros y zapatillas blancas. Además, se pueden añadir accesorios como un pañuelo de bolsillo o unos gemelos para darle un toque personalizado.',
    cleanBody:
      'Para mantener el traje de lino azul en perfectas condiciones, es recomendable seguir ciertos cuidados. Se recomienda lavarlo a mano con agua fría o tibia y un detergente suave. Es importante evitar el uso de blanqueadores o productos químicos agresivos que puedan dañar el tejido de lino. Después de lavarlo, se debe colgar para secar al aire libre evitando la exposición directa al sol. Asimismo, se aconseja plancharlo a baja temperatura con un paño húmedo para evitar brillos en el tejido. Seguir estas instrucciones ayudará a mantener el traje de lino azul en óptimas condiciones durante mucho tiempo.',
  },
  'traje-nylon-rosa': {
    prompt: 'traje nylon rosa',
    opinionBody:
      'El traje de nylon rosa es altamente valorado por su versatilidad y estética llamativa. Los usuarios elogian su resistencia al agua y durabilidad, lo que lo convierte en una excelente elección para actividades al aire libre. Además, su material ligero y transpirable garantiza comodidad durante todo el día. En cuanto al color rosa, los usuarios destacan su atractivo y feminidad, convirtiéndolo en una opción popular entre las mujeres que desean lucir elegantes y a la moda.',
    combineBody:
      'El traje de nylon rosa se puede combinar de diversas formas para crear looks impresionantes. Para un conjunto casual y relajado, puedes combinarlo con zapatillas blancas y una camiseta blanca debajo. Si buscas un estilo más sofisticado, opta por combinarlo con tacones negros y una blusa ajustada de color neutro. También puedes agregar accesorios dorados para resaltar aún más el color rosa. En resumen, las posibilidades de combinación con un traje de nylon rosa son infinitas, lo que te permite crear outfits según tu personalidad y ocasión.',
    cleanBody:
      'Para mantener tu traje de nylon rosa en buen estado, es importante seguir las instrucciones de cuidado del fabricante. En general, se recomienda lavarlo a mano con agua fría y detergente suave. Evita usar lejía o productos químicos agresivos que puedan dañar el material o decolorarlo. Después de lavarlo, déjalo secar al aire libre y evita el uso de secadoras o fuentes de calor directas. Si notas alguna mancha, puedes tratarla con un quitamanchas suave antes de lavarlo. Recuerda guardar tu traje de nylon rosa en un lugar fresco y seco, lejos de la luz solar directa, para preservar su color y calidad por más tiempo.',
  },
  'traje-poliester-azul': {
    prompt: 'traje poliester azul',
    opinionBody:
      'El traje de poliéster azul ha recibido excelentes opiniones por parte de los usuarios. Muchos destacan su durabilidad y resistencia, ya que el poliéster es un material que no se arruga fácilmente. Además, el color azul le da un toque elegante y sofisticado, perfecto para ocasiones formales. Algunos usuarios también mencionan que el traje de poliéster azul es bastante cómodo de usar, lo cual es ideal para eventos largos.',
    combineBody:
      'El traje de poliéster azul es muy versátil y se puede combinar de varias maneras. Para un look clásico, puedes combinarlo con una camisa blanca y corbata a juego. Si quieres agregar un toque de color, una camisa en tonos pastel o estampada puede ser una excelente opción. Además, puedes jugar con los accesorios, como un pañuelo de bolsillo o gemelos, para personalizar aún más tu look. En cuanto al calzado, los zapatos negros o marrones siempre son una elección segura.',
    cleanBody:
      'Para limpiar y mantener tu traje de poliéster azul en óptimas condiciones, es recomendable seguir las instrucciones de cuidado del fabricante. Por lo general, el poliéster es un material fácil de limpiar, ya que es resistente a las manchas y no se arruga con facilidad. Puedes lavar tu traje a mano o a máquina, utilizando un ciclo suave y agua fría. Evita el uso de lejía y secadora, ya que podrían dañar el tejido. Para mantenerlo siempre impecable, plancha el traje a baja temperatura y guárdalo en una funda protectora para evitar el polvo y la humedad.',
  },
  'traje-saten-claro': {
    prompt: 'traje saten claro',
    opinionBody:
      'El traje de satén claro es una elección elegante y sofisticada para cualquier ocasión. Muchos usuarios coinciden en que este material brinda un aspecto lujoso y una sensación suave al tacto. Además, el color claro brinda luminosidad y destaca en cualquier evento. A los usuarios les gusta la versatilidad de este traje, ya que puede ser utilizado tanto en eventos formales como en ocasiones más casuales. En cuanto a la calidad, los usuarios destacan que el satén es duradero y de alta calidad, lo que garantiza que el traje se mantendrá en buen estado durante mucho tiempo. En general, las opiniones sobre el traje de satén claro son muy positivas, destacando su elegancia y versatilidad.',
    combineBody:
      'El traje de satén claro es extremadamente versátil y puede combinarse de diversas formas para crear diferentes looks. Para eventos formales, puedes combinarlo con una camisa de vestir blanca y una corbata de color complementario. Para ocasiones más casuales, puedes optar por una camiseta básica en tonos neutros y unos zapatos de vestir en tonos marrones. Si quieres agregar un toque de elegancia, puedes complementarlo con un pañuelo de bolsillo a juego con la corbata o un reloj de pulsera de diseño. En general, el traje de satén claro combina bien con colores neutros y tonos pasteles, lo que permite crear looks sofisticados y modernos.',
    cleanBody:
      'El traje de satén claro es delicado y requiere cuidados especiales para mantenerlo en buen estado. Para su limpieza, se recomienda llevarlo a la tintorería, ya que ellos sabrán cómo tratar este tipo de material de forma adecuada y garantizar que no se dañe. En caso de manchas pequeñas, se puede utilizar un paño húmedo y suave para limpiar la zona afectada, evitando frotar en exceso para no dañar el tejido. Además, es importante evitar el contacto con sustancias químicas y perfumes fuertes, ya que pueden dejar manchas o',
  },
  'traje-sintetica-claro': {
    prompt: 'traje sintetica claro',
    opinionBody:
      'Los usuarios que han adquirido un traje sintético claro han destacado su gran versatilidad y comodidad. Además, el material sintético ofrece una resistencia excepcional, lo que asegura que el traje se mantenga en perfectas condiciones a lo largo del tiempo. También se destaca su color claro, el cual brinda una apariencia fresca y elegante. Los clientes están satisfechos con la calidad y el estilo que ofrece este tipo de traje, convirtiéndolo en una opción popular para diversas ocasiones.',
    combineBody:
      'El traje sintético claro puede ser combinado con una amplia variedad de accesorios y prendas para crear diferentes estilos. Para eventos formales, se puede optar por combinarlo con una camisa de vestir blanca y una corbata en tonos plateados o grises. Para ocasiones más informales, se puede utilizar una camisa de algodón en colores pastel y combinarlo con unos mocasines de cuero. Además, se pueden añadir pequeños detalles como un pañuelo de bolsillo o una corbata estampada para darle un toque personalizado al conjunto.',
    cleanBody:
      'Para garantizar la durabilidad y mantenimiento de un traje sintético claro, es importante seguir algunos consejos de limpieza. Se recomienda leer las instrucciones del fabricante antes de proceder a lavarlo. En la mayoría de los casos, se puede lavar a máquina utilizando un ciclo suave y agua fría. Se debe evitar el uso de suavizantes y secadoras, ya que pueden dañar el material sintético. Para eliminar manchas localizadas, se puede utilizar un detergente suave y frotar suavemente con un cepillo de cerdas suaves. Finalmente, se recomienda colgar el traje en una percha y dejarlo secar al aire para evitar arrugas.',
  },
  'traje-tela-marino': {
    prompt: 'traje tela marino',
    opinionBody:
      'El traje de tela marino es una elección clásica y elegante para lucir en cualquier ocasión. Los usuarios que han adquirido este producto destacan la calidad de la tela, que es resistente y duradera. Además, comentan que el color marino es versátil y se adapta fácilmente a cualquier tipo de evento, ya sea formal o casual. Sin duda, es una opción segura y sofisticada que nunca pasará de moda.',
    combineBody:
      'El traje de tela marino es tan versátil que puede combinarse con una amplia variedad de colores y accesorios. Para un look clásico y elegante, puedes combinarlo con una camisa blanca y corbata en tonos neutros como gris o negro. Si buscas un estilo más moderno, puedes optar por una camisa de color pastel, como rosa o celeste, y complementarlo con una corbata a juego. Además, puedes añadir accesorios como un reloj de pulsera o un pañuelo de bolsillo en el mismo tono del traje para darle un toque sofisticado.',
    cleanBody:
      'Para mantener el traje de tela marino en buen estado, es importante seguir ciertos cuidados. Para limpiarlo, se recomienda llevarlo a una tintorería profesional para evitar dañarlo. Sin embargo, si necesitas eliminar pequeñas manchas, puedes utilizar un paño húmedo y un poco de detergente suave, realizando movimientos suaves para no dañar la tela. Además, es importante colgarlo en una percha de calidad para evitar arrugas y mantener su forma original. Por último, es recomendable guardarlo en una funda de tela transpirable para protegerlo del polvo y la humedad. Siguiendo estos consejos de limpieza y mantenimiento, tu traje de tela marino se mantendrá impecable durante mucho tiempo.',
  },
  'traje-tela-marron': {
    prompt: 'traje tela marron',
    opinionBody:
      'El traje de tela marrón es una elección sofisticada y moderna. Muchos usuarios destacan su versatilidad, ya que es adecuado tanto para ocasiones formales como informales. La textura suave de la tela y el color marrón dan un toque de elegancia y originalidad al conjunto. Además, su material de alta calidad asegura una prenda duradera y resistente al desgaste. Sin duda, el traje de tela marrón es una excelente opción para aquellos que buscan destacar con estilo y distinción.',
    combineBody:
      'El traje de tela marrón se puede combinar de diferentes maneras para crear looks impresionantes. Para un estilo clásico y elegante, puedes optar por combinarlo con una camisa blanca y corbata en tonos neutros, como el beige o el gris. Si buscas un look más casual, puedes combinarlo con una camiseta de cuello redondo en tonos claros y zapatillas blancas. Para añadir un toque de sofisticación, puedes añadir complementos en tonos dorados o plateados, como un reloj o un pañuelo. En definitiva, el traje de tela marrón es una prenda versátil que te permitirá experimentar con diferentes combinaciones y estilos.',
    cleanBody:
      'Para mantener tu traje de tela marrón en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. En primer lugar, es recomendable leer las instrucciones de lavado que vienen en la etiqueta de la prenda. En la mayoría de los casos, los trajes de tela marrón deben ser limpiados en seco para evitar daños en el tejido. Cuando no lo estés usando, es recomendable guardarlo en una funda de tela transpirable para protegerlo del polvo y la humedad. Además, es importante evitar exponerlo directamente a la luz solar prolongada, ya que esto puede provocar decoloración. Siguiendo estos consejos, podrás disfrutar de tu traje de tela marrón durante mucho tiempo y lucir impecable en cualquier',
  },
  'vestido-cachemira-verde': {
    prompt: 'vestido cachemira verde',
    opinionBody:
      'Este vestido de cachemira verde ha recibido excelentes críticas por su suavidad y comodidad. Los usuarios destacan la calidad del material, así como su tacto lujoso y agradable. Además, el color verde ha sido muy elogiado por ser vibrante y favorecedor, perfecto para resaltar cualquier tono de piel. Sin duda, este vestido se ha convertido en una opción popular entre aquellos que buscan una prenda elegante y confortable.',
    combineBody:
      'Este versátil vestido de cachemira verde puede combinarse de múltiples formas. Si buscas un look casual chic, puedes optar por llevarlo con unas zapatillas blancas y una chaqueta de denim para un aspecto desenfadado pero sofisticado. Para una ocasión más formal, puedes combinarlo con unos tacones negros y un blazer a juego. Si quieres darle un toque más bohemio, añade unas sandalias de tiras y accesorios en tonos tierra. Este vestido es tan adaptable que las posibilidades son infinitas.',
    cleanBody:
      'Para mantener este precioso vestido de cachemira verde en perfectas condiciones, es importante seguir algunos consejos de cuidado. Primero, asegúrate de leer las instrucciones de lavado específicas del fabricante. Como la cachemira es un material delicado, es recomendable lavarlo a mano o en el ciclo suave de la lavadora con agua fría y un detergente suave para prendas delicadas. Evita retorcerlo o frotarlo vigorosamente. Después del lavado, seca el vestido extendiéndolo sobre una toalla en posición plana. Si es necesario, puedes utilizar una plancha a baja temperatura para eliminar las arrugas, pero asegúrate de colocar una tela entre la plancha y el vestido para evitar daños. Guarda el vestido en un lugar fresco y seco, preferiblemente colgado para evitar la formación de pliegues. Siguiendo estos sencillos pasos, podrás disfrutar de tu vestido de cachemira verde durante mucho tiempo.',
  },
  'vestido-jersey-verde': {
    prompt: 'vestido jersey verde',
    opinionBody:
      'Los usuarios coinciden en que el vestido jersey verde es una prenda versátil y cómoda. El material de punto le da un aspecto casual pero elegante al mismo tiempo. Además, el color verde es muy favorecedor para todo tipo de tonos de piel. Sin duda, es una excelente opción para lucir a la moda y sentirse cómoda durante todo el día.',
    combineBody:
      'El vestido jersey verde es una prenda que se puede combinar de diferentes formas para crear diferentes looks. Se puede llevar con unas botas altas y un cinturón para marcar la cintura, logrando un estilo chic y sofisticado. También se puede combinar con unas zapatillas blancas y una chaqueta de cuero para un look más casual y desenfadado. Incluso se puede agregar un pañuelo estampado o unos accesorios dorados para añadir un toque de elegancia.',
    cleanBody:
      'Para mantener el vestido jersey verde en perfecto estado, es importante seguir ciertos cuidados. Se recomienda lavarlo a mano o en ciclo delicado en la lavadora, utilizando agua fría y un detergente suave. Evitar el uso de blanqueadores o suavizantes. Para secarlo, se puede colgar en una percha para evitar deformaciones. Siempre es recomendable leer las instrucciones de cuidado del fabricante antes de proceder a la limpieza del vestido jersey verde.',
  },
  'vestido-lana-plata': {
    prompt: 'vestido lana plata',
    opinionBody:
      'El vestido de lana plata es una elección muy popular entre las mujeres debido a su elegancia y sofisticación. Las opiniones de las usuarias que lo han adquirido coinciden en que el material de lana proporciona una sensación de calidez y comodidad, perfecto para los días más fríos. Además, el color plata agrega un toque de brillo y estilo a cualquier ocasión, ya sea una fiesta o una reunión formal. Su diseño ajustado realza la figura y destaca la silueta de manera favorecedora. Sin duda, el vestido de lana plata es una opción versátil y atemporal que no pasa desapercibida.',
    combineBody:
      'El vestido de lana plata es muy versátil y se puede combinar de diversas maneras. Para un look elegante y formal, puedes combinarlo con tacones altos en color negro y accesorios minimalistas en tonos plateados. Si buscas un estilo más casual pero sofisticado, puedes optar por combinarlo con botines de tacón bajo y una chaqueta de cuero. Otra opción es añadir un cinturón delgado para marcar la cintura y un bolso de mano en tonos plateados para completar el conjunto. En resumen, el vestido de lana plata es una prenda muy versátil que se puede adaptar a diferentes estilos y ocasiones.',
    cleanBody:
      'Para mantener tu vestido de lana plata en perfectas condiciones, es importante seguir ciertos cuidados. En primer lugar, asegúrate de leer las instrucciones de lavado que vienen en la etiqueta del vestido, ya que pueden variar según el tipo de lana. En general, es recomendable lavarlo a mano o en ciclo suave en la lavadora, utilizando agua fría o tibia y un detergente suave para prendas delicadas. Evita retorcerlo para secarlo y en su lugar, déjalo secar al aire libre o utilizando una toalla limpia y presionando suavemente para eliminar el exceso de agua. Para evitar que se deforme, guárdalo colgado en un lugar fresco y',
  },
  'vestido-piel-naranja': {
    prompt: 'vestido piel naranja',
    opinionBody:
      'Este vestido de piel en color naranja ha sido muy bien recibido por nuestros clientes. No solo es elegante y llamativo, sino que también es de excelente calidad. Muchos han elogiado su suavidad y comodidad al usarlo. Además, el color naranja le da un toque vibrante y único, ideal para destacar en cualquier ocasión.',
    combineBody:
      'Este vestido de piel en color naranja es muy versátil a la hora de combinarlo. Puedes optar por accesorios en tonos neutros como el negro o el blanco, para crear un contraste elegante. Para un look más audaz, puedes combinarlo con accesorios en tonos más vibrantes, como el amarillo o el verde. También puedes jugar con estampados en colores complementarios, como el azul o el morado. Las posibilidades son infinitas, dándote la libertad de personalizar tu estilo.',
    cleanBody:
      'Para mantener tu vestido de piel en color naranja en perfectas condiciones, te recomendamos seguir algunos consejos de limpieza y cuidado. En primer lugar, evita exponerlo directamente a la luz solar, ya que esto puede dañar su color. Limpia suavemente cualquier mancha utilizando un paño húmedo y un detergente suave. Evita el uso de productos químicos agresivos o abrasivos. Almacenalo en un lugar fresco y seco, preferiblemente en una funda de tela transpirable, para evitar el contacto con otros objetos que puedan dañar su superficie de piel. Siguiendo estos consejos, podrás disfrutar de tu vestido de piel naranja por mucho tiempo.',
  },
  'vestido-poliester-azul': {
    prompt: 'vestido poliester azul',
    opinionBody:
      'Este vestido de poliéster azul ha recibido excelentes críticas por parte de los usuarios. Muchos destacan la suavidad y comodidad del material, que se adapta perfectamente al cuerpo sin sacrificar la movilidad. Además, el color azul vibrante le da un toque elegante y sofisticado, haciendo que el vestido destaque en cualquier ocasión. Sin duda, una excelente elección para lucir radiante y a la moda.',
    combineBody:
      'Este versátil vestido de poliéster azul se puede combinar de diferentes formas para crear distintos estilos. Para un look casual, puedes optar por combinarlo con unas zapatillas blancas y una chaqueta de mezclilla. Si quieres un toque más elegante, puedes acompañarlo con tacones negros y accesorios plateados. También puedes jugar con los detalles y añadir un cinturón delgado en la cintura para resaltar tu figura.',
    cleanBody:
      'Mantener este vestido de poliéster azul en perfecto estado es muy sencillo. Puedes lavarlo a mano o a máquina con agua fría y un detergente suave. Recuerda no retorcerlo y secarlo al aire libre, evitando la exposición directa al sol. Para mantener el color vibrante, es recomendable no mezclarlo con prendas de colores claros durante el lavado. Si aparecen arrugas, puedes plancharlo a baja temperatura o utilizar una vaporera. Con estos cuidados básicos, tu vestido de poliéster azul lucirá impecable durante mucho tiempo.',
  },
  'vestido-punto-negro': {
    prompt: 'vestido punto negro',
    opinionBody:
      'El vestido de punto negro es una elección clásica y versátil que nunca pasa de moda. Este tipo de vestido es muy popular entre las mujeres debido a su comodidad y estilo atemporal. Muchas usuarias han compartido sus opiniones positivas sobre este tipo de vestido, destacando su elegancia y simplicidad. Además, el punto le otorga un toque de calidez y suavidad, lo que lo convierte en una opción ideal para cualquier ocasión. Sin duda, el vestido de punto negro es una prenda imprescindible en el armario de cualquier mujer.',
    combineBody:
      'El vestido de punto negro es una prenda muy versátil que se puede combinar de diversas maneras. Para un look casual y relajado, puedes combinarlo con unas zapatillas blancas y una chaqueta de denim. Si deseas un look más elegante, puedes agregar unos tacones altos y accesorios llamativos. Asimismo, puedes jugar con diferentes estilos de calzado, como botines o sandalias, dependiendo de la ocasión. En resumen, el vestido de punto negro se adapta a cualquier estilo y es una excelente base para crear diversos conjuntos.',
    cleanBody:
      'La clave para mantener un vestido de punto negro en buen estado es cuidarlo adecuadamente. Asegúrate de leer las instrucciones de lavado del fabricante antes de proceder a limpiarlo. En general, se recomienda lavar los vestidos de punto negro a mano o en ciclo suave en la lavadora con agua fría. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el tejido y hacer que pierda su forma. Para mantener su color negro intenso, es recomendable utilizar detergentes suaves o específicos para prendas oscuras. Además, cuando no estés usando el vestido, guárdalo en un lugar oscuro y seco, evitando la exposición prolongada a la luz solar directa. Siguiendo estos consejos, podrás disfrutar de tu vestido de punto negro durante mucho tiempo.',
  },
  'vestido-sintetica-gris': {
    prompt: 'vestido sintetica gris',
    opinionBody:
      'El vestido sintético gris ha recibido excelentes opiniones por parte de nuestras clientas. Destacan su diseño elegante y versátil, perfecto para cualquier ocasión. Además, el uso de material sintético asegura durabilidad y resistencia, manteniendo su forma y color incluso después de varios lavados. Sin duda, es una prenda imprescindible en cualquier guardarropa.',
    combineBody:
      'El vestido sintético gris es una pieza muy versátil que se puede combinar de varias formas. Para un look casual, puedes agregar unas botas altas y una chaqueta de cuero. Si buscas algo más elegante, combínalo con tacones negros y un collar llamativo. También puedes agregar complementos en tonos plateados o dorados para resaltar aún más su belleza. Las posibilidades son infinitas, ¡solo depende de tu estilo y creatividad!',
    cleanBody:
      'Para mantener el vestido sintético gris en perfectas condiciones, se recomienda lavarlo a mano o en ciclo suave en la lavadora con agua fría. Evita el uso de blanqueadores y secadoras, ya que pueden dañar el material sintético. Para eliminar arrugas, utiliza una plancha a baja temperatura o utiliza un vaporizador. Al guardar el vestido, cuélgalo en un perchero o dóblalo cuidadosamente para evitar arrugas innecesarias. Sigue estos simples consejos de cuidado y disfrutarás de tu vestido sintético gris por mucho tiempo.',
  },
  'vestido-tela-verde': {
    prompt: 'vestido tela verde',
    opinionBody:
      'El vestido de tela verde ha recibido excelentes críticas por parte de nuestras clientas. Su color verde vibrante aporta frescura y alegría a cualquier look. El material de alta calidad hace que el vestido se ajuste perfectamente al cuerpo, realzando las curvas de forma elegante y favorecedora. Además, su diseño versátil permite utilizarlo tanto de día como de noche, convirtiéndolo en una pieza clave en el guardarropa de cualquier mujer. Sin duda, el vestido de tela verde es una elección acertada para lucir radiante y sofisticada en cualquier ocasión.',
    combineBody:
      'El vestido de tela verde es una prenda versátil que se puede combinar de múltiples formas. Para un look casual, puedes optar por unos botines marrones y una chaqueta de denim. Si prefieres un estilo elegante, puedes agregar unos tacones negros y un blazer a juego. Si quieres darle un toque más vistoso, puedes añadir accesorios dorados o plateados, como un collar statement y unos pendientes llamativos. En cuanto al bolso, opta por uno en tonos neutros que no reste protagonismo al vestido. Con estas combinaciones, estarás lista para lucir radiante y a la moda con tu vestido de tela verde.',
    cleanBody:
      'Para mantener tu vestido de tela verde en perfectas condiciones, te recomendamos seguir las instrucciones de lavado del fabricante. En general, la mayoría de los vestidos de tela pueden lavarse a mano o a máquina con un ciclo suave y utilizando agua fría o tibia. Evita el uso de lejía y suavizante, ya que podrían dañar el color y la textura de la tela. Para secarlo, lo mejor es tenderlo horizontalmente o colgarlo en una percha para evitar deformaciones. Si es necesario, puedes plancharlo a baja temperatura con vapor. Siguiendo estos consejos, tu vestido de tela verde se mantendrá impecable y podrás disfrutar de él durante mucho tiempo.',
  },
  'vestido-vaquera-amarillo': {
    prompt: 'vestido vaquera amarillo',
    opinionBody:
      'Este vestido vaquero amarillo se ha convertido en una opción muy popular entre nuestras usuarias. Su material de tela vaquera le da un aspecto informal y casual, perfecto para cualquier ocasión durante el día. El color amarillo le da un toque de frescura y alegría, convirtiéndolo en una elección ideal para la temporada de primavera y verano. Las opiniones destacan su comodidad, versatilidad y lo fácil que es combinarlo con diferentes accesorios.',
    combineBody:
      'El vestido vaquero amarillo es una prenda muy versátil que se puede combinar de diversas maneras para crear distintos estilos. Para un look casual y relajado, puedes agregar unas zapatillas blancas o sandalias planas. Si quieres lucir más elegante, puedes combinarlo con unos tacones altos y accesorios en tonos neutros como el beige o el blanco. Para un toque más bohemio, unas botas camperas y accesorios en tonos tierra serían ideales. La clave está en jugar con los accesorios y calzado para adaptar el vestido a tu estilo y ocasión.',
    cleanBody:
      'Para prolongar la vida útil y mantener la apariencia del vestido vaquero amarillo, es importante seguir las instrucciones de cuidado específicas para el material. En general, la tela vaquera se recomienda lavarla en agua fría y del revés. Puedes utilizar detergentes suaves y evitar el uso de lejía para evitar dañar el color. Para secarlo, es mejor colgarlo en un lugar sombreado y evitar la exposición directa al sol, ya que esto puede hacer que el color se desvanezca. Recuerda plancharlo a temperatura media y evitar el uso de vapor directamente sobre el vestido. Siguiendo estos consejos, tu vestido vaquero amarillo lucirá como nuevo por mucho más tiempo.',
  },
  'zapatillas-jersey-marron': {
    prompt: 'zapatillas jersey marron',
    opinionBody:
      'Las zapatillas jersey marrón son una elección popular entre los amantes de la moda urbana. Su diseño de material de jersey las hace cómodas y perfectas para el día a día. Los usuarios destacan su durabilidad y resistencia, lo que las convierte en unas zapatillas ideales para cualquier actividad. Además, su tono marrón es versátil y combina fácilmente con distintos estilos de vestimenta.',
    combineBody:
      'Las zapatillas jersey marrón se adaptan perfectamente a looks informales y casuales. Puedes combinarlas con unos jeans ajustados y una camiseta básica para obtener un aspecto desenfadado y moderno. Si prefieres un estilo más sofisticado, puedes usarlas con unos pantalones chinos y una camisa de lino. Sin importar la combinación que elijas, estas zapatillas agregarán un toque de estilo y comodidad a tu vestuario.',
    cleanBody:
      'Es importante cuidar adecuadamente tus zapatillas jersey marrón para mantenerlas en buen estado. Para limpiarlas, puedes usar un cepillo suave o un paño húmedo con agua y jabón neutro. Evita sumergirlas en agua y asegúrate de secarlas correctamente después de limpiarlas. Además, es recomendable aplicar un aerosol protector para repeler la suciedad y mantener el color del material. Con estos sencillos cuidados, tus zapatillas jersey marrón lucirán como nuevas por más tiempo.',
  },
  'zapatillas-lana-azul': {
    prompt: 'zapatillas lana azul',
    opinionBody:
      'Las zapatillas de lana azul son muy populares debido a su diseño y comodidad. Los usuarios destacan la suavidad del material, que mantiene los pies calientes en climas fríos. Además, el color azul le da un toque de estilo único a cualquier look. Muchos usuarios también destacan la durabilidad de estas zapatillas, ya que el material de lana es resistente y de alta calidad.',
    combineBody:
      'Las zapatillas de lana azul son versátiles y pueden combinarse con diferentes conjuntos. Para un look casual, puedes combinarlas con jeans oscuros y una chaqueta de cuero. Si deseas un estilo más deportivo, puedes usarlas con leggings y una sudadera. Además, puedes agregar un toque de color coordinando los accesorios con el tono azul de las zapatillas, como una bufanda o un bolso.',
    cleanBody:
      'Para mantener tus zapatillas de lana azul en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. Siempre es recomendable leer las instrucciones específicas del fabricante, pero en general, puedes limpiarlas a mano con agua fría y un detergente suave. Evita usar agua caliente, ya que podría encoger el material de lana. Después de limpiarlas, déjalas secar al aire libre. Además, es aconsejable guardar las zapatillas en un lugar seco y ventilado cuando no las estés usando.',
  },
  'zapatillas-organdi-verde': {
    prompt: 'zapatillas organdi verde',
    opinionBody:
      'Las zapatillas organdi verde son una elección perfecta para aquellos que buscan añadir un toque de color y estilo a su vestuario. Los usuarios quedan encantados con su diseño elegante y moderno, así como con la comodidad que ofrecen. El material de organdi les da un aspecto ligero y fresco, perfecto para la primavera y el verano. Además, el color verde agrega un toque de frescura y vitalidad a cualquier conjunto. En general, los usuarios están muy satisfechos con estas zapatillas y las recomiendan ampliamente.',
    combineBody:
      'Las zapatillas organdi verde son muy versátiles y se pueden combinar de muchas maneras diferentes. Para un look casual y relajado, puedes combinarlas con unos jeans ajustados y una camiseta blanca básica. Esto le dará un toque de color a tu conjunto sin perder la comodidad. Si buscas un look más formal, puedes combinar estas zapatillas con una falda midi y una blusa de seda en tonos neutros. Esto creará un contraste sofisticado y elegante. En definitiva, las posibilidades de combinación con las zapatillas organdi verde son infinitas, ¡solo depende de tu estilo y creatividad!',
    cleanBody:
      'Para mantener tus zapatillas organdi verde en óptimas condiciones, es importante seguir algunas recomendaciones de limpieza y cuidado. Para eliminar la suciedad superficial, puedes utilizar un cepillo suave y agua tibia con un poco de jabón neutro. Frota suavemente la superficie de las zapatillas y luego acláralas con agua limpia. Evita sumergirlas completamente en agua y no las laves a máquina. Si hay manchas difíciles de quitar, puedes aplicar un poco de detergente suave directamente sobre la mancha y frotar con cuidado. Después de limpiar, asegúrate de dejar que las zapatillas se sequen completamente al aire libre antes de guardarlas. Además, es recomendable evitar exponerlas directamente al sol o a altas temperaturas, ya que esto puede dañar el',
  },
  'zapatillas-piel-gris': {
    prompt: 'zapatillas piel gris',
    opinionBody:
      'Estas zapatillas de piel gris han recibido excelentes opiniones por parte de los usuarios. Muchos destacan la comodidad y la calidad de la piel utilizada, así como el estilo moderno y versátil que ofrecen. Además, su color gris es perfecto para combinar con cualquier prenda, lo que las convierte en un básico imprescindible en cualquier armario. Sin duda, estas zapatillas de piel gris son una excelente elección para aquellos que buscan estilo, confort y durabilidad.',
    combineBody:
      'Las zapatillas de piel gris son extremadamente versátiles y se pueden combinar con una gran variedad de prendas. Para un look casual y deportivo, puedes combinarlas con unos jeans ajustados y una camiseta básica. Si prefieres un estilo más elegante, puedes llevarlas con pantalones de vestir y una camisa blanca. Además, el color gris es ideal para crear contrastes con colores llamativos, como un abrigo rojo o una chaqueta amarilla. Sea cual sea tu estilo, las zapatillas de piel gris son una opción segura para completar cualquier outfit.',
    cleanBody:
      'Para mantener tus zapatillas de piel gris en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. Antes de limpiarlas, asegúrate de quitar cualquier suciedad o polvo con un cepillo suave. Si necesitan una limpieza más profunda, puedes utilizar un paño húmedo con jabón neutro y frotar suavemente la superficie de las zapatillas. Evita sumergirlas en agua y utiliza un paño seco para eliminar el exceso de humedad. Para mantener el color y la suavidad de la piel, aplica un producto específico para el cuidado del cuero. Recuerda guardarlas en un lugar fresco y seco cuando no las estés usando. Siguiendo estos sencillos pasos, tus zapatillas de piel gris lucirán como nuevas durante mucho tiempo.',
  },
  'zapatillas-rayon-azul': {
    prompt: 'zapatillas rayon azul',
    opinionBody:
      'Las zapatillas Rayon azul han recibido excelentes opiniones por parte de los usuarios. Su diseño moderno y versátil las convierte en el complemento perfecto para cualquier estilo urbano. Además, el material de calidad utilizado en su fabricación brinda durabilidad y comodidad, garantizando una experiencia de uso satisfactoria. Sin duda, estas zapatillas se han convertido en favoritas entre aquellos que buscan un calzado cómodo y estiloso.',
    combineBody:
      'Las zapatillas Rayon azul son extremadamente versátiles y fáciles de combinar. Su color azul vibrante añade un toque de estilo a cualquier outfit. Puedes lucirlas con pantalones vaqueros y una camiseta blanca para un look casual y desenfadado. Si buscas un estilo más elegante, combínalas con unos pantalones chinos y una camisa a juego. También lucen geniales con faldas y vestidos, dándole un toque informal pero sofisticado a tu conjunto.',
    cleanBody:
      'Para mantener las zapatillas Rayon azul en óptimas condiciones, es recomendable seguir algunos consejos de cuidado. Primero, retira el exceso de suciedad con un cepillo suave. Luego, mezcla agua tibia con un detergente suave y frota suavemente la superficie de las zapatillas con un paño o cepillo suave. Asegúrate de no sumergirlas completamente en agua. Después, aclara con agua limpia y retira el exceso de detergente. Por último, déjalas secar al aire, evitando exponerlas directamente al sol. Recuerda también protegerlas con un producto impermeabilizante para prolongar su vida útil y mantener su color vibrante.',
  },
  'zapatillas-sintetica-bronce': {
    prompt: 'zapatillas sintetica bronce',
    opinionBody:
      'Las zapatillas sintéticas color bronce han recibido excelentes críticas por parte de los usuarios. Muchos destacan su aspecto llamativo y moderno, perfecto para destacar en cualquier ocasión. Además, su material sintético ofrece una gran durabilidad y resistencia, lo que las convierte en una opción muy recomendable para aquellos que buscan unas zapatillas de alta calidad. Sin duda, las zapatillas sintéticas color bronce son una elección acertada para lucir un estilo único y sofisticado.',
    combineBody:
      'Las zapatillas sintéticas color bronce son una elección versátil que se puede combinar de diversas formas. Para un look casual y cómodo, puedes combinarlas con unos jeans ajustados y una camiseta básica. Si deseas un estilo más elegante, puedes optar por combinarlas con un vestido o una falda midi de color neutro. Además, podrás lucirlas con diferentes tipos de prendas, desde chaquetas de cuero hasta blusas estampadas, siempre aportando un toque de sofisticación y brillo a tu outfit.',
    cleanBody:
      'Para mantener las zapatillas sintéticas color bronce en buen estado, es importante seguir algunos cuidados básicos. En primer lugar, es recomendable limpiarlas regularmente con un paño húmedo para eliminar cualquier suciedad o mancha superficial. Si son impermeables, puedes utilizar un cepillo suave para eliminar el polvo acumulado. Además, es importante evitar exposiciones prolongadas al sol o a fuentes de calor, ya que esto puede dañar el material sintético. Por último, si las zapatillas presentan malos olores, puedes utilizar desodorantes especiales para calzado o insertar bolsitas de bicarbonato de sodio para absorber la humedad y neutralizar los olores. Siguiendo estos sencillos consejos, podrás disfrutar de tus zapatillas sintéticas color bronce por mucho tiempo.',
  },
  'zapatillas-sintetica-verde': {
    prompt: 'zapatillas sintetica verde',
    opinionBody:
      'Las zapatillas sintéticas verdes son una excelente elección para aquellos que buscan un calzado fresco y moderno. Muchos usuarios han elogiado la comodidad y ligereza de este tipo de zapatillas, ideales para largas caminatas o actividades deportivas. Además, destacan su resistencia y durabilidad, lo que las convierte en una inversión a largo plazo. Sin embargo, algunos usuarios mencionan que el material sintético puede ser menos transpirable que otros materiales naturales.',
    combineBody:
      'Las zapatillas sintéticas verdes son versátiles y pueden combinarse con una gran variedad de outfits. Para un look más casual, puedes combinarlas con pantalones vaqueros y una camiseta blanca, creando un contraste de colores vibrante. Si prefieres un estilo más deportivo, puedes optar por unos leggings negros y una chaqueta cortavientos en tonos similares. Para un toque de elegancia, puedes combinarlas con un vestido o falda en tonos claros y complementar el look con accesorios en colores neutros.',
    cleanBody:
      'Para mantener tus zapatillas sintéticas verdes en óptimas condiciones, es recomendable seguir algunos pasos sencillos. Primero, asegúrate de quitar cualquier suciedad superficial con un cepillo suave o un paño húmedo. Luego, prepara una solución de agua tibia y detergente suave y utiliza un cepillo pequeño para frotar suavemente las zonas más sucias. No sumerjas completamente tus zapatillas en agua y evita usar productos químicos fuertes, ya que podrían dañar el material sintético. Por último, deja que tus zapatillas se sequen al aire libre, evitando la exposición directa al sol o a fuentes de calor.',
  },
  'zapatillas-tejido-azul': {
    prompt: 'zapatillas tejido azul',
    opinionBody:
      'Las zapatillas tejido azul son altamente valoradas por los usuarios. Su diseño único y moderno, combinado con el tejido de alta calidad, las convierten en una opción elegante y cómoda para cualquier ocasión. Además, su color azul ofrece un toque de estilo llamativo y versátil. Los usuarios destacan su comodidad, durabilidad y la excelente transpirabilidad que brindan. Sin duda, las zapatillas tejido azul son una elección segura para aquellos que buscan estilo y confort en un solo producto.',
    combineBody:
      'Las zapatillas tejido azul son ideales para combinar con diferentes estilos y prendas de vestir. Gracias a su color versátil, se adaptan fácilmente a outfits casuales o más formales. Puedes llevarlas con jeans y una camiseta blanca para un look casual y relajado, o combinarlas con pantalones chinos y una camisa para una apariencia más elegante. Además, también quedan geniales con faldas o vestidos en tonos neutros para añadir un toque de color a tu atuendo. No importa cómo decidas combinarlas, las zapatillas tejido azul siempre agregan estilo y personalidad a tu look.',
    cleanBody:
      'Para mantener tus zapatillas tejido azul en excelente estado, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, es recomendable cepillar suavemente la superficie de las zapatillas para eliminar cualquier suciedad o polvo acumulado. Si hay manchas visibles, puedes utilizar un paño húmedo con agua y jabón suave para limpiarlas. Evita sumergirlas completamente en agua y utiliza productos específicos para limpiar zapatillas si es necesario. Una vez limpias, déjalas secar al aire libre, evitando la exposición directa al sol. Recuerda guardar tus zapatillas tejido azul en un lugar fresco y seco cuando no las estés utilizando. Siguiendo estos sencillos cuidados, tus zapatillas se mantendrán en óptimas condiciones por mucho tiempo',
  },
  'zapatillas-terciopelo-aguamarina': {
    prompt: 'zapatillas terciopelo aguamarina',
    opinionBody:
      'Las zapatillas de terciopelo aguamarina han recibido excelentes opiniones por parte de los usuarios. Destacan su apariencia elegante y llamativa, gracias al material de terciopelo y su color aguamarina. Además, los usuarios resaltan su comodidad y ajuste perfecto, lo que las convierte en el calzado ideal para cualquier ocasión.',
    combineBody:
      'Las zapatillas de terciopelo aguamarina son muy versátiles a la hora de combinarlas con diferentes prendas. Puedes crear un look más casual utilizando unos jeans ajustados y una camiseta blanca. Si buscas un estilo más sofisticado, puedes optar por combinarlas con una falda midi y un suéter de punto. ¡Las opciones son infinitas con estas zapatillas de terciopelo aguamarina!',
    cleanBody:
      'Para mantener tus zapatillas de terciopelo aguamarina en perfectas condiciones, es recomendable seguir algunos consejos de limpieza. En primer lugar, utiliza un cepillo suave para eliminar el polvo y la suciedad de la superficie de terciopelo. Si se produce alguna mancha, puedes utilizar un paño húmedo con agua tibia y un poco de jabón suave para limpiarla suavemente. Evita mojar en exceso las zapatillas y deja que se sequen al aire libre. Recuerda también guardarlas en un lugar fresco y seco para evitar daños en el material.',
  },
  'zapatos-cachemira-beige': {
    prompt: 'zapatos cachemira beige',
    opinionBody:
      'Los zapatos de cachemira beige son una elección elegante y sofisticada para cualquier ocasión. Los usuarios han elogiado su suavidad y confort, ya que el material de cachemira proporciona una sensación de lujo en cada paso. Además, el color beige es versátil y combina bien con diferentes estilos y tonos de ropa. Los usuarios también han destacado la durabilidad de estos zapatos, ya que el material de cachemira es resistente al desgaste. En general, las opiniones sobre los zapatos de cachemira beige son muy positivas, recomendándolos como una opción de calzado de alta calidad.',
    combineBody:
      'Los zapatos de cachemira beige son una adición versátil y elegante a cualquier conjunto. Puedes combinarlos con pantalones de vestir en tonos oscuros, como el negro o el gris, para lograr un look formal y sofisticado. Si prefieres un estilo más informal, puedes usarlos con jeans de corte recto y una camisa blanca para lograr un look cómodo pero estilizado. Para eventos más formales, puedes combinar los zapatos de cachemira beige con un traje completo y una corbata a juego. En resumen, los zapatos de cachemira beige se pueden combinar con una variedad de prendas y estilos para crear looks diferentes según la ocasión.',
    cleanBody:
      'Para mantener tus zapatos de cachemira beige en condiciones óptimas, es importante seguir algunos consejos de limpieza y cuidado. En primer lugar, asegúrate de protegerlos adecuadamente antes de usarlos por primera vez. Aplica un spray protector para evitar que se manchen y proteger el material. Si tus zapatos se ensucian, límpialos suavemente con un paño húmedo y suave. Evita usar productos químicos fuertes, ya que podrían dañar el material de cachemira. Para mantener la suavidad y el aspecto lujoso de los zapatos, puedes cepillarlos regularmente con un cepillo de cerdas suaves. Además, asegúrate de guardar los zapatos en',
  },
  'zapatos-cachemira-negro': {
    prompt: 'zapatos cachemira negro',
    opinionBody:
      'Los zapatos de cachemira negro son altamente valorados por su elegancia y estilo atemporal. Los usuarios destacan su comodidad y durabilidad, ya que la cachemira es un material suave pero resistente que se adapta perfectamente al pie. Además, el color negro es versátil y fácil de combinar, convirtiendo a estos zapatos en una opción ideal para cualquier ocasión.',
    combineBody:
      'Los zapatos de cachemira negro son extremadamente versátiles y se pueden combinar con una amplia variedad de prendas. Para un look formal, puedes optar por combinarlos con un traje negro o gris, garantizando un aspecto sofisticado y elegante. Si buscas algo más casual, puedes usarlos con jeans oscuros y una camisa blanca para lograr un equilibrio entre comodidad y estilo.',
    cleanBody:
      'Para mantener tus zapatos de cachemira negro en buen estado, es importante seguir unas simples pautas de limpieza. Para eliminar el polvo y la suciedad, utiliza un cepillo suave y frota suavemente la superficie del zapato. Si hay manchas, puedes utilizar un paño húmedo con agua y jabón suave para limpiarlas. Es importante evitar mojar completamente los zapatos, ya que la cachemira puede dañarse con el agua. Para mantener su calidad y suavidad, puedes aplicar un spray impermeabilizante específico para zapatos de cachemira. Recuerda guardar tus zapatos en un lugar fresco y seco, lejos de la luz directa del sol, para evitar que se deterioren.',
  },
  'zapatos-cachemira-oro': {
    prompt: 'zapatos cachemira oro',
    opinionBody:
      'Los zapatos de cachemira en color oro son una elección elegante y sofisticada para agregar un toque de glamour a cualquier atuendo. La suavidad y calidez de la cachemira proporcionan una comodidad excepcional, mientras que el color dorado agrega un toque de brillo y glamour. Los usuarios han elogiado la calidad y la atención al detalle de estos zapatos, destacando su comodidad y durabilidad. Además, la combinación del material de cachemira y el color oro los convierte en una opción versátil tanto para ocasiones formales como informales.',
    combineBody:
      'Los zapatos de cachemira en color oro son extremadamente versátiles y se pueden combinar con una variedad de atuendos. Para un look elegante y sofisticado, puedes combinarlos con un vestido negro ajustado o una falda lápiz acompañada de una blusa de seda en tonos neutros. Para un look más casual, prueba a combinarlos con jeans ajustados y una blusa o suéter en tonos neutros. Si quieres agregar un toque de contraste, puedes combinarlos con prendas en tonos tierra o incluso en colores brillantes para crear un look audaz y llamativo.',
    cleanBody:
      'La cachemira es un material delicado y requiere cuidado especial para mantener su suavidad y apariencia impecable. Para limpiar los zapatos de cachemira en color oro, lo mejor es utilizar un cepillo de cerdas suaves o un paño suave y húmedo para eliminar cualquier suciedad o mancha. Evita el contacto con agua directa, ya que podría dañar el material. Si es necesario, puedes utilizar un limpiador específico para prendas de cachemira, siguiendo las instrucciones del producto. Para mantener su forma, puedes rellenar los zapatos con papel de seda después de usarlos y almacenarlos en un lugar fresco y seco. Recuerda evitar la exposición directa a la luz solar o a fuentes de calor, ya que esto puede hacer que el color se desvanezca. Con el cuidado adecu',
  },
  'zapatos-franela-azul': {
    prompt: 'zapatos franela azul',
    opinionBody:
      'Los zapatos de franela azul son una elección elegante y sofisticada para cualquier ocasión. Muchos usuarios han elogiado la comodidad y la durabilidad de este material, que proporciona un ajuste perfecto y resistencia al desgaste. Además, el color azul añade un toque de estilo y originalidad a cualquier conjunto. Los clientes también han destacado la versatilidad de estos zapatos, que se pueden combinar tanto con atuendos casuales como formales. En resumen, los zapatos de franela azul son una opción popular entre aquellos que buscan un calzado de calidad y estilo.',
    combineBody:
      'Los zapatos de franela azul se pueden combinar de muchas maneras creativas y elegantes. Para un look casual y relajado, puedes combinarlos con unos vaqueros oscuros y una camiseta blanca o de rayas. Si prefieres un estilo más formal, puedes combinarlos con unos pantalones de vestir en tonos neutros, como el gris o el negro, y una camisa de color claro. Además, los zapatos de franela azul combinan muy bien con accesorios en tonos marrones, como un cinturón de cuero o una cartera. En definitiva, los zapatos de franela azul son una opción versátil que te permitirá crear una gran variedad de estilos y looks.',
    cleanBody:
      'Para mantener tus zapatos de franela azul en buen estado, es importante seguir algunos consejos de limpieza y mantenimiento. En primer lugar, asegúrate de cepillar regularmente los zapatos con un cepillo de cerdas suaves para eliminar el polvo y la suciedad acumulada. Si se producen manchas, puedes utilizar un limpiador específico para calzado de tela o franela, siguiendo las instrucciones del fabricante. Además, evita mojar los zapatos en exceso y nunca los sumerjas completamente en agua. Para el secado, déjalos al aire libre en un lugar fresco y bien ventilado, evitando la exposición directa al sol. Por último, te recomendamos utilizar',
  },
  'zapatos-lino-azul': {
    prompt: 'zapatos lino azul',
    opinionBody:
      'Los zapatos de lino azul son una elección perfecta para aquellos que buscan un estilo casual pero sofisticado. Muchos usuarios han elogiado la comodidad y transpirabilidad de este material natural, ideal para los meses de verano. Además, el color azul añade un toque de frescura y versatilidad, ya que se puede combinar fácilmente con diferentes prendas de vestir. Ya sea para una ocasión informal o para un evento más elegante, los zapatos de lino azul son una excelente opción para lucir a la moda.',
    combineBody:
      'La versatilidad de los zapatos de lino azul permite combinarlos con una amplia variedad de estilos y prendas. Para un look casual, puedes usarlos con unos vaqueros y una camiseta blanca, creando un contraste elegante. Si deseas darle un toque más formal, combínalos con un traje de color neutro y una camisa a juego. Además, puedes agregar accesorios en tonos azules o neutros para complementar el conjunto. Se trata de una opción versátil y llamativa que te permitirá destacar en cualquier ocasión.',
    cleanBody:
      'Aunque los zapatos de lino azul son muy elegantes, es importante conocer cómo limpiar y mantener este tipo de calzado para garantizar su durabilidad. Se recomienda utilizar un cepillo de cerdas suaves para eliminar el polvo y la suciedad de la superficie. En caso de manchas, puedes utilizar un paño húmedo con agua y jabón suave para limpiarlas suavemente. Es importante dejar secar los zapatos al aire libre y evitar exponerlos a altas temperaturas o luz directa del sol. Recuerda almacenarlos en un lugar fresco y seco para mantener su forma y calidad a lo largo del tiempo.',
  },
  'zapatos-organdi-oro': {
    prompt: 'zapatos organdi oro',
    opinionBody:
      'Los zapatos de organdi en color oro son una elección elegante y sofisticada para cualquier ocasión. Los usuarios que han adquirido este tipo de calzado destacan la belleza y versatilidad de este material, así como su durabilidad y comodidad. Además, el color oro agrega un toque de brillo y brillo a cualquier atuendo, convirtiéndolos en una opción popular para eventos especiales y fiestas. En resumen, los zapatos de organdi oro son altamente valorados por sus propiedades estéticas y de calidad.',
    combineBody:
      'Al tratarse de zapatos en color oro, estos pueden combinarse de diversas formas para crear conjuntos elegantes y atractivos. Para un look de noche glamoroso, se recomienda combinarlos con un vestido negro o un conjunto de ropa de fiesta en tonos neutros. Si se busca un estilo más casual pero elegante, los zapatos de organdi oro se pueden combinar con jeans ajustados y una blusa de seda en tonos suaves. También pueden ser el complemento perfecto para un vestido de noche en tonos cálidos como el rojo o el morado. En definitiva, los zapatos de organdi oro son extremadamente versátiles y pueden adaptarse a diferentes estilos y ocasiones.',
    cleanBody:
      'Para mantener los zapatos de organdi oro en óptimas condiciones, es importante seguir algunos cuidados básicos. En primer lugar, se recomienda limpiarlos regularmente con un cepillo suave o una toalla húmeda para eliminar el polvo y la suciedad. Si se produce una mancha, se debe intentar limpiarla de inmediato con un paño húmedo, evitando frotar demasiado fuerte para no dañar el material. En cuanto al almacenamiento, es recomendable guardar los zapatos en una bolsa de tela o caja protectora para evitar que se rayen o se deformen. También se aconseja evitar exponer los zapatos de organdi oro a la luz solar directa o a la humedad excesiva. Siguiendo estos consejos de limpieza y mantenimiento, los zap',
  },
  'zapatos-piel-celeste': {
    prompt: 'zapatos piel celeste',
    opinionBody:
      'Los zapatos de piel celeste son una elección atrevida y elegante para cualquier ocasión. Los usuarios que han adquirido este producto destacan la calidad de la piel, que brinda comodidad y durabilidad. Además, el color celeste añade un toque de originalidad y estilo a cualquier outfit. Los usuarios también mencionan que la combinación de colores claros y oscuros con estos zapatos es simplemente perfecta. En resumen, quienes han probado los zapatos de piel celeste están más que satisfechos con su apariencia y calidad.',
    combineBody:
      'La versatilidad de los zapatos de piel celeste es sorprendente. Puedes combinarlos con colores neutros como el blanco, el negro o el beige para crear un look elegante y sofisticado. Si buscas un outfit más llamativo, puedes optar por combinarlos con tonos pasteles o estampados florales. Los zapatos de piel celeste son ideales para eventos formales o para añadir un toque de estilo a un conjunto más informal. Sin duda, son una opción única para aquellos que quieren destacar y arriesgarse un poco en su estilo.',
    cleanBody:
      'El cuidado de los zapatos de piel celeste es fundamental para mantenerlos en buen estado durante mucho tiempo. Una buena práctica es utilizar un cepillo suave para quitar el polvo y la suciedad superficial. Si los zapatos se manchan, puedes usar un paño húmedo con un poco de jabón suave para limpiarlos, evitando frotar demasiado fuerte para no dañar la piel. Para mantener su color brillante, es recomendable utilizar un producto específico para proteger y nutrir la piel. También es importante guardar los zapatos en un lugar fresco y seco para evitar que se deformen. Siguiendo estos simples consejos, tus zapatos de piel celeste lucirán como nuevos por mucho tiempo.',
  },
  'zapatos-piel-lima': {
    prompt: 'zapatos piel lima',
    opinionBody:
      'Los zapatos de piel en color lima han sido muy bien recibidos por los clientes. Muchos coinciden en que el color lima les da un toque único y llamativo. Además, la calidad del material de piel les brinda durabilidad y comodidad. Los usuarios destacan que son perfectos para darle un toque de color a cualquier outfit, ya sea casual o más formal. En general, los clientes están satisfechos con la compra de los zapatos de piel en color lima.',
    combineBody:
      'Los zapatos de piel en color lima son una opción versátil para combinar con diferentes estilos de outfits. Para outfits casuales, se pueden combinar con jeans ajustados y una camiseta blanca para lograr un look fresco y desenfadado. También se pueden combinar con vestidos o faldas de colores neutros para resaltar el color lima de los zapatos. Para outfits más formales, se pueden combinar con pantalones de vestir en tonos oscuros y una blusa elegante. En definitiva, los zapatos de piel en color lima pueden ser el toque de color perfecto para cualquier tipo de outfit.',
    cleanBody:
      'Para limpiar y mantener los zapatos de piel en color lima, es recomendable seguir algunos pasos. En primer lugar, se debe limpiar cualquier suciedad o polvo con un cepillo suave o un paño húmedo. Luego, se puede aplicar un limpiador de cuero específico para mantener la piel hidratada y en buen estado. Es importante evitar el contacto con líquidos que puedan manchar el color lima, por lo que se recomienda no usar los zapatos en días lluviosos o protegerlos con un spray impermeabilizante. Finalmente, es recomendable guardar los zapatos en un lugar fresco y seco, preferiblemente en una bolsa de tela para protegerlos del polvo. Siguiendo estos simples consejos, los zapatos de piel en color lima se mantendrán en buen estado por mucho tiempo.',
  },
  'zapatos-saten-celeste': {
    prompt: 'zapatos saten celeste',
    opinionBody:
      'Los zapatos de satén celeste son definitivamente una elección elegante y sofisticada. Los clientes que han comprado estos zapatos elogian su delicado brillo y su suave textura. Además, destacan que son cómodos de llevar y se ajustan perfectamente al pie. Algunos usuarios incluso aseguran recibir constantes halagos cuando los usan en eventos especiales. Sin duda, los zapatos de satén celeste son una opción popular para ocasiones formales y una excelente manera de destacar en cualquier evento.',
    combineBody:
      'Los zapatos de satén celeste son muy versátiles a la hora de combinarlos con diferentes outfits. Si estás buscando un look elegante y clásico, puedes usarlos con un vestido negro o blanco y accesorios plateados. Para una apariencia más primaveral o veraniega, puedes combinarlos con un vestido floral o una falda de colores pastel. Si prefieres un estilo más relajado, puedes usarlos con unos jeans ajustados y una blusa de seda. Incluso puedes añadir un toque de color usando una bolsa o un cinturón a juego con los zapatos. No importa la ocasión, los zapatos de satén celeste seguramente agregarán un toque de gracia y sofisticación a tu conjunto.',
    cleanBody:
      'Cuando se trata de la limpieza y el mantenimiento de los zapatos de satén celeste, es importante tener cuidado para preservar su aspecto elegante. En primer lugar, es recomendable cepillar suavemente los zapatos para remover cualquier suciedad o polvo antes de guardarlos. Si encuentras alguna mancha en el satén, debes actuar de inmediato. Puedes limpiarla con un paño suave y agua tibia con jabón neutro, frotando suavemente en movimientos circulares. Evita el uso de productos químicos fuertes, ya que podrían dañar el material. Después de la limpieza, deja que los zapatos se sequen al aire libre, alejados de la luz directa del sol. Y finalmente',
  },
  'zapatos-seda-marino': {
    prompt: 'zapatos seda marino',
    opinionBody:
      'Los zapatos de seda marino son altamente valorados por su elegancia y sofisticación. Muchos usuarios comentan que el material de seda le da un toque especial a estos zapatos, brindándoles un aspecto lujoso. Además, destacan la comodidad que proporcionan, gracias a su suave textura y su ajuste perfecto. También se destaca la durabilidad de los zapatos de seda marino, ya que el material de seda es resistente y de calidad. En general, las opiniones sobre los zapatos de seda marino son muy favorables, convirtiéndolos en una opción popular entre quienes buscan calzado elegante y de alta calidad.',
    combineBody:
      'Los zapatos de seda marino son extremadamente versátiles y se pueden combinar con una gran variedad de outfits. Para un look formal, puedes combinarlos con un traje en tonos oscuros como el negro o el gris, creando una imagen sofisticada y elegante. Si buscas un estilo más casual, puedes combinarlos con jeans oscuros y una camisa blanca, logrando un equilibrio entre lo formal y lo informal. Además, los zapatos de seda marino también quedan muy bien con tonos claros como el beige o el marfil, aportando un contraste interesante en cualquier conjunto. En resumen, los zapatos de seda marino son una opción versátil que se adaptará perfectamente a cualquier estilo y ocasión.',
    cleanBody:
      'Para mantener tus zapatos de seda marino en óptimas condiciones, es importante seguir algunos consejos de limpieza y cuidado. Para eliminar la suciedad superficial, puedes utilizar un cepillo de cerdas suaves para fregar delicadamente la superficie de los zapatos. Si necesitan una limpieza más profunda, puedes utilizar un paño húmedo y un poco de detergente suave diluido en agua tibia. Sin embargo, es fundamental evitar el uso de productos químicos fuertes, ya que pueden dañar la seda. Además, es recomendable guardar los zapatos de seda marino en un lugar fresco y se',
  },
};

export default json;
