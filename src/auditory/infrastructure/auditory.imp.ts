import { InjectModel } from '@nestjs/mongoose';
import Auditory, {
  AuditoryDocument,
  Auditory as AuditorySchema,
} from '../domain/auditory';
import IAuditoryRepository from '../domain/auditory-repository';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';

@Injectable()
class AuditoryRepository implements IAuditoryRepository {
  constructor(
    @InjectModel(AuditorySchema.name)
    private auditoryModel: Model<AuditoryDocument>,
  ) {}

  async storeAuditory(entityInfo: Auditory): Promise<Auditory> {
    const storedAuditory = new this.auditoryModel(entityInfo);
    return storedAuditory.save();
  }
  async getAuditory(entityInfo: Auditory): Promise<Auditory> {
    const storedAuditory = await this.auditoryModel.findOne<Auditory>({
      entityName: entityInfo.entityName,
    });
    return storedAuditory;
  }
}

export default AuditoryRepository;
