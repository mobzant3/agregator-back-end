import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Auditory, AuditorySchema } from '../domain/auditory';
import AuditoryRepositoryImplementation from './auditory.imp';
import AuditoryRepository from '../domain/auditory-repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Auditory.name, schema: AuditorySchema },
    ]),
  ],
  providers: [
    {
      provide: AuditoryRepository,
      useClass: AuditoryRepositoryImplementation,
    },
  ],
  exports: [AuditoryRepository],
})
export class AuditoryInfrastructureModule {}
