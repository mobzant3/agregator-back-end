import { Injectable } from '@nestjs/common';
import { GetAuditory } from '../application/get-auditory';
import { StoreAuditory } from '../application/store-auditory';
import { Auditory } from '../domain/auditory';
import AuditoryQuerying from '../domain/for-auditory-querying';

@Injectable()
export class AuditoryService implements AuditoryQuerying {
  constructor(
    private readonly getAuditoryService: GetAuditory,
    private readonly storeAuditoryService: StoreAuditory,
  ) {}

  async storeAuditory(entityName: string, data: Auditory) {
    if (!entityName || !data) {
      throw 'Error';
    }

    return this.storeAuditoryService.storeAuditory({
      name: entityName,
      ...data,
    });
  }

  async getAuditory(id: any) {
    if (!id) {
      throw 'Error';
    }

    return this.getAuditoryService.getAuditory(id);
  }
}
