import { Injectable } from '@nestjs/common';
import AuditoryRepository from '../../domain/auditory-repository';

@Injectable()
class StoreAuditory {
  constructor(private readonly auditoryRepository: AuditoryRepository) {}

  async storeAuditory(entityInfo) {
    return this.auditoryRepository.storeAuditory(entityInfo);
  }
}

export default StoreAuditory;
