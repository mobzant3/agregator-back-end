import { Injectable } from '@nestjs/common';
import AuditoryRepository from '../../domain/auditory-repository';

@Injectable()
class GetAuditory {
  constructor(private readonly auditoryRepository: AuditoryRepository) {}

  async getAuditory(entityInfo) {
    return this.auditoryRepository.getAuditory(entityInfo);
  }
}

export default GetAuditory;
