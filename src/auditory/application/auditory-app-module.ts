import { Module } from '@nestjs/common';
import { AuditoryInfrastructureModule } from '../infrastructure/auditory-infrastructure.module';
import { GetAuditory } from './get-auditory';
import { StoreAuditory } from './store-auditory';

@Module({
  imports: [AuditoryInfrastructureModule],
  providers: [GetAuditory, StoreAuditory],
  exports: [GetAuditory, StoreAuditory],
})
export class AuditoryAppModule {}
