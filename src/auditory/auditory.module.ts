import { Module } from '@nestjs/common';
import { AuditoryService } from './infrastructure';
import { AuditoryAppModule } from './application/auditory-app-module';

@Module({
  imports: [AuditoryAppModule],
  providers: [AuditoryService],
  exports: [AuditoryService],
})
export class AuditoryModule {}
