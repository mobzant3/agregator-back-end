import { Auditory } from './auditory';

export default interface AuditoryQuerying {
  storeAuditory(entityName: string, data: Auditory): Promise<Auditory>;
  getAuditory(id: string): Promise<Auditory>;
}
