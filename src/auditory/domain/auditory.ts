import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type AuditoryDocument = HydratedDocument<Auditory>;

class Options {
  state: 'ERROR' | 'SUCCESS';
  message?: string;
  url?: string;
}

interface IAuditory {
  entityName: string;
  options: Options;
}

@Schema()
export class Auditory implements IAuditory {
  @Prop({ name: 'entity_name' })
  entityName: string;
  @Prop({ name: 'options' })
  options: Options;
}

export const AuditorySchema = SchemaFactory.createForClass(Auditory);

export default IAuditory;
