import Auditory from './auditory';

interface IAuditoryRepository {
  storeAuditory(entityInfo: any): Promise<Auditory>;
  getAuditory(entityInfo: any): Promise<Auditory>;
}

class AuditoryRepository implements IAuditoryRepository {
  storeAuditory(entityInfo: any): Promise<Auditory> {
    throw new Error('Method not implemented.');
  }
  getAuditory(entityInfo: any): Promise<Auditory> {
    throw new Error('Method not implemented.');
  }
}

export default AuditoryRepository;
