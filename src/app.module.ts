import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ExcelReaderModule } from './api/excel-reader/excel-reader.module';
import { ScrapperModule } from './scrapper/scrapper.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from './api/products/products.module';
import { SlugsController } from './api/slugs/slugs.controller';
import { SlugsService } from './api/slugs/slugs.service';
import { CategoryModule } from './api/categories/categories.module';
import { Auditory, AuditorySchema } from './auditory/domain/auditory';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest'),
    MongooseModule.forFeature([
      { name: Auditory.name, schema: AuditorySchema },
    ]),
    ExcelReaderModule,
    ScrapperModule,
    ProductModule,
    CategoryModule,
  ],
  controllers: [AppController, SlugsController],
  providers: [AppService, SlugsService],
})
export class AppModule {}
